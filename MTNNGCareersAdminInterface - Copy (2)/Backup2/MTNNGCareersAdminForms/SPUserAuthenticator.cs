﻿using System;

using System.Configuration;
using System.Web.Configuration;

using Microsoft.SharePoint;
//using Microsoft.SharePoint;

namespace MTNNGCareers.Administration
{
    public class SPUserAuthenticator
    {
        string _HRRecruitmentTeamGroupName;

        public SPUserAuthenticator()
        {
            _HRRecruitmentTeamGroupName = ((WebConfigurationManager.AppSettings["HRRecruitmentTeamGroup"]).ToString());
        }

        public bool ValidateUser()
        {
            bool isValidUser = true;

            // obtaining references to the currently executing website and the currently logged on user.
            SPWeb currentlyExecutingWebsite = ((SPContext.Current).Web);
            //SPUser currentUser = (currentlyExecutingWebsite.CurrentUser);

            SPGroup _HRRecruitmentTeamGroup = (currentlyExecutingWebsite.Groups[_HRRecruitmentTeamGroupName]);

            isValidUser = (_HRRecruitmentTeamGroup.ContainsCurrentUser);

            return isValidUser;
        }
    }
}

﻿using System;

using System.Configuration;
using System.Web.Configuration;

using System.Data;
using System.Data.SqlClient;

using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

using AjaxControlToolkit;

using MTNNGCareers.Administration.Common;

namespace MTNNGCareers.Administration.Forms.ControlTemplates
{
    public partial class ManageVacancyApplicants : UserControl
    {
        GridView gvVacanyApplicants;
        Label lblShortListedApplicantCount;

        int shortListedVacancyApplicantCount;

        SqlConnection connection;
        SqlCommand command;
        SqlDataAdapter dataAdapter;
        DataSet dataSet;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CreateConnection();
            }
        }

        // Routine to create a connection to the MTN NG Careers DB.
        private void CreateConnection()
        {
            string connectionString = ((WebConfigurationManager.ConnectionStrings["MTNNGCareersDBConnectionString"]).ToString());
            connection = new SqlConnection(connectionString);
        }

        // Routine to create command to be issued against MTN NG Careers DB.
        private void CreateCommand(CommandType commandType, string commandText)
        {
            command = new SqlCommand();
            command.Connection = connection;
            command.CommandType = commandType;
            command.CommandText = commandText;
        }

        private void AutomaticShortList()
        {
            CreateCommand((CommandType.StoredProcedure), "ShortListVacancyApplicants");

            
        }

        // Routine to issue command against database.
        private int IssueCommand()
        {
            dataAdapter = new SqlDataAdapter(command);
            dataSet = new DataSet();
            int rowCount = dataAdapter.Fill(dataSet);

            return rowCount;
        }

        // Routine (Event Handler) to handle the click event generated when the user clicks the Button labeled: short list.
        protected void btnFILTER_Click(object sender, EventArgs e)
        {
            FilterVacancyApplicants();
        }

        // Routine to filter applicants based on criteria specified.
        private void FilterVacancyApplicants()
        {
            CreateConnection();

            CreateCommand((CommandType.StoredProcedure), "ShortListVacancyApplicants");

            CommonMembers commonMembers = new CommonMembers();

            AddShortListingSqlParameters();

            // obtaining a reference to the parent that would contain this control.
            Control thisControlContainer = (this.Parent);
            string containerTypeName = ((thisControlContainer.GetType()).Name);

            while (containerTypeName != "TabContainer")
            {
                thisControlContainer = (thisControlContainer.Parent);
                containerTypeName = ((thisControlContainer.GetType()).Name);
            }

            TabContainer tc = ((TabContainer)thisControlContainer);

            Control VacancyApplicantsViewer1 = ((tc.Tabs[2]).FindControl("VacancyApplicantsViewer1"));

            gvVacanyApplicants = ((GridView)(VacancyApplicantsViewer1.FindControl("gvVacanyApplicants")));
            lblShortListedApplicantCount = ((Label)(VacancyApplicantsViewer1.FindControl("lblShortListedApplicantCount")));

            int rowCount = IssueCommand();
            if (rowCount > 0)
            {                
                CheckShortListedApplicants();
            }
            else
            {
                foreach(GridViewRow vacancyApplicantRecord in (gvVacanyApplicants.Rows))
                {
                    string typeName = null;

                    CheckBox cb = null;
                    
                    foreach (Control c in ((vacancyApplicantRecord.Cells[0]).Controls))
                    {
                        typeName = ((c.GetType()).Name);
                        if ((typeName.Equals("CheckBox")))
                        {
                            cb = ((CheckBox)c);
                            cb.Checked = false;
                        }
                    }
                }

                lblShortListedApplicantCount.Text = "no applicant met criteria";
            }

            tc.ActiveTabIndex = 2;
        }

        // Routine to add SqlParameters for filtering applicants.
        private void AddShortListingSqlParameters()
        {
            command.Parameters.AddWithValue("@VacancyBreakDownID", (hfVacancyBreakDownID.Value));

            if (cbCourseOfStudy.Checked)
            {
                command.Parameters.AddWithValue("@PreferredCourseOfStudy", (ddlCourseOfStudy.SelectedValue));
            }
            else
            {
                command.Parameters.AddWithValue("@PreferredCourseOfStudy", (DBNull.Value));
            }

            if (cbMinimumQualification.Checked)
            {
                command.Parameters.AddWithValue("@PreferredMinimumQualificationID", (ddlMinimumQualification.SelectedValue));
            }
            else
            {
                command.Parameters.AddWithValue("@PreferredMinimumQualificationID", (DBNull.Value));
            }

            if (cbPostDegreeYearsOfExperience.Checked)
            {
                command.Parameters.AddWithValue("@PreferredPostDegreeYearsOfExperience", (iftPostDegreeYearsOfExperience.Text));
            }
            else
            {
                command.Parameters.AddWithValue("@PreferredPostDegreeYearsOfExperience", (DBNull.Value));
            }

            if (cbClassOfQualification.Checked)
            {
                command.Parameters.AddWithValue("@PreferredClassOfQualificationID", (ddlClassOfQualification.SelectedValue));
            }
            else
            {
                command.Parameters.AddWithValue("@PreferredClassOfQualificationID", (DBNull.Value));
            }

            if (cbAdditionalQualifications.Checked)
            {
                command.Parameters.AddWithValue("@PreferredAdditionalQualifications", (iftAdditionalQualifications.Text));
            }
            else
            {
                command.Parameters.AddWithValue("@PreferredAdditionalQualifications", (DBNull.Value));
            }

            if (cbProfessionalQualifications.Checked)
            {
                command.Parameters.AddWithValue("@PreferredProfessionalQualifications", (iftProfessionalQualifications.Text));
            }
            else
            {
                command.Parameters.AddWithValue("@PreferredProfessionalQualifications", (DBNull.Value));
            }

            if (cbSkillsRequired.Checked)
            {
                command.Parameters.AddWithValue("@PreferredSkillsRequired", (iftSkillsRequired.Text));
            }
            else
            {
                command.Parameters.AddWithValue("@PreferredSkillsRequired", (DBNull.Value));
            }

            if (cbMaritalStatus.Checked)
            {
                command.Parameters.AddWithValue("@PreferredMaritalStatus", (rblMaritalStatus.SelectedValue));
            }
            else
            {
                command.Parameters.AddWithValue("@PreferredMaritalStatus", (DBNull.Value));
            }

            if (cbGender.Checked)
            {
                command.Parameters.AddWithValue("@PreferredGender", (rblGender.SelectedValue));

            }
            else
            {
                command.Parameters.AddWithValue("@PreferredGender", (DBNull.Value));
            }

            if (cbNYSCRequirement.Checked)
            {
                command.Parameters.AddWithValue("@NYSCCompletionStatus", (ddlNYSCCompletionStatus.SelectedValue));
            }
            else
            {
                command.Parameters.AddWithValue("@NYSCCompletionStatus", (DBNull.Value));
            }

            if (cbCountryOfOorigin.Checked)
            {
                command.Parameters.AddWithValue("@CountryOfOrigin", (ddlCountryOfOrigin.SelectedValue));

            }
            else
            {
                command.Parameters.AddWithValue("@CountryOfOrigin", (DBNull.Value));
            }
        }

        // Routine to determine what applicant records to check on the GridView as to meeting up with vacancy criteria.
        private void CheckShortListedApplicants()
        {
            // Retrieving the records for short listed application.
            DataTable dataTable = (dataSet.Tables[0]);
            DataRowCollection dataRows = (dataTable.Rows);

            shortListedVacancyApplicantCount = (dataRows.Count);

            string currentApplicantKey = null;

            string shortListedApplicantKey = null;
            //string vacancyAgeRange = null;
            //string sShortListedApplicantBirthDate = null;

            int shortListedApplicantCounter = 0;

            foreach(GridViewRow vacancyApplicantRecord in (gvVacanyApplicants.Rows))
            {
                int currentlyBoundRowIndex = (vacancyApplicantRecord.RowIndex);
                currentApplicantKey = (((gvVacanyApplicants.DataKeys[currentlyBoundRowIndex]).Values[2]).ToString());

                // iterating through each record of Short Listed Applicants
                foreach (DataRow dataRow in dataRows)
                {
                    shortListedApplicantCounter += 1;

                    // getting the short listed applicant's Key                    
                    shortListedApplicantKey = ((dataRow.ItemArray[0]).ToString());

                    if ((currentApplicantKey != shortListedApplicantKey) && (shortListedApplicantCounter == shortListedVacancyApplicantCount))
                    {
                        string typeName = null;

                        CheckBox cb = null;

                        foreach (Control c in ((vacancyApplicantRecord.Cells[0]).Controls))
                        {                            
                            typeName = ((c.GetType()).Name);
                            if ((typeName.Equals("CheckBox")))
                            {
                                cb = ((CheckBox)c);
                                cb.Checked = false;
                            }
                        }
                        break;
                    }
                    else if (currentApplicantKey == shortListedApplicantKey)
                    {
                        // getting the short listed age range for Vacancy in question
                        // and applicant's birth date.
                        //vacancyAgeRange = ((dataRow.ItemArray[1]).ToString());
                        //sShortListedApplicantBirthDate = ((dataRow.ItemArray[2]).ToString());

                        string typeName = null;

                        CheckBox cb = null;

                        foreach (Control c in ((vacancyApplicantRecord.Cells[0]).Controls))
                        {
                            typeName = ((c.GetType()).Name);
                            if ((typeName.Equals("CheckBox")))
                            {
                                cb = ((CheckBox)c);
                                cb.Checked = true;
                            }
                        }
                        break;
                        #region If Age Checked
                        /*if (cbAgeRange.Checked)
                        {
                            vacancyAgeRange = (vacancyAgeRange.Trim());
                            int indexOfHyphen = vacancyAgeRange.IndexOf("-");
                            string sFromAge = (vacancyAgeRange.Substring(0, (indexOfHyphen - 1)));
                            string sToAge = (vacancyAgeRange.Substring((indexOfHyphen + 2)));

                            int iFromAge = (int.Parse(sFromAge));
                            int iToAge = (int.Parse(sToAge));

                            int currentYear = ((DateTime.Now).Year);

                            DateTime dShortListedApplicantBirthDate = (DateTime.Parse(sShortListedApplicantBirthDate));
                            int applicantBirthYear = (dShortListedApplicantBirthDate.Year);

                            int applicantAge = (currentYear - applicantBirthYear);

                            if ((applicantAge >= iFromAge) && ((applicantAge <= iToAge)))
                            {
                                string typeName = null;

                                CheckBox cb = null;

                                foreach (Control c in ((vacancyApplicantRecord.Cells[0]).Controls))
                                {
                                    typeName = ((c.GetType()).Name);
                                    if ((typeName.Equals("CheckBox")))
                                    {
                                        cb = ((CheckBox)c);
                                        cb.Checked = true;
                                    }
                                }
                                break;
                            }
                        }
                        else
                        {
                            string typeName = null;

                            CheckBox cb = null;

                            foreach (Control c in ((vacancyApplicantRecord.Cells[0]).Controls))
                            {
                                typeName = ((c.GetType()).Name);
                                if ((typeName.Equals("CheckBox")))
                                {
                                    cb = ((CheckBox)c);
                                    cb.Checked = true;
                                }
                            }
                            break;
                        }*/

#endregion
                    }
                }
            }
        }

        protected void ddlNYSCCompletionStatus_DataBound(object sender, EventArgs e)
        {
            if (((ddlNYSCCompletionStatus.Items).Count) == 0)
            {
                cbNYSCRequirement.Enabled = false;
                ddlNYSCCompletionStatus.Enabled = false;
            }
            else
            {
                cbNYSCRequirement.Enabled = true;
                ddlNYSCCompletionStatus.Enabled = true;
            }
        }

        protected void ddlCourseOfStudy_DataBound(object sender, EventArgs e)
        {
            if (((ddlCourseOfStudy.Items).Count) == 0)
            {
                cbCourseOfStudy.Enabled = false;
                ddlCourseOfStudy.Enabled = false;
            }
            else
            {
                cbCourseOfStudy.Enabled = true;
                ddlCourseOfStudy.Enabled = true;
            }
        }

        protected void ddlMinimumQualification_DataBound(object sender, EventArgs e)
        {
            if (((ddlMinimumQualification.Items).Count) == 0)
            {
                cbMinimumQualification.Enabled = false;
                ddlMinimumQualification.Enabled = false;
            }
            else
            {
                cbMinimumQualification.Enabled = true;
                ddlMinimumQualification.Enabled = true;
            }
        }

        protected void ddlClassOfQualification_DataBound(object sender, EventArgs e)
        {
            if (((ddlClassOfQualification.Items).Count) == 0)
            {
                cbClassOfQualification.Enabled = false;
                ddlClassOfQualification.Enabled = false;
            }
            else
            {
                cbClassOfQualification.Enabled = true;
                ddlClassOfQualification.Enabled = true;
            }
        }

        protected void ddlCountryOfOrigin_DataBound(object sender, EventArgs e)
        {
            if (((ddlCountryOfOrigin.Items).Count) == 0)
            {
                cbCountryOfOorigin.Enabled = false;
                ddlCountryOfOrigin.Enabled = false;
            }
            else
            {
                cbCountryOfOorigin.Enabled = true;
                ddlCountryOfOrigin.Enabled = true;
            }
        }
    }
}

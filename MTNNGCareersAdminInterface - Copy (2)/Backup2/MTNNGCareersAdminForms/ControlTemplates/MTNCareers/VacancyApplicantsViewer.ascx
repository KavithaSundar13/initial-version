﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="VacancyApplicantsViewer.ascx.cs" Inherits="MTNNGCareers.Administration.Forms.ControlTemplates.VacancyApplicantsViewer" %>

<script type="text/javascript">
    var gvVacanyApplicantsClientID = '<% =gvVacanyApplicants.ClientID %>';
</script>

<script type="text/javascript" src="/HR/_layouts/MTNNGCareersAdminForms/javascript/ManageVacancyApplicants.js"></script>

<link type="text/css" rel="Stylesheet" href="/HR/_layouts/MTNNGCareersAdminForms/css/Common.css" />
<link type="text/css" rel="Stylesheet" href="/HR/_layouts/MTNNGCareersAdminForms/css/ManageVacancyApplicants.css" />
<link type="text/css" rel="Stylesheet" href="/HR/_layouts/MTNNGCareersAdminForms/css/Master.css" />

<div class="InformationIndicator">
    <asp:Label ID="lbVacancyApplicantCount" runat="server"></asp:Label>
    <br />
    <asp:Label ID="lblFilteredByDefaultApplicants" runat="server" 
        ToolTip="the number of applicants who meet the requirements of the vacancy based on their CV"></asp:Label>
        <br />
        <asp:Label ID="lblShortListedApplicantCount" runat="server" 
        ToolTip="the number of applicants who have been shortlisted for vacancy" 
        Font-Bold="True" Font-Names="Arial" Font-Size="X-Small" Font-Underline="True" 
        ForeColor="#FF3300"></asp:Label>
</div>
<div>    
    <asp:GridView ID="gvVacanyApplicants" runat="server" EnableModelValidation="True"
        AutoGenerateColumns="False" DataSourceID="vwVacancyApplicantsDataSrc" DataKeyNames="VacancyID,VacancyBreakDownID,RegistrantRowKey"
        OnRowCommand="HandleApplicantNameLinkButtonOnClick" 
        OnRowDataBound="gvVacanyApplicants_RowDataBound" 
        ondatabound="gvVacanyApplicants_DataBound" 
        onselectedindexchanged="gvVacanyApplicants_SelectedIndexChanged"
        CssClass="mGrid"  PagerStyle-CssClass="pgr"   
        AlternatingRowStyle-CssClass="alt" PageSize="30" AllowPaging="True" 
        onpageindexchanging="gvVacanyApplicants_PageIndexChanging" 
        EmptyDataText="NO APPLICATION HAS BEEN MADE FOR THIS OPENING" 
        onrowcreated="gvVacanyApplicants_RowCreated">
<AlternatingRowStyle CssClass="alt"></AlternatingRowStyle>
        <Columns>
            <asp:TemplateField ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Center">
                <HeaderTemplate>
                    <asp:CheckBox ID="cbEnlistHeader" onclick="Handle_cbEnlistHeader_onclick(event)" runat="server" />
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:CheckBox ID="cbEnlist" runat="server" />
                </ItemTemplate>

<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
            </asp:TemplateField>
            <asp:BoundField DataField="VacancyID" HeaderText="VacancyID" SortExpression="VacancyID"
                Visible="False" />
            <asp:BoundField DataField="VacancyBreakDownID" HeaderText="VacancyBreakDownID" SortExpression="VacancyBreakDownID"
                Visible="False" />
            <asp:BoundField DataField="CompanyOrOrganizationName" HeaderText="employer" SortExpression="CompanyOrOrganizationName" />
            <asp:ButtonField AccessibleHeaderText="Applicant Name" DataTextField="Applicant Name"
                HeaderText="Applicant Name" />
            <asp:BoundField DataField="RegistrantRowKey" HeaderText="RegistrantRowKey" SortExpression="RegistrantRowKey"
                Visible="False" />
            <asp:BoundField DataField="BirthDate" HeaderText="BirthDate" SortExpression="BirthDate"
                Visible="False" />
            <asp:BoundField DataField="ApplicationDate" HeaderText="Date Applied" SortExpression="ApplicationDate" />
        </Columns>
        <PagerSettings FirstPageText="First" LastPageText="Last" 
            Mode="NumericFirstLast" />

<PagerStyle CssClass="pgr"></PagerStyle>
    </asp:GridView>
    <div style="float: right;">
     <table style="float: right;">
      <tr>
       <td><asp:Button ID="btnSHORTLIST" runat="server" Text="shortlist" 
            ToolTip="click this button to short list selected applicants" 
            CssClass="MyButton" onclick="HandleShortListApplicants" 
               onclientclick="HandleShortListingInitiation(event)" /></td>
       <td><asp:Button ID="btnSUBMIT" runat="server" Text="submit" 
            ToolTip="click this button to submit selected (short-listed) applicants to the hiring manager who requested that the vacancy they applied for be published."  
            CssClass="MyButton" onclick="HandleSubmitShortListedApplicants" 
               onclientclick="return confirm('Are you sure you would like to submit the list of applicants you have short listed?');" /></td>
      </tr>
     </table>
    </div>
</div>
<div>
<asp:SqlDataSource ID="vwVacancyApplicantsDataSrc" runat="server" ConnectionString="<%$ ConnectionStrings:MTNNGCareersDBConnectionString %>"
        SelectCommand="RetrieveVacancyApplicants" 
        SelectCommandType="StoredProcedure" 
        ondatabinding="vwVacancyApplicantsDataSrc_DataBinding">
        <SelectParameters>
            <asp:ControlParameter ControlID="hfVacancyBreakDownID" Name="VacancyBreakDownID"
                PropertyName="Value" Type="Int64" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:HiddenField ID="hfVacancyID" runat="server" />
</div>
<div>
    <asp:HiddenField runat="server" ID="hfVacancyBreakDownID" />
</div>
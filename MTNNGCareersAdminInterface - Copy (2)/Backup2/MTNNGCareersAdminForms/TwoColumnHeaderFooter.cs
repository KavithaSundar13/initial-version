﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iTextSharp.text.pdf;
using iTextSharp.text;
using Microsoft.SharePoint;
using System.Web;

namespace cvregistrantSpace
{

    public class TwoColumnHeaderFooter : PdfPageEventHelper
    {

        private Font _font;
        private iTextSharp.text.Image _image;
        private PdfGState _state;

        public TwoColumnHeaderFooter()
        {
            _font = new Font(Font.FontFamily.HELVETICA, 40, Font.BOLD, new GrayColor(0.75f));
            //_image = iTextSharp.text.Image.GetInstance(SPContext.Current.Web.Url + "/_layouts/images/AccessBankBACP/mtn-logo.jpg");// (HttpContext.Current.Server.MapPath("~/images/AccessLogoCaret.png"));
            _image = iTextSharp.text.Image.GetInstance(HttpContext.Current.Server.MapPath("/_layouts/MTNNGCareersAdminForms/Reports/mtn-logo.jpg"));
            _image.SetAbsolutePosition(200, 500);
            // set transparency, see commented section below; 'image watermark'
            _state = new PdfGState()
            {
                FillOpacity = 0.3F,
                StrokeOpacity = 0.3F
            };
        }
        // This is the contentbyte object of the writer
        PdfContentByte cb;
        // we will put the final number of pages in a template
        PdfTemplate template;
        // this is the BaseFont we are going to use for the header / footer
        BaseFont bf = null;
        // This keeps track of the creation time
        DateTime PrintTime = DateTime.Now;
        #region Properties
        private string _Title;
        public string Title
        {
            get { return _Title; }
            set { _Title = value; }
        }

        private string _RegistrantName;
        public string RegistrantName
        {
            get { return _RegistrantName; }
            set { _RegistrantName = value; }
        }

        private string _HeaderLeft;
        public string HeaderLeft
        {
            get { return _HeaderLeft; }
            set { _HeaderLeft = value; }
        }
        private string _HeaderRight;
        public string HeaderRight
        {
            get { return _HeaderRight; }
            set { _HeaderRight = value; }
        }
        private Font _HeaderFont;
        public Font HeaderFont
        {
            get { return _HeaderFont; }
            set { _HeaderFont = value; }
        }
        private Font _FooterFont;
        public Font FooterFont
        {
            get { return _FooterFont; }
            set { _FooterFont = value; }
        }
        #endregion
        // we override the onOpenDocument method
        public override void OnOpenDocument(PdfWriter writer, Document document)
        {
            try
            {
                PrintTime = DateTime.Now;
                bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                cb = writer.DirectContent;
                template = cb.CreateTemplate(50, 50);
            }
            catch (DocumentException de)
            {
            }
            catch (System.IO.IOException ioe)
            {
            }
        }

        public override void OnStartPage(PdfWriter writer, Document document)
        {
            base.OnStartPage(writer, document);
            Rectangle pageSize = document.PageSize;
            if (Title != string.Empty)
            {
                cb.BeginText();
                cb.SetFontAndSize(bf, 15);
                cb.SetRGBColorFill(0, 0, 0);
                cb.SetTextMatrix(pageSize.GetLeft(40), pageSize.GetTop(40));
                cb.ShowText(Title);
                cb.EndText();
            }
            if (HeaderLeft + HeaderRight != string.Empty)
            {
                PdfPTable HeaderTable = new PdfPTable(2);
                HeaderTable.DefaultCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                HeaderTable.TotalWidth = pageSize.Width - 80;
                HeaderTable.SetWidthPercentage(new float[] { 45, 45 }, pageSize);

                PdfPCell HeaderLeftCell = new PdfPCell(new Phrase(8, HeaderLeft, HeaderFont));
                HeaderLeftCell.Padding = 5;
                HeaderLeftCell.PaddingBottom = 8;
                HeaderLeftCell.BorderWidthRight = 0;
                HeaderTable.AddCell(HeaderLeftCell);
                PdfPCell HeaderRightCell = new PdfPCell(new Phrase(8, HeaderRight, HeaderFont));
                HeaderRightCell.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                HeaderRightCell.Padding = 5;
                HeaderRightCell.PaddingBottom = 8;
                HeaderRightCell.BorderWidthLeft = 0;
                HeaderTable.AddCell(HeaderRightCell);
                cb.SetRGBColorFill(0, 0, 0);
                HeaderTable.WriteSelectedRows(0, -1, pageSize.GetLeft(40), pageSize.GetTop(50), cb);
            }
        }
        public override void OnEndPage(PdfWriter writer, Document document)
        {


            base.OnEndPage(writer, document);


            

            //Text
            //BaseColor color = new BaseColor(255, 171, 171);
            //var titleFont = FontFactory.GetFont("Arial ROUNDED MT BOLD", 100, iTextSharp.text.Font.BOLD, color);
            //var titleFont1 = FontFactory.GetFont("Arial ROUNDED MT BOLD", 40, iTextSharp.text.Font.BOLD, color);
            //ColumnText.ShowTextAligned(writer.DirectContent, Element.ALIGN_CENTER, new Phrase(">>> ", titleFont), 350f, 700, 0);

            //To Add Image
            PdfContentByte cb = writer.DirectContent;
            cb.SaveState();
            cb.SetGState(_state);
            cb.AddImage(_image);
            cb.RestoreState();


            int pageN = writer.PageNumber;
            //String text = "Page " + pageN + " of ";
            String text = "Page " + pageN;
            float len = bf.GetWidthPoint(text, 9);
            Rectangle pageSize = document.PageSize;

            cb.SetRGBColorFill(100, 100, 100);

            cb.BeginText();
            cb.SetFontAndSize(bf, 9);
            cb.SetTextMatrix(pageSize.GetLeft(40), pageSize.GetBottom(30));
            cb.ShowText(RegistrantName);
            cb.EndText();

            cb.AddTemplate(template, pageSize.GetLeft(40) + len, pageSize.GetBottom(30));

            cb.BeginText();
            cb.SetFontAndSize(bf, 9);
            // cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, AccountNumber, pageSize.GetRight(40), pageSize.GetBottom(30), 0);
            cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, text, pageSize.GetRight(40), pageSize.GetBottom(30), 0);
            cb.EndText();
        }
        public override void OnCloseDocument(PdfWriter writer, Document document)
        {
            base.OnCloseDocument(writer, document);
            template.BeginText();
            template.SetFontAndSize(bf, 9);
            template.SetTextMatrix(0, 0);
            //template.ShowText("" + (writer.PageNumber - 1));
            template.ShowText("");
            template.EndText();
        }
    }

}

﻿using System;

using System.Collections.Generic;

using System.Web.Configuration;

using System.Data;
using System.Data.SqlClient;

using System.Web.UI;
using System.Web.UI.WebControls;

using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
using Microsoft.SharePoint.Utilities;

using MTNNGCareers.Administration.Common;

using CommonUtilities = Utilities.Common;
using SharePointListManager;

namespace MTNNGCareers.Administration.Forms
{
    public enum HiringManagerAction
    {
        Approve,
        RequestChange
    }
    public partial class ShortListedApplicantsReport : LayoutsPageBase
    {
        string currEmp;
        string currPos;

        SqlConnection connection;
        SqlCommand command;
        SqlDataAdapter dataAdapter;
        DataSet dataSet;

        // Routine to create a connection to the MTN NG Careers DB.
        private void CreateConnection()
        {
            string connectionString = ((WebConfigurationManager.ConnectionStrings["MTNNGCareersDBConnectionString"]).ToString());
            connection = new SqlConnection(connectionString);
        }

        // Routine to create command to be issued against MTN NG Careers DB.
        private void CreateCommand(CommandType commandType, string commandText)
        {
            command = new SqlCommand();
            command.Connection = connection;
            command.CommandType = commandType;
            command.CommandText = commandText;
        }

        // Routine to issue command against database.
        private int IssueCommand()
        {
            dataAdapter = new SqlDataAdapter(command);
            dataSet = new DataSet();
            int rowCount = dataAdapter.Fill(dataSet);

            return rowCount;
        }

        // Routine to Work Experience and current employer
        private void RetrieveMostRecentWorkExperience(string applicantKey)
        {
            CreateConnection();

            CreateCommand((CommandType.StoredProcedure), "RetrieveVacancyApplicantMostRecenttWorkExperience");

            command.Parameters.AddWithValue("@RegistrantRowKey", applicantKey);

            int record = IssueCommand();

            int tableCount = ((dataSet.Tables).Count);

            if (tableCount > 1)
            {
                DataTable workExpTable = (dataSet.Tables[1]);
                if (((workExpTable.Rows).Count) > 0)
                {
                    currEmp = (((workExpTable.Rows[0]).ItemArray[0]).ToString());
                    currPos = (((workExpTable.Rows[0]).ItemArray[1]).ToString());
                }
                else
                {
                    currEmp = "";
                    currPos = "";
                }
            }
            else if (tableCount == 1)
            {
                DataTable workExpTable = (dataSet.Tables[0]);
                if (((workExpTable.Rows).Count) > 0)
                {
                    currEmp = (((workExpTable.Rows[0]).ItemArray[0]).ToString());
                    currPos = (((workExpTable.Rows[0]).ItemArray[1]).ToString());
                }
                else
                {
                    currEmp = "";
                    currPos = "";
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    string sVacancyBreakDownID = (Request.QueryString["VacancyBreakDownID"].ToString());                   
                    ViewState["VacancyBreakDownID"] = sVacancyBreakDownID;
                    
                    SPList _VacancyBreakDownList = ((this.Web).Lists[(VacancyBreakDownList.ListName)]);
                    SPListItem _VacancyBreakDown = (_VacancyBreakDownList.GetItemById((int.Parse(sVacancyBreakDownID))));

                    object oStatus = (_VacancyBreakDown[(VacancyBreakDownList.StatusFieldDisplayName)]);
                    if (oStatus != null)
                    {
                        string sStatus = (oStatus.ToString());

                        if ((sStatus.Equals(CommonMembers.OpeningClosed)) || (sStatus.Equals(CommonMembers.RequestChange)))
                        {
                            object oComments = _VacancyBreakDown[(VacancyBreakDownList.HiringManagerCommentFieldDisplayName)];
                            if (oComments != null)
                            {
                                iftComments.Text = (oComments.ToString());
                            }
                            
                            iftComments.Enabled = false;

                            btnApprove.Visible = false;
                            btnRequestChange.Visible = false;
                        }
                    }

                    string vacancyLookupFieldDisplayName = (VacancyBreakDownList.VacancyFieldDisplayName);
                    if (_VacancyBreakDown[vacancyLookupFieldDisplayName] != null)
                    {
                        SPFieldLookup vacancyLookupField = ((SPFieldLookup)_VacancyBreakDown.Fields[(vacancyLookupFieldDisplayName)]);
                        SPFieldLookupValue vacancyLookupFieldValue = ((SPFieldLookupValue)(vacancyLookupField.GetFieldValue((_VacancyBreakDown[vacancyLookupFieldDisplayName].ToString()))));
                        lblJobTitle.Text = (vacancyLookupFieldValue.LookupValue);

                        HiddenField hfVacancyID = ((HiddenField)VacancyViewer.FindControl("hfVacancyID"));
                        if (hfVacancyID != null)
                        {
                            hfVacancyID.Value = ((vacancyLookupFieldValue.LookupId).ToString());

                            SPList _VacancyList = ((this.Web).Lists[(VacanciesList.ListName)]);
                            SPListItem _Vacancy = _VacancyList.GetItemById((vacancyLookupFieldValue.LookupId));

                            CommonUtilities utilities = new CommonUtilities();
                            SPUser _HiringManager = (utilities.GetSPUserFromSPFieldUser(_Vacancy, (VacanciesList.RequesterFieldDisplayName)));
                            SPUser _HRAdvisor = (utilities.GetSPUserFromSPFieldUser(_Vacancy, (VacanciesList.HRAdvisorFieldDisplayName)));
                            SPUser _BusinessPartner = (utilities.GetSPUserFromSPFieldUser(_Vacancy, (VacanciesList.BusinessPartnerFieldDisplayName)));
                            ViewState["HiringManagerEmail"]  =_HiringManager.Email;
                            ViewState["HRAdvisorAndBusinessPartnerEmailAccounts"] = new string[] { _HRAdvisor.Email, _BusinessPartner.Email };

                            int iCurrentUserID = (((this.Web).CurrentUser).ID);
                            if (iCurrentUserID != (_HiringManager.ID))
                            {
                                td_CommentLinkButton.Visible = false;                                
                            }

                        }
                    }

                    DisplayBreakDownDetails(_VacancyBreakDown);
                }
                catch
                {

                }
            }
        }

        // Routine to retrieve and display break down details.
        private void DisplayBreakDownDetails(SPListItem _VacancyBreakDown)
        {
            lblNoOfOpenings.Text = ((_VacancyBreakDown[(VacancyBreakDownList.QuantityFieldDisplayName)]).ToString());
            lblEmploymentStatus.Text = ((_VacancyBreakDown[(VacancyBreakDownList.EmployementStatusFieldDisplayName)]).ToString());
            
            string departmentLookupFieldDisplayName = (VacancyBreakDownList.DepartmentLookUpFieldDisplayName);
            if (_VacancyBreakDown[departmentLookupFieldDisplayName] != null)
            {
                SPFieldLookup departmentLookupField = ((SPFieldLookup)_VacancyBreakDown.Fields[(departmentLookupFieldDisplayName)]);
                SPFieldLookupValue departmentLookupFieldValue = ((SPFieldLookupValue)(departmentLookupField.GetFieldValue((_VacancyBreakDown[departmentLookupFieldDisplayName].ToString()))));
                lblDepartment.Text = (departmentLookupFieldValue.LookupValue);
            }
            else
            {
                lblDepartment.Text = "";
            }

            string unitLookupFieldDisplayName = (VacancyBreakDownList.UnitLookUpFieldDisplayName);
            if (_VacancyBreakDown[unitLookupFieldDisplayName] != null)
            {
                SPFieldLookup unitLookupField = ((SPFieldLookup)_VacancyBreakDown.Fields[(unitLookupFieldDisplayName)]);
                SPFieldLookupValue unitLookupFieldValue = ((SPFieldLookupValue)(unitLookupField.GetFieldValue((_VacancyBreakDown[unitLookupFieldDisplayName].ToString()))));
                lblUnit.Text = (unitLookupFieldValue.LookupValue);
            }
            else
            {
                lblUnit.Text = "";
            }

            string stateLookupFieldDisplayName = (VacancyBreakDownList.StateFieldDisplayName);
            if (_VacancyBreakDown[stateLookupFieldDisplayName] != null)
            {
                SPFieldLookup stateLookupField = ((SPFieldLookup)_VacancyBreakDown.Fields[(stateLookupFieldDisplayName)]);
                SPFieldLookupValue stateLookupFieldValue = ((SPFieldLookupValue)(stateLookupField.GetFieldValue((_VacancyBreakDown[stateLookupFieldDisplayName].ToString()))));
                lblState.Text = (stateLookupFieldValue.LookupValue);
            }
            else
            {
                lblState.Text = "";
            }

            string locationLookupFieldDisplayName = (VacancyBreakDownList.LocationFieldDisplayName);
            if (_VacancyBreakDown[locationLookupFieldDisplayName] != null)
            {
                SPFieldLookup locationLookupField = ((SPFieldLookup)_VacancyBreakDown.Fields[(locationLookupFieldDisplayName)]);
                SPFieldLookupValue locationLookupFieldValue = ((SPFieldLookupValue)(locationLookupField.GetFieldValue((_VacancyBreakDown[locationLookupFieldDisplayName].ToString()))));
                lblLocation.Text = (locationLookupFieldValue.LookupValue);
            }
            else
            {
                lblLocation.Text = "";
            }
        }

        protected void gvVacancyBreakDownSLA_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow justBoundRow = (e.Row);
            int justBoundRowIndex = (justBoundRow.RowIndex);

            if (justBoundRowIndex > -1)
            {
                string applicantKey = (((gvVacancyBreakDownSLA.DataKeys[justBoundRowIndex]).Value).ToString());

                RetrieveMostRecentWorkExperience(applicantKey);

                justBoundRow.Cells[2].Text = currEmp;
                justBoundRow.Cells[3].Text = currPos;

                currEmp = "";
                currPos = "";

                TableCell applicantNameCell = (justBoundRow.Cells[1]);
                applicantNameCell.ToolTip = applicantKey;

                LinkButton applicantNameLinkButton = ((LinkButton)applicantNameCell.Controls[0]);
                applicantNameLinkButton.OnClientClick = "DisplayApplicantCV(event)";
                applicantNameLinkButton.ToolTip = "click here to view applicants c.v";
            }

            //DataRowView justBoundRowView = ((DataRowView)(justBoundRow.DataItem));
            //DataRow record = (justBoundRowView.Row);
        }

        protected void gvVacancyBreakDownSLA_DataBound(object sender, EventArgs e)
        {
            int shortListedApplicantsCount = ((gvVacancyBreakDownSLA.Rows).Count);
            lblSLACount.Text += ": (" + (shortListedApplicantsCount.ToString()) + ")";
        }

        protected void OnSubmitFeedBackInitiation(object sender, EventArgs e)
        {
            SendComment((HiringManagerAction.Approve));

            divVacancy.Attributes["style"] = "display: none";
            divShortListedApplicants.Attributes["style"] = "display: none";
            divComments.Attributes["style"] = "display: block";
        }

        protected void HandleRequestChangeInitiation(object sender, EventArgs e)
        {
            SendComment((HiringManagerAction.RequestChange));

            divVacancy.Attributes["style"] = "display: none";
            divShortListedApplicants.Attributes["style"] = "display: none";
            divComments.Attributes["style"] = "display: block";
        }

        // Routine to send comment.
        private void SendComment(HiringManagerAction action)
        {
            string hiringManagerComments = (String.Empty);

            // obtaining the Globally Unique ID of this Page's website and site collection respectively.
            SPWeb thisPageWebsite = (this.Web);
            SPSite thisPageWebsiteCollection = (thisPageWebsite.Site);
            Guid webGuid = (thisPageWebsite.ID);
            Guid siteGuid = (thisPageWebsiteCollection.ID);

            SPSecurity.RunWithElevatedPrivileges(delegate()
            {
                // Get the site in the impersonated context
                using (SPSite siteBeingImpersonated = new SPSite(siteGuid))
                {
                    // Get the web in the impersonated context
                    using (SPWeb webBeingImpersonated = siteBeingImpersonated.OpenWeb(webGuid))
                    {
                        hiringManagerComments = (iftComments.Text);

                        siteBeingImpersonated.AllowUnsafeUpdates = true;
                        webBeingImpersonated.AllowUnsafeUpdates = true;

                        SPListItem breakDownItemToBeUpdated = ((webBeingImpersonated.Lists[(VacancyBreakDownList.ListName)]).GetItemById((int.Parse((ViewState["VacancyBreakDownID"].ToString())))));
                        breakDownItemToBeUpdated[(VacancyBreakDownList.HiringManagerCommentFieldInternalName)] = hiringManagerComments;

                        if(action.Equals(HiringManagerAction.Approve))
                        {
                            breakDownItemToBeUpdated[(VacancyBreakDownList.StatusFieldInternalName)] = (CommonMembers.OpeningClosed);
                        }
                        else if (action.Equals(HiringManagerAction.RequestChange))
                        {
                            breakDownItemToBeUpdated[(VacancyBreakDownList.StatusFieldInternalName)] = (CommonMembers.RequestChange);
                        }

                        breakDownItemToBeUpdated.Update();

                        siteBeingImpersonated.AllowUnsafeUpdates = false;
                        webBeingImpersonated.AllowUnsafeUpdates = false;                        
                    }
                }
            });            

            System.Text.StringBuilder sb = new System.Text.StringBuilder("<body style='font-family: Arial; font-weight: normal; font-size: small'>");
            sb.Append("Dear HR Recruitment Team, ");
            sb.Append("<br />");
            sb.Append("<br />");
            sb.Append("Thank you for attending to my request.");
            sb.Append("<br />");
            sb.Append("<br />");                        
            sb.Append("These are my thoughts regarding the short list."); 
            sb.Append("<br />");
            sb.Append("<br />");
            sb.Append(hiringManagerComments);
            sb.Append("<br />");
            sb.Append("<br />");
            sb.Append("Kind regards.");

            sb.Append("</body>");

            EmailFacilitator mailSendingFacilitator = new EmailFacilitator();
            mailSendingFacilitator.Sender = (ViewState["HiringManagerEmail"].ToString());
            mailSendingFacilitator.Recipients = (new string[] {((WebConfigurationManager.AppSettings["HRRecruitmentTeamGroupMailAddress"]).ToString())});
            mailSendingFacilitator.CCs = ((string[]) ViewState["HRAdvisorAndBusinessPartnerEmailAccounts"]);
            mailSendingFacilitator.Subject = ("Hiring Manager's Response to Short Listed Candidates for Vacancy with Job Title: " + (lblJobTitle.Text));
            mailSendingFacilitator.Body = (sb.ToString());
            mailSendingFacilitator.SendMail();

            SPUtility.TransferToSuccessPage("Your comments have been sent to the H.R Recruitment Team. Thank you.");
        }
    }
}


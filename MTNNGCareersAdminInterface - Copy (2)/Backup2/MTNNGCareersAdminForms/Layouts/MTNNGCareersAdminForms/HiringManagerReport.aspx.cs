﻿using System;

using System.Web.UI.WebControls;

using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;

namespace MTNNGCareers.Administration.Forms
{
    public partial class HiringManagerReport : LayoutsPageBase
    {
        const string _CurrentUserMembershipID = "Current User Membership ID";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                object oRequesterMembershipID = Request.QueryString["RequesterMembershipID"];
                object oVacancyBreakDownID = Request.QueryString["VacancyBreakDownID"];

                if((oRequesterMembershipID != null) && (oVacancyBreakDownID != null))
                {
                    SPUser loggedOnUser = ((this.Web).CurrentUser);
                    ViewState["RequesterID"] = ((loggedOnUser.ID).ToString());

                    ViewState["VacancyBreakDownID"] = (oVacancyBreakDownID.ToString());
                }

                string vacancyReportPath = Server.MapPath("~/_layouts/MTNNGCareersAdminForms/Reports/OpeningShortListedApplicants.rdlc");
                rvRequester.LocalReport.ReportPath = vacancyReportPath;
                rvRequester.LocalReport.Refresh();
            }

            //rvRequester.LocalReport.SetBasePermissionsForSandboxAppDomain(AppDomain.CurrentDomain.Copy());
        }

        protected void ObjectDataSource1_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.InputParameters["Requester"] = (ViewState["RequesterID"].ToString());
            e.InputParameters["VacancyBreakDownID"] = (ViewState["VacancyBreakDownID"].ToString());
        }
    }
}

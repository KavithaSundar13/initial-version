﻿using System;
using System.Configuration;

using System.IO;

using System.Web;
using System.Web.Configuration;

using System.Data;
using System.Data.SqlClient;

using System.Collections;
using System.Collections.Generic;

using System.Web.UI;
using System.Web.UI.WebControls;

using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;

using Microsoft.SharePoint.Utilities;

using MTNNGCareers.Administration.Common;
using Utilities;
using CommonUtilities = Utilities.Common;
using SharePointListManager;

using MTNNGCareers.Administration;

using AjaxControlToolkit;

namespace MTNNGCareers.Administration.Forms
{
    public enum TemplateType
    {
        Insert,
        Edit
    }

    public partial class NewVacancy : LayoutsPageBase
    {
        string listGuid;
        string listItemId;

        List<string[,]> newVacancy;

        SPWeb thisPageWebsite;
        SPUser thisPageCurrentUser;

        const string itemToBeUpdatedID = "Item to be updated ID";

        const string listViewDataSource = "ListView Data Source";

        const string selectOptionText = "---select---";
        const string emptyDataObjectIndicator = "-----";
        const string _NOTSPECIFIED = "NOT SPECIFIED";

        // serves as an in memory table for vacacny locations.
        DataTable newVacancyBreakDownTable;

        const string _IDFieldName = "ID";

        string noOfOpenings;
        string status;
        string department;
        string unit;
        string state;
        string location;

        protected void Page_Load(object sender, EventArgs e)
        {
            // obtaining a reference to this page's website
            // and the current user respectively.
            thisPageWebsite = ((SPContext.Current).Web);
            thisPageCurrentUser = (thisPageWebsite.CurrentUser);

            SPUserAuthenticator authenticator = new SPUserAuthenticator();
            bool isValidUser = (authenticator.ValidateUser());

            if (!isValidUser)
            {
                SPUtility.Redirect((thisPageWebsite.Url), SPRedirectFlags.Default, Context);
            }

            // If this is the first time the page is loaded, ...
            if (!IsPostBack)
            {
                BindDivisionsToControl();

                BindMinimunQualificationsToControl();

                BindClassOfDegreeToControl();

                if ((Request.Params["List"]) != null)
                {
                    listGuid = (Request.Params["List"]);

                    if ((Request.Params["ID"]) != null)
                    {
                        btnPUBLISH.Visible = false;

                        listItemId = (Request.Params["ID"]);

                        ViewState[itemToBeUpdatedID] = listItemId;

                        SPList vacancyList = (thisPageWebsite.Lists[((new Guid(listGuid)))]);
                        SPListItem selectedItem = vacancyList.GetItemById((int.Parse(listItemId)));
                        string mmqual = null;
                        string cnnString = WebConfigurationManager.ConnectionStrings["MTNNGCareersDBConnectionString1"].ConnectionString;
                        SqlConnection cnn = new SqlConnection(cnnString);
                        SqlTransaction transaction;

                        cnn.Open();
                        transaction = cnn.BeginTransaction();
                        try
                        {
                            string vacancyID = int.Parse(listItemId).ToString();
                            SqlCommand comm = new SqlCommand("select * from [MTNNGCareers].[dbo].[Minimum_Qualification] where VacancyID =@vacancyID", cnn, transaction);
                            comm.Parameters.Add(new SqlParameter("vacancyID", vacancyID));
                            SqlDataReader reader = comm.ExecuteReader();
                            if (reader.HasRows)
                            {
                                while (reader.Read())
                                {
                                    mmqual = reader.GetString(1);
                                    string vl = mmqual;
                                    string[] vl1 = vl.Split(',');
                                    int len = vl1.Length;
                                    for (int i = 0; i < len; i++)
                                    {
                                        LBMinimumQualification.Items.Add(vl1[i]);
                                    }
                                }
                            }
                            reader.Close();
                            transaction.Commit();

                        }
                        catch (Exception ex)
                        {
                            
                            transaction.Rollback();

                        }

                        finally
                        {
                            cnn.Close();
                            cnn.Dispose();
                        }


                        DisplayVacancyDetails(selectedItem);
                        
                        btnUPDATE.Visible = true;
                    }
                }
            }

            RetrieveContents();

            if ((Request.Params["ID"]) == null)
            {
                if ((ddlDivision.SelectedIndex) > 0)
                {
                    lnkBtnBreakDown.Visible = true;
                }
            }
        }

        // Routine to display selected vacancy details.
        private void DisplayVacancyDetails(SPListItem listItem)
        {
            iftJobTitle.Text = ((listItem[(VacanciesList.JobTitleFieldDisplayName)]).ToString());

            string divisionFieldDisplayName = (VacanciesList.DivisionFieldDisplayName);
            SPFieldLookup spFieldLookup = ((SPFieldLookup)(listItem.Fields[divisionFieldDisplayName]));
            SPFieldLookupValue spFieldLookupValue = ((SPFieldLookupValue)(spFieldLookup.GetFieldValue(((listItem[divisionFieldDisplayName].ToString())))));
            ddlDivision.SelectedValue = ((spFieldLookupValue.LookupId).ToString());
            ddlDivision.Enabled = false;

            rblStaffLevel.SelectedValue = ((listItem[(VacanciesList.StaffLevelFieldDisplayName)]).ToString());

            bool isRequired = (bool.Parse(((listItem[(VacanciesList.IsNYSCRequiredFieldDisplayName)]).ToString())));
            if (isRequired)
            {
                rblIsNYSCRequired.SelectedValue = "1";
            }
            else
            {
                rblIsNYSCRequired.SelectedValue = "0";
            }            

            string sStartAdDate = ((listItem[(VacanciesList.StartAdvertisementDateFieldDisplayName)]).ToString());
            //sStartAdDate = sStartAdDate.Substring(0, (sStartAdDate.IndexOf(" ")));
            iftStartAdvertisementDate.Text = sStartAdDate;

            string sEndAdDate = ((listItem[(VacanciesList.EndAdvertisementDateFieldDisplayName)]).ToString());
            //sEndAdDate = sEndAdDate.Substring(0, (sEndAdDate.IndexOf(" ")));
            iftEndAdvertisementDate.Text = sEndAdDate;

            string requesterDisplayName = (VacanciesList.RequesterFieldDisplayName);
            SPFieldUser userField = ((SPFieldUser)(listItem.Fields[requesterDisplayName]));
            SPFieldUserValue userFieldValue = ((SPFieldUserValue)(userField.GetFieldValue(((listItem[requesterDisplayName].ToString())))));
            PickerEntity pickerEntity = new PickerEntity();
            pickerEntity.Key = ((userFieldValue.User).LoginName);            
            ArrayList requester = new ArrayList();
            requester.Add(pickerEntity);
            peRequester.UpdateEntities(requester);

            iftRequesterPosition.Text = ((listItem[(VacanciesList.RequesterDesignationFieldDisplayName)]).ToString());

            string businessPartnerDisplayName = (VacanciesList.BusinessPartnerFieldDisplayName);
            object oBusinessPartner = (listItem[businessPartnerDisplayName]);
            if (oBusinessPartner != null)
            {                
                userField = ((SPFieldUser)(listItem.Fields[businessPartnerDisplayName]));
                userFieldValue = ((SPFieldUserValue)(userField.GetFieldValue(((oBusinessPartner.ToString())))));
                pickerEntity = new PickerEntity();
                pickerEntity.Key = ((userFieldValue.User).LoginName);
                ArrayList businessPartner = new ArrayList();
                businessPartner.Add(pickerEntity);
                peBusinessPartner.UpdateEntities(businessPartner);
            }

            string hrAdvisorDisplayName = (VacanciesList.HRAdvisorFieldDisplayName);
            object oHRAdvisory = (listItem[hrAdvisorDisplayName]);
            if (oHRAdvisory != null)
            {
                userField = ((SPFieldUser)(listItem.Fields[hrAdvisorDisplayName]));
                userFieldValue = ((SPFieldUserValue)(userField.GetFieldValue(((oHRAdvisory.ToString())))));
                pickerEntity = new PickerEntity();
                pickerEntity.Key = ((userFieldValue.User).LoginName);
                ArrayList hrAdvisor = new ArrayList();
                hrAdvisor.Add(pickerEntity);
                peHRAdvisor.UpdateEntities(hrAdvisor);
            }

            iftJobDescription.Text = ((listItem[(VacanciesList.JobDescriptionFieldDisplayName)]).ToString());
            iftJobConditions.Text = ((listItem[(VacanciesList.JobConditionsFieldDisplayName)]).ToString());
            iftJobSkillsRequired.Text = ((listItem[(VacanciesList.SkillsRequiredFieldDisplayName)]).ToString());

            //string minQualFieldDisplayName = (VacanciesList.MinimumQualificationFieldDisplayName);
            //spFieldLookup = ((SPFieldLookup)(listItem.Fields[minQualFieldDisplayName]));
            //spFieldLookupValue = ((SPFieldLookupValue)(spFieldLookup.GetFieldValue(((listItem[minQualFieldDisplayName].ToString())))));
            //ddlMinimumQualification.SelectedValue = ((spFieldLookupValue.LookupId).ToString());

            object oProfQual = (listItem[(VacanciesList.ProfessionalQualificationFieldDisplayName)]);
            if(oProfQual != null)
            {
                iftProfessionalQualifcations.Text = (oProfQual.ToString());
            }
           
            object oAddQual = (listItem[(VacanciesList.AdditionalQualificationFieldDisplayName)]);
            if(oAddQual != null)
            {
                iftAdditionalQualifcations.Text = (oAddQual.ToString());
            }

            object oCourseOfStudy = (listItem[(VacanciesList.CourseOfStudyFieldDisplayName)]);
            if (oCourseOfStudy != null)
            {
                iftCourseOfStudy.Text = (oCourseOfStudy.ToString());
            }

            object oClassOfDegree = (listItem[(VacanciesList.ClassOfDegreeFieldDisplayName)]);
            if (oClassOfDegree != null)
            {
                string classOfDegreeFieldDisplayName = (VacanciesList.ClassOfDegreeFieldDisplayName);
                spFieldLookup = ((SPFieldLookup)(listItem.Fields[classOfDegreeFieldDisplayName]));
                spFieldLookupValue = ((SPFieldLookupValue)(spFieldLookup.GetFieldValue(((listItem[classOfDegreeFieldDisplayName].ToString())))));
                ddlClassOfDegree.SelectedValue = ((spFieldLookupValue.LookupId).ToString());
            }

            object oPostDYoE = (listItem[(VacanciesList.PostDegreeYearsOfExperienceFieldDisplayName)]);
            if (oPostDYoE != null)
            {
                iftPostDegreeYearsOfExperience.Text = (oPostDYoE.ToString());
            }

            object oGender = (listItem[(VacanciesList.GenderFieldDisplayName)]);
            if (oGender != null)
            {
                rblGender.SelectedValue = (oGender.ToString());
            }
        }

        // Routine to bind minimum qualifications to the DropDownList used to display it.
        private void BindMinimunQualificationsToControl()
        {
            SPListItemCollection _Qualifications = RetrieveQualifications();

            foreach (SPListItem _SPListItem in _Qualifications)
            {
                // getting the value of the Title Field of the current item with the collection.
                object titleFieldValue = _SPListItem[(QualificationsList.TitleFieldDisplayName)];

                if (titleFieldValue != null)
                {
                    // retrieving the value of the current item's Division Field.
                    string text = (_SPListItem[(QualificationsList.TitleFieldDisplayName)].ToString());
                    string id = (_SPListItem[_IDFieldName].ToString());

                    ListItem li = new ListItem(text, id);
                    ddlMinimumQualification.Items.Add(li);
                }
            }

            ddlMinimumQualification.Items.Insert(0, selectOptionText);
        }
        // Routine to retrieve qualifications
        private SPListItemCollection RetrieveQualifications()
        {
            SPListItemCollection _Qualifications = null;

            ListAccessor _ListAccessor = new ListAccessor(thisPageWebsite, (QualificationsList.ListName), (QualificationsList.TitleFieldInternalName), true);
            _Qualifications = (_ListAccessor.RequiredListItems);

            return _Qualifications;
        }

        // Routine to bind classes of degree to the DropDownList used to display it.
        private void BindClassOfDegreeToControl()
        {
            SPListItemCollection _ClassesOfDegree = RetrieveClassesOfDegree();

            foreach (SPListItem _SPListItem in _ClassesOfDegree)
            {
                // getting the value of the Title Field of the current item with the collection.
                object titleFieldValue = _SPListItem[(ClassOfDegreeList.TitleFieldDisplayName)];

                if (titleFieldValue != null)
                {
                    // retrieving the value of the current item's Division Field.
                    string text = (_SPListItem[(ClassOfDegreeList.TitleFieldDisplayName)].ToString());
                    string id = (_SPListItem[_IDFieldName].ToString());

                    ListItem li = new ListItem(text, id);
                    ddlClassOfDegree.Items.Add(li);
                }
            }

            ddlClassOfDegree.Items.Insert(0, selectOptionText);
        }
        // Routine to retrieve classes of degree
        private SPListItemCollection RetrieveClassesOfDegree()
        {
            SPListItemCollection _ClassesOfDegree = null;

            ListAccessor _ListAccessor = new ListAccessor(thisPageWebsite, (ClassOfDegreeList.ListName), (ClassOfDegreeList.TitleFieldInternalName), true);
            _ClassesOfDegree = (_ListAccessor.RequiredListItems);

            return _ClassesOfDegree;
        }

        // Routine to bind Divisions to the web server control used to display it.
        private void BindDivisionsToControl()
        {
            SPListItemCollection _Divisions = RetrieveDivisions();

            foreach (SPListItem _SPListItem in _Divisions)
            {
                // getting the value of the Division Field of the current item with the collection.
                object divisionFieldValue = _SPListItem[(DivisionList.DivisionFieldDisplayName)];

                if (divisionFieldValue != null)
                {
                    // retrieving the value of the current item's Division Field.
                    string text = (_SPListItem[(DivisionList.DivisionFieldDisplayName)].ToString());
                    string id = (_SPListItem[_IDFieldName].ToString());

                    ListItem li = new ListItem(text, id);
                    ddlDivision.Items.Add(li);
                }
            }

            ddlDivision.Items.Insert(0, selectOptionText);
        }
        // Routine to retrieve a listing of MTN NG Divisions.
        private SPListItemCollection RetrieveDivisions()
        {
            SPListItemCollection _Divisions = null;

            ListAccessor _ListAccessor = new ListAccessor((thisPageWebsite.ParentWeb), (DivisionList.ListName), (DivisionList.DivisionFieldInternalName), true);
            _Divisions = (_ListAccessor.RequiredListItems);

            return _Divisions;
        }

        // handles the event raised when an item is selected from the DropDownList used to display Divsions.
        protected void Handle_ddlDivision_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            if ((ddlDivision.SelectedIndex) > 0)
            {
                lv_VacancyBreakDown.EditIndex = -1;
                lv_VacancyBreakDown.Items.Clear();
                newVacancyBreakDownTable = ((DataTable)(ViewState[listViewDataSource]));
                newVacancyBreakDownTable = null;
                ViewState[listViewDataSource] = newVacancyBreakDownTable;

                BindDataToListView();

                BindDepartmentsToControl((TemplateType.Insert));

                BindStatesOfOperationToControl((TemplateType.Insert));

                string[,] criterionField = new string[1, 3];
                criterionField[0, 0] = (StatesOrProvincesOfOriginList.CountryFieldInternalName);
                criterionField[0, 1] = (CommonUtilities.FieldTypeLookUp);
                criterionField[0, 2] = "Nigeria";

                ListAccessor listAccessor = new ListAccessor(thisPageWebsite, (StatesOrProvincesOfOriginList.ListName), (new string[] { "ID", (StatesOrProvincesOfOriginList.NameFieldInternalName) }), criterionField);
                SPListItemCollection requiredItems = (listAccessor.RequiredListItems);

                byte itemCount = ((byte)requiredItems.Count);

                if (itemCount > 0)
                {
                    SPListItem listItem = null;

                    for (byte i = 0; i < itemCount; i++)
                    {
                        listItem = requiredItems[i];
                        string id = (listItem["ID"].ToString());

                        string name = (listItem[(StatesOrProvincesOfOriginList.NameFieldDisplayName)].ToString());
                    }
                }
              
                lnkBtnBreakDown.Visible = true;

                RetrieveContents();

                divJobDescription.Attributes["style"] = "display: none";
                divJobDescriptionLtr.Attributes["style"] = "display: none";
                divConditions.Attributes["style"] = "display: none";
                divConditionsLiteral.Attributes["style"] = "display: none";
                divSkillsRequired.Attributes["style"] = "display: none";
                divSkillsRequiredLiteral.Attributes["style"] = "display: none";
                divPreferences.Attributes["style"] = "display: none";
                divQualifications.Attributes["style"] = "display: none";                
                divBreakDown.Attributes["style"] = "display: none";
                divGeneralInformation.Attributes["style"] = "display: block";
            }
            else
            {
                lnkBtnBreakDown.Visible = false;
            }
        }

        // Routine to retrieve the contents (HTML mark ups) of the Description, Conditions and Skills Required.
        private void RetrieveContents()
        {
            ltrJobDescription.Text = (iftJobDescription.Text);
            ltrJobConditions.Text = (iftJobConditions.Text);
            ltrSkillsRequired.Text = (iftJobSkillsRequired.Text);
        }

        // Routine to retain Break down display focus.
        private void RetainBreakDownFocus()
        {
            lnkBtnBreakDown.Focus();
            divBreakDown.Focus();
        }
        
        // Routine to specify and bind data source to ListView.
        private void BindDataToListView()
        {
            if (ViewState[listViewDataSource] == null)
            {
                CreateVacancyBreakDownTable();
                
                lv_VacancyBreakDown.DataSource = newVacancyBreakDownTable;
                lv_VacancyBreakDown.DataBind();

                ViewState[listViewDataSource] = newVacancyBreakDownTable;
            }
            else
            {
                newVacancyBreakDownTable = ((DataTable)ViewState[listViewDataSource]);

                lv_VacancyBreakDown.DataSource = newVacancyBreakDownTable;
                lv_VacancyBreakDown.DataBind();
            }
        }
        // Routine to create an in memory table which would server as the data source for the List View.
        private void CreateVacancyBreakDownTable()
        {
            newVacancyBreakDownTable = new DataTable();

            DataColumn breakDownIDColumn = new DataColumn();
            breakDownIDColumn.ColumnName = "ID";
            newVacancyBreakDownTable.Columns.Add(breakDownIDColumn);

            DataColumn vacancyIDColumn = new DataColumn();
            vacancyIDColumn.ColumnName = "Vacancy ID";
            newVacancyBreakDownTable.Columns.Add(vacancyIDColumn);

            DataColumn _noOfOpeningsColumn = new DataColumn();
            _noOfOpeningsColumn.ColumnName = "noOfOpenings";
            newVacancyBreakDownTable.Columns.Add(_noOfOpeningsColumn);

            DataColumn _StatusColumn = new DataColumn();
            _StatusColumn.ColumnName = "Status";
            newVacancyBreakDownTable.Columns.Add(_StatusColumn);

            DataColumn _DepartmentColumn = new DataColumn();
            _DepartmentColumn.ColumnName = "Department";
            newVacancyBreakDownTable.Columns.Add(_DepartmentColumn);

            DataColumn _UnitColumn = new DataColumn();
            _UnitColumn.ColumnName = "Unit";
            newVacancyBreakDownTable.Columns.Add(_UnitColumn);

            DataColumn _StateColumn = new DataColumn();
            _StateColumn.ColumnName = "State";
            newVacancyBreakDownTable.Columns.Add(_StateColumn);

            DataColumn _LocationColumn = new DataColumn();
            _LocationColumn.ColumnName = "Location";
            newVacancyBreakDownTable.Columns.Add(_LocationColumn);
        }
        // Routine to bind Departments to the web server control used to display it.
        private void BindDepartmentsToControl(TemplateType templateType)
        {
            DropDownList ddlDepartment = null;

            if (templateType.Equals(TemplateType.Insert))
            {
                // obtaning a reference to the DropDownList used to display departments.
                ddlDepartment = ((DropDownList)((lv_VacancyBreakDown.InsertItem).FindControl("ddlDepartment_InsertTemplate")));
            }
            else
            {
                // obtaning a reference to the DropDownList used to display departments.
                ddlDepartment = ((DropDownList)((lv_VacancyBreakDown.EditItem).FindControl("ddlDepartment_EditTemplate")));
            }

            // if the selected control is found, ...
            if (ddlDepartment != null)
            {
                ddlDepartment.Items.Clear();

                string selectedDivision = ((ddlDivision.SelectedItem).Text);
                SPListItemCollection divisionDepartments = RetrieveDepartmentsByDivision(selectedDivision);

                foreach (SPListItem _SPListItem in divisionDepartments)
                {
                    string value = (_SPListItem[_IDFieldName].ToString());
                    string text = (_SPListItem[(DepartmentList.DepartmentFieldDisplayName)].ToString());

                    ListItem li = new ListItem(text, value);

                    ddlDepartment.Items.Add(li);
                }

                ddlDepartment.Items.Insert(0, selectOptionText);
            }
        }
        // Routine to retrieve Departments within a selected Division.
        private SPListItemCollection RetrieveDepartmentsByDivision(string selectedDivision)
        {
            SPListItemCollection divisionDepartments = null;

            string[,] criterionField = new string[1, 3];
            criterionField[0, 0] = (DepartmentList.DivisionFieldInternalName);
            criterionField[0, 1] = (Utilities.Common.FieldTypeLookUp);
            criterionField[0, 2] = selectedDivision;

            ListAccessor _ListAccessor = new ListAccessor((thisPageWebsite.ParentWeb), (DepartmentList.ListName), (new string[] { (DepartmentList.DepartmentFieldInternalName) }), criterionField);
            divisionDepartments = (_ListAccessor.RequiredListItems);

            return divisionDepartments;
        }
        // Routine to bind States of Operation to a selected web server control.
        private void BindStatesOfOperationToControl(TemplateType templateType)
        {
            SPListItemCollection _StatesOfOperation = RetrieveStatesOfOperation();
            DropDownList ddlState = null;

            if (templateType.Equals(TemplateType.Insert))
            {
                ddlState = ((DropDownList)(lv_VacancyBreakDown.InsertItem).FindControl("ddlState_InsertTemplate"));
            }
            else
            {
                ddlState = ((DropDownList)(lv_VacancyBreakDown.EditItem).FindControl("ddlState_EditTemplate"));
            }

            if (ddlState != null)
            {
                foreach (SPListItem _SPListItem in _StatesOfOperation)
                {
                    string value = (_SPListItem[_IDFieldName].ToString());
                    string text = (_SPListItem[(StatesOfOperationWithinNigeriaList.TitleFieldDisplayName)].ToString());

                    ListItem li = new ListItem(text, value);

                    ddlState.Items.Add(li);
                }

                ddlState.Items.Insert(0, selectOptionText);
            }
        }
        // Routine to retrieve a listing of States within within FGN where MTN NG deploys employees.
        private SPListItemCollection RetrieveStatesOfOperation()
        {
            SPListItemCollection _StatesOfOperation = null;

            ListAccessor _ListAccessor = new ListAccessor(thisPageWebsite, (StatesOfOperationWithinNigeriaList.ListName), (StatesOfOperationWithinNigeriaList.TitleFieldInternalName), true);
            _StatesOfOperation = (_ListAccessor.RequiredListItems);

            return _StatesOfOperation;
        }

        // handles the event raised when an item is selected from the DropDownList used to display departments
        protected void Handle_ddlDepartment_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            if (lv_VacancyBreakDown.EditIndex == -1)
            {
                BindUnitsOfDepartmentToControl((TemplateType.Insert));
            }
            else
            {
                BindUnitsOfDepartmentToControl((TemplateType.Edit));
            }

            RetrieveContents();
            
            divJobDescription.Attributes["style"] = "display: none";
            divJobDescriptionLtr.Attributes["style"] = "display: none";
            divConditions.Attributes["style"] = "display: none";
            divConditionsLiteral.Attributes["style"] = "display: none";
            divSkillsRequired.Attributes["style"] = "display: none";
            divSkillsRequiredLiteral.Attributes["style"] = "display: none";
            divPreferences.Attributes["style"] = "display: none";
            divQualifications.Attributes["style"] = "display: none";
            divGeneralInformation.Attributes["style"] = "display: none";
            divBreakDown.Attributes["style"] = "display: block";
        }
        // Routine to bind units of a department to the List Control to render it.
        private void BindUnitsOfDepartmentToControl(TemplateType templateType)
        {
            DropDownList ddlDepartment = null;

            if (templateType.Equals((TemplateType.Insert)))
            {
                ddlDepartment = ((DropDownList)(lv_VacancyBreakDown.InsertItem).FindControl("ddlDepartment_InsertTemplate"));
            }
            else
            {
                ddlDepartment = ((DropDownList)(lv_VacancyBreakDown.EditItem).FindControl("ddlDepartment_EditTemplate"));
            }

            if (ddlDepartment != null)
            {
                DropDownList ddlUnit = null;
                if (templateType.Equals((TemplateType.Insert)))
                {
                    ddlUnit = ((DropDownList)(lv_VacancyBreakDown.InsertItem).FindControl("ddlUnit_InsertTemplate"));
                }
                else
                {
                    ddlUnit = ((DropDownList)(lv_VacancyBreakDown.EditItem).FindControl("ddlUnit_EditTemplate"));
                }

                if (ddlUnit != null)
                {
                    ddlUnit.Items.Clear();

                    string selectedDepartment = ((ddlDepartment.SelectedItem).Text);
                    SPListItemCollection departmentUnits = RetrieveDepartmentUnits(selectedDepartment);

                    if ((departmentUnits.Count) == 0)
                    {
                        ddlUnit.Items.Add(emptyDataObjectIndicator);
                        return;
                    }

                    foreach (SPListItem _SPListItem in departmentUnits)
                    {
                        string value = (_SPListItem[_IDFieldName].ToString());
                        string text = (_SPListItem[(UnitsList.TitleFieldDisplayName)].ToString());

                        ListItem li = new ListItem(text, value);

                        ddlUnit.Items.Add(li);
                    }

                    ddlUnit.Items.Insert(0, selectOptionText);
                    ddlUnit.Focus();
                }
            }
        }
        // Routine to retrieve units of the selected departments.
        private SPListItemCollection RetrieveDepartmentUnits(string selectedDepartment)
        {
            SPListItemCollection departmentUnits = null;

            string[,] criterionField = new string[1, 3];
            criterionField[0, 0] = (UnitsList.DepartmentFieldInternalName);
            criterionField[0, 1] = (Utilities.Common.FieldTypeLookUp);
            criterionField[0, 2] = selectedDepartment;

            ListAccessor _ListAccessor = new ListAccessor((thisPageWebsite.ParentWeb), (UnitsList.ListName), (new string[] { (UnitsList.TitleFieldInternalName) }), criterionField);
            departmentUnits = (_ListAccessor.RequiredListItems);

            return departmentUnits;
        }

        // handles the event raised when an item is selected from the DropDownList used to display States of Operation.
        protected void Handle_ddlState_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            if (lv_VacancyBreakDown.EditIndex == -1)
            {
                BindLocationsOfOperationToControl((TemplateType.Insert));
            }
            else
            {
                BindLocationsOfOperationToControl((TemplateType.Edit));
            }

            RetrieveContents();

            divJobDescription.Attributes["style"] = "display: none";
            divJobDescriptionLtr.Attributes["style"] = "display: none";
            divConditions.Attributes["style"] = "display: none";
            divConditionsLiteral.Attributes["style"] = "display: none";
            divSkillsRequired.Attributes["style"] = "display: none";
            divSkillsRequiredLiteral.Attributes["style"] = "display: none";
            divPreferences.Attributes["style"] = "display: none";
            divQualifications.Attributes["style"] = "display: none";
            divGeneralInformation.Attributes["style"] = "display: none";
            divBreakDown.Attributes["style"] = "display: block";
        }
        // Routine to bind locations of operations within the specified state.
        private void BindLocationsOfOperationToControl(TemplateType templateType)
        {
            DropDownList ddlLocation = null;

            if (templateType.Equals((TemplateType.Insert)))
            {
                ddlLocation = ((DropDownList)(lv_VacancyBreakDown.InsertItem).FindControl("ddlLocation_InsertTemplate"));
            }
            else
            {
                ddlLocation = ((DropDownList)(lv_VacancyBreakDown.EditItem).FindControl("ddlLocation_EditTemplate"));
            }

            if (ddlLocation != null)
            {
                ddlLocation.Items.Clear();

                DropDownList ddlState = null;
                if (templateType.Equals((TemplateType.Insert)))
                {
                    ddlState = ((DropDownList)(lv_VacancyBreakDown.InsertItem).FindControl("ddlState_InsertTemplate"));
                }
                else
                {
                    ddlState = ((DropDownList)(lv_VacancyBreakDown.EditItem).FindControl("ddlState_EditTemplate"));
                }

                if (ddlState != null)
                {
                    string selectedState = ((ddlState.SelectedItem).Text);
                    SPListItemCollection stateLocations = RetrieveLocationsOfOperationByState(selectedState);

                    if ((stateLocations.Count) == 0)
                    {
                        ddlLocation.Items.Add(emptyDataObjectIndicator);
                        return;
                    }

                    foreach (SPListItem _SPListItem in stateLocations)
                    {
                        string value = (_SPListItem[_IDFieldName].ToString());
                        string text = (_SPListItem[(LocationsOfOperationWithinNigeriaList.TitleFieldDisplayName)].ToString());

                        ListItem li = new ListItem(text, value);

                        ddlLocation.Items.Add(li);
                    }

                    ddlLocation.Items.Insert(0, selectOptionText);                    
                }
            }
        }
        // Routine to retrieve locations of operations based on a selected state.
        private SPListItemCollection RetrieveLocationsOfOperationByState(string selectedState)
        {
            SPListItemCollection stateLocations = null;

            string[,] criterionField = new string[1, 3];
            criterionField[0, 0] = (LocationsOfOperationWithinNigeriaList.StateFieldInternalName);
            criterionField[0, 1] = (Utilities.Common.FieldTypeLookUp);
            criterionField[0, 2] = selectedState;

            ListAccessor _ListAccessor = new ListAccessor(thisPageWebsite, (LocationsOfOperationWithinNigeriaList.ListName), (new string[] { (LocationsOfOperationWithinNigeriaList.TitleFieldInternalName) }), criterionField);
            stateLocations = (_ListAccessor.RequiredListItems);

            return stateLocations;
        }

        // Routine to handle the event raised when an item is being inserted into the ListView.
        protected void Handle_lv_VacancyBreakDown_OnItemInserting(object sender, ListViewInsertEventArgs e)
        {
            // obtaining a reference to the item to be inserted.
            ListViewItem listViewItem = (e.Item);

            GetVacancyBreakDownEntries(listViewItem, (TemplateType.Insert));

            AddVacancyBreakDownTolistViewDataSource();

            BindDataToListView();

            BindDepartmentsToControl((TemplateType.Insert));

            BindStatesOfOperationToControl((TemplateType.Insert));

            RetrieveContents();

            divJobDescription.Attributes["style"] = "display: none";
            divJobDescriptionLtr.Attributes["style"] = "display: none";
            divConditions.Attributes["style"] = "display: none";
            divConditionsLiteral.Attributes["style"] = "display: none";
            divSkillsRequired.Attributes["style"] = "display: none";
            divSkillsRequiredLiteral.Attributes["style"] = "display: none";
            divPreferences.Attributes["style"] = "display: none";
            divQualifications.Attributes["style"] = "display: none";
            divGeneralInformation.Attributes["style"] = "display: none";
            divBreakDown.Attributes["style"] = "display: block";
        }
        // Routine to get entries for Vacancy Location(s)
        private void GetVacancyBreakDownEntries(ListViewItem listViewItem, TemplateType templateType)
        {
            InputFormTextBox txtNoOfOpenings;
            DropDownList ddlStatus;
            DropDownList ddlDepartment;
            DropDownList ddlUnit;
            DropDownList ddlState;
            DropDownList ddlLocation;

            if (templateType.Equals((TemplateType.Insert)))
            {
                txtNoOfOpenings = ((InputFormTextBox)listViewItem.FindControl("txtNoOfOpenings_InsertTemplate"));
                ddlStatus = ((DropDownList)listViewItem.FindControl("ddlStatus_InsertTemplate"));
                ddlDepartment = ((DropDownList)listViewItem.FindControl("ddlDepartment_InsertTemplate"));
                ddlUnit = ((DropDownList)listViewItem.FindControl("ddlUnit_InsertTemplate"));
                ddlState = ((DropDownList)listViewItem.FindControl("ddlState_InsertTemplate"));
                ddlLocation = ((DropDownList)listViewItem.FindControl("ddlLocation_InsertTemplate"));
            }
            else
            {
                txtNoOfOpenings = ((InputFormTextBox)listViewItem.FindControl("txtNoOfOpenings_EditTemplate"));
                ddlStatus = ((DropDownList)listViewItem.FindControl("ddlStatus_EditTemplate"));
                ddlDepartment = ((DropDownList)listViewItem.FindControl("ddlDepartment_EditTemplate"));
                ddlUnit = ((DropDownList)listViewItem.FindControl("ddlUnit_EditTemplate"));
                ddlState = ((DropDownList)listViewItem.FindControl("ddlState_EditTemplate"));
                ddlLocation = ((DropDownList)listViewItem.FindControl("ddlLocation_EditTemplate"));
            }

            noOfOpenings = (txtNoOfOpenings.Text);
            status = ((ddlStatus.SelectedItem).Text);

            department = (String.Empty);
            if (((ddlDepartment.Items).Count) == 0)
            {
                department = _NOTSPECIFIED;
            }
            else
            {
                if ((ddlDepartment.SelectedIndex) == 0)
                {
                    department = _NOTSPECIFIED;
                }
                else
                {
                    department = ((ddlDepartment.SelectedItem).Text);
                }
            }

            unit = (String.Empty);
            if (((ddlUnit.Items).Count) == 0)
            {
                unit = _NOTSPECIFIED;
            }
            else
            {
                if ((ddlUnit.SelectedIndex) == 0)
                {
                    unit = _NOTSPECIFIED;
                }
                else
                {
                    unit = ((ddlUnit.SelectedItem).Text); ;
                }
            }

            state = (string.Empty);
            if (((ddlState.Items).Count) == 0)
            {
                state = _NOTSPECIFIED;
            }
            else
            {
                if ((ddlState.SelectedIndex) == 0)
                {
                    state = _NOTSPECIFIED;
                }
                else
                {
                    state = ((ddlState.SelectedItem).Text);
                }
            }

            location = (String.Empty);
            if (((ddlLocation.Items).Count) == 0)
            {
                location = _NOTSPECIFIED;
            }
            else
            {
                if ((ddlLocation.SelectedIndex) == 0)
                {
                    location = _NOTSPECIFIED;
                }
                else
                {
                    location = ((ddlLocation.SelectedItem).Text); ;
                }
            }
        }
        // Routine to add inserted vacancy breakdown to the ListView's data source.
        private void AddVacancyBreakDownTolistViewDataSource()
        {
            if (ViewState[listViewDataSource] != null)
            {
                newVacancyBreakDownTable = ((DataTable)ViewState[listViewDataSource]);

                DataRow dataRow = (newVacancyBreakDownTable.NewRow());
                dataRow[2] = noOfOpenings;
                dataRow[3] = status;
                dataRow[4] = department;
                dataRow[5] = unit;
                dataRow[6] = state;
                dataRow[7] = location;

                newVacancyBreakDownTable.Rows.Add(dataRow);

                hfBreakDownCount.Value = (((newVacancyBreakDownTable.Rows).Count).ToString());

                ViewState[listViewDataSource] = newVacancyBreakDownTable;
            }
        }
        protected void Handle_lv_VacancyBreakDown_OnItemInserted(object sender, ListViewInsertedEventArgs e)
        {
            divJobDescription.Attributes["style"] = "display: none";
            divJobDescriptionLtr.Attributes["style"] = "display: none";
            divConditions.Attributes["style"] = "display: none";
            divConditionsLiteral.Attributes["style"] = "display: none";
            divSkillsRequired.Attributes["style"] = "display: none";
            divSkillsRequiredLiteral.Attributes["style"] = "display: none";
            divPreferences.Attributes["style"] = "display: none";
            divQualifications.Attributes["style"] = "display: none";
            divGeneralInformation.Attributes["style"] = "display: none";
            divBreakDown.Attributes["style"] = "display: block";
        }

        // Routine to handle the event raised when the edit button is clicked.
        protected void Handle_lv_VacancyBreakDown_OnItemEditing(object sender, ListViewEditEventArgs e)
        {
            // getting the index of the item to be edited.
            int itemToBeEditedIndex = (e.NewEditIndex);
            lv_VacancyBreakDown.EditIndex = itemToBeEditedIndex;

            newVacancyBreakDownTable = ((DataTable)ViewState[listViewDataSource]);
            DataRow dataRow = newVacancyBreakDownTable.Rows[itemToBeEditedIndex];
            
            string eDepartment = dataRow.ItemArray[4].ToString();
            string eUnit = dataRow.ItemArray[5].ToString();
            string eState = dataRow.ItemArray[6].ToString();
            string eLocation = dataRow.ItemArray[7].ToString();            

            BindDataToListView();

            BindDepartmentsToControl((TemplateType.Edit));            
            BindStatesOfOperationToControl((TemplateType.Edit));            
            

            DropDownList ddlDepartment = ((DropDownList)((lv_VacancyBreakDown.EditItem).FindControl("ddlDepartment_EditTemplate")));
            if (eDepartment != _NOTSPECIFIED)
            {
                ddlDepartment.SelectedValue = (SetDropDownListSelectedValue(ddlDepartment, eDepartment));
                BindUnitsOfDepartmentToControl((TemplateType.Edit));
            }
            else
            {
                ddlDepartment.SelectedIndex = -1;
            }

            DropDownList ddlUnit_EditTemplate = ((DropDownList)((lv_VacancyBreakDown.EditItem).FindControl("ddlUnit_EditTemplate")));
            if (eUnit != _NOTSPECIFIED)
            {
                ddlUnit_EditTemplate.SelectedValue = (SetDropDownListSelectedValue(ddlUnit_EditTemplate, eUnit));
            }
            else
            {
                ddlUnit_EditTemplate.SelectedIndex = -1;
            }

            DropDownList ddlState_EditTemplate = ((DropDownList)((lv_VacancyBreakDown.EditItem).FindControl("ddlState_EditTemplate")));
            if (eState != _NOTSPECIFIED)
            {
                ddlState_EditTemplate.SelectedValue = (SetDropDownListSelectedValue(ddlState_EditTemplate, eState));
                BindLocationsOfOperationToControl((TemplateType.Edit));
            }
            else
            {
                ddlState_EditTemplate.SelectedIndex = -1;
            }
            DropDownList ddlLocation_EditTemplate = ((DropDownList)((lv_VacancyBreakDown.EditItem).FindControl("ddlLocation_EditTemplate")));
            if (eLocation != _NOTSPECIFIED)
            {
                ddlLocation_EditTemplate.SelectedValue = (SetDropDownListSelectedValue(ddlLocation_EditTemplate, eLocation));
            }
            else
            {
                ddlLocation_EditTemplate.SelectedIndex = -1;
            }

            divJobDescription.Attributes["style"] = "display: none";
            divJobDescriptionLtr.Attributes["style"] = "display: none";
            divConditions.Attributes["style"] = "display: none";
            divConditionsLiteral.Attributes["style"] = "display: none";
            divSkillsRequired.Attributes["style"] = "display: none";
            divSkillsRequiredLiteral.Attributes["style"] = "display: none";
            divPreferences.Attributes["style"] = "display: none";
            divQualifications.Attributes["style"] = "display: none";
            divGeneralInformation.Attributes["style"] = "display: none";
            divBreakDown.Attributes["style"] = "display: block";
        }
        // Routine to set selected value for a DropDownList.
        private string SetDropDownListSelectedValue(DropDownList dropDownList, string selectedValueDisplayText)
        {
            string selectedValue = null;
            foreach (ListItem li in (dropDownList.Items))
            {
                if ((li.Text) == selectedValueDisplayText)
                {
                    selectedValue = (li.Value);
                    break;
                }
            }
            return selectedValue;
        }

        protected void Handle_lv_VacancyBreakDown_OnItemUpdating(object sender, ListViewUpdateEventArgs e)
        {
            ListViewItem listViewItem = (lv_VacancyBreakDown.Items[(e.ItemIndex)]);

            GetVacancyBreakDownEntries(listViewItem, (TemplateType.Edit));

            if (ViewState[listViewDataSource] != null)
            {
                newVacancyBreakDownTable = ((DataTable)ViewState[listViewDataSource]);

                DataRow dataRow = newVacancyBreakDownTable.Rows[(e.ItemIndex)];
                dataRow[2] = noOfOpenings;
                dataRow[3] = status;
                dataRow[4] = department;
                dataRow[5] = unit;
                dataRow[6] = state;
                dataRow[7] = location;

                dataRow.AcceptChanges();
            }

            lv_VacancyBreakDown.EditIndex = -1;

            BindDataToListView();

            BindDepartmentsToControl((TemplateType.Insert));

            BindStatesOfOperationToControl((TemplateType.Insert));

            divJobDescription.Attributes["style"] = "display: none";
            divJobDescriptionLtr.Attributes["style"] = "display: none";
            divConditions.Attributes["style"] = "display: none";
            divConditionsLiteral.Attributes["style"] = "display: none";
            divSkillsRequired.Attributes["style"] = "display: none";
            divSkillsRequiredLiteral.Attributes["style"] = "display: none";
            divPreferences.Attributes["style"] = "display: none";
            divQualifications.Attributes["style"] = "display: none";
            divGeneralInformation.Attributes["style"] = "display: none";
            divBreakDown.Attributes["style"] = "display: block";
        }

        protected void Handle_lv_VacancyBreakDown_OnItemDeleting(object sender, ListViewDeleteEventArgs e)
        {
            int itemIndexToBeDeleted = (e.ItemIndex);

            if (ViewState[listViewDataSource] != null)
            {
                newVacancyBreakDownTable = ((DataTable)ViewState[listViewDataSource]);
                newVacancyBreakDownTable.Rows.RemoveAt((itemIndexToBeDeleted));                
            }

            lv_VacancyBreakDown.Items.RemoveAt((e.ItemIndex));

            BindDataToListView();

            BindDepartmentsToControl((TemplateType.Insert));

            BindStatesOfOperationToControl((TemplateType.Insert));

            if (ViewState[listViewDataSource] != null)
            {
                newVacancyBreakDownTable = ((DataTable)ViewState[listViewDataSource]);

                int breakDownCount = ((newVacancyBreakDownTable.Rows).Count);
                if (breakDownCount > 0)
                {
                    breakDownCount = (breakDownCount - 1);
                    hfBreakDownCount.Value = (breakDownCount.ToString());
                }
            }

            divJobDescription.Attributes["style"] = "display: none";
            divJobDescriptionLtr.Attributes["style"] = "display: none";
            divConditions.Attributes["style"] = "display: none";
            divConditionsLiteral.Attributes["style"] = "display: none";
            divSkillsRequired.Attributes["style"] = "display: none";
            divSkillsRequiredLiteral.Attributes["style"] = "display: none";
            divPreferences.Attributes["style"] = "display: none";
            divQualifications.Attributes["style"] = "display: none";
            divGeneralInformation.Attributes["style"] = "display: none";
            divBreakDown.Attributes["style"] = "display: block";
        }

        protected void Handle_lv_VacancyBreakDown_OnItemCancelling(object sender, ListViewCancelEventArgs e)
        {
            lv_VacancyBreakDown.EditIndex = -1;

            BindDataToListView();

            divJobDescription.Attributes["style"] = "display: none";
            divJobDescriptionLtr.Attributes["style"] = "display: none";
            divConditions.Attributes["style"] = "display: none";
            divConditionsLiteral.Attributes["style"] = "display: none";
            divSkillsRequired.Attributes["style"] = "display: none";
            divSkillsRequiredLiteral.Attributes["style"] = "display: none";
            divPreferences.Attributes["style"] = "display: none";
            divQualifications.Attributes["style"] = "display: none";
            divGeneralInformation.Attributes["style"] = "display: none";
            divBreakDown.Attributes["style"] = "display: block";
        }

        protected void HandlePublishInitiation(object sender, EventArgs e)
        {

            
            Button submitButton = ((Button)sender);
            string buttonDisplayText = (submitButton.Text);

            newVacancy = new List<string[,]>();

            string jobTitle = GetVacancyEntries();

            if (jobTitle == null)
            {
                return;
            }

            if (buttonDisplayText.Equals("publish"))
            {
                string vacancyID = AddVacancy(newVacancy, jobTitle);
                //jobTitle = (jobTitle + " ( " + vacancyID + " )");
                
                AddVacanyBreakDown(vacancyID, jobTitle);
                string mmcollection = GetLisBoxValues(LBMinimumQualification);
                string cnnString = WebConfigurationManager.ConnectionStrings["MTNNGCareersDBConnectionString"].ConnectionString;
                SqlConnection cnn = new SqlConnection(cnnString);
                SqlTransaction transaction;

                cnn.Open();
                transaction = cnn.BeginTransaction();
                try
                {
                   
                    SqlCommand comm = new SqlCommand("insert into [MTNNGCareers].[dbo].[Minimum_Qualification] values(@vacancyid,@qualification)", cnn, transaction);
                    comm.Parameters.Add(new SqlParameter("vacancyid", vacancyID));
                    comm.Parameters.Add(new SqlParameter("qualification", mmcollection));
                    int resp = comm.ExecuteNonQuery();
                    
                    System.Threading.Thread.Sleep(500);
                   



                    transaction.Commit();
                }
               
        catch (Exception ex)
        {
           
            transaction.Rollback();

        }

        finally
        {
            cnn.Close();
            cnn.Dispose();
        }
                string successMsg = ("New Vacancy with the Job Title: " + jobTitle + " has been successfully added");
                SPSecurity.RunWithElevatedPrivileges(delegate()
                            {
                                // implementation details omitted


                                SPWeb myWeb = SPContext.Current.Web;


                                SPList docLib = myWeb.GetList(SPUrlUtility.CombineUrl(myWeb.ServerRelativeUrl, "ApplicantsCV"));
                                string vacancyJobTitle = (jobTitle);

                                string newsubname = vacancyJobTitle;
                                // string newsubname = "Applicants CV " + DateTime.Now.ToLongDateString();
                                try
                                {
                                    var subdoclib = docLib.Items.Add("", SPFileSystemObjectType.Folder, newsubname);

                                    subdoclib.Update();
                                }
                                catch (Exception ex1)
                                {

                                }
                            });

                //not qualified
                SPSecurity.RunWithElevatedPrivileges(delegate()
               {
                    // implementation details omitted


                    SPWeb myWeb = SPContext.Current.Web;


                    SPList docLib = myWeb.GetList(SPUrlUtility.CombineUrl(myWeb.ServerRelativeUrl, "ApplicantsCV"));
                    string vacancyJobTitle = (jobTitle +"_Not Qualified Applicants");

                    string newsubname = vacancyJobTitle.Trim();
                    // string newsubname = "Applicants CV " + DateTime.Now.ToLongDateString();
                    try
                    {
                        var subdoclib = docLib.Items.Add("", SPFileSystemObjectType.Folder, newsubname);

                        subdoclib.Update();
                    }
                    catch (Exception ex1)
                    {

                    }
                });

                           
                SPUtility.TransferToSuccessPage(successMsg);
            }
            else
            {
                ListManipulator updateListItem = new ListManipulator(thisPageWebsite, (VacanciesList.ListName));
                updateListItem.UpdateItemToList((int.Parse(ViewState[itemToBeUpdatedID].ToString())), newVacancy);
                
                string mmcollection = GetLisBoxValues(LBMinimumQualification);
                string cnnString = WebConfigurationManager.ConnectionStrings["MTNNGCareersDBConnectionString"].ConnectionString;
                SqlConnection cnn = new SqlConnection(cnnString);
                SqlTransaction transaction;

                cnn.Open();
                transaction = cnn.BeginTransaction();
                try
                {
                    SqlCommand comm = new SqlCommand("update [MTNNGCareers].[dbo].[Minimum_Qualification] set Qualification=@qualification where VacancyID=@vacancyid", cnn,transaction);
                    comm.Parameters.Add(new SqlParameter("vacancyid", int.Parse(ViewState[itemToBeUpdatedID].ToString()).ToString()));
                   comm.Parameters.Add(new SqlParameter("qualification", mmcollection));
                    int resp = comm.ExecuteNonQuery();
                    System.Threading.Thread.Sleep(500);
                    transaction.Commit();
                }
                
        catch (Exception ex)
        {
            
            transaction.Rollback();

        }

        finally
        {
            cnn.Close();
            cnn.Dispose();
        }

                string successMsg = ("Vacancy with the Job Title: " + jobTitle + " has been successfully updated");
                SPUtility.TransferToSuccessPage(successMsg);
            }

            RetainBreakDownFocus();
        }

        // Routine Uto get entries for a vacancy.
        private string GetVacancyEntries()
        {
            string[,] jobTitle = new string[1, 3];
            jobTitle[0, 0] = (VacanciesList.JobTitleFieldInternalName);
            jobTitle[0, 1] = (CommonUtilities.FieldTypeText);
            jobTitle[0, 2] = (iftJobTitle.Text);
            newVacancy.Add(jobTitle);

            string[,] jobDescription = new string[1, 3];
            jobDescription[0, 0] = (VacanciesList.JobDescriptionFieldInternalName);
            jobDescription[0, 1] = (CommonUtilities.FieldTypeText);
            jobDescription[0, 2] = (ltrJobDescription.Text);
            newVacancy.Add(jobDescription);

            string[,] jobConditions = new string[1, 3];
            jobConditions[0, 0] = (VacanciesList.JobConditionsFieldInternalName);
            jobConditions[0, 1] = (CommonUtilities.FieldTypeText);
            jobConditions[0, 2] = (ltrJobConditions.Text);
            newVacancy.Add(jobConditions);

            #region Requester
            if (((peRequester.ResolvedEntities).Count) > 0)
            {
                string[,] requester = new string[1, 3];
                requester[0, 0] = (VacanciesList.RequesterFieldInternalName);
                requester[0, 1] = (CommonUtilities.FieldTypeUser);
                string requesterAccountName = ((peRequester.Accounts[0]).ToString());
                SPUser _Requester = (thisPageWebsite.EnsureUser(requesterAccountName));
                requester[0, 2] = (((_Requester.ID).ToString()));
                newVacancy.Add(requester);
            }
            else
            {
                CommonUtilities commonUtilities = new CommonUtilities();
                commonUtilities.NotifyUser((this.ClientScript), "Hiring Manager specified does not have an account on the MTN Domain.");

                return null;
            }

            #endregion

            string[,] requesterDesignation = new string[1, 3];
            requesterDesignation[0, 0] = (VacanciesList.RequesterDesignationFieldInternalName);
            requesterDesignation[0, 1] = (CommonUtilities.FieldTypeText);
            requesterDesignation[0, 2] = (iftRequesterPosition.Text);
            newVacancy.Add(requesterDesignation);

            #region Business Partner
            if (((peBusinessPartner.ResolvedEntities).Count) > 0)
            {
                string[,] businessPartner = new string[1, 3];
                businessPartner[0, 0] = (VacanciesList.BusinessPartnerFieldInternalName);
                businessPartner[0, 1] = (CommonUtilities.FieldTypeUser);
                string businessPartnerAccountName = ((peBusinessPartner.Accounts[0]).ToString());
                SPUser _BusinessPartner = (thisPageWebsite.EnsureUser(businessPartnerAccountName));
                businessPartner[0, 2] = (((_BusinessPartner.ID).ToString()));
                newVacancy.Add(businessPartner);
            }
            else
            {
                CommonUtilities commonUtilities = new CommonUtilities();
                commonUtilities.NotifyUser((this.ClientScript), "Business Partner specified does not have an account on the MTN Domain.");

                return null;
            }

            #endregion

            #region H.R Advisor
            if (((peHRAdvisor.ResolvedEntities).Count) > 0)
            {
                string[,] hrAdvisor = new string[1, 3];
                hrAdvisor[0, 0] = (VacanciesList.HRAdvisorFieldInternalName);
                hrAdvisor[0, 1] = (CommonUtilities.FieldTypeUser);
                string hrAdvisorAccountName = ((peHRAdvisor.Accounts[0]).ToString());
                SPUser _HRAdvisor = (thisPageWebsite.EnsureUser(hrAdvisorAccountName));
                hrAdvisor[0, 2] = (((_HRAdvisor.ID).ToString()));
                newVacancy.Add(hrAdvisor);
            }
            else
            {
                CommonUtilities commonUtilities = new CommonUtilities();
                commonUtilities.NotifyUser((this.ClientScript), "Business Partner specified does not have an account on the MTN Domain.");

                return null;
            }

            #endregion
            
            /*if (cbRequesterUnavailable.Checked)
            {
                if (((peDelegate.ResolvedEntities).Count) > 0)
                {
                    string[,] requesterDelegate = new string[1, 3];
                    requesterDelegate[0, 0] = (VacanciesList.RequesterDelegateFieldInternalName);
                    requesterDelegate[0, 1] = (CommonUtilities.FieldTypeUser);
                    string requesterDelegateAccountName = ((peDelegate.Accounts[0]).ToString());
                    SPUser _Delegate = (thisPageWebsite.EnsureUser(requesterDelegateAccountName));
                    requesterDelegate[0, 2] = (((_Delegate.ID).ToString()));
                    newVacancy.Add(requesterDelegate);
                }
            }*/

            string[,] publisher = new string[1, 3];
            publisher[0, 0] = (VacanciesList.PublisherFieldInternalName);
            publisher[0, 1] = (CommonUtilities.FieldTypeUser);
            publisher[0, 2] = ((thisPageCurrentUser.ID).ToString());
            newVacancy.Add(publisher);

            string[,] division = new string[1, 3];
            division[0, 0] = (VacanciesList.DivisionFieldInternalName);
            division[0, 1] = (CommonUtilities.FieldTypeLookUp);
            division[0, 2] = (ddlDivision.SelectedValue);
            newVacancy.Add(division);

            string[,] staffLevel = new string[1, 3];
            staffLevel[0, 0] = (VacanciesList.StaffLevelFieldInternalName);
            staffLevel[0, 1] = (CommonUtilities.FieldTypeText);
            staffLevel[0, 2] = (rblStaffLevel.SelectedValue);
            newVacancy.Add(staffLevel);

            string[,] isNYSCRequired = new string[1, 3];
            isNYSCRequired[0, 0] = (VacanciesList.IsNYSCRequiredFieldInternalName);
            isNYSCRequired[0, 1] = (CommonUtilities.FieldTypeYesOrNo);
            isNYSCRequired[0, 2] = (rblIsNYSCRequired.SelectedValue);
            newVacancy.Add(isNYSCRequired);

            /*string[,] ageRange = new string[1, 3];
            ageRange[0, 0] = (VacanciesList.AgeRangeFieldInternalName);
            ageRange[0, 1] = (CommonUtilities.FieldTypeText);
            ageRange[0, 2] = ((iftFromAgeRange.Text) + " - " + (iftToAgeRange.Text));
            newVacancy.Add(ageRange);*/

            string[,] courseOfStudy = new string[1, 3];
            courseOfStudy[0, 0] = (VacanciesList.CourseOfStudyFieldDisplayName);
            courseOfStudy[0, 1] = (CommonUtilities.FieldTypeText);
            courseOfStudy[0, 2] = (iftCourseOfStudy.Text);
            newVacancy.Add(courseOfStudy);
             //int cintt = LBMinimumQualification.Items.Count;
            if ((ddlMinimumQualification.SelectedIndex) > 0)
            //if (LBMinimumQualification.SelectedIndex != -1)
            {
                string[,] minimumQualification = new string[1, 3];
                minimumQualification[0, 0] = (VacanciesList.MinimumQualificationFieldInternalName);
                minimumQualification[0, 1] = (CommonUtilities.FieldTypeLookUp);
                minimumQualification[0, 2] = (ddlMinimumQualification.SelectedValue);
                newVacancy.Add(minimumQualification);
            }

            string[,] additionalQualification = new string[1, 3];
            additionalQualification[0, 0] = (VacanciesList.AdditionalQualificationFieldInternalName);
            additionalQualification[0, 1] = (CommonUtilities.FieldTypeText);
            string qualifications = (iftAdditionalQualifcations.Text);
            IsolateQualifications(ref qualifications);
            additionalQualification[0, 2] = qualifications;
            newVacancy.Add(additionalQualification);

            string[,] professionalQualification = new string[1, 3];
            professionalQualification[0, 0] = (VacanciesList.ProfessionalQualificationFieldInternalName);
            professionalQualification[0, 1] = (CommonUtilities.FieldTypeText);
            qualifications = (iftProfessionalQualifcations.Text);
            IsolateQualifications(ref qualifications);
            professionalQualification[0, 2] = qualifications;
            newVacancy.Add(professionalQualification);

            if ((iftPostDegreeYearsOfExperience.Text) != "")
            {
                string[,] postDegreeYearsOfExperience = new string[1, 3];
                postDegreeYearsOfExperience[0, 0] = (VacanciesList.PostDegreeYearsOfExperienceFieldInternalName);
                postDegreeYearsOfExperience[0, 1] = (CommonUtilities.FieldTypeNumber);
                postDegreeYearsOfExperience[0, 2] = (iftPostDegreeYearsOfExperience.Text);
                newVacancy.Add(postDegreeYearsOfExperience);
            }

            if ((ddlClassOfDegree.SelectedIndex) > 0)
            {
                string[,] classOfDegree = new string[1, 3];
                classOfDegree[0, 0] = (VacanciesList.ClassOfDegreeFieldInternalName);
                classOfDegree[0, 1] = (CommonUtilities.FieldTypeLookUp);
                classOfDegree[0, 2] = (ddlClassOfDegree.SelectedValue);
                newVacancy.Add(classOfDegree);
            }

            string[,] skillsRequired = new string[1, 3];
            skillsRequired[0, 0] = (VacanciesList.SkillsRequiredFieldInternalName);
            skillsRequired[0, 1] = (CommonUtilities.FieldTypeText);
            skillsRequired[0, 2] = (ltrSkillsRequired.Text);
            newVacancy.Add(skillsRequired);

            if ((rblMaritalStatus.SelectedIndex) > -1)
            {
                string[,] maritalStatus = new string[1, 3];
                maritalStatus[0, 0] = (VacanciesList.MaritalStatusFieldInternalName);
                maritalStatus[0, 1] = (CommonUtilities.FieldTypeText);
                maritalStatus[0, 2] = (rblMaritalStatus.SelectedValue);
                newVacancy.Add(maritalStatus);
            }

            if ((rblGender.SelectedIndex) > -1)
            {
                string[,] gender = new string[1, 3];
                gender[0, 0] = (VacanciesList.GenderFieldInternalName);
                gender[0, 1] = (CommonUtilities.FieldTypeText);
                gender[0, 2] = (rblGender.SelectedValue);
                newVacancy.Add(gender);
            }

            string[,] startAdvertisementDate = new string[1, 3];
            startAdvertisementDate[0, 0] = (VacanciesList.StartAdvertisementDateFieldInternalName);
            startAdvertisementDate[0, 1] = (CommonUtilities.FieldTypeText);
            startAdvertisementDate[0, 2] = (iftStartAdvertisementDate.Text);
            newVacancy.Add(startAdvertisementDate);

            string[,] endAdvertisementDate = new string[1, 3];
            endAdvertisementDate[0, 0] = (VacanciesList.EndAdvertisementDateFieldInternalName);
            endAdvertisementDate[0, 1] = (CommonUtilities.FieldTypeText);
            endAdvertisementDate[0, 2] = (iftEndAdvertisementDate.Text);
            newVacancy.Add(endAdvertisementDate);

            return jobTitle[0, 2];
        }

        // Routine to put a delimeter at the end of each additional and professional qualifications.
        private void IsolateQualifications(ref string qualification)
        {
            qualification = (qualification.Replace((Environment.NewLine), "; "));
        }

        // Routine to add a new Vacancy.
        private string AddVacancy(List<string[,]> vacancy, string jobTitle)
        {
            ListManipulator listManipulator = new ListManipulator(thisPageWebsite, (VacanciesList.ListName));
            string vacancyID = (listManipulator.AddItemToList(vacancy, jobTitle, false));

            return vacancyID;
        }

        // Routine to add vacancy break down as an item to the Vacancy Break Down List
        private void AddVacanyBreakDown(string vacancyID, string jobTitle)
        {
            if (ViewState[listViewDataSource] != null)
            {
                List<string[,]> newVacancyBreakDown;

                newVacancyBreakDownTable = ((DataTable)ViewState[listViewDataSource]);

                int rowCount = ((newVacancyBreakDownTable.Rows).Count);

                int rowCounter = 0;

                foreach (DataRow dataRow in (newVacancyBreakDownTable.Rows))
                {
                    newVacancyBreakDown = (new List<string[,]>());

                    rowCounter += 1;

                    dataRow[1] = vacancyID;

                    string[,] vacancy = new string[1, 3];
                    vacancy[0, 0] = (VacancyBreakDownList.VacancyFieldInternalName);
                    vacancy[0, 1] = (CommonUtilities.FieldTypeLookUp);
                    vacancy[0, 2] = vacancyID;
                    newVacancyBreakDown.Add(vacancy);

                    string[,] itemTitle = new string[1, 3];
                    itemTitle[0, 0] = (VacancyBreakDownList.TitleFieldInternalName);
                    itemTitle[0, 1] = (CommonUtilities.FieldTypeText);
                    itemTitle[0, 2] = (jobTitle + " - " + (rowCounter.ToString()));
                    newVacancyBreakDown.Add(itemTitle);

                    string[,] noOfOpenings = new string[1, 3];
                    noOfOpenings[0, 0] = (VacancyBreakDownList.QuantityFieldInternalName);
                    noOfOpenings[0, 1] = (CommonUtilities.FieldTypeNumber);
                    noOfOpenings[0, 2] = (dataRow[2].ToString());
                    newVacancyBreakDown.Add(noOfOpenings);

                    string[,] status = new string[1, 3];
                    status[0, 0] = (VacancyBreakDownList.EmployementStatusFieldInternalName);
                    status[0, 1] = (CommonUtilities.FieldTypeText);
                    status[0, 2] = (dataRow[3].ToString());
                    newVacancyBreakDown.Add(status);

                    if ((dataRow[4].ToString()) != _NOTSPECIFIED)
                    {
                        string[,] department = new string[1, 3];
                        department[0, 0] = (VacancyBreakDownList.DepartmentLookUpFieldInternalName);
                        department[0, 1] = (CommonUtilities.FieldTypeLookUp);
                        department[0, 2] = GetItemID("Department", (dataRow[4].ToString()));
                        newVacancyBreakDown.Add(department);
                    }

                    if ((dataRow[5].ToString()) != _NOTSPECIFIED)
                    {
                        string[,] unit = new string[1, 3];
                        unit[0, 0] = (VacancyBreakDownList.UnitLookUpFieldInternalName);
                        unit[0, 1] = (CommonUtilities.FieldTypeLookUp);
                        unit[0, 2] = GetItemID("Unit", (dataRow[5].ToString()));
                        newVacancyBreakDown.Add(unit);
                    }

                    if ((dataRow[6].ToString()) != _NOTSPECIFIED)
                    {
                        string[,] state = new string[1, 3];
                        state[0, 0] = (VacancyBreakDownList.StateFieldInternalName);
                        state[0, 1] = (CommonUtilities.FieldTypeLookUp);
                        state[0, 2] = GetItemID("State", (dataRow[6].ToString()));
                        newVacancyBreakDown.Add(state);
                    }

                    if ((dataRow[7].ToString()) != _NOTSPECIFIED)
                    {
                        string[,] location = new string[1, 3];
                        location[0, 0] = (VacancyBreakDownList.LocationFieldInternalName);
                        location[0, 1] = (CommonUtilities.FieldTypeLookUp);
                        location[0, 2] = GetItemID("Location", (dataRow[7].ToString()));
                        newVacancyBreakDown.Add(location);
                    }

                    string[,] openingStatus = new string[1, 3];
                    openingStatus[0, 0] = (VacancyBreakDownList.StatusFieldInternalName);
                    openingStatus[0, 1] = (CommonUtilities.FieldTypeText);
                    openingStatus[0, 2] = (CommonMembers.PendingApplicantShortListing);
                    newVacancyBreakDown.Add(openingStatus);

                    ListManipulator listManipulator = new ListManipulator(thisPageWebsite, (VacancyBreakDownList.ListName));
                    string breakDownID = listManipulator.AddItemToList(newVacancyBreakDown);               
                }
            }
        }
        
        // Routine to get the Item ID of an item selected.
        private string GetItemID(string item, string itemName)
        {
            string requiredID = null;

            if (item.Equals("Vacancy"))
            {
                string[,] criterionField = new string[1, 3];
                criterionField[0, 0] = (VacanciesList.JobTitleFieldInternalName);
                criterionField[0, 1] = (CommonUtilities.FieldTypeText);
                criterionField[0, 2] = itemName;

                ListAccessor listAccessor = new ListAccessor(thisPageWebsite, (VacanciesList.ListName), (new string[] { _IDFieldName }), criterionField);
                foreach (SPListItem listItem in (listAccessor.RequiredListItems))
                {
                    requiredID = (listItem[_IDFieldName].ToString());
                }
            }
            else if (item.Equals("Department"))
            {
                string[,] criterionField = new string[1, 3];
                criterionField[0, 0] = (DepartmentList.DepartmentFieldInternalName);
                criterionField[0, 1] = (CommonUtilities.FieldTypeText);
                criterionField[0, 2] = itemName;

                ListAccessor listAccessor = new ListAccessor((thisPageWebsite.ParentWeb), (DepartmentList.ListName), (new string[] { _IDFieldName }), criterionField);
                foreach (SPListItem listItem in (listAccessor.RequiredListItems))
                {
                    requiredID = (listItem[_IDFieldName].ToString());
                }
            }
            else if (item.Equals("Unit"))
            {
                string[,] criterionField = new string[1, 3];
                criterionField[0, 0] = (UnitsList.TitleFieldInternalName);
                criterionField[0, 1] = (CommonUtilities.FieldTypeText);
                criterionField[0, 2] = itemName;

                ListAccessor listAccessor = new ListAccessor((thisPageWebsite.ParentWeb), (UnitsList.ListName), (new string[] { _IDFieldName }), criterionField);
                foreach (SPListItem listItem in (listAccessor.RequiredListItems))
                {
                    requiredID = (listItem[_IDFieldName].ToString());
                }
            }
            else if (item.Equals("State"))
            {
                string[,] criterionField = new string[1, 3];
                criterionField[0, 0] = (StatesOfOperationWithinNigeriaList.TitleFieldInternalName);
                criterionField[0, 1] = (CommonUtilities.FieldTypeText);
                criterionField[0, 2] = itemName;

                ListAccessor listAccessor = new ListAccessor(thisPageWebsite, (StatesOfOperationWithinNigeriaList.ListName), (new string[] { _IDFieldName }), criterionField);
                foreach (SPListItem listItem in (listAccessor.RequiredListItems))
                {
                    requiredID = (listItem[_IDFieldName].ToString());
                }
            }
            else if (item.Equals("Location"))
            {
                string[,] criterionField = new string[1, 3];
                criterionField[0, 0] = (LocationsOfOperationWithinNigeriaList.TitleFieldInternalName);
                criterionField[0, 1] = (CommonUtilities.FieldTypeText);
                criterionField[0, 2] = itemName;

                ListAccessor listAccessor = new ListAccessor(thisPageWebsite, (LocationsOfOperationWithinNigeriaList.ListName), (new string[] { _IDFieldName }), criterionField);
                foreach (SPListItem listItem in (listAccessor.RequiredListItems))
                {
                    requiredID = (listItem[_IDFieldName].ToString());
                }
            }

            return requiredID;
        }

        protected void AddMMQualification_Click(object sender, EventArgs e)
        {
            if (ddlMinimumQualification.SelectedIndex > 0)
            {
                LBMinimumQualification.Items.Add(ddlMinimumQualification.SelectedItem);
            }
        }

        protected void RemoveMMMQualification_Click(object sender, EventArgs e)
        {
            if (LBMinimumQualification.SelectedIndex != -1)
            {
                LBMinimumQualification.Items.Remove(LBMinimumQualification.SelectedItem);
            }
        }

        protected string GetLisBoxValues(ListBox lb)
        {
            int cint = lb.Items.Count;
            int i = 0;
            string itemvalue = null;
            foreach (ListItem item in lb.Items)
            {
                itemvalue += item.Text;
                i++;
                if (i < cint)
                {
                    itemvalue += ",";

                }

            }

            return itemvalue;

        }
        
    }
}

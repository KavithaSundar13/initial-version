﻿using System;

using System.Web.UI;
using System.Web.UI.WebControls;

using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;

using MTNNGCareers.Administration.Common;

namespace MTNNGCareers.Administration.Forms
{
    public partial class DisplayVacancyFrm : LayoutsPageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string sVacancyID = (Request.Params["ID"]);

                HiddenField hfVacancyID = ((HiddenField) VacancyViewer1.FindControl("hfVacancyID"));
                hfVacancyID.Value = sVacancyID;
            }
        }

        
    }
}



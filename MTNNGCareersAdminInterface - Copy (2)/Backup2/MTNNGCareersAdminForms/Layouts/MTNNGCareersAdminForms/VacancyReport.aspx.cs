﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;


using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
using Microsoft.SharePoint.Utilities;

using Microsoft.Reporting.WebForms;

using SharePointListManager;

using MTNNGCareers.Administration.Common;

using CommonUtilities =  Utilities.Common;

using System.Collections.Specialized;

namespace MTNNGCareers.Administration.Forms
{
    public partial class VacancyReport : LayoutsPageBase
    {

        #region Report (.rdlc) Parameters.

        ReportParameter _RequesterRptParam;
        ReportParameter _PublisherRptParam;
        ReportParameter _DivisionRptParam;
        ReportParameter _DepartmentRptParam;
        ReportParameter _StaffLevelRptParam;
        ReportParameter _GenderRptParam;
        ReportParameter _StateOfOperationIDRptParam;
        ReportParameter _LocationOfOperationIDRptParam;
        ReportParameter _StartAdvertisementDateRptParam;
        ReportParameter _EndAdvertisementDateRptParam;

        #endregion
        
        protected void Page_Load(object sender, EventArgs e)
        {
            SPUserAuthenticator authenticator = new SPUserAuthenticator();
            bool isValidUser = (authenticator.ValidateUser());

            if (!isValidUser)
            {
                SPUtility.Redirect(((this.Web).Url), SPRedirectFlags.Default, Context);
            }

            if (!IsPostBack)
            {
                DisplayRequesters();

                DisplayPublishers();

                DisplayDivisions();

                DisplayStaffLevels();

                DisplayPreferredGenders();

                DisplayStateOfOperation();

                InitializeReportParameters();

                string vacancyReportPath = Server.MapPath("~/_layouts/MTNNGCareersAdminForms/Reports/VacancyReport.rdlc");
                rptVwrVacancy.LocalReport.ReportPath = vacancyReportPath;
                rptVwrVacancy.LocalReport.Refresh();
            }
        }

        protected void ObjectDataSource1_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            SetParameterValues(e);
        }
        
        // Routine to set values for both the Object Data Source Select parameters and the Report parametrs.
        private void SetParameterValues(ObjectDataSourceSelectingEventArgs e)
        {
            if (((cbRequester.Checked)) && ((ddlRequester.SelectedIndex) != 0))
            {
                string requesterMembershipID = (ddlRequester.SelectedValue);
                if (e != null)
                {
                    e.InputParameters["Requester"] = requesterMembershipID;
                }
                _RequesterRptParam = new ReportParameter("Requester", requesterMembershipID);
            }

            if (((cbPublisher.Checked)) && ((ddlPublisher.SelectedIndex) != 0))
            {
                string publisherMembershipID = (ddlPublisher.SelectedValue);
                if (e != null)
                {
                    e.InputParameters["Publisher"] = publisherMembershipID;
                }
                _PublisherRptParam = new ReportParameter("Publisher", publisherMembershipID);
            }

            if (((cbDivision.Checked)) && ((ddlDivision.SelectedIndex) != 0))
            {
                string divisionID = (ddlDivision.SelectedValue);
                if (e != null)
                {
                    e.InputParameters["Division"] = divisionID;
                }
                _DivisionRptParam = new ReportParameter("Division", divisionID);
            }

            if (((cbDepartment.Checked)) && ((ddlDepartment.SelectedIndex) != 0))
            {
                string departmentID = (ddlDepartment.SelectedValue);
                if (e != null)
                {
                    e.InputParameters["Department"] = departmentID;
                }
                _DepartmentRptParam = new ReportParameter("Department", departmentID);
            }

            if (((cbStaffLevel.Checked)) && ((ddlStaffLevel.SelectedIndex) != 0))
            {
                string staffLevel = (ddlStaffLevel.SelectedValue);
                if (e != null)
                {
                    e.InputParameters["StaffLevel"] = staffLevel;
                }
                _StaffLevelRptParam = new ReportParameter("StaffLevel", staffLevel);
            }

            if (((cbGender.Checked)) && ((ddlGender.SelectedIndex) != 0))
            {
                string gender = (ddlGender.SelectedValue);
                if (e != null)
                {
                    e.InputParameters["Gender"] = gender;
                }
                _GenderRptParam = new ReportParameter("Gender", gender);
            }

            if (((cbStateOfOperation.Checked)) && ((ddlStateOfOperation.SelectedIndex) != 0))
            {
                string stateOfOperationID =  (ddlStateOfOperation.SelectedValue);
                if (e != null)
                {
                    e.InputParameters["StateOfOperationID"] = stateOfOperationID;
                }
                _StateOfOperationIDRptParam = new ReportParameter("StateOfOperationID", stateOfOperationID);
            }

            if (((cbLocationOfOperation.Checked)) && ((ddlLocationOfOperation.SelectedIndex) != 0))
            {
                string locationOfOperationID = (ddlLocationOfOperation.SelectedValue);
                if (e != null)
                {
                    e.InputParameters["LocationOfOperationID"] = locationOfOperationID;
                }
                _LocationOfOperationIDRptParam = new ReportParameter("LocationOfOperationID", locationOfOperationID);
            }

            if (cbAdvertisementDuration.Checked)
            {
                if (e != null)
                {
                    e.InputParameters["StartAdvertisementDate"] = (iftStartAdvertisementDate.Text);
                    e.InputParameters["EndAdvertisementDate"] = (iftEndAdvertisementDate.Text);
                }
            }
        }

        // Routine to add List Items to a DropDownList.
        private void AddDropDownListItems(string text, string value, DropDownList dropDownList)
        {
            ListItem listItem = new ListItem(text, value);

            dropDownList.Items.Add(listItem);
        }

        // Routine to retrieve all requesters from the Vacancies List,
        // that is all persons who have requested the HR Recruitment Team to publish a vacancy
        // and display each as an item option in the drop down list used to present it.
        private void  DisplayRequesters()
        {
            ListAccessor accessor = new ListAccessor((this.Web), (VacanciesList.ListName), (new string[] { (VacanciesList.RequesterFieldInternalName) }), (VacanciesList.RequesterFieldInternalName), true);
            SPListItemCollection requestersItemCollection = (accessor.RequiredListItems);

            string previousRequesterMembershipID = null;
            string previousRequesterName = null;

            foreach (SPListItem item in requestersItemCollection)
            {
                CommonUtilities utilities = new CommonUtilities();
                SPUser requesterUser = ((utilities.GetSPUserFromSPFieldUser(item, (VacanciesList.RequesterFieldDisplayName))));

                if ((!(String.IsNullOrEmpty(previousRequesterMembershipID))) && (previousRequesterMembershipID != ((requesterUser.ID).ToString())))
                {
                    if (requesterUser != null)
                    {
                        AddDropDownListItems((requesterUser.Name), ((requesterUser.ID).ToString()), ddlRequester);
                    }
                }
                else if ((String.IsNullOrEmpty(previousRequesterMembershipID)))
                {
                    if (requesterUser != null)
                    {
                        AddDropDownListItems((requesterUser.Name), ((requesterUser.ID).ToString()), ddlRequester);
                    }
                }

                if (requesterUser != null)
                {
                    previousRequesterMembershipID = ((requesterUser.ID).ToString());
                    previousRequesterName = (requesterUser.Name);
                }
            }

            ddlRequester.Items.Insert(0, (new ListItem("---select---", "DBNull.Value")));
        }

        // Routine to retrieve and display all publishers of a vacancy.
        private void DisplayPublishers()
        {
            ListAccessor accessor = new ListAccessor((this.Web), (VacanciesList.ListName), (new string[] { (VacanciesList.PublisherFieldInternalName) }), (VacanciesList.RequesterFieldInternalName), true);
            SPListItemCollection requestersItemCollection = (accessor.RequiredListItems);

            string previousPublisherMembershipID = null;
            string previousPublisherName = null;

            foreach (SPListItem item in requestersItemCollection)
            {
                CommonUtilities utilities = new CommonUtilities();
                SPUser publisherUser = ((utilities.GetSPUserFromSPFieldUser(item, (VacanciesList.PublisherFieldDisplayName))));

                if ((!(String.IsNullOrEmpty(previousPublisherMembershipID))) && (previousPublisherMembershipID != ((publisherUser.ID).ToString())))
                {
                    if (publisherUser != null)
                    {
                        AddDropDownListItems((publisherUser.Name), ((publisherUser.ID).ToString()), ddlPublisher);
                    }
                }
                else if((String.IsNullOrEmpty(previousPublisherMembershipID)))
                {
                    if (publisherUser != null)
                    {
                        AddDropDownListItems((publisherUser.Name), ((publisherUser.ID).ToString()), ddlPublisher);
                    }
                }
                if (publisherUser != null)
                {
                    previousPublisherMembershipID = ((publisherUser.ID).ToString());
                    previousPublisherName = (publisherUser.Name);
                }
            }

            ddlPublisher.Items.Insert(0, (new ListItem("---select---", "DBNull.Value")));
        }

        // Routine to retrieve and display all divisions for which a vacancy has been published.
        private void DisplayDivisions()
        {
            ListAccessor accessor = new ListAccessor((this.Web.ParentWeb), (DivisionList.ListName), (new string[] { "ID", (DivisionList.DivisionFieldInternalName) }), (DivisionList.DivisionFieldInternalName), true);
            SPListItemCollection divisionItemCollection = (accessor.RequiredListItems);

            string previosDivisionID = null;
            string previousDivision = null;

            foreach (SPListItem item in divisionItemCollection)
            {
                if ((!(String.IsNullOrEmpty(previosDivisionID))) && (previosDivisionID != ((item.ID).ToString())))
                {
                    AddDropDownListItems((item[(DivisionList.DivisionFieldDisplayName)].ToString()), ((item.ID).ToString()), ddlDivision);
                }
                else if ((String.IsNullOrEmpty(previosDivisionID)))
                {
                    AddDropDownListItems((item[(DivisionList.DivisionFieldDisplayName)].ToString()), ((item.ID).ToString()), ddlDivision);
                }

                previosDivisionID = ((item.ID).ToString());
                previousDivision = (item[(DivisionList.DivisionFieldDisplayName)].ToString());                
            }

            ddlDivision.Items.Insert(0, (new ListItem("---select---", "DBNull.Value")));
        }

        protected void ddlDivision_SelectedIndexChanged(object sender, EventArgs e)
        {
            string selectedDivision = ((ddlDivision.SelectedItem).Text);

            ddlDepartment.Items.Clear();

            DisplayDivisionDepartments(selectedDivision);
        }

        // Routine to display departments within a selected division.
        private void DisplayDivisionDepartments(string selectedDivision)
        {
            string[,] criterionField = new string[1, 3];
            criterionField[0, 0] = (DepartmentList.DivisionFieldInternalName);
            criterionField[0, 1] = (Utilities.Common.FieldTypeLookUp);
            criterionField[0, 2] = selectedDivision;

            ListAccessor accessor = new ListAccessor(((this.Web).ParentWeb), (DepartmentList.ListName), (new string[] { ("ID"), (DepartmentList.DepartmentFieldInternalName) }), criterionField);
            SPListItemCollection divisionDepartmentItems = (accessor.RequiredListItems);

            string previosDepartmentID = null;
            string previousDepartment = null;

            foreach (SPListItem item in divisionDepartmentItems)
            {
                if ((!(String.IsNullOrEmpty(previosDepartmentID))) && (previosDepartmentID != ((item.ID).ToString())))
                {
                    AddDropDownListItems((item[(DepartmentList.DepartmentFieldDisplayName)].ToString()), ((item.ID).ToString()), ddlDepartment);
                }
                else if ((String.IsNullOrEmpty(previosDepartmentID)))
                {
                    AddDropDownListItems((item[(DepartmentList.DepartmentFieldDisplayName)].ToString()), ((item.ID).ToString()), ddlDepartment);
                }

                previosDepartmentID = ((item.ID).ToString());
                previousDepartment = (item[(DepartmentList.DepartmentFieldDisplayName)].ToString());
            }

            ddlDepartment.Items.Insert(0, (new ListItem("---select---", "DBNull.Value")));
        }

        // Routine to retrieve and display all staff levels for vacancies posted.
        private void DisplayStaffLevels()
        {
            ListAccessor accessor = new ListAccessor((this.Web), (VacanciesList.ListName), (new string[] { "ID", (VacanciesList.StaffLevelFieldInternalName) }), (VacanciesList.StaffLevelFieldInternalName), true);
            SPListItemCollection staffLevelsItemCollection = (accessor.RequiredListItems);

            string previousStaffLevel = null;
            string staffLevel = null;
            foreach (SPListItem item in staffLevelsItemCollection)
            {
                staffLevel = (item[(VacanciesList.StaffLevelFieldDisplayName)].ToString());

                if ((!(String.IsNullOrEmpty(previousStaffLevel))) && (previousStaffLevel != staffLevel))
                {
                    AddDropDownListItems(staffLevel, staffLevel, ddlStaffLevel);
                }
                else if ((String.IsNullOrEmpty(previousStaffLevel)))
                {
                    AddDropDownListItems(staffLevel, staffLevel, ddlStaffLevel);
                }

                previousStaffLevel = staffLevel;
            }

            ddlStaffLevel.Items.Insert(0, (new ListItem("---select---", "DBNull.Value")));
        }

        // Routine to retrieve and display all preferred genders
        private void DisplayPreferredGenders()
        {
            ListAccessor accessor = new ListAccessor((this.Web), (VacanciesList.ListName), (new string[] { "ID", (VacanciesList.GenderFieldInternalName) }), (VacanciesList.GenderFieldInternalName), true);
            SPListItemCollection genderItemCollection = (accessor.RequiredListItems);

            string previousGender = null;
            string gender = null;
            foreach (SPListItem item in genderItemCollection)
            {
                object oGender = (item[(VacanciesList.GenderFieldDisplayName)]);
                if(oGender == null)
                {
                    continue;
                }
                else
                {
                    gender = (oGender.ToString());

                    if ((!(String.IsNullOrEmpty(previousGender))) && (previousGender != gender))
                    {
                        AddDropDownListItems(gender, gender, ddlGender);
                    }
                    else if ((String.IsNullOrEmpty(previousGender)))
                    {
                        AddDropDownListItems(gender, gender, ddlGender);
                    }

                    previousGender = gender;
                }                
            }

            ddlGender.Items.Insert(0, (new ListItem("---select---", "DBNull.Value")));
        }

        // Routine to retrieve and display all states of operation for which a vacancy has been published.
        private void DisplayStateOfOperation()
        {
            ListAccessor accessor = new ListAccessor((this.Web), (StatesOfOperationWithinNigeriaList.ListName), (new string[] { "ID", (StatesOfOperationWithinNigeriaList.TitleFieldInternalName) }), (StatesOfOperationWithinNigeriaList.TitleFieldInternalName), true);
            SPListItemCollection stateOfOperationItemCollection = (accessor.RequiredListItems);

            foreach (SPListItem item in stateOfOperationItemCollection)
            {
                AddDropDownListItems((item[(StatesOfOperationWithinNigeriaList.TitleFieldDisplayName)].ToString()), ((item.ID).ToString()), ddlStateOfOperation);
            }

            ddlStateOfOperation.Items.Insert(0, (new ListItem("---select---", "DBNull.Value")));
        }

        protected void ddlStateOfOperation_SelectedIndexChanged(object sender, EventArgs e)
        {
            string selectedStateOfoperation = ((ddlStateOfOperation.SelectedItem).Text);

            ddlLocationOfOperation.Items.Clear();

            DisplayLocationOfOperation(selectedStateOfoperation);
        }

        // Routine to retrieve and display all locations of operation for which a vacancy has been published.
        private void DisplayLocationOfOperation(string selectedStateOfoperation)
        {
            ListAccessor accessor = new ListAccessor((this.Web), (LocationsOfOperationWithinNigeriaList.ListName), (new string[] { "ID", (LocationsOfOperationWithinNigeriaList.TitleFieldInternalName) }), (LocationsOfOperationWithinNigeriaList.TitleFieldInternalName), true);
            SPListItemCollection locationOfOperationItemCollection = (accessor.RequiredListItems);

            foreach (SPListItem item in locationOfOperationItemCollection)
            {
                AddDropDownListItems((item[(LocationsOfOperationWithinNigeriaList.TitleFieldDisplayName)].ToString()), ((item.ID).ToString()), ddlLocationOfOperation);
            }

            ddlLocationOfOperation.Items.Insert(0, (new ListItem("---select---", "DBNull.Value")));
        }

        protected void btnGenerateReport_Click(object sender, EventArgs e)
        {   
            InitializeReportParameters();

            SetParameterValues(null);

            this.rptVwrVacancy.LocalReport.SetParameters(new ReportParameterCollection(){ _RequesterRptParam, _PublisherRptParam, _DivisionRptParam, _DepartmentRptParam, _StaffLevelRptParam, _GenderRptParam, _StateOfOperationIDRptParam, _LocationOfOperationIDRptParam, _StartAdvertisementDateRptParam, _EndAdvertisementDateRptParam });

            this.rptVwrVacancy.LocalReport.Refresh();
        }        

        // Routine to initialize Report Parameters.
        private void InitializeReportParameters()
        {
            _RequesterRptParam = new ReportParameter("Requester");
            _PublisherRptParam = new ReportParameter("Publisher");
            _DivisionRptParam = new ReportParameter("Division");
            _DepartmentRptParam = new ReportParameter("Department");
            _StaffLevelRptParam = new ReportParameter("StaffLevel");
            _GenderRptParam = new ReportParameter("Gender");
            _StateOfOperationIDRptParam = new ReportParameter("StateOfOperationID");
            _LocationOfOperationIDRptParam = new ReportParameter("LocationOfOperationID");
            _StartAdvertisementDateRptParam = new ReportParameter("StartAdvertisementDate");
            _EndAdvertisementDateRptParam = new ReportParameter("EndAdvertisementDate");
        }
    }
}

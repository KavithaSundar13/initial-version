﻿var lnkBtnBreakDown;

var divGeneralInformation;
var divJobDescription;
var divJobDescriptionLtr;
var divConditions;
var divConditionsLiteral;
var divSkillsRequired;
var divSkillsRequiredLiteral;
var divPreferences;
var divQualifications;
var divBreakDown;

var td_JobDescription;
var td_JobConditions;
var td_SkillsRequired;

var iftJobDescription;
var iftJobConditions;
var iftJobSkillsRequired;

// to hold the selected advertisement start date as a string and Date object.
var sSelectedAdvertisementStartDate;
var dSelectedAdvertisementStartDate;

// holds a boolean value which indicates whether or 
// not whether an entry for a vacancy breakdown is valid.
var isVacancyBreakdownValid = true;

// to hold the count of the number of entries made for a vacancies breakdown.
var breakDownEntryCount;

// to hold a reference to a required HTML Element.
var requiredElement;

// to hold the name of the entry currently being evaluated;
var currentEntryName;

function HandleContentToPresent(event)
{
    var eventSource = GetEventSource(event);
    var eventSource_innerText = (eventSource.innerText);

    ObtainRequiredDivsReferences();

    ObtainRequiredCells();

    DetermineContentToPresent(eventSource_innerText);

    PreventPostBack(event);
}

function ObtainRequiredDivsReferences()
{
    divGeneralInformation = (document.getElementById(divGeneralInformationClientID));
    divJobDescription = (document.getElementById(divJobDescriptionClientID));
    divJobDescriptionLtr = (document.getElementById(divJobDescriptionLtrClientID));
    divConditions = (document.getElementById(divConditionsClientID));
    divConditionsLiteral = (document.getElementById(divConditionsLiteralClientID));
    divSkillsRequired = (document.getElementById(divSkillsRequiredClientID));
    divSkillsRequiredLiteral = (document.getElementById(divSkillsRequiredLiteralClientID));
    divPreferences = (document.getElementById(divPreferencesClientID));
    divQualifications = (document.getElementById(divQualificationsClientID));
    divBreakDown = (document.getElementById(divBreakDownClientID));
}

function ObtainRequiredCells()
{
    td_JobDescription = (document.getElementById('td_JobDescription'));
    td_JobConditions = (document.getElementById('td_JobConditions'));
    td_SkillsRequired = (document.getElementById('td_SkillsRequired'));
}

function DetermineContentToPresent(eventSource_textContent)
{
    if (eventSource_textContent == 'General Information')
    {
        divJobDescription.style.display = NoneDisplayStyle;
        divJobDescriptionLtr.style.display = NoneDisplayStyle;
        divConditions.style.display = NoneDisplayStyle;
        divConditionsLiteral.style.display = NoneDisplayStyle;
        divSkillsRequired.style.display = NoneDisplayStyle;
        divSkillsRequiredLiteral.style.display = NoneDisplayStyle;
        divPreferences.style.display = NoneDisplayStyle;
        divQualifications.style.display = NoneDisplayStyle;
        divBreakDown.style.display = NoneDisplayStyle;
        divGeneralInformation.style.display = BlockDisplayStyle;
    }
    else if (eventSource_textContent == 'Job Description')
    {

        divConditions.style.display = NoneDisplayStyle;
        divConditionsLiteral.style.display = NoneDisplayStyle;
        divSkillsRequired.style.display = NoneDisplayStyle;
        divSkillsRequiredLiteral.style.display = NoneDisplayStyle;
        divPreferences.style.display = NoneDisplayStyle;
        divQualifications.style.display = NoneDisplayStyle;
        divGeneralInformation.style.display = NoneDisplayStyle;
        divBreakDown.style.display = NoneDisplayStyle;

        if ((td_JobDescription.innerText) != '')
        {
            divJobDescription.style.display = NoneDisplayStyle;
            divJobDescriptionLtr.style.display = BlockDisplayStyle;
        }
        else
        {
            divJobDescriptionLtr.style.display = NoneDisplayStyle;
            divJobDescription.style.display = BlockDisplayStyle;
        }
    }
    else if (eventSource_textContent == 'Job Condition')
    {
        divJobDescriptionLtr.style.display = NoneDisplayStyle;
        divSkillsRequired.style.display = NoneDisplayStyle;
        divSkillsRequiredLiteral.style.display = NoneDisplayStyle;
        divPreferences.style.display = NoneDisplayStyle;
        divQualifications.style.display = NoneDisplayStyle;
        divGeneralInformation.style.display = NoneDisplayStyle;
        divJobDescription.style.display = NoneDisplayStyle;
        divBreakDown.style.display = NoneDisplayStyle;

        if ((td_JobConditions.innerText) != '')
        {
            divConditions.style.display = NoneDisplayStyle;
            divConditionsLiteral.style.display = BlockDisplayStyle;
        }
        else
        {
            divConditionsLiteral.style.display = NoneDisplayStyle;
            divConditions.style.display = BlockDisplayStyle;
        }
    }
    else if (eventSource_textContent == 'Experience & Training')
    {
        divJobDescriptionLtr.style.display = NoneDisplayStyle;
        divConditionsLiteral.style.display = NoneDisplayStyle;
        divPreferences.style.display = NoneDisplayStyle;
        divQualifications.style.display = NoneDisplayStyle;
        divGeneralInformation.style.display = NoneDisplayStyle;
        divJobDescription.style.display = NoneDisplayStyle;
        divConditions.style.display = NoneDisplayStyle;
        divBreakDown.style.display = NoneDisplayStyle;


        if ((td_SkillsRequired.innerText) != '')
        {
            divSkillsRequired.style.display = NoneDisplayStyle;
            divSkillsRequiredLiteral.style.display = BlockDisplayStyle;
        }
        else
        {
            divSkillsRequiredLiteral.style.display = NoneDisplayStyle;
            divSkillsRequired.style.display = BlockDisplayStyle;
        }
    }
    else if (eventSource_textContent == 'Qualifications')
    {
        divGeneralInformation.style.display = NoneDisplayStyle;
        divJobDescription.style.display = NoneDisplayStyle;
        divJobDescriptionLtr.style.display = NoneDisplayStyle;
        divConditions.style.display = NoneDisplayStyle;
        divConditionsLiteral.style.display = NoneDisplayStyle;
        divSkillsRequiredLiteral.style.display = NoneDisplayStyle;
        divSkillsRequired.style.display = NoneDisplayStyle;
        divPreferences.style.display = NoneDisplayStyle;
        divBreakDown.style.display = NoneDisplayStyle;
        divQualifications.style.display = BlockDisplayStyle;
    }
    else if (eventSource_textContent == 'Preferences')
    {
        divGeneralInformation.style.display = NoneDisplayStyle;
        divJobDescription.style.display = NoneDisplayStyle;
        divJobDescriptionLtr.style.display = NoneDisplayStyle;
        divConditions.style.display = NoneDisplayStyle;
        divConditionsLiteral.style.display = NoneDisplayStyle;
        divSkillsRequiredLiteral.style.display = NoneDisplayStyle;
        divSkillsRequired.style.display = NoneDisplayStyle;
        divQualifications.style.display = NoneDisplayStyle;
        divBreakDown.style.display = NoneDisplayStyle;
        divPreferences.style.display = BlockDisplayStyle;
    }
    else if (eventSource_textContent == 'Break down')
    {
        divGeneralInformation.style.display = NoneDisplayStyle;
        divJobDescription.style.display = NoneDisplayStyle;
        divJobDescriptionLtr.style.display = NoneDisplayStyle;
        divConditions.style.display = NoneDisplayStyle;
        divConditionsLiteral.style.display = NoneDisplayStyle;
        divSkillsRequiredLiteral.style.display = NoneDisplayStyle;
        divSkillsRequired.style.display = NoneDisplayStyle;
        divQualifications.style.display = NoneDisplayStyle;
        divPreferences.style.display = NoneDisplayStyle;
        divBreakDown.style.display = BlockDisplayStyle;
    }
}

function HandleEditInitiation(event)
{
    var eventSource = GetEventSource(event);
    var eventSource_innerText = (eventSource.innerText);

    ObtainRequiredDivsReferences();

    ObtainRequiredCells();

    ObtainInputFieldReferences();

    if ((eventSource.id) == lnkBtnEDITJobDescriptionClientID)
    {
        divJobDescriptionLtr.style.display = NoneDisplayStyle;
        iftJobDescription.value = (td_JobDescription.innerHTML);
        divJobDescription.style.display = BlockDisplayStyle;
    }
    else if ((eventSource.id) == lnkBtnEDITJobConditionsClientID)
    {
        divConditionsLiteral.style.display = NoneDisplayStyle;
        iftJobConditions.value = (td_JobConditions.innerHTML);
        divConditions.style.display = BlockDisplayStyle;
    }
    else if ((eventSource.id) == lnkBtnEDITSkillsRequiredClientID)
    {
        divSkillsRequiredLiteral.style.display = NoneDisplayStyle;
        iftJobSkillsRequired.value = (td_SkillsRequired.innerHTML);
        divSkillsRequired.style.display = BlockDisplayStyle;
    }

    PreventPostBack(event);
}

function ObtainInputFieldReferences()
{
    iftJobDescription = (document.getElementById(iftJobDescriptionClientID));
    iftJobConditions = (document.getElementById(iftJobConditionsClientID));
    iftJobSkillsRequired = (document.getElementById(iftJobSkillsRequiredClientID));
}

// Routine to handle the event that occurs when selection is made from the select element used to display divisions.
function Handle_ddlDivision_onchange()
{
    var lnkBtnBreakDown = (document.getElementById(lnkBtnBreakDownClientID));

    var ddlDivision = (document.getElementById(ddlDivisionClientID));

    if ((ddlDivision.selectedIndex) > 0)
    {
        lnkBtnBreakDown.style.display = BlockDisplayStyle;
    }
    else
    {
        lnkBtnBreakDown.style.display = NoneDisplayStyle;
    }

}

//var iftStartAdvertisementDate = '<% =iftStartAdvertisementDate.ClientID %>';
//var iftEndAdvertisementDate = '<% =iftEndAdvertisementDate.ClientID %>';

function ValidateAdvertisementStartDate()
{
    // obtaining a reference to the input element of type text used to display the selected date
    // and then getting the date specified.
    var iftStartAdvertisementDate = (document.getElementById(iftStartAdvertisementDateClientID));
    sSelectedAdvertisementStartDate = (iftStartAdvertisementDate.value);
    dSelectedAdvertisementStartDate = (new Date(sSelectedAdvertisementStartDate));

    // obtaining a reference to the current date.
    var currentDate = (new Date());

    if (currentDate > dSelectedAdvertisementStartDate)
    {
        if ((currentDate.getDate()) == (dSelectedAdvertisementStartDate.getDate()))
        {
            if ((currentDate.getFullYear()) == (dSelectedAdvertisementStartDate.getFullYear()))
            {
                return;
            }
        }

        PreventPostBack(event);

        isVacanyValid = false;
        iftStartAdvertisementDate.value = '';
        alert('Please, ensure that the advertisement start date is later than today.');

    }
    else
    {
        isVacanyValid = true;
    }
}

function ValidateAdvertisementEndDate()
{
    // obtaining the reference to the text field used to display selected end ad. date and the date specified.
    var iftEndAdvertisementDate = (document.getElementById(iftEndAdvertisementDateClientID));
    var sSelectedAdvertisementEndDate = (iftEndAdvertisementDate.value);
    var dSelectedAdvertisementEndDate = (new Date(sSelectedAdvertisementEndDate));

    if (sSelectedAdvertisementStartDate != null)
    {
        if (dSelectedAdvertisementStartDate > dSelectedAdvertisementEndDate)
        {
            PreventPostBack(event);

            isVacanyValid = false;
            iftEndAdvertisementDate.value = '';
            alert('Please, ensure that the advertisement end date is later than the start date.');
        }
        else
        {
            isVacanyValid = true;
        }
    }
}

// Routine to handle the onclick event of an input type element of type button (The insert button of the ListView control)
function Handle_btnINSERT_onclick(event)
{
    isVacancyBreakdownValid = true;

    var eventSourceRow = GetEventSourceRow(event);

    var quantity = GetBreakdownEntry(eventSourceRow, 0);

    ValidateQuantity(quantity);

    if (!isVacancyBreakdownValid)
    {
        PreventPostBack(event);

        return;
    }

    ValidateVacancyBreakdown(eventSourceRow);
}

// Routine to obtain a reference to the table row element that contains the button clicked.
function GetEventSourceRow(event)
{
    var eventSource = GetEventSource(event);

    // to hold the row that contains the source of the event.
    var eventSourceRow;
    // to hold the parent element(s) of the event source
    var eventSourceParentElement;

    eventSourceParentElement = (eventSource.parentElement);

    while ((eventSourceParentElement.tagName) != 'TR')
    {
        eventSourceParentElement = (eventSourceParentElement.parentElement);
    }

    eventSourceRow = eventSourceParentElement;

    return eventSourceRow;
}

// Routine to validate location specified to ensure it doesn't already exists.
function ValidateVacancyBreakdown(eventSourceRow)
{
    var eventSourceRowIndex = (eventSourceRow.rowIndex);
    if (eventSourceRowIndex <= 1)
    {
        return;
    }

    // obtaining a reference to the parent element of the object's row that triggered the onclick event.
    var eventSourceParent = (eventSourceRow.parentElement);

    ValidateBreakdownEntry(eventSourceParent, eventSourceRow);
}

// accepts the parent of the row that houses the element containing the entry to be validated.
function ValidateBreakdownEntry(eventSourceParent, eventSourceRow)
{
    // getting the count of rows within table/tbody element.
    var rowCount = ((eventSourceParent.rows).length);
    breakDownEntryCount = rowCount;

    // to hold a reference to the current row being evaluated.
    var currentRow;
    // to hold a reference to the cell used to house the select element that displays the value to be compared to.
    var currentCell;
    // to hold the value to be compared to.
    var valueToBeComparedTo;

    // to hold concatenation of entries to be made and entries to compared to.
    var entriesToBeValidated = '';
    var valuesToBeComparedTo = '';

    // for each row, ...
    for (var i = (rowCount - 2); (i > -1); i--)
    {
        if (!isVacancyBreakdownValid)
        {
            break;
        }

        valuesToBeComparedTo = '';
        currentRow = ((eventSourceParent.rows).item(i));

        // for each cell, within a row, ...
        for (var j = 0; (j <= 5); j++)
        {
            // if this is the first iteration, ...
            if (i == (rowCount - 2))
            {
                // getting the entry to be validated.
                var entryToBeValidated = GetBreakdownEntry(eventSourceRow, j);

                if ((entryToBeValidated == emptyDataObjectIndicator) || (entryToBeValidated == selectOptionText))
                {
                    entryToBeValidated = _NOTSPECIFIED;
                }

                entriesToBeValidated += entryToBeValidated;
            }

            // getting the value a cell within the aforemtioned row.
            currentCell = ((currentRow.cells).item(j));
            valueToBeComparedTo = GetBlockElementText(currentCell);
            valueToBeComparedTo = valueToBeComparedTo.replace(/\s+$/, '');
            valuesToBeComparedTo += valueToBeComparedTo;

            // if currently evaluating the last cell, which is the sixth one, ...
            if (j == 5)
            {
                if (valuesToBeComparedTo == entriesToBeValidated)
                {
                    PreventPostBack(event);

                    isVacancyBreakdownValid = false;

                    alert('Please ensure that entries are unique. The entries made in row ' + (i + 1) + '  are exactly the same with the entries you\'re about to make.');

                    eventSourceRow.focus();

                    break;
                }
                else
                {
                    isVacancyBreakdownValid = true;
                }
            }
        }
    }
}

// Accepts the row of the entry and the cell whose text content is currently being evaluated.
function GetBreakdownEntry(eventSourceRow, currentCellIndex)
{
    // obtaining references to the cell and then element within cell 
    // that contains the element wherein the entry was made.
    var requiredCell = ((eventSourceRow.cells).item(currentCellIndex));
    requiredElement = ((requiredCell.childNodes).item(0));
    // getting the type of element within the table cell.
    var elementTagName = (requiredElement.tagName);
    var entry;

    if (elementTagName == 'INPUT')
    {
        entry = (requiredElement.value);
    }
    else
    {
        // getting the count of options within select element.
        var optionCount = ((requiredElement.options).length);
        if (optionCount > 0)
        {
            // getting the index of the option selected within from select element.
            var optionIndex = (requiredElement.selectedIndex);
            // obtaining a reference to the selected option.
            var selectedOption = ((requiredElement.options).item(optionIndex));
            entry = (selectedOption.text);
        }
        else
        {
            entry = emptyDataObjectIndicator;
        }
    }

    entry = entry.replace(/\s+$/, '');

    return entry;
}

// Routine to ensure that a quantity has either been entered or to validate quantity entered.
function ValidateQuantity(quantity)
{
    if (quantity == '')
    {
        PreventPostBack(event);

        alert('Please, specify the number of employees required.');

        requiredElement.focus();

        isVacancyBreakdownValid = false;
    }
    else
    {
        if (isNaN(quantity))
        {
            PreventPostBack(event);

            alert('Please ensure that the number of employees required specified is a number');

            requiredElement.focus();

            isVacancyBreakdownValid = false;
        }
        else
        {
            isVacancyBreakdownValid = true;
        }
    }
}


// Routine to get the name of the current entry being evaluated.
function GetEntryNameBeingEvaluated(currentCellIndex)
{
    if (currentCellIndex == 1)
    {
        currentEntryName = 'status';
    }
    else if (currentCellIndex == 2)
    {
        currentEntryName = 'department';
    }
    else if (currentCellIndex == 3)
    {
        currentEntryName = 'unit';
    }
    else if (currentCellIndex == 4)
    {
        currentEntryName = 'state';
    }
    else if (currentCellIndex == 5)
    {
        currentEntryName = 'location';
    }
}

function Handle_btnDELETE_onclick(event)
{
    var answer = confirm('Are you sure you would like to delete this entry?');
    if (!answer)
    {
        PreventPostBack(event);
    }
}

function HandlePublishInitiationOnClient(event)
{
    if (!(Page_ClientValidate("Vacancy")))
    {
        PreventPostBack(event);

        alert('Please, ensure you have entered all required information. \n All fields marked with an asterix are required. ');
    }
    else
    {
        ObtainInputFieldReferences();

        if ((RTE_GetRichEditTextOnly(iftJobDescriptionClientID)) == '')
        {
            if ((td_JobDescription.innerText) == '')
            {
                PreventPostBack(event);
                alert('Please, ensure you enter in the job description');
                ObtainRequiredDivsReferences();
                DetermineContentToPresent('Job Description');
                return;
            }
        }

        if ((RTE_GetRichEditTextOnly(iftJobConditionsClientID)) == '')
        {
            if ((td_JobConditions.innerText) == '')
            {
                PreventPostBack(event);
                alert('Please, ensure you enter in the job condition(s)');
                ObtainRequiredDivsReferences();
                DetermineContentToPresent('Job Condition');
                return;
            }
        }

        if ((RTE_GetRichEditTextOnly(iftJobSkillsRequiredClientID)) == '')
        {
            if ((td_SkillsRequired.innerText) == '')
            {
                PreventPostBack(event);
                alert('Please, ensure you specify the required Experience & Training');
                ObtainRequiredDivsReferences();
                DetermineContentToPresent('Experience & Training');
                return;
            }
        }

        var eventSource = GetEventSource(event);
        var displayButtonText = (eventSource.value);

        if (displayButtonText == 'publish')
        {
            var hfBreakDownCount = (document.getElementById(hfBreakDownCountClientID));
            var enteredBreakDownCount = (hfBreakDownCount.value);
            if (enteredBreakDownCount == '')
            {
                PreventPostBack(event);
                alert('Please, ensure that you specify break down details for the vacancy you wish to add.');
                ObtainRequiredDivsReferences();
                DetermineContentToPresent('Break down');
                return;

            }
        }
    }
}
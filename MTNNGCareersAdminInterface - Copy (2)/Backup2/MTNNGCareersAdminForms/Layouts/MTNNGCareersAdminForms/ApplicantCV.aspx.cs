﻿using System;
using System.Web.Configuration;
using System.Data;
using System.Data.SqlClient;

using System.Web.UI;
using System.Web.UI.WebControls;

using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
using Microsoft.SharePoint.Utilities;

using MTNNGCareers.Administration.Common;

using CommonUtilities = Utilities.Common;
using SharePointListManager;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text;
using iTextSharp.text.pdf;
using registrantSpace;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace MTNNGCareers.Administration.Forms
{
    public partial class ApplicantCV : LayoutsPageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var reqr = Request.QueryString["RegistrantKey"].Trim();
                DownloadPDF_MSingle(reqr);
            }
        }

        protected void DownloadPDF_MSingle(string rowkey)
        {
            string filenamefromusers = String.Empty;
            // Create a Document object
            var document = new Document(PageSize.A4, 50, 50, 25, 25);

            // Create a new PdfWrite object, writing the output to a MemoryStream
            var output = new MemoryStream();
            var writer = PdfWriter.GetInstance(document, output);


            //Pagination
            TwoColumnHeaderFooter PageEventHandler = new TwoColumnHeaderFooter();
            writer.PageEvent = PageEventHandler;
            // Define the page header
            //PageEventHandler.Title = "Tonytsund";// Title;
            PageEventHandler.RegistrantName = "CV";
            PageEventHandler.HeaderFont = FontFactory.GetFont(BaseFont.COURIER_BOLD, 10, Font.BOLD);


            List<int> regEmp = new List<int>();
            regEmp.Add(701226);
            regEmp.Add(168615);

            List<IElement> parsedHtmlElements = new List<IElement>();

            // Open the Document for writing
            document.Open();

            //string contents = File.ReadAllText(Server.MapPath("/_layouts/MTNNGCareersAdminForms/cvTemplate/CV.htm"));
            string contents = string.Empty;
            string cnnString = WebConfigurationManager.ConnectionStrings["MTNNGCareersDBConnectionString"].ConnectionString;
            SqlConnection cnn = new SqlConnection(cnnString);
            SqlTransaction transaction;

            cnn.Open();
            transaction = cnn.BeginTransaction();

            //Create List of Contents string
            List<string> Contents = new List<string>();
            StringBuilder builder = new StringBuilder();
            try
            {
                //foreach (GridViewRow row in gvVacanyApplicants.Rows)
                //{
                //    // Access the CheckBox
                //    CheckBox cb = (CheckBox)row.FindControl("cbEnlist");
                //    if (cb.Checked)
                //    {
                //        var Datakey = Convert.ToInt32(gvVacanyApplicants.DataKeys[row.RowIndex].Values["RegistrantRowKey"]);
                //    }
                //}

                //foreach (var parValue in regEmp)
               

                    // Access the CheckBox

                var parValue = rowkey;// Convert.ToInt32(gvVacanyApplicants.DataKeys[row.RowIndex].Values["RegistrantRowKey"]);

                        contents = File.ReadAllText(Server.MapPath("/_layouts/MTNNGCareersAdminForms/cvTemplate/CV.htm"));

                        // Command Objects for the transaction
                        SqlCommand cmd1 = new SqlCommand("RetrieveRegistrant", cnn);
                        SqlCommand cmd2 = new SqlCommand("RetrieveRegistrantContacts", cnn);
                        SqlCommand cmd3 = new SqlCommand("RetrieveRegistrantEducationalHistory", cnn);
                        SqlCommand cmd4 = new SqlCommand("RetrieveRegistrantProfessionalCertification", cnn);
                        SqlCommand cmd5 = new SqlCommand("RetrieveRegistrantSkills", cnn);
                        SqlCommand cmd6 = new SqlCommand("RetrieveRegistrantWorkExperience", cnn);

                        cmd1.CommandType = CommandType.StoredProcedure;
                        cmd2.CommandType = CommandType.StoredProcedure;
                        cmd3.CommandType = CommandType.StoredProcedure;
                        cmd4.CommandType = CommandType.StoredProcedure;
                        cmd5.CommandType = CommandType.StoredProcedure;
                        cmd6.CommandType = CommandType.StoredProcedure;

                        cmd1.Parameters.Add(new SqlParameter("@RegistrantRowKey", SqlDbType.BigInt, 50));
                        cmd1.Parameters["@RegistrantRowKey"].Value = parValue;

                        cmd1.Parameters.Add("@RowNoAffected", SqlDbType.Int).Direction = (ParameterDirection.Output);
                        cmd1.Parameters.Add("@ErrorMessage", SqlDbType.NVarChar, 256).Direction = (ParameterDirection.Output);

                        cmd2.Parameters.Add(new SqlParameter("@RegistrantRowKey", SqlDbType.BigInt, 50));
                        cmd2.Parameters["@RegistrantRowKey"].Value = parValue;// paramValue3;

                        cmd3.Parameters.Add(new SqlParameter("@RegistrantRowKey", SqlDbType.BigInt, 50));
                        cmd3.Parameters["@RegistrantRowKey"].Value = parValue;

                        cmd3.Parameters.Add("@RowNoAffected", SqlDbType.Int).Direction = (ParameterDirection.Output);
                        cmd3.Parameters.Add("@ErrorMessage", SqlDbType.NVarChar, 256).Direction = (ParameterDirection.Output);

                        cmd4.Parameters.Add(new SqlParameter("@RegistrantRowKey", SqlDbType.BigInt, 50));
                        cmd4.Parameters["@RegistrantRowKey"].Value = parValue;

                        cmd4.Parameters.Add("@RowNoAffected", SqlDbType.Int).Direction = (ParameterDirection.Output);
                        cmd4.Parameters.Add("@ErrorMessage", SqlDbType.NVarChar, 256).Direction = (ParameterDirection.Output);

                        cmd5.Parameters.Add(new SqlParameter("@RegistrantRowKey", SqlDbType.BigInt, 50));
                        cmd5.Parameters["@RegistrantRowKey"].Value = parValue;

                        cmd5.Parameters.Add("@RowNoAffected", SqlDbType.Int).Direction = (ParameterDirection.Output);
                        cmd5.Parameters.Add("@ErrorMessage", SqlDbType.NVarChar, 256).Direction = (ParameterDirection.Output);

                        cmd6.Parameters.Add(new SqlParameter("@RegistrantRowKey", SqlDbType.BigInt, 50));
                        cmd6.Parameters["@RegistrantRowKey"].Value = parValue;

                        cmd6.Parameters.Add("@RowNoAffected", SqlDbType.Int).Direction = (ParameterDirection.Output);
                        cmd6.Parameters.Add("@ErrorMessage", SqlDbType.NVarChar, 256).Direction = (ParameterDirection.Output);


                        cmd1.Transaction = transaction;
                        SqlDataReader basicInfodr = cmd1.ExecuteReader();
                        if (basicInfodr.Read())
                        {

                            // Replace the placeholders with the user-specified text
                            string FullName = basicInfodr[1].ToString() + " " + basicInfodr[2].ToString() + " " + basicInfodr[3].ToString() + " " + basicInfodr[4].ToString();
                            filenamefromusers = basicInfodr[2].ToString() + basicInfodr[3].ToString() + basicInfodr[4].ToString();
                            filenamefromusers = filenamefromusers.Trim();
                            //Random n = new Random();
                           // int stp = n.Next(1, 2000);
                           // filenamefromusers = filenamefromusers + stp.ToString();
                            filenamefromusers = filenamefromusers.Trim();

                            string rowkeyids = "CVVIEW.ashx?Id=" + rowkey;
                            string cvupurl = GetuploadedFile(rowkeyids);
                            cvupurl = cvupurl.Trim();
                            cvupurl = cvupurl + rowkeyids.Trim();

                            string uploadedcv = string.Format("<a href=\"{0}\">Uploaded CV</a>", cvupurl);

                            if (CheckForDownload(rowkey) == true)
                            {
                                contents = contents.Replace("[UploadedCV]", uploadedcv);
                            }
                            else
                            {
                                contents = contents.Replace("[UploadedCV]", "");
                            }
                            contents = contents.Replace("[Name]", FullName);
                            string BirthDate = basicInfodr[5] != DBNull.Value ? basicInfodr[5].ToString() : "N/A";
                            contents = contents.Replace("[BirthDate]", BirthDate);
                            string Gender = basicInfodr[6] != DBNull.Value ? basicInfodr[6].ToString() : "N/A";
                            contents = contents.Replace("[Gender]", Gender);
                            string MaritalStatus = basicInfodr[7] != DBNull.Value ? basicInfodr[7].ToString() : "N/A";
                            contents = contents.Replace("[MaritalStatus]", MaritalStatus);
                            string Nationality = basicInfodr[8] != DBNull.Value ? basicInfodr[8].ToString() : "N/A";
                            contents = contents.Replace("[Nationality]", Nationality);
                            string StateProvince = basicInfodr[9] != DBNull.Value ? basicInfodr[9].ToString() : "N/A";
                            contents = contents.Replace("[StateProvince]", StateProvince);
                            string NYSCCompletionStatus = basicInfodr[13] != DBNull.Value ? basicInfodr[13].ToString() : "N/A";
                            contents = contents.Replace("[NYSCCompletionStatus]", NYSCCompletionStatus);
                            string PostDgrs = basicInfodr[15] != DBNull.Value ? basicInfodr[15].ToString() : "N/A";
                            contents = contents.Replace("[PostDgrs]", PostDgrs);

                        }
                        basicInfodr.Close();

                        //Contact Information
                        cmd2.Transaction = transaction;
                        SqlDataReader contactinfodr = cmd2.ExecuteReader();
                        if (contactinfodr.Read())
                        {

                            // Replace the placeholders with the user-specified text
                            string CountryOfResidence = contactinfodr[2].ToString();
                            contents = contents.Replace("[Country]", CountryOfResidence);
                            string StateProvince = contactinfodr[3] != DBNull.Value ? contactinfodr[3].ToString() : "N/A";
                            contents = contents.Replace("[StateProvince]", StateProvince);
                            string City = contactinfodr[5] != DBNull.Value ? contactinfodr[5].ToString() : "N/A";
                            contents = contents.Replace("[City]", City);
                            string Address = contactinfodr[6] != DBNull.Value ? contactinfodr[6].ToString() : "N/A";
                            contents = contents.Replace("[Address]", Address);
                            string MobilePhone = contactinfodr[8] != DBNull.Value ? contactinfodr[8].ToString() : "N/A";
                            contents = contents.Replace("[MobilePhone]", MobilePhone);
                            string PhoneOther = contactinfodr[9] != DBNull.Value ? contactinfodr[9].ToString() : "N/A";
                            contents = contents.Replace("[PhoneOther]", PhoneOther);


                        }
                        contactinfodr.Close();


                        //Education History 
                        var itemEduTable = @"<table>";

                        cmd3.Transaction = transaction;
                        SqlDataReader eduinfodr = cmd3.ExecuteReader();
                        while (eduinfodr.Read())
                        {

                            string Level = eduinfodr[3] != DBNull.Value ? eduinfodr[3].ToString() : "N/A";

                            string Institution = eduinfodr[4] != DBNull.Value ? eduinfodr[4].ToString() : "N/A";

                            string StudyCourse = eduinfodr[5] != DBNull.Value ? eduinfodr[5].ToString() : "N/A";

                            string Qualification = eduinfodr[7] != DBNull.Value ? eduinfodr[7].ToString() : "N/A";

                            string Class = eduinfodr[9] != DBNull.Value ? eduinfodr[9].ToString() : "N/A";

                            string StartDate = eduinfodr[8] != DBNull.Value ? eduinfodr[8].ToString() : "N/A";

                            string EndDate = eduinfodr[9] != DBNull.Value ? eduinfodr[9].ToString() : "N/A";

                            itemEduTable += string.Format("<tr><td style=\"font-size: 10px\">Level</td><td style=\"font-size: 10px\">{0}</td></tr><tr><td style=\"font-size: 10px\">Institution</td><td style=\"font-size: 10px\">{1}</td></tr><tr><td style=\"font-size: 10px\">Study Course</td><td style=\"font-size: 10px\">{2}</td></tr><tr><td style=\"font-size: 10px\">Qualification</td><td style=\"font-size: 10px\">{3}</td></tr><tr><td style=\"font-size: 10px\">Class</td><td style=\"font-size: 10px\">{4}</td></tr><tr><td style=\"font-size: 10px\">Start Date</td><td style=\"font-size: 10px\">{5}</td></tr><tr><td style=\"font-size: 10px\">End Date</td><td style=\"font-size: 10px\">{6}</td></tr><tr><td colspan=\"2\"><hr style=\"width:50%\"/></td></tr>", Level, Institution, StudyCourse, Qualification, Class, StartDate, EndDate);
                            // Replace the placeholders with the user-specified text

                        }
                        itemEduTable += "</table><hr/>";
                        contents = contents.Replace("[EDUCATIONALHISTORY]", itemEduTable);
                        eduinfodr.Close();

                        //Profesional Info 
                        var itemprofTable = @"<table>";

                        cmd4.Transaction = transaction;
                        SqlDataReader profinfodr = cmd4.ExecuteReader();
                        while (profinfodr.Read())
                        {

                            string Organisation = profinfodr[2] != DBNull.Value ? profinfodr[2].ToString() : "N/A";

                            string Certificate = profinfodr[3] != DBNull.Value ? profinfodr[3].ToString() : "N/A";

                            string Status = profinfodr[4] != DBNull.Value ? profinfodr[4].ToString() : "N/A";

                            string DateJoined = profinfodr[5] != DBNull.Value ? profinfodr[5].ToString() : "N/A";


                            itemprofTable += string.Format("<tr><td style=\"font-size: 10px\">Organisation</td><td style=\"font-size: 10px\">{0}</td></tr><tr><td style=\"font-size: 10px\">Certificate</td><td style=\"font-size: 10px\">{1}</td></tr><tr><td style=\"font-size: 10px\">Status</td><td style=\"font-size: 10px\">{2}</td></tr><tr><td style=\"font-size: 10px\">Date Joined</td><td style=\"font-size: 10px\">{3}</td></tr><tr><td colspan=\"2\"><hr style=\"width:50%\"/></td></tr>", Organisation, Certificate, Status, DateJoined);
                            // Replace the placeholders with the user-specified text

                        }
                        itemprofTable += "</table>";
                        contents = contents.Replace("[PROFESSIONALASSOCIATION]", itemprofTable);
                        profinfodr.Close();


                        //Skill Info 
                        var itemskillTable = @"<table>";

                        cmd5.Transaction = transaction;
                        SqlDataReader skillinfodr = cmd5.ExecuteReader();
                        while (skillinfodr.Read())
                        {

                            string skill = skillinfodr[2] != DBNull.Value ? skillinfodr[2].ToString() : "N/A";

                            string skillProfiencylevel = skillinfodr[4] != DBNull.Value ? skillinfodr[4].ToString() : "N/A";

                            string YearofExperience = skillinfodr[5] != DBNull.Value ? skillinfodr[5].ToString() : "N/A";

                            itemskillTable += string.Format("<tr><td style=\"font-size: 10px\">Skill</td><td style=\"font-size: 10px\">{0}</td></tr><tr><td style=\"font-size: 10px\">Pro. Level</td><td style=\"font-size: 10px\">{1}</td></tr><tr><td style=\"font-size: 10px\">Year of Exp.</td><td style=\"font-size: 10px\">{2}</td></tr><tr><td colspan=\"2\"><hr style=\"width:50%\"/></td></tr>", skill, skillProfiencylevel, YearofExperience);
                            // Replace the placeholders with the user-specified text

                        }
                        itemskillTable += "</table>";
                        contents = contents.Replace("[SKILL]", itemskillTable);
                        skillinfodr.Close();



                        //WorkExperience Info 
                        var itemworkexpTable = @"<table>";

                        cmd6.Transaction = transaction;
                        SqlDataReader workexpinfodr = cmd6.ExecuteReader();
                        while (workexpinfodr.Read())
                        {

                            string company = workexpinfodr[2] != DBNull.Value ? workexpinfodr[2].ToString() : "N/A";

                            string position = workexpinfodr[3] != DBNull.Value ? workexpinfodr[3].ToString() : "N/A";

                            string jobdescription = workexpinfodr[4] != DBNull.Value ? workexpinfodr[4].ToString() : "N/A";

                            string startdatework = workexpinfodr[5] != DBNull.Value ? workexpinfodr[5].ToString() : "N/A";

                            string enddatework = workexpinfodr[6] != DBNull.Value ? workexpinfodr[6].ToString() : "N/A";

                            itemworkexpTable += string.Format("<tr><td style=\"font-size: 10px\">Company</td><td style=\"font-size: 10px\">{0}</td></tr><tr><td style=\"font-size: 10px\">Position</td><td style=\"font-size: 10px\">{1}</td></tr><tr><td colspan=\"2\"></td></tr><tr><td style=\"font-size: 10px\">Job Description</td><td style=\"font-size: 10px\">{2}</td></tr><tr><td style=\"font-size: 10px\">Start Date</td><td style=\"font-size: 10px\">{3}</td></tr><tr><td style=\"font-size: 10px\">End Date</td><td style=\"font-size: 10px\">{4}</td></tr><tr><td colspan=\"2\"><hr style=\"width:50%\"/></td></tr>", company, position, jobdescription, startdatework, enddatework);
                            // Replace the placeholders with the user-specified text

                        }
                        itemworkexpTable += "</table>";
                        contents = contents.Replace("[WORKEXPERIENCE]", itemworkexpTable);
                        workexpinfodr.Close();

                        builder.AppendLine(contents);
                   
                
                //foreach (var eachContent in Contents)
                //{
                //var parsedHtmlElements = HTMLWorker.ParseToList(new StringReader(contents), null);
                parsedHtmlElements = HTMLWorker.ParseToList(new StringReader(builder.ToString()), null);
                foreach (var htmlElement in parsedHtmlElements)
                    document.Add(htmlElement as IElement);
                //}
                transaction.Commit();

            }
            catch (SqlException sqlEx)
            {
                transaction.Rollback();
            }

            finally
            {
                cnn.Close();
                cnn.Dispose();
            }

            //var parsedHtmlElements = HTMLWorker.ParseToList(new StringReader(contents), null);
            //foreach (var htmlElement in parsedHtmlElements)
            //    document.Add(htmlElement as IElement);

            document.Close();


            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Disposition", string.Format("attachment;filename=Receipt-{0}.pdf",filenamefromusers.Trim()));
            Response.BinaryWrite(output.ToArray());
            System.Threading.Thread.Sleep(1000);

        }




        protected string GetuploadedFile(string rowkey)
        {
            string url = "";
            try
            {
                SPSecurity.RunWithElevatedPrivileges(delegate()
                {
                    // implementation details omitted


                    SPWeb myWeb = SPContext.Current.Web;


                    //SPList docList = myWeb.GetList(SPUrlUtility.CombineUrl(myWeb.ServerRelativeUrl, "/List/MTNCareerSettings"));
                    SPListItemCollection docList = myWeb.GetList(SPUrlUtility.CombineUrl(myWeb.ServerRelativeUrl, "Lists/MTNCareerSettings")).Items;
                    foreach (SPListItem item in docList)
                    {
                        string itemTitle = (string)item["Title"];
                        if (itemTitle == "UploadedCVURL")
                        {
                            url = item["Values"].ToString();
                        }

                    }

                });
            }
            catch (Exception ex)
            {
                
            }

            return url;


        }

        protected bool CheckForDownload(string rowkey)
        {
            bool resp = false;
            string cnnString = WebConfigurationManager.ConnectionStrings["MTNNGCareersDBConnectionString"].ConnectionString;
            SqlConnection cnn = new SqlConnection(cnnString);
            SqlTransaction transaction;

            cnn.Open();
            transaction = cnn.BeginTransaction();
            try
            {
                SqlCommand comm = new SqlCommand("select * from [MTNNGCareers].[dbo].[CVUpload] where RegistrantRowKey =@rowkey", cnn, transaction);
                comm.Parameters.Add(new SqlParameter("rowkey", rowkey));
                SqlDataReader reader = comm.ExecuteReader();
                if (reader.HasRows)
                {

                    while (reader.Read())
                    {
                        resp = true;
                    }
                }
                reader.Close();
                transaction.Commit();
               
            }
            catch (SqlException sqlEx)
            {
              
                transaction.Rollback();
                resp = false;
            }

            finally
            {
                cnn.Close();
                cnn.Dispose();
            }

            return resp;
        }


    }
}

﻿using System;

using System.Collections;
using System.Collections.Generic;

using System.Web;
using System.Web.UI.WebControls;

using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;

using Microsoft.SharePoint.Utilities;

using MTNNGCareers.Administration.Common;
using CommonUtilities = Utilities.Common;
using SharePointListManager;

namespace MTNNGCareers.Administration.Forms
{
    public partial class ManageVacancyBreakDown : LayoutsPageBase
    {
        const string _IDFieldName = "ID";

        const string selectOptionText = "---select---";
        const string emptyDataObjectIndicator = "-----";
        const string _NOTSPECIFIED = "NOT SPECIFIED";

        const string itemToBeUpdatedID = "Item to be updated ID";

        string listGUID;
        string listItemID;

        SPWeb currentWebSite;

        List<string[,]> _BreakDown = new List<string[,]>();

        protected void Page_Load(object sender, EventArgs e)
        {
            currentWebSite = (this.Web);

            SPUserAuthenticator authenticator = new SPUserAuthenticator();
            bool isValidUser = (authenticator.ValidateUser());

            if (!isValidUser)
            {
                SPUtility.Redirect((currentWebSite.Url), SPRedirectFlags.Default, Context);
            }

            if (!IsPostBack)
            {
                listGUID = Request.Params["List"];
                if (listGUID != null)
                {
                    listItemID = Request.Params["ID"];
                    if (listItemID != null)
                    {
                        ViewState[itemToBeUpdatedID] = listItemID;

                        SPList vacancyBreakDownList = (currentWebSite.Lists[((new Guid(listGUID)))]);
                        SPListItem selectedItem = (vacancyBreakDownList.GetItemById((int.Parse(listItemID))));

                        DisplayVacancyBreakDownDetails(selectedItem);
                    }
                }   
            }
        }

        private void DisplayVacancyBreakDownDetails(SPListItem selectedListItem)
        {
            string vacancyFieldDisplayName = (VacancyBreakDownList.VacancyFieldDisplayName);
            SPFieldLookup spFieldLookup = ((SPFieldLookup)(selectedListItem.Fields[vacancyFieldDisplayName]));
            SPFieldLookupValue spFieldLookupValue = ((SPFieldLookupValue)(spFieldLookup.GetFieldValue(((selectedListItem[vacancyFieldDisplayName].ToString())))));
            lblJobTitle.Text = (spFieldLookupValue.LookupValue);
            string sLookUpID = ((spFieldLookupValue.LookupId).ToString());

            lblJobTitle.Visible = true;
           
            iftNoOfOpenings.Text = (selectedListItem[(VacancyBreakDownList.QuantityFieldDisplayName)].ToString());

            ddlStatus.SelectedValue = ((selectedListItem[(VacancyBreakDownList.EmployementStatusFieldDisplayName)]).ToString());

            #region Display Departments

            string sDivision = (String.Empty);
            string departmentFieldDisplayName = (VacancyBreakDownList.DepartmentLookUpFieldDisplayName);
            Object oDepartment = selectedListItem[(departmentFieldDisplayName)];

            string[,] criterionField = new string[1, 3];
            criterionField[0, 0] = "ID";
            criterionField[0, 1] = (CommonUtilities.FieldTypeNumber);
            criterionField[0, 2] = sLookUpID;

            ListAccessor accessor = new ListAccessor(currentWebSite, (VacanciesList.ListName), (new string[] {(VacanciesList.DivisionFieldInternalName)}), criterionField);
            foreach (SPListItem requiredListItem in (accessor.RequiredListItems))
            {
                SPList vacancyList = (currentWebSite.Lists[(VacanciesList.ListName)]);
                SPListItem vacancy = (vacancyList.GetItemById((int.Parse(sLookUpID))));
                spFieldLookup = ((SPFieldLookup)(vacancy.Fields[(VacanciesList.DivisionFieldDisplayName)]));
                spFieldLookupValue = ((SPFieldLookupValue)(spFieldLookup.GetFieldValue(((vacancy[(VacanciesList.DivisionFieldDisplayName)].ToString())))));
                sDivision = (spFieldLookupValue.LookupValue);

                criterionField = new string[1, 3];
                criterionField[0, 0] = (DepartmentList.DivisionFieldInternalName);
                criterionField[0, 1] = (CommonUtilities.FieldTypeLookUp);
                criterionField[0, 2] = sDivision;

                accessor = new ListAccessor((currentWebSite.ParentWeb), (DepartmentList.ListName), (new string[] { (DepartmentList.DepartmentFieldInternalName) }), criterionField);
                foreach (SPListItem departments in (accessor.RequiredListItems))
                {
                    string departmentID = ((departments.ID).ToString());
                    string departmentName = ((departments[(DepartmentList.DepartmentFieldDisplayName)]).ToString());

                    ListItem item = new ListItem(departmentName, departmentID);
                    ddlDepartment.Items.Add(item);
                }

                ddlDepartment.Items.Insert(0, selectOptionText);

                if (oDepartment != null)
                {
                    spFieldLookup = ((SPFieldLookup)(selectedListItem.Fields[(VacancyBreakDownList.DepartmentLookUpFieldDisplayName)]));
                    spFieldLookupValue = ((SPFieldLookupValue)(spFieldLookup.GetFieldValue(((selectedListItem[(VacancyBreakDownList.DepartmentLookUpFieldDisplayName)].ToString())))));
                    ddlDepartment.SelectedValue = ((spFieldLookupValue.LookupId).ToString());
                }
                break;
            }
                
            #endregion

            #region Display Units

            if ((ddlDepartment.SelectedIndex) > 0)
            {  
                criterionField = new string[1, 3];
                criterionField[0, 0] = (UnitsList.DepartmentFieldInternalName);
                criterionField[0, 1] = (Utilities.Common.FieldTypeLookUp);
                criterionField[0, 2] = ((ddlDepartment.SelectedItem).Text);

                accessor = new ListAccessor((currentWebSite.ParentWeb), (UnitsList.ListName), (new string[] { (UnitsList.TitleFieldInternalName) }), criterionField);
                foreach (SPListItem unit in (accessor.RequiredListItems))
                {
                    string unitId = ((unit.ID).ToString());
                    string unitName = ((unit[(UnitsList.TitleFieldDisplayName)]).ToString());

                    ListItem listItem = new ListItem(unitName, unitId);

                    ddlUnit.Items.Add(listItem);
                }

                ddlUnit.Items.Insert(0, selectOptionText);

                object oUnit = selectedListItem[(VacancyBreakDownList.UnitLookUpFieldDisplayName)];
                if (oUnit != null)
                {
                    spFieldLookup = ((SPFieldLookup)(selectedListItem.Fields[(VacancyBreakDownList.UnitLookUpFieldDisplayName)]));
                    spFieldLookupValue = ((SPFieldLookupValue)(spFieldLookup.GetFieldValue(((selectedListItem[(VacancyBreakDownList.UnitLookUpFieldDisplayName)].ToString())))));
                    ddlUnit.SelectedValue = ((spFieldLookupValue.LookupId).ToString());
                }
            }

            #endregion

            #region Display States

            object oState = (selectedListItem[(VacancyBreakDownList.StateFieldDisplayName)]);

            accessor = new ListAccessor(currentWebSite, (StatesOfOperationWithinNigeriaList.ListName), (StatesOfOperationWithinNigeriaList.TitleFieldInternalName), true);
            foreach (SPListItem state in (accessor.RequiredListItems))
            {
                string stateId = ((state.ID).ToString());
                string stateName = ((state[(StatesOfOperationWithinNigeriaList.TitleFieldDisplayName)]).ToString());

                ListItem listItem = new ListItem(stateName, stateId);

                ddlState.Items.Add(listItem);
            }

            ddlState.Items.Insert(0, selectOptionText);

            if (oState != null)
            {
                spFieldLookup = ((SPFieldLookup)(selectedListItem.Fields[(VacancyBreakDownList.StateFieldDisplayName)]));
                spFieldLookupValue = ((SPFieldLookupValue)(spFieldLookup.GetFieldValue(((selectedListItem[(VacancyBreakDownList.StateFieldDisplayName)].ToString())))));
                ddlState.SelectedValue = ((spFieldLookupValue.LookupId).ToString());
            }
            
            #endregion

            #region Display Locations

            if ((ddlState.SelectedIndex) > 0)
            {
                string stateId = (ddlState.SelectedValue);
                criterionField = new string[1, 3];
                criterionField[0, 0] = (LocationsOfOperationWithinNigeriaList.StateFieldInternalName);
                criterionField[0, 1] = (Utilities.Common.FieldTypeLookUp);
                criterionField[0, 2] = ((ddlState.SelectedItem).Text);

                accessor = new ListAccessor(currentWebSite, (LocationsOfOperationWithinNigeriaList.ListName), (new string[] { (LocationsOfOperationWithinNigeriaList.TitleFieldInternalName) }), criterionField);
                foreach (SPListItem state in (accessor.RequiredListItems))
                {
                    string locationId = ((state.ID).ToString());
                    string locationName = ((state[(LocationsOfOperationWithinNigeriaList.TitleFieldDisplayName)]).ToString());

                    ListItem listItem = new ListItem(locationName, locationId);

                    ddlLocation.Items.Add(listItem);
                }

                ddlLocation.Items.Insert(0, selectOptionText);

                object oLocation = selectedListItem[(VacancyBreakDownList.LocationFieldDisplayName)];
                if (oLocation != null)
                {
                    spFieldLookup = ((SPFieldLookup)(selectedListItem.Fields[(VacancyBreakDownList.LocationFieldDisplayName)]));
                    spFieldLookupValue = ((SPFieldLookupValue)(spFieldLookup.GetFieldValue(((selectedListItem[(VacancyBreakDownList.LocationFieldDisplayName)].ToString())))));
                    ddlLocation.SelectedValue = ((spFieldLookupValue.LookupId).ToString());
                }
            }

            #endregion

            btnCOMMAND.Text = "UPDATE";
            btnCOMMAND.ToolTip = "click here to update item.";
        }

        // handles the event raised when an item is selected from the DropDownList used to display departments
        protected void Handle_ddlDepartment_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            if((ddlDepartment.SelectedIndex) > 0)
            {
            BindUnitsOfDepartmentToControl();
            }
        }
        // Routine to bind units of a department to the List Control to render it.
        private void BindUnitsOfDepartmentToControl()
        {
            ddlUnit.Items.Clear();

            string selectedDepartment = ((ddlDepartment.SelectedItem).Text);
            SPListItemCollection departmentUnits = RetrieveDepartmentUnits(selectedDepartment);

            if ((departmentUnits.Count) == 0)
            {
                ddlUnit.Items.Add(emptyDataObjectIndicator);
                return;
            }

            foreach (SPListItem _SPListItem in departmentUnits)
            {
                string value = (_SPListItem[_IDFieldName].ToString());
                string text = (_SPListItem[(UnitsList.TitleFieldDisplayName)].ToString());

                ListItem li = new ListItem(text, value);

                ddlUnit.Items.Add(li);
            }

            ddlUnit.Items.Insert(0, selectOptionText);
        }
        // Routine to retrieve units of the selected departments.
        private SPListItemCollection RetrieveDepartmentUnits(string selectedDepartment)
        {
            SPListItemCollection departmentUnits = null;

            string[,] criterionField = new string[1, 3];
            criterionField[0, 0] = (UnitsList.DepartmentFieldInternalName);
            criterionField[0, 1] = (Utilities.Common.FieldTypeLookUp);
            criterionField[0, 2] = selectedDepartment;

            ListAccessor _ListAccessor = new ListAccessor((currentWebSite.ParentWeb), (UnitsList.ListName), (new string[] { (UnitsList.TitleFieldInternalName) }), criterionField);
            departmentUnits = (_ListAccessor.RequiredListItems);

            return departmentUnits;
        }

        // handles the event raised when an item is selected from the DropDownList used to display States of Operation.
        protected void Handle_ddlState_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            if ((ddlState.SelectedIndex) > 0)
            {
                BindLocationsOfOperationToControl();
            }
        }
        // Routine to bind locations of operations within the specified state.
        private void BindLocationsOfOperationToControl()
        {
            ddlLocation.Items.Clear();
            string selectedState = ((ddlState.SelectedItem).Text);
            SPListItemCollection stateLocations = RetrieveLocationsOfOperationByState(selectedState);

            if ((stateLocations.Count) == 0)
            {
                ddlLocation.Items.Add(emptyDataObjectIndicator);
                return;
            }

            foreach (SPListItem _SPListItem in stateLocations)
            {
                string value = (_SPListItem[_IDFieldName].ToString());
                string text = (_SPListItem[(LocationsOfOperationWithinNigeriaList.TitleFieldDisplayName)].ToString());

                ListItem li = new ListItem(text, value);

                ddlLocation.Items.Add(li);
            }

            ddlLocation.Items.Insert(0, selectOptionText);

        }        
        
        // Routine to retrieve locations of operations based on a selected state.
        private SPListItemCollection RetrieveLocationsOfOperationByState(string selectedState)
        {
            SPListItemCollection stateLocations = null;

            string[,] criterionField = new string[1, 3];
            criterionField[0, 0] = (LocationsOfOperationWithinNigeriaList.StateFieldInternalName);
            criterionField[0, 1] = (Utilities.Common.FieldTypeLookUp);
            criterionField[0, 2] = selectedState;

            ListAccessor _ListAccessor = new ListAccessor(currentWebSite, (LocationsOfOperationWithinNigeriaList.ListName), (new string[] { (LocationsOfOperationWithinNigeriaList.TitleFieldInternalName) }), criterionField);
            stateLocations = (_ListAccessor.RequiredListItems);

            return stateLocations;
        }

        protected void btnCOMMAND_Click(object sender, EventArgs e)
        {
            UpdateVacanyBreakDown();

            SPUtility.TransferToSuccessPage("Update Successful");
        }

        // Routine to add vacancy break down as an item to the Vacancy Break Down List
        private void UpdateVacanyBreakDown()
        {
            string[,] noOfOpenings = new string[1, 3];
            noOfOpenings[0, 0] = (VacancyBreakDownList.QuantityFieldInternalName);
            noOfOpenings[0, 1] = (CommonUtilities.FieldTypeNumber);
            noOfOpenings[0, 2] = (iftNoOfOpenings.Text);
            _BreakDown.Add(noOfOpenings);

            string[,] status = new string[1, 3];
            status[0, 0] = (VacancyBreakDownList.EmployementStatusFieldInternalName);
            status[0, 1] = (CommonUtilities.FieldTypeText);
            status[0, 2] = (ddlStatus.SelectedValue);
            _BreakDown.Add(status);

            string[,] department = new string[1, 3];
            department[0, 0] = (VacancyBreakDownList.DepartmentLookUpFieldInternalName);
                            
            if ((((ddlDepartment.Items).Count) > 0) && ((ddlDepartment.SelectedIndex) > 0))
            {
                department[0, 1] = (CommonUtilities.FieldTypeLookUp);
                department[0, 2] = (ddlDepartment.SelectedValue);
                _BreakDown.Add(department);
            }
            else
            {
                department[0, 1] = (CommonUtilities.FieldTypeText);
                department[0, 2] = "";
                _BreakDown.Add(department);
            }

            string[,] unit = new string[1, 3];
            unit[0, 0] = (VacancyBreakDownList.UnitLookUpFieldInternalName);            
            if ((((ddlUnit.Items).Count) > 0) && ((ddlUnit.SelectedIndex) > 0))
            {
                unit[0, 1] = (CommonUtilities.FieldTypeLookUp);
                unit[0, 2] = (ddlUnit.SelectedValue);
                _BreakDown.Add(unit);
            }
            else
            {
                unit[0, 1] = (CommonUtilities.FieldTypeText);
                unit[0, 2] = "";
                _BreakDown.Add(unit);
            }

            string[,] state = new string[1, 3];
            state[0, 0] = (VacancyBreakDownList.StateFieldInternalName);
            if ((((ddlState.Items).Count) > 0) && ((ddlState.SelectedIndex) > 0))
            {
                state[0, 1] = (CommonUtilities.FieldTypeLookUp);
                state[0, 2] = (ddlState.SelectedValue);
                _BreakDown.Add(state);
            }
            else
            {
                state[0, 1] = (CommonUtilities.FieldTypeText);
                state[0, 2] = "";
                _BreakDown.Add(state);
            }

            string[,] location = new string[1, 3];
            location[0, 0] = (VacancyBreakDownList.LocationFieldInternalName);
            if ((((ddlLocation.Items).Count) > 0) && ((ddlLocation.SelectedIndex) > 0))
            {                
                location[0, 1] = (CommonUtilities.FieldTypeLookUp);
                location[0, 2] = (ddlLocation.SelectedValue);
                _BreakDown.Add(location);
            }
            else
            {
                location[0, 1] = (CommonUtilities.FieldTypeText);
                location[0, 2] = "";
                _BreakDown.Add(location);
            }

            ListManipulator listManipulator = new ListManipulator(currentWebSite, (VacancyBreakDownList.ListName));
            listManipulator.UpdateItemToList((int.Parse(ViewState[itemToBeUpdatedID].ToString())), (_BreakDown));
        }
    }
}

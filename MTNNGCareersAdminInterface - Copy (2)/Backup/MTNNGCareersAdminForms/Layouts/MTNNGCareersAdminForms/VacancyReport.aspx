﻿<%@ Assembly Name="MTNNGCareers.Administration.Forms, Version=1.0.0.0, Culture=neutral, PublicKeyToken=5e7c3f80f0f7816f" %>
<%@ Import Namespace="Microsoft.SharePoint.ApplicationPages" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Register Assembly="AjaxControlToolkit, Version=3.0.30930.28736, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e"
    Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="VacancyReport.aspx.cs"
    Inherits="MTNNGCareers.Administration.Forms.VacancyReport" DynamicMasterPageFile="~masterurl/default.master" %>

<asp:Content ID="PageHead" ContentPlaceHolderID="PlaceHolderAdditionalPageHead" runat="server">
    <script type="text/javascript" src="/HR/_layouts/MTNNGCareersAdminForms/javascript/Common.js"></script>
    <!--<script type="text/javascript" src="javascript/NewVacancyFrm.js"></script>-->
    <script type="text/javascript">

        var cbAdvertisementDurationClientID = '<%= cbAdvertisementDuration.ClientID %>';
        var cbPublisherClientID = '<%= ddlRequester.ClientID %>';
        var cbRequesterClientID = '<%= cbRequester.ClientID %>';
        var cbDivisionClientID = '<%= cbDivision.ClientID %>';
        var cbDepartmentClientID = '<%= cbDepartment.ClientID %>';
        var cbStaffLevelClientID = '<%= cbStaffLevel.ClientID %>';
        var cbGenderClientID = '<%= cbGender.ClientID %>';
        var cbStateOfOperationClientID = '<%= cbStateOfOperation.ClientID %>';
        var cbLocationOfOperationClientID = '<%= cbLocationOfOperation.ClientID %>';

        var iftStartAdvertisementDateClientID = '<%= iftStartAdvertisementDate.ClientID %>';
        var iftEndAdvertisementDateClientID = '<%= iftEndAdvertisementDate.ClientID %>';
        var ddlRequesterClientID = '<%= cbAdvertisementDuration.ClientID %>';
        var ddlPublisherClientID = '<%= ddlRequester.ClientID %>';
        var ddlDivisionClientID = '<%= ddlDivision.ClientID %>';
        var ddlDepartmentClientID = '<%= ddlDepartment.ClientID %>';
        var ddlStaffLevelClientID = '<%= ddlStaffLevel.ClientID %>';
        var ddlGenderClientID = '<%= ddlGender.ClientID %>';
        var ddlStateOfOperationClientID = '<%= ddlStateOfOperation.ClientID %>';
        var ddlLocationOfOperationClientID = '<%= ddlLocationOfOperation.ClientID %>';

        function HandleGenerateReportInitiation(event)
        {
            var cbAdvertisementDuration = (document.getElementById(cbAdvertisementDurationClientID));
            var cbPublisher = (document.getElementById(cbPublisherClientID));
            var cbRequester = (document.getElementById(cbRequesterClientID));
            var cbDivision = (document.getElementById(cbDivisionClientID));
            var cbDepartment = (document.getElementById(cbDepartmentClientID));
            var cbStaffLevel = (document.getElementById(cbStaffLevelClientID));
            var cbGender = (document.getElementById(cbGenderClientID));
            var cbStateOfOperation = (document.getElementById(cbStateOfOperationClientID));
            var cbLocationOfOperation = (document.getElementById(cbLocationOfOperationClientID));

            var iftStartAdvertisementDate = (document.getElementById(iftStartAdvertisementDateClientID));
            var iftEndAdvertisementDate = (document.getElementById(iftEndAdvertisementDateClientID));
            var ddlRequester = (document.getElementById(ddlRequesterClientID));
            var ddlPublisher = (document.getElementById(ddlPublisherClientID));
            var ddlDivision = (document.getElementById(ddlDivisionClientID));
            var ddlDepartment = (document.getElementById(ddlDepartmentClientID));
            var ddlStaffLevel = (document.getElementById(ddlStaffLevelClientID));
            var ddlGender = (document.getElementById(ddlGenderClientID));
            var ddlStateOfOperation = (document.getElementById(ddlStateOfOperationClientID));
            var ddlLocationOfOperation = (document.getElementById(ddlLocationOfOperationClientID));



            if ((cbAdvertisementDuration.checked))
            {
                if ((iftStartAdvertisementDate.value) == '')
                {
                    PreventPostBack(event);
                    alert('Please ensure you specify a start date');
                    iftStartAdvertisementDate.focus();
                    return;
                }

                if ((iftEndAdvertisementDate.value) == '')
                {
                    PreventPostBack(event);
                    alert('Please ensure you specify an end date');
                    iftEndAdvertisementDate.focus();
                    return;
                }
                else
                {
                    // obtaining the reference to the text field used to display selected end ad. date and the date specified.
                    var iftEndAdvertisementDate = (document.getElementById(iftEndAdvertisementDateClientID));
                    var sSelectedAdvertisementEndDate = (iftEndAdvertisementDate.value);
                    var dSelectedAdvertisementEndDate = (new Date(sSelectedAdvertisementEndDate));

                    if (sSelectedAdvertisementStartDate != null)
                    {
                        if (dSelectedAdvertisementStartDate > dSelectedAdvertisementEndDate)
                        {
                            PreventPostBack(event);
                            alert('Please, ensure that the advertisement end date is later than the start date.');
                            iftEndAdvertisementDate.focus();
                            return;
                        }
                    }
                }
            }

            if ((cbRequester.checked))
            {
                if (((ddlRequester.selectedIndex) == 0) || ((ddlRequester.selectedIndex) == -1))
                {
                    PreventPostBack(event);
                    alert('Please ensure you specify a publisher');
                    iftStartAdvertisementDate.focus();
                    return;
                }
            }

            if ((cbRequester.checked))
            {
                if (((ddlRequester.selectedIndex) == 0) || ((ddlRequester.selectedIndex) == -1))
                {
                    PreventPostBack(event);
                    alert('Please ensure you specify the Hiring Manager');
                    ddlRequester.focus();
                    return;
                }
            }

            if ((cbPublisher.checked))
            {
                if (((ddlPublisher.selectedIndex) == 0) || ((ddlPublisher.selectedIndex) == -1))
                {
                    PreventPostBack(event);
                    alert('Please ensure you specify the Publisher');
                    ddlPublisher.focus();
                    return;
                }
            }

            if ((cbDivision.checked))
            {
                if (((ddlDivision.selectedIndex) == 0) || ((ddlDivision.selectedIndex) == -1))
                {
                    PreventPostBack(event);
                    alert('Please ensure you specify the Division');
                    ddlDivision.focus();
                    return;
                }
            }

            if ((cbDepartment.checked))
            {
                if (((ddlDepartment.selectedIndex) == 0) || ((ddlDepartment.selectedIndex) == -1))
                {
                    PreventPostBack(event);
                    alert('Please ensure you specify the Department');
                    ddlDepartment.focus();
                    return;
                }
            }

            if ((cbStaffLevel.checked))
            {
                if (((ddlStaffLevel.selectedIndex) == 0) || ((ddlStaffLevel.selectedIndex) == -1))
                {
                    PreventPostBack(event);
                    alert('Please ensure you specify the Staff Level');
                    ddlStaffLevel.focus();
                    return;
                }
            }

            if ((cbGender.checked))
            {
                if (((ddlGender.selectedIndex) == 0) || ((ddlGender.selectedIndex) == -1))
                {
                    PreventPostBack(event);
                    alert('Please ensure you specify the Gender');
                    ddlGender.focus();
                    return;
                }
            }

            if ((cbStateOfOperation.checked))
            {
                if (((ddlStateOfOperation.selectedIndex) == 0) || ((ddlStateOfOperation.selectedIndex) == -1))
                {
                    PreventPostBack(event);
                    alert('Please ensure you specify the state');
                    ddlStateOfOperation.focus();
                    return;
                }
            }

            if ((cbLocationOfOperation.checked))
            {
                if (((ddlLocationOfOperation.selectedIndex) == 0) || ((ddlLocationOfOperation.selectedIndex) == -1))
                {
                    PreventPostBack(event);
                    alert('Please ensure you specify the location');
                    ddlLocationOfOperation.focus();
                    return;
                }
            }
        }

    </script>
</asp:Content>
<asp:Content ID="Main" ContentPlaceHolderID="PlaceHolderMain" runat="server">
    <div>
        <table class="Fill">
            <tr>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <div style="padding-left: 5%; padding-right: 5%">
                        <div>
                            <table class="Fill">
                                <tr>
                                    <td>
                                        <asp:CheckBox ID="cbAdvertisementDuration" runat="server" />
                                    </td>
                                    <td>
                                        published date
                                    </td>
                                    <td>
                                        <SharePoint:InputFormTextBox ID="iftStartAdvertisementDate" runat="server" ToolTip="specify the start date for advertising."
                                            ControlToValidate="iftStartAdvertisementDate" contentEditable="false" CssClass="ms-input"
                                            ValidationGroup="ValidateVacancy"></SharePoint:InputFormTextBox>
                                        <cc1:CalendarExtender ID="calendarExtStartAvertisementDate" TargetControlID="iftStartAdvertisementDate"
                                            PopupButtonID="imgBtnStartAdvertisementDate" runat="server">
                                        </cc1:CalendarExtender>
                                        <span class="RequiredIndicator">*</span><asp:ImageButton ID="imgBtnStartAdvertisementDate"
                                            runat="server" ImageUrl="~/_layouts/images/CALENDAR.GIF" ImageAlign="Middle" />
                                    </td>
                                    <td>
                                        expiry date
                                    </td>
                                    <td>
                                        <SharePoint:InputFormTextBox ID="iftEndAdvertisementDate" runat="server" contentEditable="false"
                                            ToolTip="specify the end date for advertising." CssClass="ms-input"></SharePoint:InputFormTextBox>
                                        <span class="RequiredIndicator">*</span><cc1:CalendarExtender ID="calendarExtEndAvertisementDate"
                                            TargetControlID="iftEndAdvertisementDate" PopupButtonID="imgBtnEndAdvertisementDate"
                                            runat="server" >
                                        </cc1:CalendarExtender>
                                        <asp:ImageButton ID="imgBtnEndAdvertisementDate" runat="server" ImageUrl="~/_layouts/images/CALENDAR.GIF" ImageAlign="Middle" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div>
                            <table style="margin: 0px auto;">
                                <tr>
                                    <td>
                                        <asp:CheckBox ID="cbRequester" runat="server" />
                                    </td>
                                    <td>
                                        Hiring Manager
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlRequester" runat="server" CssClass="DropDownListStyle">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="cbPublisher" runat="server" />
                                    </td>
                                    <td>
                                        Publisher
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlPublisher" runat="server" CssClass="DropDownListStyle">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:CheckBox ID="cbDivision" runat="server" />
                                    </td>
                                    <td>
                                        Division
                                    </td>
                                    <td>
                                        <asp:UpdatePanel ID="upDivision" runat="server">
                                            <ContentTemplate>
                                                <asp:DropDownList ID="ddlDivision" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlDivision_SelectedIndexChanged"
                                                    CssClass="DropDownListStyle">
                                                </asp:DropDownList>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="cbDepartment" runat="server" />
                                    </td>
                                    <td>
                                        Department
                                    </td>
                                    <td>
                                        <asp:UpdatePanel ID="upDepartment" runat="server">
                                            <ContentTemplate>
                                                <asp:DropDownList ID="ddlDepartment" runat="server" CssClass="DropDownListStyle">
                                                </asp:DropDownList>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:CheckBox ID="cbStaffLevel" runat="server" />
                                    </td>
                                    <td>
                                        Staff Level
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlStaffLevel" runat="server" CssClass="DropDownListStyle">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="cbGender" runat="server" />
                                    </td>
                                    <td>
                                        Gender
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlGender" runat="server" CssClass="DropDownListStyle">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:CheckBox ID="cbStateOfOperation" runat="server" />
                                    </td>
                                    <td>
                                        State
                                    </td>
                                    <td>
                                        <asp:UpdatePanel ID="upStateOfOperation" runat="server">
                                            <ContentTemplate>
                                                <asp:DropDownList ID="ddlStateOfOperation" AutoPostBack="true" OnSelectedIndexChanged="ddlStateOfOperation_SelectedIndexChanged"
                                                    runat="server" CssClass="DropDownListStyle">
                                                </asp:DropDownList>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="cbLocationOfOperation" runat="server" />
                                    </td>
                                    <td>
                                        Location
                                    </td>
                                    <td>
                                        <asp:UpdatePanel ID="upLocationOfOperation" runat="server">
                                            <ContentTemplate>
                                                <asp:DropDownList ID="ddlLocationOfOperation" runat="server" CssClass="DropDownListStyle">
                                                </asp:DropDownList>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        <asp:Button ID="btnGenerateReport" runat="server" OnClientClick="HandleGenerateReportInitiation(event)" OnClick="btnGenerateReport_Click"
                                            Text="Generate Report" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <div>
                        <rsweb:ReportViewer ID="rptVwrVacancy" runat="server" Font-Names="Verdana" Font-Size="8pt"
                            Height="1000px" InteractiveDeviceInfos="(Collection)" WaitMessageFont-Names="Verdana"
                            WaitMessageFont-Size="14pt" Width="100%">
                            <LocalReport>
                                <DataSources>
                                    <rsweb:ReportDataSource DataSourceId="ObjectDataSource1" Name="VacancyReportDataSet" />
                                </DataSources>
                            </LocalReport>
                        </rsweb:ReportViewer>
                        <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" SelectMethod="GetData"
                            TypeName="MTNNGCareers.Administration.Forms.MTNNGCareersReportDataSourceTableAdapters.RetrieveVacancyReportTableAdapter"
                            OnSelecting="ObjectDataSource1_Selecting" OldValuesParameterFormatString="{0}">
                            <SelectParameters>
                                <asp:Parameter DefaultValue="" Name="Requester" Type="Int32" />
                                <asp:Parameter DefaultValue="" Name="Publisher" Type="Int32" />
                                <asp:Parameter DefaultValue="" Name="Division" Type="Byte" />
                                <asp:Parameter DefaultValue="" Name="Department" Type="Byte" />
                                <asp:Parameter DefaultValue="" Name="StaffLevel" Type="String" />
                                <asp:Parameter DefaultValue="" Name="Gender" Type="String" />
                                <asp:Parameter DefaultValue="" Name="StateOfOperationID" Type="Byte" />
                                <asp:Parameter DefaultValue="" Name="LocationOfOperationID" Type="Int32" />
                                <asp:Parameter Name="StartAdvertisementDate" Type="DateTime" DefaultValue="" />
                                <asp:Parameter Name="EndAdvertisementDate" Type="DateTime" DefaultValue="" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
<asp:Content ID="PageTitle" ContentPlaceHolderID="PlaceHolderPageTitle" runat="server">
 MTN Nigeria External Vacancies Report
</asp:Content>
<asp:Content ID="PageTitleInTitleArea" ContentPlaceHolderID="PlaceHolderPageTitleInTitleArea"
    runat="server">
    MTN Nigeria External Vacancies Report
</asp:Content>

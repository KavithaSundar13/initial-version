﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Import Namespace="Microsoft.SharePoint.ApplicationPages" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Register Assembly="AjaxControlToolkit, Version=3.0.30930.28736, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e"
    Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI.WebControls" TagPrefix="asp" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NewVacancy.aspx.cs" Inherits="MTNNGCareers.Administration.Forms.NewVacancy"
    DynamicMasterPageFile="~masterurl/default.master" %>

<asp:Content ID="PageHead" ContentPlaceHolderID="PlaceHolderAdditionalPageHead" runat="server">
    <script type="text/javascript">
        var hfBreakDownCountClientID = '<% =hfBreakDownCount.ClientID %>';

        var lnkBtnGeneralInformationClientID = '<% =lnkBtnGeneralInformation.ClientID %>';
        var lnkBtnJobDescriptionClientID = '<% =lnkBtnJobDescription.ClientID %>';
        var lnkBtnJobConditionClientID = '<% =lnkBtnJobCondition.ClientID %>';
        var lnkBtnSkillsRequiredClientID = '<% =lnkBtnSkillsRequired.ClientID %>';
        var lnkBtnQualificationsClientID = '<% =lnkBtnQualifications.ClientID %>';
        var lnkBtnPreferencesClientID = '<% =lnkBtnPreferences.ClientID %>';
        var lnkBtnBreakDownClientID = '<% =lnkBtnBreakDown.ClientID %>';

        var divGeneralInformationClientID = '<% =divGeneralInformation.ClientID %>';
        var divJobDescriptionClientID = '<% =divJobDescription.ClientID %>';
        var divJobDescriptionLtrClientID = '<% =divJobDescriptionLtr.ClientID %>';
        var divConditionsClientID = '<% =divConditions.ClientID %>';
        var divConditionsLiteralClientID = '<% =divConditionsLiteral.ClientID %>';
        var divSkillsRequiredClientID = '<% =divSkillsRequired.ClientID %>';
        var divSkillsRequiredLiteralClientID = '<% =divSkillsRequiredLiteral.ClientID %>';
        var divPreferencesClientID = '<% =divPreferences.ClientID %>';
        var divQualificationsClientID = '<% =divQualifications.ClientID %>';
        var divBreakDownClientID = '<% =divBreakDown.ClientID %>';

        var ltrJobDescriptionClientID = '<% =ltrJobDescription.ClientID %>';
        var ltrJobConditionsClientID = '<% =ltrJobConditions.ClientID %>';
        var ltrSkillsRequiredClientID = '<% =ltrSkillsRequired.ClientID %>';

        var lnkBtnEDITJobDescriptionClientID = '<% =lnkBtnEDITJobDescription.ClientID %>';
        var lnkBtnEDITJobConditionsClientID = '<% =lnkBtnEDITJobConditions.ClientID %>';
        var lnkBtnEDITSkillsRequiredClientID = '<% =lnkBtnEDITSkillsRequired.ClientID %>';

        var ddlDivisionClientID = '<% =ddlDivision.ClientID %>';

        var iftJobDescriptionClientID = '<% =iftJobDescription.ClientID %>';
        var iftJobConditionsClientID = '<% =iftJobConditions.ClientID %>';
        var iftJobSkillsRequiredClientID = '<% =iftJobSkillsRequired.ClientID %>';

        var iftStartAdvertisementDateClientID = '<% =iftStartAdvertisementDate.ClientID %>';
        var iftEndAdvertisementDateClientID = '<% =iftEndAdvertisementDate.ClientID %>';
    </script>
    <script type="text/javascript" src="/HR/_layouts/MTNNGCareersAdminForms/javascript/Common.js"></script>
    <script type="text/javascript" src="/HR/_layouts/MTNNGCareersAdminForms/javascript/NewVacancy.js"></script>
    <link type="text/css" rel="Stylesheet" href="/HR/_layouts/MTNNGCareersAdminForms/css/Common.css" />
    <link type="text/css" rel="Stylesheet" href="/HR/_layouts/MTNNGCareersAdminForms/css/NewVacancy.css" />
</asp:Content>
<asp:Content ID="Main" ContentPlaceHolderID="PlaceHolderMain" runat="server">
    <asp:HiddenField runat="server" ID="hfBreakDownCount" />
    <div>
        <table class="TabStyle-i">
            <tr>
                <td class="CareersTab">
                    <asp:LinkButton ID="lnkBtnGeneralInformation" runat="server" CssClass="LinkButtonStyle"
                        ToolTip="click here to specify General Information about the Vacancy you wish to publish"
                        OnClientClick="HandleContentToPresent(event)">General Information</asp:LinkButton>
                </td>
                <td class="CareersTab">
                    <asp:LinkButton ID="lnkBtnBreakDown" runat="server" CssClass="LinkButtonStyle" ToolTip="click here to specify the no. of openings, location, e.t.c."
                        OnClientClick="HandleContentToPresent(event)" Visible="false">Break down</asp:LinkButton>
                </td>
                <td class="CareersTab">
                    <asp:LinkButton ID="lnkBtnJobDescription" runat="server" CssClass="LinkButtonStyle"
                        ToolTip="click here to specify the description of the job's vacancy" OnClientClick="HandleContentToPresent(event)">Job Description</asp:LinkButton>
                </td>
                <td class="CareersTab">
                    <asp:LinkButton ID="lnkBtnJobCondition" runat="server" CssClass="LinkButtonStyle"
                        ToolTip="click here to specify the condition of the job's vacancy" OnClientClick="HandleContentToPresent(event)">Job Condition</asp:LinkButton>
                </td>
                <td class="CareersTab">
                    <asp:LinkButton ID="lnkBtnSkillsRequired" runat="server" CssClass="LinkButtonStyle"
                        ToolTip="click here to specify expected experiences and training" OnClientClick="HandleContentToPresent(event)">Experience & Training</asp:LinkButton>
                </td>
                <td class="CareersTab">
                    <asp:LinkButton ID="lnkBtnQualifications" runat="server" CssClass="LinkButtonStyle"
                        ToolTip="click here to specify General Information about the Vacancy you wish to publish"
                        OnClientClick="HandleContentToPresent(event)">Qualifications</asp:LinkButton>
                </td>
                <td class="CareersTab">
                    <asp:LinkButton ID="lnkBtnPreferences" runat="server" CssClass="LinkButtonStyle"
                        ToolTip="click here to specify General Information about the Vacancy you wish to publish"
                        OnClientClick="HandleContentToPresent(event)">Preferences</asp:LinkButton>
                </td>
            </tr>
        </table>
    </div>
    <div id="divGeneralInformation" runat="server" style="display: block;">
        <table class="TableStyle">
            <tr>
                <td class="FieldName">
                    Job Title <span class="RequiredIndicator">*</span>
                </td>
                <td class="FieldValue">
                    <SharePoint:InputFormTextBox ID="iftJobTitle" runat="server" CssClass="iftJobTitle"
                        ValidationGroup="Vacancy"></SharePoint:InputFormTextBox>
                    <SharePoint:InputFormRequiredFieldValidator ID="ifrvJobTitle" runat="server" ControlToValidate="iftJobTitle"
                        Font-Names="Arial" Font-Size="X-Small" ValidationGroup="Vacancy" ErrorMessage="REQUIRED"><br /><span role="alert"></span></SharePoint:InputFormRequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="FieldName">
                    Division <span class="RequiredIndicator">*</span>
                </td>
                <td class="FieldValue">
                    <asp:DropDownList ID="ddlDivision" runat="server" AutoPostBack="true" OnSelectedIndexChanged="Handle_ddlDivision_OnSelectedIndexChanged"
                        class="DropDownListStyle" ValidationGroup="Vacancy">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="rfvDivision" runat="server" ControlToValidate="ddlDivision"
                        ErrorMessage="RequiredFieldValidator" Font-Names="Arial" Font-Size="X-Small"
                        SetFocusOnError="True" ValidationGroup="Vacancy">REQUIRED</asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="FieldName">
                    Staff Level <span class="RequiredIndicator">*</span>
                </td>
                <td class="FieldValue">
                    <asp:RadioButtonList ID="rblStaffLevel" runat="server" RepeatDirection="Horizontal"
                        class="RadioButtonListStyle" ValidationGroup="Vacancy">
                        <asp:ListItem>level 1</asp:ListItem>
                        <asp:ListItem>level 2</asp:ListItem>
                        <asp:ListItem>level 3</asp:ListItem>
                        <asp:ListItem>level 3H</asp:ListItem>
                        <asp:ListItem>level 4</asp:ListItem>
                        <asp:ListItem>level 5</asp:ListItem>
                        <asp:ListItem Enabled="false" Value="NOT SPECIFIED">NOT SPECIFIED</asp:ListItem>
                    </asp:RadioButtonList>
                    <asp:RequiredFieldValidator ID="rfvStaffLevel" runat="server" ControlToValidate="rblStaffLevel"
                        ErrorMessage="RequiredFieldValidator" Font-Names="Arial" Font-Size="X-Small"
                        SetFocusOnError="True" ValidationGroup="Vacancy">REQUIRED</asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="FieldName">
                    Is NYSC Required <span class="RequiredIndicator">*</span>
                </td>
                <td class="FieldValue">
                    <asp:RadioButtonList ID="rblIsNYSCRequired" runat="server" RepeatDirection="Horizontal"
                        class="RadioButtonListStyle" ValidationGroup="Vacancy">
                        <asp:ListItem Value="1">yes</asp:ListItem>
                        <asp:ListItem Value="0">no</asp:ListItem>
                    </asp:RadioButtonList>
                    <asp:RequiredFieldValidator ID="rfvIsNYSCRequired" runat="server" ControlToValidate="rblIsNYSCRequired"
                        ErrorMessage="RequiredFieldValidator" Font-Names="Arial" Font-Size="X-Small"
                        SetFocusOnError="True" ValidationGroup="Vacancy">REQUIRED</asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="FieldName">
                    Start Advertisement Date <span class="RequiredIndicator">*</span>
                </td>
                <td class="FieldValue">
                    <SharePoint:InputFormTextBox ID="iftStartAdvertisementDate" runat="server" ToolTip="specify the start date for advertising."
                        ControlToValidate="iftStartAdvertisementDate" contentEditable="false" CssClass="iftDate"
                        ValidationGroup="Vacancy"></SharePoint:InputFormTextBox>
                    <cc1:CalendarExtender ID="calendarExtStartAvertisementDate" TargetControlID="iftStartAdvertisementDate"
                        PopupButtonID="imgBtnStartAdvertisementDate" runat="server" OnClientDateSelectionChanged="ValidateAdvertisementStartDate">
                    </cc1:CalendarExtender>
                    <asp:ImageButton ID="imgBtnStartAdvertisementDate" runat="server" ImageUrl="~/_layouts/images/CALENDAR.GIF"
                        ImageAlign="Middle" />
                    <SharePoint:InputFormRequiredFieldValidator ID="ifrvStartAdvertisementDate" runat="server"
                        ControlToValidate="iftStartAdvertisementDate" Font-Names="Arial" Font-Size="X-Small"
                        ValidationGroup="Vacancy" ErrorMessage="REQUIRED"><br /><span role="alert">REQUIRED</span></SharePoint:InputFormRequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="FieldName">
                    End Avertisement Date <span class="RequiredIndicator">*</span>
                </td>
                <td class="FieldValue">
                    <SharePoint:InputFormTextBox ID="iftEndAdvertisementDate" runat="server" contentEditable="false"
                        ToolTip="specify the end date for advertising." CssClass="iftDate" ValidationGroup="Vacancy"></SharePoint:InputFormTextBox>
                    <cc1:CalendarExtender ID="calendarExtEndAvertisementDate" TargetControlID="iftEndAdvertisementDate"
                        PopupButtonID="imgBtnEndAdvertisementDate" runat="server" OnClientDateSelectionChanged="ValidateAdvertisementEndDate">
                    </cc1:CalendarExtender>
                    <asp:ImageButton ID="imgBtnEndAdvertisementDate" runat="server" ImageUrl="~/_layouts/images/CALENDAR.GIF"
                        ImageAlign="Middle" />
                    <SharePoint:InputFormRequiredFieldValidator ID="ifrvEndAdvertisement" runat="server"
                        ControlToValidate="iftEndAdvertisementDate" Font-Names="Arial" Font-Size="X-Small"
                        ValidationGroup="Vacancy" ErrorMessage="REQUIRED"><br /><span role="alert">REQUIRED</span></SharePoint:InputFormRequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="FieldName">
                    Hiring Manager <span class="RequiredIndicator">*</span>
                </td>
                <td class="FieldValue">
                    <SharePoint:PeopleEditor ID="peRequester" runat="server" MultiSelect="false" />
                </td>
            </tr>
            <tr>
                <td class="FieldName">
                    Hiring Manager Position <span class="RequiredIndicator">*</span>
                </td>
                <td class="FieldValue">
                    <SharePoint:InputFormTextBox ID="iftRequesterPosition" runat="server" CssClass="iftRequesterPosition"
                        ValidationGroup="Vacancy"></SharePoint:InputFormTextBox>
                    <SharePoint:InputFormRequiredFieldValidator ID="ifrvPosition" runat="server" ControlToValidate="iftRequesterPosition"
                        Font-Names="Arial" Font-Size="X-Small" ValidationGroup="Vacancy" ErrorMessage="REQUIRED"><br /><span role="alert">REQUIRED</span></SharePoint:InputFormRequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="FieldName">
                    Business Partner <span class="RequiredIndicator">*</span>
                </td>
                <td class="FieldValue">
                    <SharePoint:PeopleEditor ID="peBusinessPartner" runat="server" MultiSelect="false" />
                </td>
            </tr>
            <tr>
                <td class="FieldName">
                    H.R Advisor <span class="RequiredIndicator">*</span>
                </td>
                <td class="FieldValue">
                    <SharePoint:PeopleEditor ID="peHRAdvisor" runat="server" MultiSelect="false" />
                </td>
            </tr>
        </table>
    </div>
    <div>
    </div>
    <div id="divBreakDown" runat="server" style="display: none;">
        <asp:ListView ID="lv_VacancyBreakDown" runat="server" InsertItemPosition="LastItem"
            OnItemInserting="Handle_lv_VacancyBreakDown_OnItemInserting" OnItemInserted="Handle_lv_VacancyBreakDown_OnItemInserted"
            OnItemEditing="Handle_lv_VacancyBreakDown_OnItemEditing" OnItemUpdating="Handle_lv_VacancyBreakDown_OnItemUpdating"
            OnItemDeleting="Handle_lv_VacancyBreakDown_OnItemDeleting" OnItemCanceling="Handle_lv_VacancyBreakDown_OnItemCancelling">
            <LayoutTemplate>
                <table class="mtn_careers_form">
                    <tr class="yellow_header">
                        <td style="width: 10%; text-align: center;">
                            no. of openings
                        </td>
                        <td>
                            status
                        </td>
                        <td>
                            department
                        </td>
                        <td>
                            unit
                        </td>
                        <td>
                            state
                        </td>
                        <td>
                            location
                        </td>
                    </tr>
                    <tr id="itemPlaceholder" runat="server">
                    </tr>
                </table>
            </LayoutTemplate>
            <ItemTemplate>
                <tr>
                    <td>
                        <asp:Label ID="lbNoOfOpenings_ItemTemplate" runat="server" Text='<%# Eval("noOfOpenings") %>' style="text-align: center;" />
                        <%-- Text='<%# Quantity %>' --%>
                    </td>
                    <td>
                        <asp:Label ID="lbStatus_ItemTemplate" runat="server" Text='<%# Eval("Status") %>' />
                        <%-- Text='<%# Status %>' --%>
                    </td>
                    <td>
                        <asp:Label ID="lbDepartment_ItemTemplate" runat="server" Text='<%# Eval("Department") %>' />
                        <%-- Text='<%# Department %>' --%>
                    </td>
                    <td>
                        <asp:Label ID="lbUnit_ItemTemplate" runat="server" Text='<%# Eval("Unit") %>' />
                        <%-- Text='<%# Unit %>' --%>
                    </td>
                    <td>
                        <asp:Label ID="lbState_ItemTemplate" runat="server" Text='<%# Eval("State") %>' />
                        <%-- Text='<%# State %>' --%>
                    </td>
                    <td>
                        <asp:Label ID="lbLocation_ItemTemplate" runat="server" Text='<%# Eval("Location") %>' />
                        <%-- Text='<%# Location %>' --%>
                    </td>
                    <td>
                        <asp:Button ID="btnEDIT_ItemTemplate" runat="server" CommandName="Edit" Text="edit"
                            CssClass="MyButton" />
                    </td>
                    <td>
                        <asp:Button ID="btnDELETE_ItemTemplate" runat="server" OnClientClick="Handle_btnDELETE_onclick(event)"
                            CommandName="Delete" Text="delete" CssClass="MyButton" />
                    </td>
                </tr>
            </ItemTemplate>
            <AlternatingItemTemplate>
                <tr class="gray">
                    <td>
                        <asp:Label ID="lbNoOfOpenings_AlternatingItemTemplate" runat="server" Text='<%# Eval("noOfOpenings") %>' />
                        <%-- Text='<%# Quantity %>' --%>
                    </td>
                    <td>
                        <asp:Label ID="lbStatus_AlternatingItemTemplate" runat="server" Text='<%# Eval("Status") %>' />
                        <%-- Text='<%# Status %>' --%>
                    </td>
                    <td>
                        <asp:Label ID="lbDepartment_AlternatingItemTemplate" runat="server" Text='<%# Eval("Department") %>' />
                        <%-- Text='<%# Department %>'--%>
                    </td>
                    <td>
                        <asp:Label ID="lbUnit_AlternatingItemTemplate" runat="server" Text='<%# Eval("Unit") %>' />
                        <%-- Text='<%# Unit %>' --%>
                    </td>
                    <td>
                        <asp:Label ID="lbState_AlternatingItemTemplate" runat="server" Text='<%# Eval("State") %>' />
                        <%-- Text='<%# State %>' --%>
                    </td>
                    <td>
                        <asp:Label ID="lbLocation_AlternatingItemTemplate" runat="server" Text='<%# Eval("Location") %>' />
                        <%-- Text='<%# Location %>' --%>
                    </td>
                    <td>
                        <asp:Button ID="btnEDIT_AlternatingItemTemplate" runat="server" CommandName="Edit"
                            Text="edit" CssClass="MyButton" />
                    </td>
                    <td>
                        <asp:Button ID="btnDELETE_AlternatingItemTemplate" runat="server" OnClientClick="Handle_btnDELETE_onclick(event)"
                            CommandName="Delete" Text="delete" CssClass="MyButton" />
                    </td>
                </tr>
            </AlternatingItemTemplate>
            <EditItemTemplate>
                <tr class="edit">
                    <td>
                        <SharePoint:InputFormTextBox ID="txtNoOfOpenings_EditTemplate" runat="server" Text='<%# Bind("noOfOpenings") %>'
                            CssClass="txtNoOfOpeningsStyle"></SharePoint:InputFormTextBox>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlStatus_EditTemplate" runat="server" CssClass="DropDownListStyle"
                            Text='<%# Bind("Status") %>'>
                            <asp:ListItem>permanent</asp:ListItem>
                            <asp:ListItem>contract</asp:ListItem>
                            <asp:ListItem>consultant</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlDepartment_EditTemplate" runat="server" AutoPostBack="true"
                            OnSelectedIndexChanged="Handle_ddlDepartment_OnSelectedIndexChanged" CssClass="DropDownListStyle" />
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlUnit_EditTemplate" runat="server" CssClass="DropDownListStyle" />
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlState_EditTemplate" runat="server" AutoPostBack="true" OnSelectedIndexChanged="Handle_ddlState_OnSelectedIndexChanged"
                            CssClass="DropDownListStyle" />
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlLocation_EditTemplate" runat="server" CssClass="DropDownListStyle" />
                    </td>
                    <td>
                        <asp:Button ID="btnUpdate_EditTemplate" runat="server" CommandName="Update" Text="update"
                            CssClass="MyButton" />
                    </td>
                    <td>
                        <asp:Button ID="btnCANCEL_EditTemplate" runat="server" CommandName="Cancel" Text="cancel"
                            CssClass="MyButton" />
                    </td>
                </tr>
            </EditItemTemplate>
            <InsertItemTemplate>
                <tr>
                    <td>
                        <SharePoint:InputFormTextBox ID="txtNoOfOpenings_InsertTemplate" runat="server" CssClass="txtNoOfOpeningsStyle"></SharePoint:InputFormTextBox>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlStatus_InsertTemplate" runat="server" CssClass="DropDownListStyle">
                            <asp:ListItem>permanent</asp:ListItem>
                            <asp:ListItem>contract</asp:ListItem>
                            <asp:ListItem>consultant</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlDepartment_InsertTemplate" runat="server" AutoPostBack="true"
                            OnSelectedIndexChanged="Handle_ddlDepartment_OnSelectedIndexChanged" CssClass="DropDownListStyle" />
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlUnit_InsertTemplate" runat="server" CssClass="DropDownListStyle" />
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlState_InsertTemplate" runat="server" AutoPostBack="true"
                            OnSelectedIndexChanged="Handle_ddlState_OnSelectedIndexChanged" CssClass="DropDownListStyle" />
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlLocation_InsertTemplate" runat="server" CssClass="DropDownListStyle" />
                    </td>
                    <td>
                        <asp:Button ID="btnINSERT" runat="server" OnClientClick="Handle_btnINSERT_onclick(event)"
                            Text="add" CommandName="Insert" CssClass="MyButton" />
                    </td>
                    <td>
                        <asp:Button ID="btnCANCEL_InsertItemTemplate" runat="server" Text="cancel" CssClass="MyButton" />
                    </td>
                </tr>
            </InsertItemTemplate>
        </asp:ListView>
    </div>
    <div>
    </div>
    <div id="divJobDescription" runat="server" style="display: none;">
        <table class="TableStyle">
            <tr>
                <td class="FieldName" style="vertical-align: top;">
                    Job Description <span class="RequiredIndicator">*</span>
                </td>
                <td class="FieldValue">
                    <SharePoint:InputFormTextBox ID="iftJobDescription" runat="server" Columns="50" RichText="True"
                        Rows="30" TextMode="MultiLine"></SharePoint:InputFormTextBox>
                </td>
            </tr>
        </table>
    </div>
    <div>
    </div>
    <div id="divJobDescriptionLtr" runat="server" style="display: none;">
        <table class="TableStyle">
            <tr>
                <td class="FieldName" style="vertical-align: top;">
                    Job Description
                    <br />
                    <asp:LinkButton ID="lnkBtnEDITJobDescription" runat="server" CssClass="LinkButtonStyle"
                        ToolTip="click here to edit Job Description." OnClientClick="HandleEditInitiation(event)">EDIT</asp:LinkButton>
                </td>
                <td id="td_JobDescription" class="FieldValue">
                    <asp:Literal ID="ltrJobDescription" runat="server"></asp:Literal>
                </td>
            </tr>
        </table>
    </div>
    <div>
    </div>
    <div id="divConditions" runat="server" style="display: none;">
        <table class="TableStyle">
            <tr>
                <td class="FieldName" style="vertical-align: top;">
                    Job Conditions <span class="RequiredIndicator">*</span>
                </td>
                <td class="FieldValue">
                    <SharePoint:InputFormTextBox ID="iftJobConditions" runat="server" Columns="50" RichText="True"
                        Rows="30" TextMode="MultiLine"></SharePoint:InputFormTextBox>
                </td>
            </tr>
        </table>
    </div>
    <div>
    </div>
    <div id="divConditionsLiteral" runat="server" style="display: none;">
        <table class="TableStyle">
            <tr>
                <td class="FieldName" style="vertical-align: top;">
                    Job Conditions
                    <br />
                    <asp:LinkButton ID="lnkBtnEDITJobConditions" runat="server" CssClass="LinkButtonStyle"
                        ToolTip="click here to edit Job Conditions." OnClientClick="HandleEditInitiation(event)">EDIT</asp:LinkButton>
                </td>
                <td id="td_JobConditions" class="FieldValue">
                    <asp:Literal ID="ltrJobConditions" runat="server"></asp:Literal>
                    <br />
                </td>
            </tr>
        </table>
    </div>
    <div>
    </div>
    <div id="divSkillsRequired" runat="server" style="display: none;">
        <table class="TableStyle">
            <tr>
                <td class="FieldName" style="vertical-align: top;">
                    Experience & Training <span class="RequiredIndicator">*</span>
                </td>
                <td class="FieldValue">
                    <SharePoint:InputFormTextBox ID="iftJobSkillsRequired" runat="server" Columns="50"
                        RichText="True" Rows="30" TextMode="MultiLine"></SharePoint:InputFormTextBox>
                </td>
            </tr>
        </table>
    </div>
    <div>
    </div>
    <div id="divSkillsRequiredLiteral" runat="server" style="display: none;">
        <table class="TableStyle">
            <tr>
                <td class="FieldName" style="vertical-align: top;">
                    Skills Required
                    <br />
                    <asp:LinkButton ID="lnkBtnEDITSkillsRequired" runat="server" CssClass="LinkButtonStyle"
                        ToolTip="click here to edit Experience(s) & Training." OnClientClick="HandleEditInitiation(event)">EDIT</asp:LinkButton>
                </td>
                <td id="td_SkillsRequired" class="FieldValue">
                    <asp:Literal ID="ltrSkillsRequired" runat="server"></asp:Literal>
                    <br />
                </td>
            </tr>
        </table>
    </div>
    <div>
    </div>
    <div id="divQualifications" runat="server" style="display: none;">
        <table class="TableStyle">
            <tr>
                <td class="FieldName">
                Minimum Qualifications <span class="RequiredIndicator">*</span>
                </td>
                <td class="FieldValue">
                   
                <!--added content -->
                
        <asp:UpdatePanel ID="UpdatePanelMMQualification" runat="server">
            <ContentTemplate>
                 <table style="width:100%;">
            <tr>
                <td class="auto-style1">
                    <table style="width:100%;">
                        <tr>
                            <td class="auto-style2">
                               
                                <asp:DropDownList ID="ddlMinimumQualification" runat="server" CssClass="DropDownListStyle" Width="150px">
                    </asp:DropDownList>
                    

                            </td>
                            <td>
                                <asp:Button ID="BtnAddQualification" runat="server" OnClick="AddMMQualification_Click" Text="Add Qualification" CssClass="MyButton" />
                            </td>
                        </tr>
                    </table>
                </td>
                <td>
                    <table style="width:100%;">
                        <tr>
                            <td class="auto-style3">
                                <asp:ListBox ID="LBMinimumQualification" runat="server"  Width="200px" SelectionMode="Multiple"></asp:ListBox>
                                    <asp:RequiredFieldValidator ID="rfvMinimumQualification" runat="server" ControlToValidate="LBMinimumQualification"
                         ErrorMessage="RequiredFieldValidator" Font-Names="Arial"
                        Font-Size="X-Small" SetFocusOnError="True" ValidationGroup="Vacancy">REQUIRED</asp:RequiredFieldValidator>
                        
                            </td>
                            <td>
                                <asp:Button ID="BtnRemoveQualification" runat="server" OnClick="RemoveMMMQualification_Click" style="margin-left: 0px" Text="Remove Qualification" CssClass="MyButton" />
                               
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
         </ContentTemplate>
        </asp:UpdatePanel>


                <!--end Content-->
                
                
                </td>
            </tr>
            <tr>
                <td class="FieldName" style="vertical-align: top;">
                    Professional Qualifications
                </td>
                <td class="FieldValue">
                    <SharePoint:InputFormTextBox ID="iftProfessionalQualifcations" runat="server" Columns="50"
                        Rows="10" TextMode="MultiLine"></SharePoint:InputFormTextBox>
                </td>
            </tr>
            <tr>
                <td class="FieldName" style="vertical-align: top;">
                    Additional Qualifications
                </td>
                <td class="FieldValue">
                    <SharePoint:InputFormTextBox ID="iftAdditionalQualifcations" runat="server" Columns="50"
                        Rows="10" TextMode="MultiLine"></SharePoint:InputFormTextBox>
                </td>
            </tr>
        </table>
    </div>
    <div>
    </div>
    <div id="divPreferences" runat="server" style="display: none;">
        <table class="TableStyle">
            <tr>
                <td class="FieldName">
                    Course of Study
                </td>
                <td class="FieldValue">
                    <SharePoint:InputFormTextBox ID="iftCourseOfStudy" runat="server" CssClass="iftCourseOfStudy"></SharePoint:InputFormTextBox>
                </td>
            </tr>
            <tr>
                <td class="FieldName">
                    Class of Degree
                </td>
                <td class="FieldValue">
                    <asp:DropDownList ID="ddlClassOfDegree" runat="server" class="DropDownListStyle">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="FieldName">
                    Post Degree Years of Experience
                </td>
                <td class="FieldValue">
                    <SharePoint:InputFormTextBox ID="iftPostDegreeYearsOfExperience" runat="server" CssClass="iftPostDegreeYearsOfExperience"></SharePoint:InputFormTextBox>
                </td>
            </tr>
            <tr style="display: none;">
                <td class="FieldName">
                    Marital Status
                </td>
                <td class="FieldValue">
                    <asp:RadioButtonList ID="rblMaritalStatus" runat="server" RepeatDirection="Horizontal"
                        class="RadioButtonListStyle" CssClass="RadioButtonListStyle">
                        <asp:ListItem>single</asp:ListItem>
                        <asp:ListItem>married</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td class="FieldName">
                    Gender
                </td>
                <td class="FieldValue">
                    <asp:RadioButtonList ID="rblGender" runat="server" RepeatDirection="Horizontal" class="RadioButtonListStyle"
                        CssClass="RadioButtonListStyle">
                        <asp:ListItem>male</asp:ListItem>
                        <asp:ListItem>female</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td class="FieldName">
                </td>
                <td class="FieldValue">
                    <asp:Button ID="btnUPDATE" runat="server" Visible="false" OnClientClick="HandlePublishInitiationOnClient(event)"
                        OnClick="HandlePublishInitiation" ValidationGroup="Vacancy" Text="UPDATE" CssClass="MyButton"
                        ToolTip="click here to update vacancy." />
                    <asp:Button ID="btnPUBLISH" runat="server" OnClientClick="HandlePublishInitiationOnClient(event)"
                        OnClick="HandlePublishInitiation" ValidationGroup="Vacancy" Text="publish" CssClass="MyButton" />
                </td>
            </tr>
        </table>
        <div>
            <%--<asp:Button ID="btnPUBLISH" runat="server" OnClientClick="HandlePublishInitiationOnClient(event)"
                OnClick="HandlePublishInitiation" ValidationGroup="Vacancy" Text="publish" CssClass="MyButton" />--%>
        </div>
    </div>
</asp:Content>
<asp:Content ID="PageTitle" ContentPlaceHolderID="PlaceHolderPageTitle" runat="server">
    New Vacancy Form
</asp:Content>
<asp:Content ID="PageTitleInTitleArea" ContentPlaceHolderID="PlaceHolderPageTitleInTitleArea"
    runat="server">
    New Vacancy Form
</asp:Content>

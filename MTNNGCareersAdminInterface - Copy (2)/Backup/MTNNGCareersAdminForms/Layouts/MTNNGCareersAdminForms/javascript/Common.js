﻿var isVacanyValid = true;

var VisibleClassStyle = 'Visible';
var InvisibleClassStyle = 'Invisible';
var VisibleTableRowClassStyle = 'VisibleTableRow';

var BlockDisplayStyle = 'block';
var TableRowDisplayStyle = 'table-row';
var NoneDisplayStyle = 'none';

var selectOptionText = '---select---';
var emptyDataObjectIndicator = '-----';
var _NOTSPECIFIED = 'NOT SPECIFIED';

// Routine to prevent post back to the web server.
function PreventPostBack(event)
{
    try
    {
        event.returnValue = false;
    }
    catch(e)
    {
        event.preventDefault();
    }
}

// Routine to get the source of an event.
function GetEventSource(event)
{
    var eventSource = null;
    try
    {
        eventSource = (event.srcElement);
    }
    catch (e)
    {
        eventSource = (event.target); 
    }

    return eventSource;
}

function GetBlockElementText(blockElement)
{
    var blockElementTextContent;

    try
    {
        blockElementTextContent = (blockElement.innerText);
    }
    catch (e)
    {
        blockElementTextContent = (blockElement.textContent);
    }

    return blockElementTextContent;
}

// Routine to validate People Picker Control entry s to whether an entry was made and validated.
function ValidatePeoplePicker(txtAreaForPeoplePicker, designation, td_PeoplePicker)
{
    var entry = (txtAreaForPeoplePicker.value);
    
    if (entry == '&#160;')
    {
        PreventPostBack(event);

        isVacanyValid = false;

        alert('Please, specify the ' + designation + '. If you have done so, ensure you validate.');
        td_PeoplePicker.focus();
    }
    else
    {
        // obtaining a reference to the parent element of the text area element.
        var txtAreaForPeoplePickerParentElement = (txtAreaForPeoplePicker.parentNode);
        var firstChild = ((txtAreaForPeoplePickerParentElement.childNodes).item(0));

        var childCount = ((firstChild.childNodes).length);
        for (var i = 0; (i < childCount); i++)
        {
            var child = ((firstChild.childNodes).item(i));

            if (child.tagName == 'SPAN')
            {
                var confirmatioNDIV = ((child.childNodes).item(0));
                var isResolved = (confirmatioNDIV.getAttribute('isresolved'));

                if (isResolved == 'False')
                {
                    PreventPostBack(event);

                    isVacanyValid = false;

                    alert('Invalid ' + designation);
                    td_PeoplePicker.focus();
                }
            }
        }
    }
}

function DisplayApplicantCV(event)
{
    var eventSource = GetEventSource(event);

    // to hold the cell that contains the source of the event.
    var eventSourceCell;
    // to hold the parent element(s) of the event source
    var eventSourceParentElement;

    eventSourceParentElement = (eventSource.parentElement);

    while ((eventSourceParentElement.tagName) != 'TD')
    {
        eventSourceParentElement = (eventSourceParentElement.parentElement);
    }

    eventSourceCell = eventSourceParentElement;

    var registrantKey = (eventSourceCell.title);

    window.open('ApplicantCV.aspx?RegistrantKey=' + registrantKey);

    PreventPostBack(event);
}
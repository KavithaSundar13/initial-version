﻿using System;

using System.Configuration;
using System.Web.Configuration;

using System.Data;
using System.Data.SqlClient;

using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;

using AjaxControlToolkit;

using MTNNGCareers.Administration.Common;
using SharePointListManager;
using CommonUtilities = Utilities.Common;

using Microsoft.SharePoint.Utilities;

namespace MTNNGCareers.Administration.Forms.ControlTemplates
{
    public partial class VacancyApplicantsViewer : UserControl
    {

        const string _ApplicantsView = "Applicant View";
        const string _ShortListedApplicantsView = "ShortListed Applicants View";

        string sVacancyID;
        string sVacancyBreakDownID;

        string sRegistrantKey;

        int shortListedVacancyApplicantCount = 0;

        int repeatCount = 0;

        SqlConnection connection;
        SqlCommand command;
        SqlDataAdapter dataAdapter;
        DataSet dataSet;

        int defaultShortListedApplicantsCount;

        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                CreateConnection();

                RetrieveDefaultShortListedApplicants();
            }
        }

        // Routine to create a connection to the MTN NG Careers DB.
        private void CreateConnection()
        {
            string connectionString = ((WebConfigurationManager.ConnectionStrings["MTNNGCareersDBConnectionString"]).ToString());
            connection = new SqlConnection(connectionString);
        }

        protected void gvVacanyApplicants_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow currentlyBoundRow = (e.Row);
            int rowIndex = (currentlyBoundRow.RowIndex);
            if (rowIndex > -1)
            {                
                TableCell applicantNameCell = (currentlyBoundRow.Cells[4]);

                LinkButton applicantNameLinkButton = ((LinkButton)applicantNameCell.Controls[0]);
                applicantNameLinkButton.ToolTip = "click here to view applicants c.v";

                if (defaultShortListedApplicantsCount > 0)
                {
                    CheckShortListedApplicants(currentlyBoundRow);
                }

                DataRowView dataRowView = ((DataRowView) (currentlyBoundRow.DataItem));
                DataRow record = (dataRowView.Row);
                bool isShortListed = Convert.ToBoolean(((record.ItemArray[7]).ToString()));

                if (isShortListed)
                {
                    string typeName = null;
                    CheckBox cb = null;

                    foreach (Control c in ((currentlyBoundRow.Cells[0]).Controls))
                    {
                        typeName = ((c.GetType()).Name);
                        if ((typeName.Equals("CheckBox")))
                        {
                            cb = ((CheckBox)c);
                            cb.Checked = true;

                            shortListedVacancyApplicantCount += 1;
                        }
                    }
                }
            }
        }

        protected void HandleApplicantNameLinkButtonOnClick(object sender, GridViewCommandEventArgs e)
        {   
            string sClickedRowIndex = ((e.CommandArgument).ToString());
            int iClickedRowIndex = (int.Parse(sClickedRowIndex));

            try
            {
                sVacancyID = (((gvVacanyApplicants.DataKeys[iClickedRowIndex]).Values[0]).ToString());
                sVacancyBreakDownID = (((gvVacanyApplicants.DataKeys[iClickedRowIndex]).Values[1]).ToString());

                sRegistrantKey = (((gvVacanyApplicants.DataKeys[iClickedRowIndex]).Values[2]).ToString());
                string vacancyApplicantCVUrl = ("~/_layouts/MTNNGCareersAdminForms/VacancyApplicantCVViewer.aspx?RegKey=" + sRegistrantKey);

                // obtaining a reference to the parent that would contain this control.
                Control thisControlContainer = (this.Parent);
                string containerTypeName = ((thisControlContainer.GetType()).Name);

                while (containerTypeName != "TabContainer")
                {
                    thisControlContainer = (thisControlContainer.Parent);
                    containerTypeName = ((thisControlContainer.GetType()).Name);
                }

                TabContainer tc = ((TabContainer)thisControlContainer);

                Control CVViewerCtrl = ((tc.Tabs[3]).FindControl("VacancyApplicantsCVViewer1"));

                DisplayRegistrant(CVViewerCtrl);

                Label lbDynamicPanelHeaderText = ((Label)tc.Tabs[3].FindControl("lbDynamicPanelHeaderText"));
                lbDynamicPanelHeaderText.Text = "CV";
                tc.Tabs[3].Visible = true;
                tc.ActiveTabIndex = 3;
            }
            catch
            {

            }
        }

        // Routine to display Registrant data
        private void DisplayRegistrant(Control CVViewerCtrl)
        {
            HiddenField hfRegistrantKey = ((HiddenField)(CVViewerCtrl.FindControl("hfRegistrantKey")));
            hfRegistrantKey.Value = sRegistrantKey;

            CreateConnection();

            CreateCommand((CommandType.StoredProcedure), "RetrieveRegistrant");

            command.Parameters.AddWithValue("@RegistrantRowKey", sRegistrantKey);

            command.Parameters.Add("@RowNoAffected", SqlDbType.Int).Direction = (ParameterDirection.Output);
            command.Parameters.Add("@ErrorMessage", SqlDbType.NVarChar, 256).Direction = (ParameterDirection.Output);
            
            IssueCommand();

            DataTable dataTable = (dataSet.Tables[0]);

            Label lbSalutation = ((Label)(CVViewerCtrl.FindControl("lbSalutation")));
            lbSalutation.Text = (((dataTable.Rows[0]).ItemArray[2]).ToString());

            Label lbSurname = ((Label)(CVViewerCtrl.FindControl("lbSurname")));
            lbSurname.Text = (((dataTable.Rows[0]).ItemArray[3]).ToString());

            Label lbFirstName = ((Label)(CVViewerCtrl.FindControl("lbFirstName")));
            lbFirstName.Text = (((dataTable.Rows[0]).ItemArray[4]).ToString());

            Label lbOtherNames = ((Label)(CVViewerCtrl.FindControl("lbOtherNames")));
            object oOtherNames = ((dataTable.Rows[0]).ItemArray[5]);
            if (oOtherNames != null)
            {
                lbOtherNames.Text = (oOtherNames.ToString());
            }
            else
            {
                lbOtherNames.Text = "---";
            }

            Label lbAge = ((Label)(CVViewerCtrl.FindControl("lbAge")));
            lbAge.Text = (((dataTable.Rows[0]).ItemArray[6]).ToString());
                        
            Label lbGender = ((Label)(CVViewerCtrl.FindControl("lbGender")));
            lbGender.Text = (((dataTable.Rows[0]).ItemArray[7]).ToString());

            Label lbMaritalStatus = ((Label)(CVViewerCtrl.FindControl("lbMaritalStatus")));
            lbMaritalStatus.Text = (((dataTable.Rows[0]).ItemArray[8]).ToString());

            TabContainer tcApplicant = ((TabContainer)(CVViewerCtrl.FindControl("tcApplicant")));

            Label lbCountryOfOrigin = ((Label)((tcApplicant.Tabs[0]).FindControl("lbCountryOfOrigin")));
            lbCountryOfOrigin.Text = (((dataTable.Rows[0]).ItemArray[9]).ToString());

            Label lbStateOrProvinceOfOrigin = ((Label)((tcApplicant.Tabs[0]).FindControl("lbStateOrProvinceOfOrigin")));
            object oStateOfOrigin = ((dataTable.Rows[0]).ItemArray[10]);
            if (oStateOfOrigin != null)
            {
                lbStateOrProvinceOfOrigin.Text = (oStateOfOrigin.ToString());
            }
            else
            {
                oStateOfOrigin = ((dataTable.Rows[0]).ItemArray[11]);
                if (oStateOfOrigin != null)
                {
                    lbStateOrProvinceOfOrigin.Text = (oStateOfOrigin.ToString());
                }
            }

            Label lbLocalGovernmentArea = ((Label)((tcApplicant.Tabs[0]).FindControl("lbLocalGovernmentArea")));
            object oLocalGovernmentArea = ((dataTable.Rows[0]).ItemArray[12]);
            if (oLocalGovernmentArea != null)
            {
                lbLocalGovernmentArea.Text = (oLocalGovernmentArea.ToString());
            }
            else
            {
                oLocalGovernmentArea = ((dataTable.Rows[0]).ItemArray[13]);
                if (oLocalGovernmentArea != null)
                {
                    lbLocalGovernmentArea.Text = (oLocalGovernmentArea.ToString());
                }
            }

            Label lbNYSCCompletionStatus = ((Label)((tcApplicant.Tabs[0]).FindControl("lbNYSCCompletionStatus")));
            lbNYSCCompletionStatus.Text = (((dataTable.Rows[0]).ItemArray[14]).ToString());

            Label lbMTNStaffQ = ((Label)((tcApplicant.Tabs[0]).FindControl("lbMTNStaffQ")));
            lbMTNStaffQ.Text = (((dataTable.Rows[0]).ItemArray[15]).ToString());

            dataTable = dataSet.Tables[1];

            Label lbCountryOfResidence = ((Label)((tcApplicant.Tabs[0]).FindControl("lbCountryOfResidence")));
            object oCountryOfResidence = ((dataTable.Rows[0]).ItemArray[2]);
            if (oCountryOfResidence != null)
            {
                lbCountryOfResidence.Text = (oCountryOfResidence.ToString());
            }
            else
            {
                lbCountryOfResidence.Text = "NOT SPECIFIED";
            }

            Label lbStateOrProvinceOfResidence = ((Label)((tcApplicant.Tabs[0]).FindControl("lbStateOrProvinceOfResidence")));
            object oStateOrProvinceOfResidence = ((dataTable.Rows[0]).ItemArray[3]);
            if (oStateOrProvinceOfResidence != null)
            {
                lbStateOrProvinceOfResidence.Text = (oStateOrProvinceOfResidence.ToString());
            }
            else
            {
                oStateOrProvinceOfResidence = ((dataTable.Rows[0]).ItemArray[4]);
                if (oStateOrProvinceOfResidence != null)
                {
                    lbStateOrProvinceOfResidence.Text = (oStateOrProvinceOfResidence.ToString());
                }
                else
                {
                    lbStateOrProvinceOfResidence.Text = "NOT SPECIFIED";
                }
            }

            Label lbCity = ((Label)((tcApplicant.Tabs[0]).FindControl("lbCity")));
            object oCity = ((dataTable.Rows[0]).ItemArray[5]);
            if (oCity != null)
            {
                lbCity.Text = (oCity.ToString());
            }
            else
            {
                lbCity.Text = "NOT SPECIFIED";
            }

            Label lbAddress = ((Label)((tcApplicant.Tabs[0]).FindControl("lbAddress")));
            lbAddress.Text = ((((dataTable.Rows[0]).ItemArray[6]).ToString()) + (((dataTable.Rows[0]).ItemArray[7]).ToString()));           

            
            Label lbMobilePhone = ((Label)((tcApplicant.Tabs[0]).FindControl("lbMobilePhone")));
            lbMobilePhone.Text = (((dataTable.Rows[0]).ItemArray[8]).ToString());

            Label lbContactPhoneNo = ((Label)((tcApplicant.Tabs[0]).FindControl("lbContactPhoneNo")));
            object oContactPhoneNo = ((dataTable.Rows[0]).ItemArray[9]);
            if (oContactPhoneNo != null)
            {
                lbContactPhoneNo.Text = (oContactPhoneNo.ToString());
            }
            else
            {
                lbContactPhoneNo.Text = "NOT SPECIFIED";
            }


            GridView gvApplicantEducationalHistory = ((GridView)((tcApplicant.Tabs[1]).FindControl("gvApplicantEducationalHistory")));
            gvApplicantEducationalHistory.DataBind();

            GridView gvProfessionalCertification = ((GridView)((tcApplicant.Tabs[2]).FindControl("gvProfessionalCertification")));
            gvProfessionalCertification.DataBind();

            GridView gvRegistrantSkills = ((GridView)((tcApplicant.Tabs[3]).FindControl("gvRegistrantSkills")));
            gvRegistrantSkills.DataBind();

            GridView gvRegistrantWorkExperience = ((GridView)((tcApplicant.Tabs[4]).FindControl("gvRegistrantWorkExperience")));
            gvRegistrantWorkExperience.DataBind();
        }

        // Routine to create command to be issued against MTN NG Careers DB.
        private void CreateCommand(CommandType commandType, string commandText)
        {
            command = new SqlCommand();
            command.Connection = connection;
            command.CommandType = commandType;
            command.CommandText = commandText;
        }

        private void RetrieveDefaultShortListedApplicants()
        {
            CreateCommand((CommandType.StoredProcedure), "RetrieveDefaultVacancyApplicantsShortList");

            sVacancyID = (hfVacancyID.Value);
            sVacancyBreakDownID = (hfVacancyBreakDownID.Value);

            command.Parameters.AddWithValue("@VacancyID", sVacancyID);
            command.Parameters.AddWithValue("@VacancyBreakDownID", sVacancyBreakDownID);

            IssueCommand();            
        }

        // Routine to retrieve applicants who have been shortlisted *** NOT DEFAULT ***.
        private void RetrieveShortListedApplicants()
        {
            CreateCommand((CommandType.StoredProcedure), "RetrieveShortListedApplicants");

            sVacancyBreakDownID = (hfVacancyBreakDownID.Value);

            command.Parameters.AddWithValue("@VacancyBreakDownID", sVacancyBreakDownID);

            IssueCommand();
        }

        // Routine to issue command against database.
        private void IssueCommand()
        {
            dataAdapter = new SqlDataAdapter(command);
            dataSet = new DataSet();
            defaultShortListedApplicantsCount = dataAdapter.Fill(dataSet);
        }

        // Routine to determine what applicant records to check on the GridView as to meeting up with vacancy criteria.
        private void CheckShortListedApplicants(GridViewRow currentlyBoundRow)
        {
            // Retrieving the records for short listed application.
            DataTable dataTable = (dataSet.Tables[0]);
            DataRowCollection dataRows = (dataTable.Rows);

            string currentApplicantKey = null;

            string shortListedApplicantKey = null;
            //string vacancyAgeRange = null;
            //string sShortListedApplicantBirthDate = null;
                        
            int currentlyBoundRowIndex = (currentlyBoundRow.RowIndex);
            currentApplicantKey = (((gvVacanyApplicants.DataKeys[currentlyBoundRowIndex]).Values[2]).ToString());

            // iterating through each record of Short Listed Applicants
            foreach (DataRow dataRow in dataRows)
            {
                // getting the short listed applicant's Key                    
                shortListedApplicantKey = ((dataRow.ItemArray[0]).ToString());

                if (currentApplicantKey == shortListedApplicantKey)
                {
                    string typeName = null;
                    CheckBox cb = null;

                    foreach (Control c in ((currentlyBoundRow.Cells[0]).Controls))
                    {
                        typeName = ((c.GetType()).Name);
                        if ((typeName.Equals("CheckBox")))
                        {
                            cb = ((CheckBox)c);
                            cb.Checked = true;
                        }
                    }
                    break;

                    #region Code to Filter based on age range
                    /* getting the short listed age range for Vacancy in question
                    // and applicant's birth date.
                    vacancyAgeRange = ((dataRow.ItemArray[1]).ToString());
                    sShortListedApplicantBirthDate = ((dataRow.ItemArray[2]).ToString());

                    vacancyAgeRange = (vacancyAgeRange.Trim());
                    int indexOfHyphen = vacancyAgeRange.IndexOf("-");
                    string sFromAge = (vacancyAgeRange.Substring(0, (indexOfHyphen - 1)));
                    string sToAge = (vacancyAgeRange.Substring((indexOfHyphen + 2)));

                    int iFromAge = (int.Parse(sFromAge));
                    int iToAge = (int.Parse(sToAge));

                    int currentYear = ((DateTime.Now).Year);

                    DateTime dShortListedApplicantBirthDate = (DateTime.Parse(sShortListedApplicantBirthDate));
                    int applicantBirthYear = (dShortListedApplicantBirthDate.Year);

                    int applicantAge = (currentYear - applicantBirthYear);

                    if ((applicantAge >= iFromAge) && ((applicantAge <= iToAge)))
                    {
                        shortListedVacancyApplicantCount += 1;

                        string typeName = null;

                        CheckBox cb = null;

                        foreach (Control c in ((currentlyBoundRow.Cells[0]).Controls))
                        {
                            typeName = ((c.GetType()).Name);
                            if ((typeName.Equals("CheckBox")))
                            {
                                cb = ((CheckBox)c);
                                cb.Checked = true;
                            }
                        }
                        break;
                    }*/
                    #endregion
                }
            }
        }

        protected void gvVacanyApplicants_DataBound(object sender, EventArgs e)
        {
            string sVacancyApplicantCount = (((gvVacanyApplicants.Rows).Count).ToString());
            int iVacancyApplicantCount = (int.Parse(sVacancyApplicantCount));
            iVacancyApplicantCount = (iVacancyApplicantCount - repeatCount);
            shortListedVacancyApplicantCount = (shortListedVacancyApplicantCount - repeatCount);

            if (iVacancyApplicantCount <= 0)
            {
                lbVacancyApplicantCount.Text = (String.Empty);
                lblFilteredByDefaultApplicants.Text = (String.Empty);

                btnSHORTLIST.Visible = false;
                btnSUBMIT.Visible = false;
            }
            else
            {
                lbVacancyApplicantCount.Text = "no. of applicants (" + (iVacancyApplicantCount.ToString()) + ")";
                lblFilteredByDefaultApplicants.Text = (String.Empty);//"no. of filtered applicants (" + (shortListedVacancyApplicantCount.ToString()) + ")";
                lblShortListedApplicantCount.Text = "no. of short listed applicants (" + (shortListedVacancyApplicantCount.ToString()) + ")";

                btnSHORTLIST.Visible = true;
                btnSUBMIT.Visible = true;
            }
        }

        protected void gvVacanyApplicants_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void gvVacanyApplicants_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvVacanyApplicants.PageIndex = (e.NewPageIndex);
            gvVacanyApplicants.DataBind();
        }

        protected void HandleShortListApplicants(object sender, EventArgs e)
        {
            CreateConnection();

            CreateCommand((CommandType.StoredProcedure), "[ShortListApplicant]");
                        
            TableCell checkBoxCell = null;
            CheckBox cb = null;

            string controlTypeName = null;

            connection.Open();

            // iterating through each row in the GridView that displays applicants of selected vacancy.
            foreach (GridViewRow gridViewRow in (gvVacanyApplicants.Rows))
            {
                int currentRowIndex = (gridViewRow.RowIndex);
                DataKey dataKey = (gvVacanyApplicants.DataKeys[currentRowIndex]);
                object oVacancyBreakDownID = (dataKey.Values[1]);
                object oRegistrantRowKey = (dataKey.Values[2]);

                command.Parameters.AddWithValue("@VacancyBreakDownID", (oVacancyBreakDownID.ToString()));
                command.Parameters.AddWithValue("@RegistrantRowKey", (oRegistrantRowKey.ToString()));

                command.Parameters.Add("@RowNoAffected", SqlDbType.Int).Direction = (ParameterDirection.Output);
                command.Parameters.Add("@ErrorMessage", SqlDbType.NVarChar, 256).Direction = (ParameterDirection.Output);

                // obtaining a references to the cell that contains a checkbox for shortlisting an applicants.
                checkBoxCell = (gridViewRow.Cells[0]);

                foreach (Control c in (checkBoxCell.Controls))
                {
                    controlTypeName = ((c.GetType()).Name);
                    if ((controlTypeName.Equals("CheckBox")))
                    {                        
                        cb = ((CheckBox)c);

                        if ((cb.Checked))
                        {
                            command.Parameters.AddWithValue("@IsShortListed", "1");
                        }
                        else
                        {
                            command.Parameters.AddWithValue("@IsShortListed", "0");
                        }
                        break;
                    }
                }

                command.ExecuteNonQuery();
                command.Parameters.Clear();
            }

            connection.Close();

            ViewState[_ApplicantsView] = _ShortListedApplicantsView;

            RetrieveShortListedApplicants();
        }

        // Sends a mail to Hiring Manager
        protected void HandleSubmitShortListedApplicants(object sender, EventArgs e)
        {
            SPWeb currentWebsite = (((LayoutsPageBase)(this.Page)).Web);

            string[,] criterionField = new string[1, 3];
            criterionField[0, 0] = ("ID");
            criterionField[0, 1] = (CommonUtilities.FieldTypeNumber);
            criterionField[0, 2] = (hfVacancyID.Value);

            ListAccessor accessor = new ListAccessor(currentWebsite, (VacanciesList.ListName), (new string[] {(VacanciesList.RequesterFieldInternalName), (VacanciesList.JobTitleFieldInternalName)}), criterionField );
            SPListItemCollection listItems = (accessor.RequiredListItems);

            if ((listItems.Count) > 0)
            {
                criterionField = new string[1, 3];
                criterionField[0, 0] = (ExternalCareersMailsList.TypeOfMailFieldInternalName);
                criterionField[0, 1] = (CommonUtilities.FieldTypeText);
                criterionField[0, 2] = ((WebConfigurationManager.AppSettings["HiringManagerShortlistingNotificationMailType"]).ToString());

                accessor = new ListAccessor(currentWebsite, (ExternalCareersMailsList.ListName), (new string[] { (ExternalCareersMailsList.SubjectFieldInternalName), (ExternalCareersMailsList.BodyFieldInternalName) }), criterionField);
                SPListItemCollection requiredMailDefinition = (accessor.RequiredListItems);

                if ((requiredMailDefinition.Count) > 0)
                {
                    SPListItem requiredItem = listItems[0];
         
                    string jobTitle = (requiredItem[(VacanciesList.JobTitleFieldDisplayName)].ToString());

                    CommonUtilities utilities = new CommonUtilities();
                    SPUser hiringManagerSPUser = (utilities.GetSPUserFromSPFieldUser(requiredItem, (VacanciesList.RequesterFieldDisplayName)));

                    SPListItem requiredMailItem = requiredMailDefinition[0];
                    object oSubject = requiredMailItem[(ExternalCareersMailsList.SubjectFieldDisplayName)];
                    string sSubject = (String.Empty);
                    if (oSubject != null)
                    {
                        sSubject = (oSubject.ToString());
                        sSubject = sSubject.Replace("@JobTitle", jobTitle);
                    }

                    object oBody = requiredMailItem[(ExternalCareersMailsList.BodyFieldDisplayName)];
                    string sBody = (String.Empty);
                    if (oBody != null)
                    {
                        string sBreakDownID = (hfVacancyBreakDownID.Value);
                        string requesterMembershipID = ((hiringManagerSPUser.ID).ToString());

                        sBody = (oBody.ToString());
                        sBody = (sBody.Replace("@HiringManager", (hiringManagerSPUser.Name)));

                        sBody = (sBody.Replace("@JobTitle", jobTitle));

                        string url = ((WebConfigurationManager.AppSettings["HiringManagerVacanyShortlistedApplicantsLink"]).ToString());
                        string link = "<a href='" + url + "?RequesterMembershipID=" + requesterMembershipID + "&VacancyBreakDownID=" + sBreakDownID + "'>Shortlisted Applicants</a>";
                        sBody = sBody.Replace("@VacancyShortListedApplicantsLink", link);
                    }

                    EmailFacilitator mailSendingFacilitator = new EmailFacilitator();
                    mailSendingFacilitator.Sender = ((WebConfigurationManager.AppSettings["HRRecruitmentTeamGroupMailAddress"]).ToString());
                    mailSendingFacilitator.Recipients = new string[] { (hiringManagerSPUser.Email) };
                    mailSendingFacilitator.Subject = sSubject;
                    mailSendingFacilitator.Body = sBody;
                    mailSendingFacilitator.SendMail();

                    SPUtility.TransferToSuccessPage("A report of the short listed applicants have been sent to the Hiring Manager");
                }
                else
                {
                    return; 
                }
                
            }

            
            //<add key="SMTPServer" value="ojtsrv07.mtn.com.ng" />
        }

        protected void vwVacancyApplicantsDataSrc_DataBinding(object sender, EventArgs e)
        {
            //Type type =  (e.GetType());
        }

        protected void gvVacanyApplicants_RowCreated(object sender, GridViewRowEventArgs e)
        {
            GridViewRow justCreatedRow = (e.Row);
            int justCreatedRowIndex = (justCreatedRow.RowIndex);

            if (justCreatedRowIndex > 0)
            {
                DataKey justCreatedRowKeys = (gvVacanyApplicants.DataKeys[justCreatedRowIndex]);
                string justCreatedRegistrantKey = ((justCreatedRowKeys.Values[2]).ToString());

                int immediatePreviouslyCreatedRowIndex = (justCreatedRowIndex - 1);
                GridViewRow immediatePreviouslyCreatedRow = (gvVacanyApplicants.Rows[immediatePreviouslyCreatedRowIndex]);

                DataKey immediatePreviouslyCreatedRowKeys = (gvVacanyApplicants.DataKeys[immediatePreviouslyCreatedRowIndex]);
                string immediatePreviousRegistrantKey = ((immediatePreviouslyCreatedRowKeys.Values[2]).ToString());

                if (immediatePreviousRegistrantKey == justCreatedRegistrantKey)
                {
                    justCreatedRow.Visible = false;
                    repeatCount += 1;
                }
            }
        }

        //private string RetrieveRequesterMembershipID(string breakDownID)
        //{
        //    CreateConnection();

        //    CreateCommand((CommandType.Text), "SELECT Requester FROM Vacancy INNER JOIN VacancyBreakDown ON Vacancy.ID=VacancyBreakDown.VacancyID WHERE VacancyBreakDown.ID=@VacancyBreakDownID");
        //    command.Parameters.AddWithValue("@VacancyBreakDownID", breakDownID);

        //    IssueCommand();

        //    string requesterMembershipID = (String.Empty);
        //    if (defaultShortListedApplicantsCount > 0)
        //    {
        //        requesterMembershipID = (((command.Parameters["@VacancyBreakDownID"]).Value).ToString());
        //    }

        //    return requesterMembershipID;
        //}
    }
}

﻿using System;
using System.Web.Configuration;
using System.Data;
using System.Data.SqlClient;

using System.Web.UI;
using System.Web.UI.WebControls;

using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
using Microsoft.SharePoint.Utilities;

using MTNNGCareers.Administration.Common;

using CommonUtilities = Utilities.Common;
using SharePointListManager;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text;
using iTextSharp.text.pdf;
using cvregistrantSpace;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace MTNNGCareers.Administration.Forms
{
    // REASONS to HIDE the GridView's Row that displays APPLICANT's RECORD.
    // 1. If the Row's record has already been displayed.
    // 2. If the Row's record doesn't meet a filter criteria.
    // 3. If the Row's record doesn't meet a short listing criteria.

    public partial class ManageVacancyApplicants : LayoutsPageBase
    {
        const string _ApplicantsView = "Applicant View";
        const string _ShortListedApplicantsView = "ShortListed Applicants View";

        string sVacancyBreakDownListItemID;
        string vacancyJobTitle = null;
        int shortListedVacancyApplicantCount;

        SqlConnection connection;
        SqlCommand command;
        SqlDataAdapter dataAdapter;
        DataSet dataSet;

        string sVacancyID;
        string sVacancyBreakDownID;

        int repeatCount = 0;

        int iHiddenGridViewRowIndex;

        bool isPostBack = true;

        bool shouldInvokeFilterOperation = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            SPUserAuthenticator authenticator = new SPUserAuthenticator();
            bool isValidUser = (authenticator.ValidateUser());
            string valuesb = GetuploadedFile("783930");
            if (!isValidUser)
            {
                SPUtility.Redirect(((this.Web).Url), SPRedirectFlags.Default, Context);
            }

            if (!IsPostBack)
            {
                isPostBack = false;

                RetrieveIDs();

                CreateConnection();
            }
        }

        // Routine to retrieve the IDs of the Vacancy and the Break Down selected
        // and pass them unto the respective User Controls.        
        private void RetrieveIDs()
        {
            object oVacancyBreakDownListGUID = (Request.Params["List"]);
            if (oVacancyBreakDownListGUID != null)
            {
                string svacancyBreakDownListGUID = (oVacancyBreakDownListGUID.ToString());

                object oVacancyBreakDownListItemID = (Request.Params["ID"]);
                if (oVacancyBreakDownListItemID == null)
                {
                    return;
                }

                sVacancyBreakDownListItemID = (oVacancyBreakDownListItemID.ToString());
                ViewState["VacancyBreakDownItemID"] = sVacancyBreakDownListItemID;

                SPList vacancyBreakDownList = ((this.Web).Lists[(new Guid(svacancyBreakDownListGUID))]);

                if (vacancyBreakDownList != null)
                {
                    // Retrieving the ID of the vacancy break down.
                    SPListItem vacancyBreakDownListItem = (vacancyBreakDownList.GetItemById((int.Parse(sVacancyBreakDownListItemID))));
                    hfVacancyBreakDownID.Value =  ((vacancyBreakDownListItem.ID).ToString());
                    object oCurrentStatus = vacancyBreakDownListItem[(VacancyBreakDownList.StatusFieldDisplayName)];
                    File.WriteAllText(@"c:\ExcelFile\oCurrentStatus.log", oCurrentStatus.ToString());
                    ViewState["CurrentStatus"] = oCurrentStatus;

                    if (oCurrentStatus != null)
                    {
                        string sCurrentStatus = (oCurrentStatus.ToString());

                        if (sCurrentStatus.Equals(CommonMembers.PendingHiringManagerEvaluation))
                        {
                            btnFILTER.Visible = false;
                            btnSHORTLIST.Visible = false;
                            btnSUBMIT.Visible = false;
                        }
                        else if (sCurrentStatus.Equals(CommonMembers.OpeningClosed))
                        {
                            btnFILTER.Visible = false;
                            btnSHORTLIST.Visible = false;
                            btnSUBMIT.Visible = false;
                        }
                        else
                        {
                            btnFILTER.Visible = true;
                            btnSHORTLIST.Visible = true;
                            btnSUBMIT.Visible = true;
                        }
                    }

                    // Retrieving the ID of the vacancy.
                    string vacancyFieldDisplayName = (VacancyBreakDownList.VacancyFieldDisplayName);
                    SPFieldLookup lookupField = ((SPFieldLookup)vacancyBreakDownListItem.Fields[vacancyFieldDisplayName]);
                    SPFieldLookupValue lookupFieldValue = ((SPFieldLookupValue)lookupField.GetFieldValue((vacancyBreakDownListItem[vacancyFieldDisplayName].ToString())));

                    SPListItem _Vacancy = (((this.Web).Lists[(VacanciesList.ListName)]).GetItemById(lookupFieldValue.LookupId));
                    lblJobTitle.Text = (_Vacancy[(VacanciesList.JobTitleFieldDisplayName)].ToString());
                    //Get Job Role......

                    vacancyJobTitle = lblJobTitle.Text;
                    
                    Session["VacancyJobTitle"] = vacancyJobTitle;
                    
                    //End Get Job Role
                    hfVacancyID.Value = ((lookupFieldValue.LookupId).ToString());

                    HiddenField hfVacancyID1 = ((HiddenField)(VacancyViewer1.FindControl("hfVacancyID")));
                    hfVacancyID1.Value = ((lookupFieldValue.LookupId).ToString());

                    DisplayBreakDownDetails(vacancyBreakDownListItem);
                }
            }
        }

        // Routine to retrieve and display break down details.
        private void DisplayBreakDownDetails(SPListItem _VacancyBreakDown)
        {
            lblNoOfOpenings.Text = ((_VacancyBreakDown[(VacancyBreakDownList.QuantityFieldDisplayName)]).ToString());
            lblEmploymentStatus.Text = ((_VacancyBreakDown[(VacancyBreakDownList.EmployementStatusFieldDisplayName)]).ToString());

            string departmentLookupFieldDisplayName = (VacancyBreakDownList.DepartmentLookUpFieldDisplayName);
            if (_VacancyBreakDown[departmentLookupFieldDisplayName] != null)
            {
                SPFieldLookup departmentLookupField = ((SPFieldLookup)_VacancyBreakDown.Fields[(departmentLookupFieldDisplayName)]);
                SPFieldLookupValue departmentLookupFieldValue = ((SPFieldLookupValue)(departmentLookupField.GetFieldValue((_VacancyBreakDown[departmentLookupFieldDisplayName].ToString()))));
                lblDepartment.Text = (departmentLookupFieldValue.LookupValue);
            }
            else
            {
                lblDepartment.Text = "";
            }

            string unitLookupFieldDisplayName = (VacancyBreakDownList.UnitLookUpFieldDisplayName);
            if (_VacancyBreakDown[unitLookupFieldDisplayName] != null)
            {
                SPFieldLookup unitLookupField = ((SPFieldLookup)_VacancyBreakDown.Fields[(unitLookupFieldDisplayName)]);
                SPFieldLookupValue unitLookupFieldValue = ((SPFieldLookupValue)(unitLookupField.GetFieldValue((_VacancyBreakDown[unitLookupFieldDisplayName].ToString()))));
                lblUnit.Text = (unitLookupFieldValue.LookupValue);
            }
            else
            {
                lblUnit.Text = "";
            }

            string stateLookupFieldDisplayName = (VacancyBreakDownList.StateFieldDisplayName);
            if (_VacancyBreakDown[stateLookupFieldDisplayName] != null)
            {
                SPFieldLookup stateLookupField = ((SPFieldLookup)_VacancyBreakDown.Fields[(stateLookupFieldDisplayName)]);
                SPFieldLookupValue stateLookupFieldValue = ((SPFieldLookupValue)(stateLookupField.GetFieldValue((_VacancyBreakDown[stateLookupFieldDisplayName].ToString()))));
                lblState.Text = (stateLookupFieldValue.LookupValue);
            }
            else
            {
                lblState.Text = "";
            }

            string locationLookupFieldDisplayName = (VacancyBreakDownList.LocationFieldDisplayName);
            if (_VacancyBreakDown[locationLookupFieldDisplayName] != null)
            {
                SPFieldLookup locationLookupField = ((SPFieldLookup)_VacancyBreakDown.Fields[(locationLookupFieldDisplayName)]);
                SPFieldLookupValue locationLookupFieldValue = ((SPFieldLookupValue)(locationLookupField.GetFieldValue((_VacancyBreakDown[locationLookupFieldDisplayName].ToString()))));
                lblLocation.Text = (locationLookupFieldValue.LookupValue);
            }
            else
            {
                lblLocation.Text = "";
            }
        }

        // Routine to create a connection to the MTN NG Careers DB.
        private void CreateConnection()
        {
            string connectionString = ((WebConfigurationManager.ConnectionStrings["MTNNGCareersDBConnectionString"]).ToString());
            connection = new SqlConnection(connectionString);
        }

        // Routine to create command to be issued against MTN NG Careers DB.
        private void CreateCommand(CommandType commandType, string commandText)
        {
            command = new SqlCommand();
            command.Connection = connection;
            command.CommandType = commandType;
            command.CommandText = commandText;
        }

        private void AutomaticShortList()
        {
            CreateCommand((CommandType.StoredProcedure), "ShortListVacancyApplicants");
        }

        // Routine to issue command against database.
        private int IssueCommand()
        {

            dataAdapter = new SqlDataAdapter(command);
            dataSet = new DataSet();
            int rowCount = dataAdapter.Fill(dataSet);

            return rowCount;
        }

        // Routine (Event Handler) to handle the click event generated when the user clicks the Button labeled: filter.
        protected void HandleFilterInitiation(object sender, EventArgs e)
        {
            CreateConnection();

            if ((gvVacanyApplicants.AllowPaging))
            {
                gvVacanyApplicants.AllowPaging = false;

                shouldInvokeFilterOperation = true;
            }
            else
            {
                shouldInvokeFilterOperation = false;

                FilterVacancyApplicants();

                divVacancy.Attributes["style"] = "display: none";
                divCriteria.Attributes["style"] = "display: none";
                divApplicants.Attributes["style"] = "display: block";
            }
        }

        // Routine to filter applicants based on criteria specified.
        private void FilterVacancyApplicants()
        {
            // Creating a connection to the database and issuing the command to actually filter applicants
            // that meet specified criteria.
            CreateConnection();

            CreateCommand((CommandType.StoredProcedure), "ShortListVacancyApplicants");

            CommonMembers commonMembers = new CommonMembers();

            // Based on the criteria specified, sql stored procedure input variables are created.
            AddShortListingSqlParameters();

            int rowCount = IssueCommand();

            // If at least an applicant meets the specified criterion/criteria, ...
            if (rowCount > 0)
            {
                // check applicants that meet specified criteria,criterion
                DetermineRecordsToDisplay();

                btnSHORTLIST.Enabled = true;
                btnSHORTLIST.ToolTip = "click this button to short list selected applicants.";

                lblFilteredApplicants.ForeColor = System.Drawing.Color.Black;
                lblFilteredApplicants.Font.Bold = true;
                lblFilteredApplicants.Font.Underline = false;
                lblFilteredApplicants.Text = ("number of filtered applicants" + " (" + (rowCount.ToString()) + ")");
            }
            else
            {
                // uncheck every row in the GridView used to display all applicants
                foreach (GridViewRow vacancyApplicantRecord in (gvVacanyApplicants.Rows))
                {
                    if (!(vacancyApplicantRecord.Visible))
                    {
                        continue;
                    }

                    string typeName = null;

                    CheckBox cb = null;

                    foreach (Control c in ((vacancyApplicantRecord.Cells[0]).Controls))
                    {
                        typeName = ((c.GetType()).Name);
                        if ((typeName.Equals("CheckBox")))
                        {
                            cb = ((CheckBox)c);
                            cb.Checked = false;
                        }
                    }

                    btnSHORTLIST.Enabled = false;
                    btnSHORTLIST.ToolTip = "the shortlist function has been disabled since no applicant meets specified criterion/criteria.";

                    lblFilteredApplicants.ForeColor = System.Drawing.Color.Red;
                    lblFilteredApplicants.Font.Bold = true;
                    lblFilteredApplicants.Font.Underline = true;
                    lblFilteredApplicants.Text = "no applicant met the criteria you specified.";
                }

                lblFilteredApplicants.Visible = true;
            }
        }

        // Routine to add SqlParameters for filtering applicants.
        private void AddShortListingSqlParameters()
        {
            command.Parameters.AddWithValue("@VacancyBreakDownID", (hfVacancyBreakDownID.Value));

            if (cbCourseOfStudy.Checked)
            {
                command.Parameters.AddWithValue("@PreferredCourseOfStudy", (CourseOfStudyInputFormTextBox.Text));
            }
            else
            {
                command.Parameters.AddWithValue("@PreferredCourseOfStudy", (DBNull.Value));
            }

            if (cbMinimumQualification.Checked)
            {
                command.Parameters.AddWithValue("@PreferredMinimumQualificationID", (ddlMinimumQualification.SelectedValue));
            }
            else
            {
                command.Parameters.AddWithValue("@PreferredMinimumQualificationID", (DBNull.Value));
            }

            if (cbPostDegreeYearsOfExperience.Checked)
            {
                command.Parameters.AddWithValue("@PreferredPostDegreeYearsOfExperience", (iftPostDegreeYearsOfExperience.Text));
            }
            else
            {
                command.Parameters.AddWithValue("@PreferredPostDegreeYearsOfExperience", (DBNull.Value));
            }

            if (cbClassOfQualification.Checked)
            {
                string classOfQualification = "";
                foreach (System.Web.UI.WebControls.ListItem item in chkClassOfQualification.Items)
                {
                    if (item.Selected)
                        classOfQualification += item.Value + ",";
                }
                int lastindexofComma = classOfQualification.LastIndexOf(',');
                classOfQualification = classOfQualification.Remove(lastindexofComma, 1);
                command.Parameters.AddWithValue("@PreferredClassOfQualificationID", classOfQualification);
            }
            else
            {
                command.Parameters.AddWithValue("@PreferredClassOfQualificationID", (DBNull.Value));
            }

            if (cbAdditionalQualifications.Checked)
            {
                command.Parameters.AddWithValue("@PreferredAdditionalQualifications", (iftAdditionalQualifications.Text));
            }
            else
            {
                command.Parameters.AddWithValue("@PreferredAdditionalQualifications", (DBNull.Value));
            }

            if (cbProfessionalQualifications.Checked)
            {
                command.Parameters.AddWithValue("@PreferredProfessionalQualifications", (iftProfessionalQualifications.Text));
            }
            else
            {
                command.Parameters.AddWithValue("@PreferredProfessionalQualifications", (DBNull.Value));
            }

            if (cbSkillsRequired.Checked)
            {
                command.Parameters.AddWithValue("@PreferredSkillsRequired", (iftSkillsRequired.Text));
            }
            else
            {
                command.Parameters.AddWithValue("@PreferredSkillsRequired", (DBNull.Value));
            }

            if (cbJobDescription.Checked)
            {
                command.Parameters.AddWithValue("@ExpectedJobDescription", (JobDescriptionInputFormTextBox.Text));
            }
            else
            {
                command.Parameters.AddWithValue("@ExpectedJobDescription", (DBNull.Value));
            }

            if (cbMaritalStatus.Checked)
            {
                command.Parameters.AddWithValue("@PreferredMaritalStatus", (rblMaritalStatus.SelectedValue));
            }
            else
            {
                command.Parameters.AddWithValue("@PreferredMaritalStatus", (DBNull.Value));
            }

            if (cbGender.Checked)
            {
                command.Parameters.AddWithValue("@PreferredGender", (rblGender.SelectedValue));

            }
            else
            {
                command.Parameters.AddWithValue("@PreferredGender", (DBNull.Value));
            }

            if (cbNYSCRequirement.Checked)
            {
                string nyscCompletedStatus = "";
                foreach (System.Web.UI.WebControls.ListItem item in chkNYSCCompletionStatus.Items)
                {
                    if (item.Selected)
                        nyscCompletedStatus += item.Text + ",";
                }
                int lastindexofComma = nyscCompletedStatus.LastIndexOf(',');
                nyscCompletedStatus = nyscCompletedStatus.Remove(lastindexofComma, 1);
                command.Parameters.AddWithValue("@NYSCCompletionStatus", (nyscCompletedStatus));
                //command.Parameters.AddWithValue("@NYSCCompletionStatus", (rdoNYSCCompletionStatus.SelectedValue));
            }
            else
            {
                command.Parameters.AddWithValue("@NYSCCompletionStatus", (DBNull.Value));
            }

            if (cbCountryOfOorigin.Checked)
            {
                command.Parameters.AddWithValue("@CountryOfOrigin", (ddlCountryOfOrigin.SelectedValue));

            }
            else
            {
                command.Parameters.AddWithValue("@CountryOfOrigin", (DBNull.Value));
            }

            if (cboHasWorkWithMTN.Checked)
            {
                command.Parameters.AddWithValue("@HasWorkWithMTN", (rdoWorkWithMTN.SelectedItem.Text));
            }
            else
            {
                command.Parameters.AddWithValue("@HasWorkWithMTN", (DBNull.Value));
            }
        }

        // Routine to determine the applicant record(s) to show.
        // The basis is the criterion/criteria specified.
        private void DetermineRecordsToDisplay()
        {
            // Retrieving the records for short listed application.
            DataTable dataTable = (dataSet.Tables[0]);
            DataRowCollection dataRows = (dataTable.Rows);

            // getting the count of applicants that met the filtering criterion/criteria
            shortListedVacancyApplicantCount = (dataRows.Count);

            string currentApplicantKey = null;

            string shortListedApplicantKey = null;

            //string vacancyAgeRange = null;
            //string sShortListedApplicantBirthDate = null;

            int shortListedApplicantCounter = 0;

            // Iterating through each row in the GridView
            foreach (GridViewRow vacancyApplicantRecord in (gvVacanyApplicants.Rows))
            {
                // Skipping invisible rows. *** BECAUSE A ROW COULD BE INVISIBLE NOT BECAUSE ITS RECORD ALREADY EXISTS, BUT BECAUSE IT DOESN'T MEET A FILTER CRITERION/CRITERIA ***
                /*if (!(vacancyApplicantRecord.Visible))
                {
                    continue;
                }*/

                // getting the index of the current row being evaluated.
                int currentlyBoundRowIndex = (vacancyApplicantRecord.RowIndex);
                // getting the unique identifier for the applicant displayed on the current row.
                currentApplicantKey = (((gvVacanyApplicants.DataKeys[currentlyBoundRowIndex]).Values[2]).ToString());

                shortListedApplicantCounter = 0;

                // iterating through each record of Short Listed Applicants
                foreach (DataRow dataRow in dataRows)
                {
                    shortListedApplicantCounter += 1;

                    // getting the short listed applicant's Key                    
                    shortListedApplicantKey = ((dataRow.ItemArray[0]).ToString());

                    // If the currently evaluated applicants key is not the same as the currently evaluated short listed applicant key
                    // and the currently evaluated applicant's key is being compared to the last short listed applicant's key, ...
                    if ((currentApplicantKey != shortListedApplicantKey) && (shortListedApplicantCounter == shortListedVacancyApplicantCount))
                    {
                        string typeName = null;

                        CheckBox cb = null;

                        foreach (Control c in ((vacancyApplicantRecord.Cells[0]).Controls))
                        {
                            typeName = ((c.GetType()).Name);
                            if ((typeName.Equals("CheckBox")))
                            {
                                cb = ((CheckBox)c);
                                cb.Checked = false;

                                gvVacanyApplicants.Rows[currentlyBoundRowIndex].Visible = false;
                                break;
                            }
                        }
                        break;
                    }
                    else if (currentApplicantKey == shortListedApplicantKey)
                    {
                        // getting the short listed age range for Vacancy in question
                        // and applicant's birth date.
                        //vacancyAgeRange = ((dataRow.ItemArray[1]).ToString());
                        //sShortListedApplicantBirthDate = ((dataRow.ItemArray[2]).ToString());

                        string typeName = null;

                        CheckBox cb = null;

                        if (!shouldInvokeFilterOperation)
                        {
                            DetermineGridViewRowVisibility(currentlyBoundRowIndex);
                        }

                        /*foreach (Control c in ((vacancyApplicantRecord.Cells[0]).Controls))
                        {
                            typeName = ((c.GetType()).Name);
                            if ((typeName.Equals("CheckBox")))
                            {
                                cb = ((CheckBox)c);
                                cb.Checked = true;

                                if (!shouldInvokeFilterOperation)
                                {
                                    DetermineGridViewRowVisibility(currentlyBoundRowIndex);
                                }
                                break;
                            }
                        }*/
                        break;
                        #region If Age Checked
                        /*if (cbAgeRange.Checked)
                        {
                            vacancyAgeRange = (vacancyAgeRange.Trim());
                            int indexOfHyphen = vacancyAgeRange.IndexOf("-");
                            string sFromAge = (vacancyAgeRange.Substring(0, (indexOfHyphen - 1)));
                            string sToAge = (vacancyAgeRange.Substring((indexOfHyphen + 2)));

                            int iFromAge = (int.Parse(sFromAge));
                            int iToAge = (int.Parse(sToAge));

                            int currentYear = ((DateTime.Now).Year);

                            DateTime dShortListedApplicantBirthDate = (DateTime.Parse(sShortListedApplicantBirthDate));
                            int applicantBirthYear = (dShortListedApplicantBirthDate.Year);

                            int applicantAge = (currentYear - applicantBirthYear);

                            if ((applicantAge >= iFromAge) && ((applicantAge <= iToAge)))
                            {
                                string typeName = null;

                                CheckBox cb = null;

                                foreach (Control c in ((vacancyApplicantRecord.Cells[0]).Controls))
                                {
                                    typeName = ((c.GetType()).Name);
                                    if ((typeName.Equals("CheckBox")))
                                    {
                                        cb = ((CheckBox)c);
                                        cb.Checked = true;
                                    }
                                }
                                break;
                            }
                        }
                        else
                        {
                            string typeName = null;

                            CheckBox cb = null;

                            foreach (Control c in ((vacancyApplicantRecord.Cells[0]).Controls))
                            {
                                typeName = ((c.GetType()).Name);
                                if ((typeName.Equals("CheckBox")))
                                {
                                    cb = ((CheckBox)c);
                                    cb.Checked = true;
                                }
                            }
                            break;
                        }*/

                        #endregion
                    }
                }
            }
        }

        // Routine to determine what applicant records to check on the GridView as to meeting up with vacancy criteria.
        private void DetermineRecordsToDisplay(GridViewRow currentlyBoundRow)
        {
            // Retrieving the records for short listed application.
            DataTable dataTable = (dataSet.Tables[0]);
            DataRowCollection dataRows = (dataTable.Rows);

            string currentApplicantKey = null;

            string shortListedApplicantKey = null;
            //string vacancyAgeRange = null;
            //string sShortListedApplicantBirthDate = null;

            int currentlyBoundRowIndex = (currentlyBoundRow.RowIndex);
            currentApplicantKey = (((gvVacanyApplicants.DataKeys[currentlyBoundRowIndex]).Values[2]).ToString());

            // iterating through each record of Short Listed Applicants
            foreach (DataRow dataRow in dataRows)
            {
                // getting the short listed applicant's Key                    
                shortListedApplicantKey = ((dataRow.ItemArray[0]).ToString());

                if (currentApplicantKey == shortListedApplicantKey)
                {
                    string typeName = null;
                    CheckBox cb = null;

                    foreach (Control c in ((currentlyBoundRow.Cells[0]).Controls))
                    {
                        typeName = ((c.GetType()).Name);
                        if ((typeName.Equals("CheckBox")))
                        {
                            cb = ((CheckBox)c);
                            cb.Checked = true;
                        }
                    }
                    break;

                    #region Code to Filter based on age range
                    /* getting the short listed age range for Vacancy in question
                    // and applicant's birth date.
                    vacancyAgeRange = ((dataRow.ItemArray[1]).ToString());
                    sShortListedApplicantBirthDate = ((dataRow.ItemArray[2]).ToString());

                    vacancyAgeRange = (vacancyAgeRange.Trim());
                    int indexOfHyphen = vacancyAgeRange.IndexOf("-");
                    string sFromAge = (vacancyAgeRange.Substring(0, (indexOfHyphen - 1)));
                    string sToAge = (vacancyAgeRange.Substring((indexOfHyphen + 2)));

                    int iFromAge = (int.Parse(sFromAge));
                    int iToAge = (int.Parse(sToAge));

                    int currentYear = ((DateTime.Now).Year);

                    DateTime dShortListedApplicantBirthDate = (DateTime.Parse(sShortListedApplicantBirthDate));
                    int applicantBirthYear = (dShortListedApplicantBirthDate.Year);

                    int applicantAge = (currentYear - applicantBirthYear);

                    if ((applicantAge >= iFromAge) && ((applicantAge <= iToAge)))
                    {
                        shortListedVacancyApplicantCount += 1;

                        string typeName = null;

                        CheckBox cb = null;

                        foreach (Control c in ((currentlyBoundRow.Cells[0]).Controls))
                        {
                            typeName = ((c.GetType()).Name);
                            if ((typeName.Equals("CheckBox")))
                            {
                                cb = ((CheckBox)c);
                                cb.Checked = true;
                            }
                        }
                        break;
                    }*/
                    #endregion
                }
            }
        }

        private void RetrieveDefaultShortListedApplicants()
        {
            CreateCommand((CommandType.StoredProcedure), "RetrieveDefaultVacancyApplicantsShortList");

            sVacancyID = (hfVacancyID.Value);
            sVacancyBreakDownID = (hfVacancyBreakDownID.Value);

            command.Parameters.AddWithValue("@VacancyID", sVacancyID);
            command.Parameters.AddWithValue("@VacancyBreakDownID", sVacancyBreakDownID);

            IssueCommand();
        }

        // Routine to retrieve applicants who have been shortlisted *** NOT DEFAULT ***.
        private void RetrieveShortListedApplicants()
        {
            CreateCommand((CommandType.StoredProcedure), "RetrieveShortListedApplicants");

            sVacancyBreakDownID = (hfVacancyBreakDownID.Value);

            command.Parameters.AddWithValue("@VacancyBreakDownID", sVacancyBreakDownID);

            IssueCommand();
        }

        //protected void ddlNYSCCompletionStatus_DataBound(object sender, EventArgs e)
        //{
        //    if (((ddlNYSCCompletionStatus.Items).Count) == 0)
        //    {
        //        cbNYSCRequirement.Enabled = false;
        //        ddlNYSCCompletionStatus.Enabled = false;
        //    }
        //    else
        //    {
        //        cbNYSCRequirement.Enabled = true;
        //        ddlNYSCCompletionStatus.Enabled = true;
        //    }
        //}

        protected void ddlMinimumQualification_DataBound(object sender, EventArgs e)
        {
            if (((ddlMinimumQualification.Items).Count) == 0)
            {
                cbMinimumQualification.Enabled = false;
                ddlMinimumQualification.Enabled = false;
            }
            else
            {
                cbMinimumQualification.Enabled = true;
                ddlMinimumQualification.Enabled = true;
            }
        }

        //protected void ddlClassOfQualification_DataBound(object sender, EventArgs e)
        //{
        //    if (((ddlClassOfQualification.Items).Count) == 0)
        //    {
        //        cbClassOfQualification.Enabled = false;
        //        ddlClassOfQualification.Enabled = false;
        //    }
        //    else
        //    {
        //        cbClassOfQualification.Enabled = true;
        //        ddlClassOfQualification.Enabled = true;
        //    }
        //}

        protected void ddlCountryOfOrigin_DataBound(object sender, EventArgs e)
        {
            if (((ddlCountryOfOrigin.Items).Count) == 0)
            {
                cbCountryOfOorigin.Enabled = false;
                ddlCountryOfOrigin.Enabled = false;
            }
            else
            {
                cbCountryOfOorigin.Enabled = true;
                ddlCountryOfOrigin.Enabled = true;
            }
        }

        // Implementing this event handler to hide rows whose records have already been displayed.
        protected void gvVacanyApplicants_RowCreated(object sender, GridViewRowEventArgs e)
        {
            GridViewRow justCreatedRow = (e.Row);
            int justCreatedRowIndex = (justCreatedRow.RowIndex);

            if (justCreatedRowIndex > 0)
            {
                DataKey justCreatedRowKeys = (gvVacanyApplicants.DataKeys[justCreatedRowIndex]);
                string justCreatedRowRegistrantKey = ((justCreatedRowKeys.Values[2]).ToString());

                foreach (GridViewRow gridViewRow in (gvVacanyApplicants.Rows))
                {
                    DataKey currentlyEvaluatedRowDataKeys = (gvVacanyApplicants.DataKeys[(gridViewRow.RowIndex)]);
                    string currentlyEvaluatedRowRegistrantKey = ((currentlyEvaluatedRowDataKeys.Values[2]).ToString());

                    if (currentlyEvaluatedRowRegistrantKey == justCreatedRowRegistrantKey)
                    {
                        iHiddenGridViewRowIndex = justCreatedRowIndex;
                        justCreatedRow.Visible = false;
                        repeatCount += 1;
                    }
                }
            }
        }

        // Implementing this handler to set the links to applicant CVs and hide rows that display applicants' record 
        // which have not been short listed.
        protected void gvVacanyApplicants_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow currentlyBoundRow = (e.Row);
            int rowIndex = (currentlyBoundRow.RowIndex);

            if (rowIndex > -1)
            {
                DataKey currentlyBoundRowDataKeys = (gvVacanyApplicants.DataKeys[rowIndex]);
                string currentlyBoundRowRegistrantKey = ((currentlyBoundRowDataKeys.Values[2]).ToString());

                TableCell applicantNameCell = (currentlyBoundRow.Cells[4]);
                applicantNameCell.ToolTip = currentlyBoundRowRegistrantKey;

                LinkButton applicantNameLinkButton = ((LinkButton)applicantNameCell.Controls[0]);
                applicantNameLinkButton.OnClientClick = "DisplayApplicantCV(event)";
                applicantNameLinkButton.ToolTip = "click here to view applicants c.v";

                /*if (defaultShortListedApplicantsCount > 0)
                {
                    DetermineRecordsToDisplay(currentlyBoundRow);
                }*/

                DataRowView dataRowView = ((DataRowView)(currentlyBoundRow.DataItem));
                DataRow record = (dataRowView.Row);
                bool isShortListed = Convert.ToBoolean(((record.ItemArray[7]).ToString()));

                string typeName = null;
                CheckBox cb = null;

                // Invoking routine to retrieve short listed applicantsand getting the count of short listed applicants respectively.
                RetrieveShortListedApplicants();
                int retrievedShortListedApplicantCount = (((dataSet.Tables[0]).Rows).Count);

                foreach (Control c in ((currentlyBoundRow.Cells[0]).Controls))
                {
                    typeName = ((c.GetType()).Name);
                    if ((typeName.Equals("CheckBox")))
                    {
                        if (isShortListed)
                        {
                            cb = ((CheckBox)c);
                            cb.Checked = true;

                            if ((currentlyBoundRow.Visible))
                            {
                                shortListedVacancyApplicantCount += 1;
                            }
                        }
                        else
                        {
                            cb = ((CheckBox)c);
                            cb.Checked = false;

                            // If there is at least 1 short listed applicant, hide any row that isn't shortlisted.
                            if (retrievedShortListedApplicantCount > 0)
                            {
                                currentlyBoundRow.Visible = false;
                            }
                        }
                    }
                }
            }
        }

        protected void gvVacanyApplicants_DataBound(object sender, EventArgs e)
        {
            if (shouldInvokeFilterOperation)
            {
                FilterVacancyApplicants();

                divVacancy.Attributes["style"] = "display: none";
                divCriteria.Attributes["style"] = "display: none";
                divApplicants.Attributes["style"] = "display: block";
            }

            string sVacancyApplicantCount = (((gvVacanyApplicants.Rows).Count).ToString());
            int iVacancyApplicantCount = (int.Parse(sVacancyApplicantCount));
            iVacancyApplicantCount = (iVacancyApplicantCount - repeatCount);

            if (iVacancyApplicantCount <= 0)
            {
                lbVacancyApplicantCount.Text = (String.Empty);
                lblFilteredApplicants.Text = (String.Empty);

                btnSHORTLIST.Visible = false;
                btnSUBMIT.Visible = false;
            }
            else
            {
                if (!isPostBack)
                {
                    lbVacancyApplicantCount.Text = "number of applicants (" + (iVacancyApplicantCount.ToString()) + ")";
                    lblShortListedApplicantCount.Text = "number of short listed applicants (" + (shortListedVacancyApplicantCount.ToString()) + ")";
                }

                object oCurrentStatus = (ViewState["CurrentStatus"]);
                if (oCurrentStatus != null)
                {
                    string sCurrentStatus = (oCurrentStatus.ToString());
                    if ((!isPostBack) && (sCurrentStatus.Equals(CommonMembers.PendingHiringManagerEvaluation)))
                    {
                        btnFILTER.Visible = false;
                        btnSHORTLIST.Visible = false;
                        btnSUBMIT.Visible = false;
                    }
                    else if (sCurrentStatus.Equals(CommonMembers.OpeningClosed))
                    {
                        btnFILTER.Visible = false;
                        btnSHORTLIST.Visible = false;
                        btnSUBMIT.Visible = false;
                    }
                    else
                    {
                        btnFILTER.Visible = true;
                        btnSHORTLIST.Visible = true;
                        btnSUBMIT.Visible = true;
                    }
                }
            }
        }

        protected void gvVacanyApplicants_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            
                CreateConnection();
                gvVacanyApplicants.PageIndex = (e.NewPageIndex);
               
                gvVacanyApplicants.DataBind();
           
        }

        protected void HandleApplicantNameLinkButtonOnClick(object sender, GridViewCommandEventArgs e)
        {
            string sClickedRowIndex = ((e.CommandArgument).ToString());
            int iClickedRowIndex = (int.Parse(sClickedRowIndex));
        }

        protected void HandleShortListApplicants(object sender, EventArgs e)
        {
            CreateConnection();

            CreateCommand((CommandType.StoredProcedure), "ShortListApplicant");

            TableCell checkBoxCell = null;
            CheckBox cb = null;

            string controlTypeName = null;

            connection.Open();

            // iterating through each row in the GridView that displays applicants of selected vacancy.
            foreach (GridViewRow gridViewRow in (gvVacanyApplicants.Rows))
            {
                if (!(gridViewRow.Visible))
                {
                    continue;
                }

                int currentRowIndex = (gridViewRow.RowIndex);
                DataKey dataKey = (gvVacanyApplicants.DataKeys[currentRowIndex]);
                object oVacancyBreakDownID = (dataKey.Values[1]);
                object oRegistrantRowKey = (dataKey.Values[2]);

                command.Parameters.AddWithValue("@VacancyBreakDownID", (oVacancyBreakDownID.ToString()));
                command.Parameters.AddWithValue("@RegistrantRowKey", (oRegistrantRowKey.ToString()));

                command.Parameters.Add("@RowNoAffected", SqlDbType.Int).Direction = (ParameterDirection.Output);
                command.Parameters.Add("@ErrorMessage", SqlDbType.NVarChar, 256).Direction = (ParameterDirection.Output);

                // obtaining a references to the cell that contains a checkbox for shortlisting an applicants.
                checkBoxCell = (gridViewRow.Cells[0]);

                foreach (Control c in (checkBoxCell.Controls))
                {
                    controlTypeName = ((c.GetType()).Name);
                    if ((controlTypeName.Equals("CheckBox")))
                    {
                        cb = ((CheckBox)c);

                        if ((cb.Checked))
                        {
                            command.Parameters.AddWithValue("@IsShortListed", "1");
                        }
                        else
                        {
                            command.Parameters.AddWithValue("@IsShortListed", "0");
                        }
                        break;
                    }
                }

                command.ExecuteNonQuery();
                command.Parameters.Clear();
            }

            connection.Close();

            ViewState[_ApplicantsView] = _ShortListedApplicantsView;

            RetrieveShortListedApplicants();
            DetermineRecordsToDisplay();

            lblFilteredApplicants.Visible = false;
            lblShortListedApplicantCount.Text = "number of short listed applicant(s): (" + (hfApplicantsToBeShortListedCount.Value) + ")";
            lblShortListedApplicantCount.Visible = true;

            CommonUtilities commonUtilities = new CommonUtilities();
            commonUtilities.NotifyUser((((Page)this).ClientScript), "Successfully Short listed selected applicants");

            divVacancy.Attributes["style"] = "display: none";
            divCriteria.Attributes["style"] = "display: none";
            divApplicants.Attributes["style"] = "display: block";

            btnSUBMIT.Visible = true;
        }

        // Sends a mail to Hiring Manager, H.R Advisor an Business Partner 
        protected void HandleSubmitShortListedApplicants(object sender, EventArgs e)
        {
            SPWeb currentWebsite = (((LayoutsPageBase)(this.Page)).Web);

            string[,] criterionField = new string[1, 3];
            criterionField[0, 0] = ("ID");
            criterionField[0, 1] = (CommonUtilities.FieldTypeNumber);
            criterionField[0, 2] = (hfVacancyID.Value);

            ListAccessor accessor = new ListAccessor(currentWebsite, (VacanciesList.ListName), (new string[] { (VacanciesList.RequesterFieldInternalName), (VacanciesList.BusinessPartnerFieldInternalName), (VacanciesList.HRAdvisorFieldInternalName), (VacanciesList.JobTitleFieldInternalName) }), criterionField);
            SPListItemCollection listItems = (accessor.RequiredListItems);

            if ((listItems.Count) > 0)
            {
                criterionField = new string[1, 3];
                criterionField[0, 0] = (ExternalCareersMailsList.TypeOfMailFieldInternalName);
                criterionField[0, 1] = (CommonUtilities.FieldTypeText);
                criterionField[0, 2] = ((WebConfigurationManager.AppSettings["HiringManagerShortlistingNotificationMailType"]).ToString());

                accessor = new ListAccessor(currentWebsite, (ExternalCareersMailsList.ListName), (new string[] { (ExternalCareersMailsList.SubjectFieldInternalName), (ExternalCareersMailsList.BodyFieldInternalName) }), criterionField);
                SPListItemCollection requiredMailDefinition = (accessor.RequiredListItems);

                if ((requiredMailDefinition.Count) > 0)
                {
                    SPListItem requiredItem = listItems[0];

                    string jobTitle = (requiredItem[(VacanciesList.JobTitleFieldDisplayName)].ToString());

                    CommonUtilities utilities = new CommonUtilities();
                    SPUser hiringManagerSPUser = (utilities.GetSPUserFromSPFieldUser(requiredItem, (VacanciesList.RequesterFieldDisplayName)));

                    utilities = new CommonUtilities();
                    SPUser businessPartnerSPUser = (utilities.GetSPUserFromSPFieldUser(requiredItem, (VacanciesList.BusinessPartnerFieldDisplayName)));

                    utilities = new CommonUtilities();
                    SPUser hrAdvisorSPUser = (utilities.GetSPUserFromSPFieldUser(requiredItem, (VacanciesList.HRAdvisorFieldDisplayName)));

                    SPListItem requiredMailItem = requiredMailDefinition[0];
                    object oSubject = requiredMailItem[(ExternalCareersMailsList.SubjectFieldDisplayName)];
                    string sSubject = (String.Empty);
                    if (oSubject != null)
                    {
                        sSubject = (oSubject.ToString());
                        sSubject = sSubject.Replace("@JobTitle", jobTitle);
                    }

                    object oBody = requiredMailItem[(ExternalCareersMailsList.BodyFieldDisplayName)];
                    string sBody = (String.Empty);
                    if (oBody != null)
                    {
                        string sBreakDownID = (hfVacancyBreakDownID.Value);
                        string requesterMembershipID = ((hiringManagerSPUser.ID).ToString());

                        sBody = (oBody.ToString());
                        //sBody = (sBody.Replace("@HiringManager", (hiringManagerSPUser.Name)));

                        sBody = (sBody.Replace("@JobTitle", jobTitle));

                        string url = ((WebConfigurationManager.AppSettings["HiringManagerVacanyShortlistedApplicantsLink"]).ToString());
                        string link = "<a href='" + url + "?VacancyBreakDownID=" + sBreakDownID + "'>Shortlisted Applicants</a>";
                        sBody = sBody.Replace("@VacancyShortListedApplicantsLink", link);
                    }

                    EmailFacilitator mailSendingFacilitator = new EmailFacilitator();
                    mailSendingFacilitator.Sender = ((WebConfigurationManager.AppSettings["HRRecruitmentTeamGroupMailAddress"]).ToString());
                    mailSendingFacilitator.Recipients = new string[] { (hiringManagerSPUser.Email), (businessPartnerSPUser.Email), (hrAdvisorSPUser.Email) };
                    mailSendingFacilitator.Subject = sSubject;
                    mailSendingFacilitator.Body = sBody;
                    mailSendingFacilitator.SendMail();

                    string sVacancyBreakDownID = (ViewState["VacancyBreakDownItemID"].ToString());
                    if (sVacancyBreakDownID != null)
                    {
                        string[,] fieldValues = new string[1, 3];
                        fieldValues[0, 0] = (VacancyBreakDownList.StatusFieldInternalName);
                        fieldValues[0, 1] = (CommonUtilities.FieldTypeText);
                        fieldValues[0, 2] = (CommonMembers.PendingHiringManagerEvaluation);

                        ListManipulator maipulator = new ListManipulator((this.Web), (VacancyBreakDownList.ListName));
                        maipulator.UpdateItemToList((int.Parse(sVacancyBreakDownID)), fieldValues);

                        btnSHORTLIST.Visible = false;
                        btnSUBMIT.Visible = false;
                    }

                    SPUtility.TransferToSuccessPage("A report of the short listed applicants has been sent to the Hiring Manager");
                }
                else
                {
                    return;
                }
            }
        }

        private void DetermineGridViewRowVisibility(int gridViewRowIndex)
        {
            GridViewRow gridViewRowBeingEvaluated = (gvVacanyApplicants.Rows[gridViewRowIndex]);
            DataKey gridViewRowBeingEvaluatedDataKeys = (gvVacanyApplicants.DataKeys[gridViewRowIndex]);
            string gridViewRowBeingEvaluatedRegistrantKey = ((gridViewRowBeingEvaluatedDataKeys.Values[2]).ToString());

            int gridViewRowCount = ((gvVacanyApplicants.Rows).Count);

            foreach (GridViewRow gridViewRow in (gvVacanyApplicants.Rows))
            {
                DataKey currentGridViewRowDataKeys = (gvVacanyApplicants.DataKeys[(gridViewRow.RowIndex)]);
                string currentGridViewRowRegistrantKey = ((currentGridViewRowDataKeys.Values[2]).ToString());

                // if a row exists with the same record and it's visible and the row is different from the passed row, hide the passed row
                if ((currentGridViewRowRegistrantKey == gridViewRowBeingEvaluatedRegistrantKey) && (gridViewRow.Visible) && ((gridViewRow.RowIndex) != gridViewRowIndex))
                {
                    gridViewRowBeingEvaluated.Visible = false;
                    break;
                }
                // else if a row exists with the same record and the row is not visible (***and the row compared agai nst is the last row in the GridView, ...)
                else if ((currentGridViewRowRegistrantKey == gridViewRowBeingEvaluatedRegistrantKey) && (!(gridViewRow.Visible))) //&& ((gridViewRow.RowIndex) == gridViewRowCount - 1))
                {
                    gridViewRowBeingEvaluated.Visible = true;
                }
            }
        }

        //protected void DownloadPDF_Click()
        //{
        //    foreach (GridViewRow row in gvVacanyApplicants.Rows)
        //    {
        //        // Access the CheckBox
        //        CheckBox cb = (CheckBox)row.FindControl("cbEnlist");
        //        if (cb.Checked)
        //        {
        //            var Datakey = Convert.ToInt32(gvVacanyApplicants.DataKeys[row.RowIndex].Values["RegistrantRowKey"]);
        //        }
        //    }
        //}

        protected void DownloadPDF_SingleClick()
        {
            string filenamefromusers = String.Empty;
            // Create a Document object
            
            string cnnString = WebConfigurationManager.ConnectionStrings["MTNNGCareersDBConnectionString"].ConnectionString;
            SqlConnection cnn = new SqlConnection(cnnString);
            SqlTransaction transaction;

            cnn.Open();
            transaction = cnn.BeginTransaction();

            
            try
            {
               
                //foreach (GridViewRow row in gvVacanyApplicants.Rows)
                //{
                //    // Access the CheckBox
                //    CheckBox cb = (CheckBox)row.FindControl("cbEnlist");
                //    if (cb.Checked)
                //    {
                //        var Datakey = Convert.ToInt32(gvVacanyApplicants.DataKeys[row.RowIndex].Values["RegistrantRowKey"]);
                //    }
                //}
                int i = 0;
                //foreach (var parValue in regEmp)
                foreach (GridViewRow row in gvVacanyApplicants.Rows)
                {
                    
                    var document = new Document(PageSize.A4, 50, 50, 25, 25);

                    // Create a new PdfWrite object, writing the output to a MemoryStream
                    var output = new MemoryStream();
                    var writer = PdfWriter.GetInstance(document, output);


                    //Pagination
                    TwoColumnHeaderFooter PageEventHandler = new TwoColumnHeaderFooter();
                    writer.PageEvent = PageEventHandler;
                    // Define the page header
                    //PageEventHandler.Title = "Tonytsund";// Title;
                    PageEventHandler.RegistrantName = "CV";
                    PageEventHandler.HeaderFont = FontFactory.GetFont(BaseFont.COURIER_BOLD, 10, Font.BOLD);


                    List<int> regEmp = new List<int>();
                    regEmp.Add(701226);
                    regEmp.Add(168615);

                    List<IElement> parsedHtmlElements = new List<IElement>();

                    // Open the Document for writing
                    document.Open();

                    //string contents = File.ReadAllText(Server.MapPath("/_layouts/MTNNGCareersAdminForms/cvTemplate/CV.htm"));
                    string contents = string.Empty;

                    //Create List of Contents string
                    List<string> Contents = new List<string>();
                    StringBuilder builder = new StringBuilder();
                    // Access the CheckBox
                    CheckBox cb = (CheckBox)row.FindControl("cbEnlist");
                    if (cb.Checked)
                    {
                        

                        var parValue = Convert.ToInt32(gvVacanyApplicants.DataKeys[row.RowIndex].Values["RegistrantRowKey"]);

                        string rowkey = parValue.ToString();

                        contents = File.ReadAllText(Server.MapPath("/_layouts/MTNNGCareersAdminForms/cvTemplate/CV.htm"));

                        // Command Objects for the transaction
                        SqlCommand cmd1 = new SqlCommand("RetrieveRegistrant", cnn);
                        SqlCommand cmd2 = new SqlCommand("RetrieveRegistrantContacts", cnn);
                        SqlCommand cmd3 = new SqlCommand("RetrieveRegistrantEducationalHistory", cnn);
                        SqlCommand cmd4 = new SqlCommand("RetrieveRegistrantProfessionalCertification", cnn);
                        SqlCommand cmd5 = new SqlCommand("RetrieveRegistrantSkills", cnn);
                        SqlCommand cmd6 = new SqlCommand("RetrieveRegistrantWorkExperience", cnn);

                        cmd1.CommandType = CommandType.StoredProcedure;
                        cmd2.CommandType = CommandType.StoredProcedure;
                        cmd3.CommandType = CommandType.StoredProcedure;
                        cmd4.CommandType = CommandType.StoredProcedure;
                        cmd5.CommandType = CommandType.StoredProcedure;
                        cmd6.CommandType = CommandType.StoredProcedure;

                        cmd1.Parameters.Add(new SqlParameter("@RegistrantRowKey", SqlDbType.BigInt, 50));
                        cmd1.Parameters["@RegistrantRowKey"].Value = parValue;

                        cmd1.Parameters.Add("@RowNoAffected", SqlDbType.Int).Direction = (ParameterDirection.Output);
                        cmd1.Parameters.Add("@ErrorMessage", SqlDbType.NVarChar, 256).Direction = (ParameterDirection.Output);

                        cmd2.Parameters.Add(new SqlParameter("@RegistrantRowKey", SqlDbType.BigInt, 50));
                        cmd2.Parameters["@RegistrantRowKey"].Value = parValue;// paramValue3;

                        cmd3.Parameters.Add(new SqlParameter("@RegistrantRowKey", SqlDbType.BigInt, 50));
                        cmd3.Parameters["@RegistrantRowKey"].Value = parValue;

                        cmd3.Parameters.Add("@RowNoAffected", SqlDbType.Int).Direction = (ParameterDirection.Output);
                        cmd3.Parameters.Add("@ErrorMessage", SqlDbType.NVarChar, 256).Direction = (ParameterDirection.Output);

                        cmd4.Parameters.Add(new SqlParameter("@RegistrantRowKey", SqlDbType.BigInt, 50));
                        cmd4.Parameters["@RegistrantRowKey"].Value = parValue;

                        cmd4.Parameters.Add("@RowNoAffected", SqlDbType.Int).Direction = (ParameterDirection.Output);
                        cmd4.Parameters.Add("@ErrorMessage", SqlDbType.NVarChar, 256).Direction = (ParameterDirection.Output);

                        cmd5.Parameters.Add(new SqlParameter("@RegistrantRowKey", SqlDbType.BigInt, 50));
                        cmd5.Parameters["@RegistrantRowKey"].Value = parValue;

                        cmd5.Parameters.Add("@RowNoAffected", SqlDbType.Int).Direction = (ParameterDirection.Output);
                        cmd5.Parameters.Add("@ErrorMessage", SqlDbType.NVarChar, 256).Direction = (ParameterDirection.Output);

                        cmd6.Parameters.Add(new SqlParameter("@RegistrantRowKey", SqlDbType.BigInt, 50));
                        cmd6.Parameters["@RegistrantRowKey"].Value = parValue;

                        cmd6.Parameters.Add("@RowNoAffected", SqlDbType.Int).Direction = (ParameterDirection.Output);
                        cmd6.Parameters.Add("@ErrorMessage", SqlDbType.NVarChar, 256).Direction = (ParameterDirection.Output);


                        cmd1.Transaction = transaction;
                        SqlDataReader basicInfodr = cmd1.ExecuteReader();
                        if (basicInfodr.Read())
                        {

                            // Replace the placeholders with the user-specified text
                            string FullName = basicInfodr[1].ToString() + " " + basicInfodr[2].ToString() + " " + basicInfodr[3].ToString() + " " + basicInfodr[4].ToString();
                            filenamefromusers = basicInfodr[1].ToString() + basicInfodr[2].ToString() + basicInfodr[3].ToString() + basicInfodr[4].ToString();
                            filenamefromusers = filenamefromusers.Trim();
                            Random n = new Random();
                            int stp = n.Next(1, 2000);
                            filenamefromusers = filenamefromusers + stp.ToString();
                            filenamefromusers = filenamefromusers.Trim();
                            string rowkeyids = "CVVIEW.ashx?Id=" + rowkey;
                            string cvupurl = GetuploadedFile(rowkeyids);
                            cvupurl = cvupurl.Trim();
                            cvupurl = cvupurl + rowkeyids.Trim();
                            string uploadedcv = string.Format("<a href=\"{0}\">Uploaded CV</a>", cvupurl);
                            if (CheckForDownload(rowkey) == true)
                            {
                                contents = contents.Replace("[UploadedCV]", uploadedcv);
                            }
                            else
                            {
                                contents = contents.Replace("[UploadedCV]", "");
                            }

                            contents = contents.Replace("[Name]", FullName);
                            string BirthDate = basicInfodr[5] != DBNull.Value ? basicInfodr[5].ToString() : "N/A";
                            contents = contents.Replace("[BirthDate]", BirthDate);
                            string Gender = basicInfodr[6] != DBNull.Value ? basicInfodr[6].ToString() : "N/A";
                            contents = contents.Replace("[Gender]", Gender);
                            string MaritalStatus = basicInfodr[7] != DBNull.Value ? basicInfodr[7].ToString() : "N/A";
                            contents = contents.Replace("[MaritalStatus]", MaritalStatus);
                            string Nationality = basicInfodr[8] != DBNull.Value ? basicInfodr[8].ToString() : "N/A";
                            contents = contents.Replace("[Nationality]", Nationality);
                            string StateProvince = basicInfodr[9] != DBNull.Value ? basicInfodr[9].ToString() : "N/A";
                            contents = contents.Replace("[StateProvince]", StateProvince);
                            string NYSCCompletionStatus = basicInfodr[13] != DBNull.Value ? basicInfodr[13].ToString() : "N/A";
                            contents = contents.Replace("[NYSCCompletionStatus]", NYSCCompletionStatus);
                            string PostDgrs = basicInfodr[15] != DBNull.Value ? basicInfodr[15].ToString() : "N/A";
                            contents = contents.Replace("[PostDgrs]", PostDgrs);

                        }
                        basicInfodr.Close();

                        //Contact Information
                        cmd2.Transaction = transaction;
                        SqlDataReader contactinfodr = cmd2.ExecuteReader();
                        if (contactinfodr.Read())
                        {

                            // Replace the placeholders with the user-specified text
                            string CountryOfResidence = contactinfodr[2].ToString();
                            contents = contents.Replace("[Country]", CountryOfResidence);
                            string StateProvince = contactinfodr[3] != DBNull.Value ? contactinfodr[3].ToString() : "N/A";
                            contents = contents.Replace("[StateProvince]", StateProvince);
                            string City = contactinfodr[5] != DBNull.Value ? contactinfodr[5].ToString() : "N/A";
                            contents = contents.Replace("[City]", City);
                            string Address = contactinfodr[6] != DBNull.Value ? contactinfodr[6].ToString() : "N/A";
                            contents = contents.Replace("[Address]", Address);
                            string MobilePhone = contactinfodr[8] != DBNull.Value ? contactinfodr[8].ToString() : "N/A";
                            contents = contents.Replace("[MobilePhone]", MobilePhone);
                            string PhoneOther = contactinfodr[9] != DBNull.Value ? contactinfodr[9].ToString() : "N/A";
                            contents = contents.Replace("[PhoneOther]", PhoneOther);


                        }
                        contactinfodr.Close();


                        //Education History 
                        var itemEduTable = @"<table>";

                        cmd3.Transaction = transaction;
                        SqlDataReader eduinfodr = cmd3.ExecuteReader();
                        while (eduinfodr.Read())
                        {

                            string Level = eduinfodr[3] != DBNull.Value ? eduinfodr[3].ToString() : "N/A";

                            string Institution = eduinfodr[4] != DBNull.Value ? eduinfodr[4].ToString() : "N/A";

                            string StudyCourse = eduinfodr[5] != DBNull.Value ? eduinfodr[5].ToString() : "N/A";

                            string Qualification = eduinfodr[7] != DBNull.Value ? eduinfodr[7].ToString() : "N/A";

                            string Class = eduinfodr[9] != DBNull.Value ? eduinfodr[9].ToString() : "N/A";

                            string StartDate = eduinfodr[8] != DBNull.Value ? eduinfodr[8].ToString() : "N/A";

                            string EndDate = eduinfodr[9] != DBNull.Value ? eduinfodr[9].ToString() : "N/A";

                            itemEduTable += string.Format("<tr><td style=\"font-size: 10px\">Level</td><td style=\"font-size: 10px\">{0}</td></tr><tr><td style=\"font-size: 10px\">Institution</td><td style=\"font-size: 10px\">{1}</td></tr><tr><td style=\"font-size: 10px\">Study Course</td><td style=\"font-size: 10px\">{2}</td></tr><tr><td style=\"font-size: 10px\">Qualification</td><td style=\"font-size: 10px\">{3}</td></tr><tr><td style=\"font-size: 10px\">Class</td><td style=\"font-size: 10px\">{4}</td></tr><tr><td style=\"font-size: 10px\">Start Date</td><td style=\"font-size: 10px\">{5}</td></tr><tr><td style=\"font-size: 10px\">End Date</td><td style=\"font-size: 10px\">{6}</td></tr><tr><td colspan=\"2\"><hr style=\"width:50%\"/></td></tr>", Level, Institution, StudyCourse, Qualification, Class, StartDate, EndDate);
                            // Replace the placeholders with the user-specified text

                        }
                        itemEduTable += "</table><hr/>";
                        contents = contents.Replace("[EDUCATIONALHISTORY]", itemEduTable);
                        eduinfodr.Close();

                        //Profesional Info 
                        var itemprofTable = @"<table>";

                        cmd4.Transaction = transaction;
                        SqlDataReader profinfodr = cmd4.ExecuteReader();
                        while (profinfodr.Read())
                        {

                            string Organisation = profinfodr[2] != DBNull.Value ? profinfodr[2].ToString() : "N/A";

                            string Certificate = profinfodr[3] != DBNull.Value ? profinfodr[3].ToString() : "N/A";

                            string Status = profinfodr[4] != DBNull.Value ? profinfodr[4].ToString() : "N/A";

                            string DateJoined = profinfodr[5] != DBNull.Value ? profinfodr[5].ToString() : "N/A";


                            itemprofTable += string.Format("<tr><td style=\"font-size: 10px\">Organisation</td><td style=\"font-size: 10px\">{0}</td></tr><tr><td style=\"font-size: 10px\">Certificate</td><td style=\"font-size: 10px\">{1}</td></tr><tr><td style=\"font-size: 10px\">Status</td><td style=\"font-size: 10px\">{2}</td></tr><tr><td style=\"font-size: 10px\">Date Joined</td><td style=\"font-size: 10px\">{3}</td></tr><tr><td colspan=\"2\"><hr style=\"width:50%\"/></td></tr>", Organisation, Certificate, Status, DateJoined);
                            // Replace the placeholders with the user-specified text

                        }
                        itemprofTable += "</table>";
                        contents = contents.Replace("[PROFESSIONALASSOCIATION]", itemprofTable);
                        profinfodr.Close();


                        //Skill Info 
                        var itemskillTable = @"<table>";

                        cmd5.Transaction = transaction;
                        SqlDataReader skillinfodr = cmd5.ExecuteReader();
                        while (skillinfodr.Read())
                        {

                            string skill = skillinfodr[2] != DBNull.Value ? skillinfodr[2].ToString() : "N/A";

                            string skillProfiencylevel = skillinfodr[4] != DBNull.Value ? skillinfodr[4].ToString() : "N/A";

                            string YearofExperience = skillinfodr[5] != DBNull.Value ? skillinfodr[5].ToString() : "N/A";

                            itemskillTable += string.Format("<tr><td style=\"font-size: 10px\">Skill</td><td style=\"font-size: 10px\">{0}</td></tr><tr><td style=\"font-size: 10px\">Pro. Level</td><td style=\"font-size: 10px\">{1}</td></tr><tr><td style=\"font-size: 10px\">Year of Exp.</td><td style=\"font-size: 10px\">{2}</td></tr><tr><td colspan=\"2\"><hr style=\"width:50%\"/></td></tr>", skill, skillProfiencylevel, YearofExperience);
                            // Replace the placeholders with the user-specified text

                        }
                        itemskillTable += "</table>";
                        contents = contents.Replace("[SKILL]", itemskillTable);
                        skillinfodr.Close();



                        //WorkExperience Info 
                        var itemworkexpTable = @"<table>";

                        cmd6.Transaction = transaction;
                        SqlDataReader workexpinfodr = cmd6.ExecuteReader();
                        while (workexpinfodr.Read())
                        {

                            string company = workexpinfodr[2] != DBNull.Value ? workexpinfodr[2].ToString() : "N/A";

                            string position = workexpinfodr[3] != DBNull.Value ? workexpinfodr[3].ToString() : "N/A";

                            string jobdescription = workexpinfodr[4] != DBNull.Value ? workexpinfodr[4].ToString() : "N/A";

                            string startdatework = workexpinfodr[5] != DBNull.Value ? workexpinfodr[5].ToString() : "N/A";

                            string enddatework = workexpinfodr[6] != DBNull.Value ? workexpinfodr[6].ToString() : "N/A";

                            itemworkexpTable += string.Format("<tr><td style=\"font-size: 10px\">Company</td><td style=\"font-size: 10px\">{0}</td></tr><tr><td style=\"font-size: 10px\">Position</td><td style=\"font-size: 10px\">{1}</td></tr><tr><td colspan=\"2\"></td></tr><tr><td style=\"font-size: 10px\">Job Description</td><td style=\"font-size: 10px\">{2}</td></tr><tr><td style=\"font-size: 10px\">Start Date</td><td style=\"font-size: 10px\">{3}</td></tr><tr><td style=\"font-size: 10px\">End Date</td><td style=\"font-size: 10px\">{4}</td></tr><tr><td colspan=\"2\"><hr style=\"width:50%\"/></td></tr>", company, position, jobdescription, startdatework, enddatework);
                            // Replace the placeholders with the user-specified text

                        }
                        itemworkexpTable += "</table>";
                        contents = contents.Replace("[WORKEXPERIENCE]", itemworkexpTable);
                        workexpinfodr.Close();

                        builder.AppendLine(contents);
                        System.Threading.Thread.Sleep(1000);

                        parsedHtmlElements = HTMLWorker.ParseToList(new StringReader(builder.ToString()), null);
                        foreach (var htmlElement in parsedHtmlElements)
                            document.Add(htmlElement as IElement);

                        System.Threading.Thread.Sleep(1000);



                        document.Close();

                        System.Threading.Thread.Sleep(1000);

                       Response.ContentType = "application/pdf";
                       Response.AddHeader("Content-Disposition", string.Format("attachment;filename=Receipt-{0}.pdf", filenamefromusers.Trim()));
                       Response.BinaryWrite(output.ToArray());
                       System.Threading.Thread.Sleep(1000);
                        i++;
                        Response.Write("<br/>Stand :" + i.ToString());

                   }

                   
                }
                //foreach (var eachContent in Contents)
                //{
                //var parsedHtmlElements = HTMLWorker.ParseToList(new StringReader(contents), null);
                transaction.Commit();

               
                

            }
            catch (SqlException sqlEx)
            {
                transaction.Rollback();
            }

            finally
            {
                cnn.Close();
                cnn.Dispose();
            }

            //var parsedHtmlElements = HTMLWorker.ParseToList(new StringReader(contents), null);
            //foreach (var htmlElement in parsedHtmlElements)
            //    document.Add(htmlElement as IElement);

            

        }

        protected void Download_multipleClick()
        {
            // Create a Document object
            var document = new Document(PageSize.A4, 50, 50, 25, 25);

            // Create a new PdfWrite object, writing the output to a MemoryStream
            var output = new MemoryStream();
            var writer = PdfWriter.GetInstance(document, output);


            //Pagination
            TwoColumnHeaderFooter PageEventHandler = new TwoColumnHeaderFooter();
            writer.PageEvent = PageEventHandler;
            // Define the page header
            //PageEventHandler.Title = "Tonytsund";// Title;
            PageEventHandler.RegistrantName = "CV";
            PageEventHandler.HeaderFont = FontFactory.GetFont(BaseFont.COURIER_BOLD, 10, Font.BOLD);


            List<int> regEmp = new List<int>();
            regEmp.Add(701226);
            regEmp.Add(168615);

            List<IElement> parsedHtmlElements = new List<IElement>();

            // Open the Document for writing
            document.Open();

            //string contents = File.ReadAllText(Server.MapPath("/_layouts/MTNNGCareersAdminForms/cvTemplate/CV.htm"));
            string contents = string.Empty;
            string cnnString = WebConfigurationManager.ConnectionStrings["MTNNGCareersDBConnectionString"].ConnectionString;
            SqlConnection cnn = new SqlConnection(cnnString);
            SqlTransaction transaction;

            cnn.Open();
            transaction = cnn.BeginTransaction();

            //Create List of Contents string
            List<string> Contents = new List<string>();
            StringBuilder builder = new StringBuilder();
            try
            {
                //foreach (GridViewRow row in gvVacanyApplicants.Rows)
                //{
                //    // Access the CheckBox
                //    CheckBox cb = (CheckBox)row.FindControl("cbEnlist");
                //    if (cb.Checked)
                //    {
                //        var Datakey = Convert.ToInt32(gvVacanyApplicants.DataKeys[row.RowIndex].Values["RegistrantRowKey"]);
                //    }
                //}

                //foreach (var parValue in regEmp)
                foreach (GridViewRow row in gvVacanyApplicants.Rows)
                {

                    // Access the CheckBox
                    CheckBox cb = (CheckBox)row.FindControl("cbEnlist");
                    if (cb.Checked)
                    {
                        var parValue = Convert.ToInt32(gvVacanyApplicants.DataKeys[row.RowIndex].Values["RegistrantRowKey"]);
                        string rowkey = parValue.ToString();

                        contents = File.ReadAllText(Server.MapPath("/_layouts/MTNNGCareersAdminForms/cvTemplate/CV.htm"));

                        // Command Objects for the transaction
                        SqlCommand cmd1 = new SqlCommand("RetrieveRegistrant", cnn);
                        SqlCommand cmd2 = new SqlCommand("RetrieveRegistrantContacts", cnn);
                        SqlCommand cmd3 = new SqlCommand("RetrieveRegistrantEducationalHistory", cnn);
                        SqlCommand cmd4 = new SqlCommand("RetrieveRegistrantProfessionalCertification", cnn);
                        SqlCommand cmd5 = new SqlCommand("RetrieveRegistrantSkills", cnn);
                        SqlCommand cmd6 = new SqlCommand("RetrieveRegistrantWorkExperience", cnn);

                        cmd1.CommandType = CommandType.StoredProcedure;
                        cmd2.CommandType = CommandType.StoredProcedure;
                        cmd3.CommandType = CommandType.StoredProcedure;
                        cmd4.CommandType = CommandType.StoredProcedure;
                        cmd5.CommandType = CommandType.StoredProcedure;
                        cmd6.CommandType = CommandType.StoredProcedure;

                        cmd1.Parameters.Add(new SqlParameter("@RegistrantRowKey", SqlDbType.BigInt, 50));
                        cmd1.Parameters["@RegistrantRowKey"].Value = parValue;

                        cmd1.Parameters.Add("@RowNoAffected", SqlDbType.Int).Direction = (ParameterDirection.Output);
                        cmd1.Parameters.Add("@ErrorMessage", SqlDbType.NVarChar, 256).Direction = (ParameterDirection.Output);

                        cmd2.Parameters.Add(new SqlParameter("@RegistrantRowKey", SqlDbType.BigInt, 50));
                        cmd2.Parameters["@RegistrantRowKey"].Value = parValue;// paramValue3;

                        cmd3.Parameters.Add(new SqlParameter("@RegistrantRowKey", SqlDbType.BigInt, 50));
                        cmd3.Parameters["@RegistrantRowKey"].Value = parValue;

                        cmd3.Parameters.Add("@RowNoAffected", SqlDbType.Int).Direction = (ParameterDirection.Output);
                        cmd3.Parameters.Add("@ErrorMessage", SqlDbType.NVarChar, 256).Direction = (ParameterDirection.Output);

                        cmd4.Parameters.Add(new SqlParameter("@RegistrantRowKey", SqlDbType.BigInt, 50));
                        cmd4.Parameters["@RegistrantRowKey"].Value = parValue;

                        cmd4.Parameters.Add("@RowNoAffected", SqlDbType.Int).Direction = (ParameterDirection.Output);
                        cmd4.Parameters.Add("@ErrorMessage", SqlDbType.NVarChar, 256).Direction = (ParameterDirection.Output);

                        cmd5.Parameters.Add(new SqlParameter("@RegistrantRowKey", SqlDbType.BigInt, 50));
                        cmd5.Parameters["@RegistrantRowKey"].Value = parValue;

                        cmd5.Parameters.Add("@RowNoAffected", SqlDbType.Int).Direction = (ParameterDirection.Output);
                        cmd5.Parameters.Add("@ErrorMessage", SqlDbType.NVarChar, 256).Direction = (ParameterDirection.Output);

                        cmd6.Parameters.Add(new SqlParameter("@RegistrantRowKey", SqlDbType.BigInt, 50));
                        cmd6.Parameters["@RegistrantRowKey"].Value = parValue;

                        cmd6.Parameters.Add("@RowNoAffected", SqlDbType.Int).Direction = (ParameterDirection.Output);
                        cmd6.Parameters.Add("@ErrorMessage", SqlDbType.NVarChar, 256).Direction = (ParameterDirection.Output);


                        cmd1.Transaction = transaction;
                        SqlDataReader basicInfodr = cmd1.ExecuteReader();
                        if (basicInfodr.Read())
                        {
                            string rowkeyids = "CVVIEW.ashx?Id=" + rowkey;
                            string cvupurl = GetuploadedFile(rowkeyids);
                            cvupurl = cvupurl.Trim();
                            cvupurl = cvupurl + rowkeyids.Trim();
                            string uploadedcv = string.Format("<a href=\"{0}\">Uploaded CV</a>", cvupurl);
                            if (CheckForDownload(rowkey) == true)
                            {
                                contents = contents.Replace("[UploadedCV]", uploadedcv);
                            }
                            else
                            {
                                contents = contents.Replace("[UploadedCV]", "");
                            }
                            // Replace the placeholders with the user-specified text
                            string FullName = basicInfodr[1].ToString() + " " + basicInfodr[2].ToString() + " " + basicInfodr[3].ToString() + " " + basicInfodr[4].ToString();
                            contents = contents.Replace("[Name]", FullName);
                            string BirthDate = basicInfodr[5] != DBNull.Value ? basicInfodr[5].ToString() : "N/A";
                            contents = contents.Replace("[BirthDate]", BirthDate);
                            string Gender = basicInfodr[6] != DBNull.Value ? basicInfodr[6].ToString() : "N/A";
                            contents = contents.Replace("[Gender]", Gender);
                            string MaritalStatus = basicInfodr[7] != DBNull.Value ? basicInfodr[7].ToString() : "N/A";
                            contents = contents.Replace("[MaritalStatus]", MaritalStatus);
                            string Nationality = basicInfodr[8] != DBNull.Value ? basicInfodr[8].ToString() : "N/A";
                            contents = contents.Replace("[Nationality]", Nationality);
                            string StateProvince = basicInfodr[9] != DBNull.Value ? basicInfodr[9].ToString() : "N/A";
                            contents = contents.Replace("[StateProvince]", StateProvince);
                            string NYSCCompletionStatus = basicInfodr[13] != DBNull.Value ? basicInfodr[13].ToString() : "N/A";
                            contents = contents.Replace("[NYSCCompletionStatus]", NYSCCompletionStatus);
                            string PostDgrs = basicInfodr[15] != DBNull.Value ? basicInfodr[15].ToString() : "N/A";
                            contents = contents.Replace("[PostDgrs]", PostDgrs);

                        }
                        basicInfodr.Close();

                        //Contact Information
                        cmd2.Transaction = transaction;
                        SqlDataReader contactinfodr = cmd2.ExecuteReader();
                        if (contactinfodr.Read())
                        {

                            // Replace the placeholders with the user-specified text
                            string CountryOfResidence = contactinfodr[2].ToString();
                            contents = contents.Replace("[Country]", CountryOfResidence);
                            string StateProvince = contactinfodr[3] != DBNull.Value ? contactinfodr[3].ToString() : "N/A";
                            contents = contents.Replace("[StateProvince]", StateProvince);
                            string City = contactinfodr[5] != DBNull.Value ? contactinfodr[5].ToString() : "N/A";
                            contents = contents.Replace("[City]", City);
                            string Address = contactinfodr[6] != DBNull.Value ? contactinfodr[6].ToString() : "N/A";
                            contents = contents.Replace("[Address]", Address);
                            string MobilePhone = contactinfodr[8] != DBNull.Value ? contactinfodr[8].ToString() : "N/A";
                            contents = contents.Replace("[MobilePhone]", MobilePhone);
                            string PhoneOther = contactinfodr[9] != DBNull.Value ? contactinfodr[9].ToString() : "N/A";
                            contents = contents.Replace("[PhoneOther]", PhoneOther);


                        }
                        contactinfodr.Close();


                        //Education History 
                        var itemEduTable = @"<table>";

                        cmd3.Transaction = transaction;
                        SqlDataReader eduinfodr = cmd3.ExecuteReader();
                        while (eduinfodr.Read())
                        {

                            string Level = eduinfodr[3] != DBNull.Value ? eduinfodr[3].ToString() : "N/A";

                            string Institution = eduinfodr[4] != DBNull.Value ? eduinfodr[4].ToString() : "N/A";

                            string StudyCourse = eduinfodr[5] != DBNull.Value ? eduinfodr[5].ToString() : "N/A";

                            string Qualification = eduinfodr[7] != DBNull.Value ? eduinfodr[7].ToString() : "N/A";

                            string Class = eduinfodr[9] != DBNull.Value ? eduinfodr[9].ToString() : "N/A";

                            string StartDate = eduinfodr[8] != DBNull.Value ? eduinfodr[8].ToString() : "N/A";

                            string EndDate = eduinfodr[9] != DBNull.Value ? eduinfodr[9].ToString() : "N/A";

                            itemEduTable += string.Format("<tr><td style=\"font-size: 10px\">Level</td><td style=\"font-size: 10px\">{0}</td></tr><tr><td style=\"font-size: 10px\">Institution</td><td style=\"font-size: 10px\">{1}</td></tr><tr><td style=\"font-size: 10px\">Study Course</td><td style=\"font-size: 10px\">{2}</td></tr><tr><td style=\"font-size: 10px\">Qualification</td><td style=\"font-size: 10px\">{3}</td></tr><tr><td style=\"font-size: 10px\">Class</td><td style=\"font-size: 10px\">{4}</td></tr><tr><td style=\"font-size: 10px\">Start Date</td><td style=\"font-size: 10px\">{5}</td></tr><tr><td style=\"font-size: 10px\">End Date</td><td style=\"font-size: 10px\">{6}</td></tr><tr><td colspan=\"2\"><hr style=\"width:50%\"/></td></tr>", Level, Institution, StudyCourse, Qualification, Class, StartDate, EndDate);
                            // Replace the placeholders with the user-specified text

                        }
                        itemEduTable += "</table><hr/>";
                        contents = contents.Replace("[EDUCATIONALHISTORY]", itemEduTable);
                        eduinfodr.Close();

                        //Profesional Info 
                        var itemprofTable = @"<table>";

                        cmd4.Transaction = transaction;
                        SqlDataReader profinfodr = cmd4.ExecuteReader();
                        while (profinfodr.Read())
                        {

                            string Organisation = profinfodr[2] != DBNull.Value ? profinfodr[2].ToString() : "N/A";

                            string Certificate = profinfodr[3] != DBNull.Value ? profinfodr[3].ToString() : "N/A";

                            string Status = profinfodr[4] != DBNull.Value ? profinfodr[4].ToString() : "N/A";

                            string DateJoined = profinfodr[5] != DBNull.Value ? profinfodr[5].ToString() : "N/A";


                            itemprofTable += string.Format("<tr><td style=\"font-size: 10px\">Organisation</td><td style=\"font-size: 10px\">{0}</td></tr><tr><td style=\"font-size: 10px\">Certificate</td><td style=\"font-size: 10px\">{1}</td></tr><tr><td style=\"font-size: 10px\">Status</td><td style=\"font-size: 10px\">{2}</td></tr><tr><td style=\"font-size: 10px\">Date Joined</td><td style=\"font-size: 10px\">{3}</td></tr><tr><td colspan=\"2\"><hr style=\"width:50%\"/></td></tr>", Organisation, Certificate, Status, DateJoined);
                            // Replace the placeholders with the user-specified text

                        }
                        itemprofTable += "</table>";
                        contents = contents.Replace("[PROFESSIONALASSOCIATION]", itemprofTable);
                        profinfodr.Close();


                        //Skill Info 
                        var itemskillTable = @"<table>";

                        cmd5.Transaction = transaction;
                        SqlDataReader skillinfodr = cmd5.ExecuteReader();
                        while (skillinfodr.Read())
                        {

                            string skill = skillinfodr[2] != DBNull.Value ? skillinfodr[2].ToString() : "N/A";

                            string skillProfiencylevel = skillinfodr[4] != DBNull.Value ? skillinfodr[4].ToString() : "N/A";

                            string YearofExperience = skillinfodr[5] != DBNull.Value ? skillinfodr[5].ToString() : "N/A";

                            itemskillTable += string.Format("<tr><td style=\"font-size: 10px\">Skill</td><td style=\"font-size: 10px\">{0}</td></tr><tr><td style=\"font-size: 10px\">Pro. Level</td><td style=\"font-size: 10px\">{1}</td></tr><tr><td style=\"font-size: 10px\">Year of Exp.</td><td style=\"font-size: 10px\">{2}</td></tr><tr><td colspan=\"2\"><hr style=\"width:50%\"/></td></tr>", skill, skillProfiencylevel, YearofExperience);
                            // Replace the placeholders with the user-specified text

                        }
                        itemskillTable += "</table>";
                        contents = contents.Replace("[SKILL]", itemskillTable);
                        skillinfodr.Close();



                        //WorkExperience Info 
                        var itemworkexpTable = @"<table>";

                        cmd6.Transaction = transaction;
                        SqlDataReader workexpinfodr = cmd6.ExecuteReader();
                        while (workexpinfodr.Read())
                        {

                            string company = workexpinfodr[2] != DBNull.Value ? workexpinfodr[2].ToString() : "N/A";

                            string position = workexpinfodr[3] != DBNull.Value ? workexpinfodr[3].ToString() : "N/A";

                            string jobdescription = workexpinfodr[4] != DBNull.Value ? workexpinfodr[4].ToString() : "N/A";

                            string startdatework = workexpinfodr[5] != DBNull.Value ? workexpinfodr[5].ToString() : "N/A";

                            string enddatework = workexpinfodr[6] != DBNull.Value ? workexpinfodr[6].ToString() : "N/A";

                            itemworkexpTable += string.Format("<tr><td style=\"font-size: 10px\">Company</td><td style=\"font-size: 10px\">{0}</td></tr><tr><td style=\"font-size: 10px\">Position</td><td style=\"font-size: 10px\">{1}</td></tr><tr><td colspan=\"2\"></td></tr><tr><td style=\"font-size: 10px\">Job Description</td><td style=\"font-size: 10px\">{2}</td></tr><tr><td style=\"font-size: 10px\">Start Date</td><td style=\"font-size: 10px\">{3}</td></tr><tr><td style=\"font-size: 10px\">End Date</td><td style=\"font-size: 10px\">{4}</td></tr><tr><td colspan=\"2\"><hr style=\"width:50%\"/></td></tr>", company, position, jobdescription, startdatework, enddatework);
                            // Replace the placeholders with the user-specified text

                        }
                        itemworkexpTable += "</table>";
                        contents = contents.Replace("[WORKEXPERIENCE]", itemworkexpTable);
                        workexpinfodr.Close();

                        builder.AppendLine(contents);
                    }
                }
                //foreach (var eachContent in Contents)
                //{
                //var parsedHtmlElements = HTMLWorker.ParseToList(new StringReader(contents), null);
                parsedHtmlElements = HTMLWorker.ParseToList(new StringReader(builder.ToString()), null);
                foreach (var htmlElement in parsedHtmlElements)
                    document.Add(htmlElement as IElement);
                //}
                transaction.Commit();

            }
            catch (SqlException sqlEx)
            {
                transaction.Rollback();
            }

            finally
            {
                cnn.Close();
                cnn.Dispose();
            }

            //var parsedHtmlElements = HTMLWorker.ParseToList(new StringReader(contents), null);
            //foreach (var htmlElement in parsedHtmlElements)
            //    document.Add(htmlElement as IElement);

            document.Close();

            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Disposition", string.Format("attachment;filename=Receipt-{0}.pdf", "ApplicantsCV"));
            Response.BinaryWrite(output.ToArray());

        }


        protected void DownloadPDF_MSingle()
        {
            string filenamefromusers = String.Empty;
            // Create a Document object
            var document = new Document(PageSize.A4, 50, 50, 25, 25);

            // Create a new PdfWrite object, writing the output to a MemoryStream
            var output = new MemoryStream();
            var writer = PdfWriter.GetInstance(document, output);


            //Pagination
            TwoColumnHeaderFooter PageEventHandler = new TwoColumnHeaderFooter();
            writer.PageEvent = PageEventHandler;
            // Define the page header
            //PageEventHandler.Title = "Tonytsund";// Title;
            PageEventHandler.RegistrantName = "CV";
            PageEventHandler.HeaderFont = FontFactory.GetFont(BaseFont.COURIER_BOLD, 10, Font.BOLD);


            List<int> regEmp = new List<int>();
            regEmp.Add(701226);
            regEmp.Add(168615);

            List<IElement> parsedHtmlElements = new List<IElement>();

            // Open the Document for writing
            document.Open();

            //string contents = File.ReadAllText(Server.MapPath("/_layouts/MTNNGCareersAdminForms/cvTemplate/CV.htm"));
            string contents = string.Empty;
            string cnnString = WebConfigurationManager.ConnectionStrings["MTNNGCareersDBConnectionString"].ConnectionString;
            SqlConnection cnn = new SqlConnection(cnnString);
            SqlTransaction transaction;

            cnn.Open();
            transaction = cnn.BeginTransaction();

            //Create List of Contents string
            List<string> Contents = new List<string>();
            StringBuilder builder = new StringBuilder();
            try
            {
                //foreach (GridViewRow row in gvVacanyApplicants.Rows)
                //{
                //    // Access the CheckBox
                //    CheckBox cb = (CheckBox)row.FindControl("cbEnlist");
                //    if (cb.Checked)
                //    {
                //        var Datakey = Convert.ToInt32(gvVacanyApplicants.DataKeys[row.RowIndex].Values["RegistrantRowKey"]);
                //    }
                //}

                //foreach (var parValue in regEmp)
                foreach (GridViewRow row in gvVacanyApplicants.Rows)
                {

                    // Access the CheckBox
                    CheckBox cb = (CheckBox)row.FindControl("cbEnlist");
                    if (cb.Checked)
                    {
                        var parValue = Convert.ToInt32(gvVacanyApplicants.DataKeys[row.RowIndex].Values["RegistrantRowKey"]);

                        string rowkey = parValue.ToString();
                        contents = File.ReadAllText(Server.MapPath("/_layouts/MTNNGCareersAdminForms/cvTemplate/CV.htm"));

                        // Command Objects for the transaction
                        SqlCommand cmd1 = new SqlCommand("RetrieveRegistrant", cnn);
                        SqlCommand cmd2 = new SqlCommand("RetrieveRegistrantContacts", cnn);
                        SqlCommand cmd3 = new SqlCommand("RetrieveRegistrantEducationalHistory", cnn);
                        SqlCommand cmd4 = new SqlCommand("RetrieveRegistrantProfessionalCertification", cnn);
                        SqlCommand cmd5 = new SqlCommand("RetrieveRegistrantSkills", cnn);
                        SqlCommand cmd6 = new SqlCommand("RetrieveRegistrantWorkExperience", cnn);

                        cmd1.CommandType = CommandType.StoredProcedure;
                        cmd2.CommandType = CommandType.StoredProcedure;
                        cmd3.CommandType = CommandType.StoredProcedure;
                        cmd4.CommandType = CommandType.StoredProcedure;
                        cmd5.CommandType = CommandType.StoredProcedure;
                        cmd6.CommandType = CommandType.StoredProcedure;

                        cmd1.Parameters.Add(new SqlParameter("@RegistrantRowKey", SqlDbType.BigInt, 50));
                        cmd1.Parameters["@RegistrantRowKey"].Value = parValue;

                        cmd1.Parameters.Add("@RowNoAffected", SqlDbType.Int).Direction = (ParameterDirection.Output);
                        cmd1.Parameters.Add("@ErrorMessage", SqlDbType.NVarChar, 256).Direction = (ParameterDirection.Output);

                        cmd2.Parameters.Add(new SqlParameter("@RegistrantRowKey", SqlDbType.BigInt, 50));
                        cmd2.Parameters["@RegistrantRowKey"].Value = parValue;// paramValue3;

                        cmd3.Parameters.Add(new SqlParameter("@RegistrantRowKey", SqlDbType.BigInt, 50));
                        cmd3.Parameters["@RegistrantRowKey"].Value = parValue;

                        cmd3.Parameters.Add("@RowNoAffected", SqlDbType.Int).Direction = (ParameterDirection.Output);
                        cmd3.Parameters.Add("@ErrorMessage", SqlDbType.NVarChar, 256).Direction = (ParameterDirection.Output);

                        cmd4.Parameters.Add(new SqlParameter("@RegistrantRowKey", SqlDbType.BigInt, 50));
                        cmd4.Parameters["@RegistrantRowKey"].Value = parValue;

                        cmd4.Parameters.Add("@RowNoAffected", SqlDbType.Int).Direction = (ParameterDirection.Output);
                        cmd4.Parameters.Add("@ErrorMessage", SqlDbType.NVarChar, 256).Direction = (ParameterDirection.Output);

                        cmd5.Parameters.Add(new SqlParameter("@RegistrantRowKey", SqlDbType.BigInt, 50));
                        cmd5.Parameters["@RegistrantRowKey"].Value = parValue;

                        cmd5.Parameters.Add("@RowNoAffected", SqlDbType.Int).Direction = (ParameterDirection.Output);
                        cmd5.Parameters.Add("@ErrorMessage", SqlDbType.NVarChar, 256).Direction = (ParameterDirection.Output);

                        cmd6.Parameters.Add(new SqlParameter("@RegistrantRowKey", SqlDbType.BigInt, 50));
                        cmd6.Parameters["@RegistrantRowKey"].Value = parValue;

                        cmd6.Parameters.Add("@RowNoAffected", SqlDbType.Int).Direction = (ParameterDirection.Output);
                        cmd6.Parameters.Add("@ErrorMessage", SqlDbType.NVarChar, 256).Direction = (ParameterDirection.Output);


                        cmd1.Transaction = transaction;
                        SqlDataReader basicInfodr = cmd1.ExecuteReader();
                        if (basicInfodr.Read())
                        {

                            // Replace the placeholders with the user-specified text
                            string FullName = basicInfodr[1].ToString() + " " + basicInfodr[2].ToString() + " " + basicInfodr[3].ToString() + " " + basicInfodr[4].ToString();
                            filenamefromusers = basicInfodr[1].ToString() + basicInfodr[2].ToString() + basicInfodr[3].ToString() + basicInfodr[4].ToString();
                            filenamefromusers = filenamefromusers.Trim();
                            Random n = new Random();
                            int stp = n.Next(1, 2000);
                            filenamefromusers = filenamefromusers + stp.ToString();
                            filenamefromusers = filenamefromusers.Trim();

                            string rowkeyids = "CVVIEW.ashx?Id=" + rowkey;
                            string cvupurl = GetuploadedFile(rowkeyids);
                            cvupurl = cvupurl.Trim();
                            cvupurl = cvupurl + rowkeyids.Trim();
                            string uploadedcv = string.Format("<a href=\"{0}\">Uploaded CV</a>", cvupurl);
                            if (CheckForDownload(rowkey) == true)
                            {
                                contents = contents.Replace("[UploadedCV]", uploadedcv);
                            }
                            else
                            {
                                contents = contents.Replace("[UploadedCV]", "");
                            }
                            contents = contents.Replace("[Name]", FullName);
                            string BirthDate = basicInfodr[5] != DBNull.Value ? basicInfodr[5].ToString() : "N/A";
                            contents = contents.Replace("[BirthDate]", BirthDate);
                            string Gender = basicInfodr[6] != DBNull.Value ? basicInfodr[6].ToString() : "N/A";
                            contents = contents.Replace("[Gender]", Gender);
                            string MaritalStatus = basicInfodr[7] != DBNull.Value ? basicInfodr[7].ToString() : "N/A";
                            contents = contents.Replace("[MaritalStatus]", MaritalStatus);
                            string Nationality = basicInfodr[8] != DBNull.Value ? basicInfodr[8].ToString() : "N/A";
                            contents = contents.Replace("[Nationality]", Nationality);
                            string StateProvince = basicInfodr[9] != DBNull.Value ? basicInfodr[9].ToString() : "N/A";
                            contents = contents.Replace("[StateProvince]", StateProvince);
                            string NYSCCompletionStatus = basicInfodr[13] != DBNull.Value ? basicInfodr[13].ToString() : "N/A";
                            contents = contents.Replace("[NYSCCompletionStatus]", NYSCCompletionStatus);
                            string PostDgrs = basicInfodr[15] != DBNull.Value ? basicInfodr[15].ToString() : "N/A";
                            contents = contents.Replace("[PostDgrs]", PostDgrs);

                        }
                        basicInfodr.Close();

                        //Contact Information
                        cmd2.Transaction = transaction;
                        SqlDataReader contactinfodr = cmd2.ExecuteReader();
                        if (contactinfodr.Read())
                        {

                            // Replace the placeholders with the user-specified text
                            string CountryOfResidence = contactinfodr[2].ToString();
                            contents = contents.Replace("[Country]", CountryOfResidence);
                            string StateProvince = contactinfodr[3] != DBNull.Value ? contactinfodr[3].ToString() : "N/A";
                            contents = contents.Replace("[StateProvince]", StateProvince);
                            string City = contactinfodr[5] != DBNull.Value ? contactinfodr[5].ToString() : "N/A";
                            contents = contents.Replace("[City]", City);
                            string Address = contactinfodr[6] != DBNull.Value ? contactinfodr[6].ToString() : "N/A";
                            contents = contents.Replace("[Address]", Address);
                            string MobilePhone = contactinfodr[8] != DBNull.Value ? contactinfodr[8].ToString() : "N/A";
                            contents = contents.Replace("[MobilePhone]", MobilePhone);
                            string PhoneOther = contactinfodr[9] != DBNull.Value ? contactinfodr[9].ToString() : "N/A";
                            contents = contents.Replace("[PhoneOther]", PhoneOther);


                        }
                        contactinfodr.Close();


                        //Education History 
                        var itemEduTable = @"<table>";

                        cmd3.Transaction = transaction;
                        SqlDataReader eduinfodr = cmd3.ExecuteReader();
                        while (eduinfodr.Read())
                        {

                            string Level = eduinfodr[3] != DBNull.Value ? eduinfodr[3].ToString() : "N/A";

                            string Institution = eduinfodr[4] != DBNull.Value ? eduinfodr[4].ToString() : "N/A";

                            string StudyCourse = eduinfodr[5] != DBNull.Value ? eduinfodr[5].ToString() : "N/A";

                            string Qualification = eduinfodr[7] != DBNull.Value ? eduinfodr[7].ToString() : "N/A";

                            string Class = eduinfodr[9] != DBNull.Value ? eduinfodr[9].ToString() : "N/A";

                            string StartDate = eduinfodr[8] != DBNull.Value ? eduinfodr[8].ToString() : "N/A";

                            string EndDate = eduinfodr[9] != DBNull.Value ? eduinfodr[9].ToString() : "N/A";

                            itemEduTable += string.Format("<tr><td style=\"font-size: 10px\">Level</td><td style=\"font-size: 10px\">{0}</td></tr><tr><td style=\"font-size: 10px\">Institution</td><td style=\"font-size: 10px\">{1}</td></tr><tr><td style=\"font-size: 10px\">Study Course</td><td style=\"font-size: 10px\">{2}</td></tr><tr><td style=\"font-size: 10px\">Qualification</td><td style=\"font-size: 10px\">{3}</td></tr><tr><td style=\"font-size: 10px\">Class</td><td style=\"font-size: 10px\">{4}</td></tr><tr><td style=\"font-size: 10px\">Start Date</td><td style=\"font-size: 10px\">{5}</td></tr><tr><td style=\"font-size: 10px\">End Date</td><td style=\"font-size: 10px\">{6}</td></tr><tr><td colspan=\"2\"><hr style=\"width:50%\"/></td></tr>", Level, Institution, StudyCourse, Qualification, Class, StartDate, EndDate);
                            // Replace the placeholders with the user-specified text

                        }
                        itemEduTable += "</table><hr/>";
                        contents = contents.Replace("[EDUCATIONALHISTORY]", itemEduTable);
                        eduinfodr.Close();

                        //Profesional Info 
                        var itemprofTable = @"<table>";

                        cmd4.Transaction = transaction;
                        SqlDataReader profinfodr = cmd4.ExecuteReader();
                        while (profinfodr.Read())
                        {

                            string Organisation = profinfodr[2] != DBNull.Value ? profinfodr[2].ToString() : "N/A";

                            string Certificate = profinfodr[3] != DBNull.Value ? profinfodr[3].ToString() : "N/A";

                            string Status = profinfodr[4] != DBNull.Value ? profinfodr[4].ToString() : "N/A";

                            string DateJoined = profinfodr[5] != DBNull.Value ? profinfodr[5].ToString() : "N/A";


                            itemprofTable += string.Format("<tr><td style=\"font-size: 10px\">Organisation</td><td style=\"font-size: 10px\">{0}</td></tr><tr><td style=\"font-size: 10px\">Certificate</td><td style=\"font-size: 10px\">{1}</td></tr><tr><td style=\"font-size: 10px\">Status</td><td style=\"font-size: 10px\">{2}</td></tr><tr><td style=\"font-size: 10px\">Date Joined</td><td style=\"font-size: 10px\">{3}</td></tr><tr><td colspan=\"2\"><hr style=\"width:50%\"/></td></tr>", Organisation, Certificate, Status, DateJoined);
                            // Replace the placeholders with the user-specified text

                        }
                        itemprofTable += "</table>";
                        contents = contents.Replace("[PROFESSIONALASSOCIATION]", itemprofTable);
                        profinfodr.Close();


                        //Skill Info 
                        var itemskillTable = @"<table>";

                        cmd5.Transaction = transaction;
                        SqlDataReader skillinfodr = cmd5.ExecuteReader();
                        while (skillinfodr.Read())
                        {

                            string skill = skillinfodr[2] != DBNull.Value ? skillinfodr[2].ToString() : "N/A";

                            string skillProfiencylevel = skillinfodr[4] != DBNull.Value ? skillinfodr[4].ToString() : "N/A";

                            string YearofExperience = skillinfodr[5] != DBNull.Value ? skillinfodr[5].ToString() : "N/A";

                            itemskillTable += string.Format("<tr><td style=\"font-size: 10px\">Skill</td><td style=\"font-size: 10px\">{0}</td></tr><tr><td style=\"font-size: 10px\">Pro. Level</td><td style=\"font-size: 10px\">{1}</td></tr><tr><td style=\"font-size: 10px\">Year of Exp.</td><td style=\"font-size: 10px\">{2}</td></tr><tr><td colspan=\"2\"><hr style=\"width:50%\"/></td></tr>", skill, skillProfiencylevel, YearofExperience);
                            // Replace the placeholders with the user-specified text

                        }
                        itemskillTable += "</table>";
                        contents = contents.Replace("[SKILL]", itemskillTable);
                        skillinfodr.Close();



                        //WorkExperience Info 
                        var itemworkexpTable = @"<table>";

                        cmd6.Transaction = transaction;
                        SqlDataReader workexpinfodr = cmd6.ExecuteReader();
                        while (workexpinfodr.Read())
                        {

                            string company = workexpinfodr[2] != DBNull.Value ? workexpinfodr[2].ToString() : "N/A";

                            string position = workexpinfodr[3] != DBNull.Value ? workexpinfodr[3].ToString() : "N/A";

                            string jobdescription = workexpinfodr[4] != DBNull.Value ? workexpinfodr[4].ToString() : "N/A";

                            string startdatework = workexpinfodr[5] != DBNull.Value ? workexpinfodr[5].ToString() : "N/A";

                            string enddatework = workexpinfodr[6] != DBNull.Value ? workexpinfodr[6].ToString() : "N/A";

                            itemworkexpTable += string.Format("<tr><td style=\"font-size: 10px\">Company</td><td style=\"font-size: 10px\">{0}</td></tr><tr><td style=\"font-size: 10px\">Position</td><td style=\"font-size: 10px\">{1}</td></tr><tr><td colspan=\"2\"></td></tr><tr><td style=\"font-size: 10px\">Job Description</td><td style=\"font-size: 10px\">{2}</td></tr><tr><td style=\"font-size: 10px\">Start Date</td><td style=\"font-size: 10px\">{3}</td></tr><tr><td style=\"font-size: 10px\">End Date</td><td style=\"font-size: 10px\">{4}</td></tr><tr><td colspan=\"2\"><hr style=\"width:50%\"/></td></tr>", company, position, jobdescription, startdatework, enddatework);
                            // Replace the placeholders with the user-specified text

                        }
                        itemworkexpTable += "</table>";
                        contents = contents.Replace("[WORKEXPERIENCE]", itemworkexpTable);
                        workexpinfodr.Close();

                        builder.AppendLine(contents);
                    }
                }
                //foreach (var eachContent in Contents)
                //{
                //var parsedHtmlElements = HTMLWorker.ParseToList(new StringReader(contents), null);
                parsedHtmlElements = HTMLWorker.ParseToList(new StringReader(builder.ToString()), null);
                foreach (var htmlElement in parsedHtmlElements)
                    document.Add(htmlElement as IElement);
                //}
                transaction.Commit();

            }
            catch (SqlException sqlEx)
            {
                transaction.Rollback();
            }

            finally
            {
                cnn.Close();
                cnn.Dispose();
            }

            //var parsedHtmlElements = HTMLWorker.ParseToList(new StringReader(contents), null);
            //foreach (var htmlElement in parsedHtmlElements)
            //    document.Add(htmlElement as IElement);

            document.Close();


            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Disposition", string.Format("attachment;filename=Receipt-{0}.pdf", filenamefromusers.Trim()));
            Response.BinaryWrite(output.ToArray());
            System.Threading.Thread.Sleep(1000);            

        }



        protected void DownloadPDF_Single_Click(object sender, EventArgs e)
        {
            //DownloadPDF_MSingle();
            DownloadPDF_SingleClick();
        }

        protected void DownloadPDF_Click(object sender, EventArgs e)
        {

            Download_multipleClick();
        }

        
        protected void btnCallEventFromJs_Click(object sender, EventArgs e)
        {
           
                        DownloadPDF_SingleClickcv();
         }


        //start here.............

       
        //Starting From .............

        protected void DownloadPDF_SingleClickcv()
        {
            string filenamefromusers = String.Empty;
           
            // Create a Document object
            string urltr = String.Empty;
        
            
            try
            {
                string cnnString = WebConfigurationManager.ConnectionStrings["MTNNGCareersDBConnectionString"].ConnectionString;
                SqlConnection cnn = new SqlConnection(cnnString);
                SqlTransaction transaction;
               
                cnn.Open();
                transaction = cnn.BeginTransaction();
               

                try
                {
                    
                    //foreach (GridViewRow row in gvVacanyApplicants.Rows)
                    //{
                    //    // Access the CheckBox
                    //    CheckBox cb = (CheckBox)row.FindControl("cbEnlist");
                    //    if (cb.Checked)
                    //    {
                    //        var Datakey = Convert.ToInt32(gvVacanyApplicants.DataKeys[row.RowIndex].Values["RegistrantRowKey"]);
                    //    }
                    //}
                    int i = 0;
                    //foreach (var parValue in regEmp)
                   
                     SPSecurity.RunWithElevatedPrivileges(delegate()
                            {
                                // implementation details omitted
                           
                            
                            SPWeb myWeb = SPContext.Current.Web;

                           
                            SPList docLib = myWeb.GetList(SPUrlUtility.CombineUrl(myWeb.ServerRelativeUrl, "ApplicantsCV"));
                            vacancyJobTitle = (Session["VacancyJobTitle"].ToString());
                          
                            string newsubname = vacancyJobTitle;
                               // string newsubname = "Applicants CV " + DateTime.Now.ToLongDateString();
                            try
                            {
                                var subdoclib = docLib.Items.Add("", SPFileSystemObjectType.Folder, newsubname);
                               
                                subdoclib.Update();
                            }
                            catch (Exception ex1)
                            {
                                
                            }
                           
                    foreach (GridViewRow row in gvVacanyApplicants.Rows)
                    {

                        var document = new Document(PageSize.A4, 50, 50, 25, 25);

                        // Create a new PdfWrite object, writing the output to a MemoryStream
                        var output = new MemoryStream();
                        var writer = PdfWriter.GetInstance(document, output);


                        //Pagination
                        TwoColumnHeaderFooter PageEventHandler = new TwoColumnHeaderFooter();
                        writer.PageEvent = PageEventHandler;
                        // Define the page header
                        //PageEventHandler.Title = "Tonytsund";// Title;
                        PageEventHandler.RegistrantName = "CV";
                        PageEventHandler.HeaderFont = FontFactory.GetFont(BaseFont.COURIER_BOLD, 10, Font.BOLD);


                        List<int> regEmp = new List<int>();
                        regEmp.Add(701226);
                        regEmp.Add(168615);

                        List<IElement> parsedHtmlElements = new List<IElement>();

                        // Open the Document for writing
                        document.Open();

                        //string contents = File.ReadAllText(Server.MapPath("/_layouts/MTNNGCareersAdminForms/cvTemplate/CV.htm"));
                        string contents = string.Empty;

                        //Create List of Contents string
                        List<string> Contents = new List<string>();
                        StringBuilder builder = new StringBuilder();
                        // Access the CheckBox
                        CheckBox cb = (CheckBox)row.FindControl("cbEnlist");
                        if (cb.Checked)
                        {


                            var parValue = Convert.ToInt32(gvVacanyApplicants.DataKeys[row.RowIndex].Values["RegistrantRowKey"]);

                            string rowkey = parValue.ToString();
                            contents = File.ReadAllText(Server.MapPath("/_layouts/MTNNGCareersAdminForms/cvTemplate/CV.htm"));

                            // Command Objects for the transaction
                            SqlCommand cmd1 = new SqlCommand("RetrieveRegistrant", cnn);
                            SqlCommand cmd2 = new SqlCommand("RetrieveRegistrantContacts", cnn);
                            SqlCommand cmd3 = new SqlCommand("RetrieveRegistrantEducationalHistory", cnn);
                            SqlCommand cmd4 = new SqlCommand("RetrieveRegistrantProfessionalCertification", cnn);
                            SqlCommand cmd5 = new SqlCommand("RetrieveRegistrantSkills", cnn);
                            SqlCommand cmd6 = new SqlCommand("RetrieveRegistrantWorkExperience", cnn);

                            cmd1.CommandType = CommandType.StoredProcedure;
                            cmd2.CommandType = CommandType.StoredProcedure;
                            cmd3.CommandType = CommandType.StoredProcedure;
                            cmd4.CommandType = CommandType.StoredProcedure;
                            cmd5.CommandType = CommandType.StoredProcedure;
                            cmd6.CommandType = CommandType.StoredProcedure;

                            cmd1.Parameters.Add(new SqlParameter("@RegistrantRowKey", SqlDbType.BigInt, 50));
                            cmd1.Parameters["@RegistrantRowKey"].Value = parValue;

                            cmd1.Parameters.Add("@RowNoAffected", SqlDbType.Int).Direction = (ParameterDirection.Output);
                            cmd1.Parameters.Add("@ErrorMessage", SqlDbType.NVarChar, 256).Direction = (ParameterDirection.Output);

                            cmd2.Parameters.Add(new SqlParameter("@RegistrantRowKey", SqlDbType.BigInt, 50));
                            cmd2.Parameters["@RegistrantRowKey"].Value = parValue;// paramValue3;

                            cmd3.Parameters.Add(new SqlParameter("@RegistrantRowKey", SqlDbType.BigInt, 50));
                            cmd3.Parameters["@RegistrantRowKey"].Value = parValue;

                            cmd3.Parameters.Add("@RowNoAffected", SqlDbType.Int).Direction = (ParameterDirection.Output);
                            cmd3.Parameters.Add("@ErrorMessage", SqlDbType.NVarChar, 256).Direction = (ParameterDirection.Output);

                            cmd4.Parameters.Add(new SqlParameter("@RegistrantRowKey", SqlDbType.BigInt, 50));
                            cmd4.Parameters["@RegistrantRowKey"].Value = parValue;

                            cmd4.Parameters.Add("@RowNoAffected", SqlDbType.Int).Direction = (ParameterDirection.Output);
                            cmd4.Parameters.Add("@ErrorMessage", SqlDbType.NVarChar, 256).Direction = (ParameterDirection.Output);

                            cmd5.Parameters.Add(new SqlParameter("@RegistrantRowKey", SqlDbType.BigInt, 50));
                            cmd5.Parameters["@RegistrantRowKey"].Value = parValue;

                            cmd5.Parameters.Add("@RowNoAffected", SqlDbType.Int).Direction = (ParameterDirection.Output);
                            cmd5.Parameters.Add("@ErrorMessage", SqlDbType.NVarChar, 256).Direction = (ParameterDirection.Output);

                            cmd6.Parameters.Add(new SqlParameter("@RegistrantRowKey", SqlDbType.BigInt, 50));
                            cmd6.Parameters["@RegistrantRowKey"].Value = parValue;

                            cmd6.Parameters.Add("@RowNoAffected", SqlDbType.Int).Direction = (ParameterDirection.Output);
                            cmd6.Parameters.Add("@ErrorMessage", SqlDbType.NVarChar, 256).Direction = (ParameterDirection.Output);


                            cmd1.Transaction = transaction;
                            SqlDataReader basicInfodr = cmd1.ExecuteReader();
                            if (basicInfodr.Read())
                            {

                                // Replace the placeholders with the user-specified text
                                string FullName = basicInfodr[1].ToString() + " " + basicInfodr[2].ToString() + " " + basicInfodr[3].ToString() + " " + basicInfodr[4].ToString();
                                filenamefromusers = basicInfodr[2].ToString() +" "+ basicInfodr[3].ToString() + " " + basicInfodr[4].ToString();
                                filenamefromusers = filenamefromusers.Trim();
                                Random n = new Random();
                                int stp = n.Next(1, 2000);
                                filenamefromusers = filenamefromusers +" key_"+stp.ToString();
                                filenamefromusers = filenamefromusers.Trim();

                                string rowkeyids = "CVVIEW.ashx?Id=" + rowkey;
                                string cvupurl = GetuploadedFile(rowkeyids);
                                cvupurl = cvupurl.Trim();
                                cvupurl = cvupurl + rowkeyids.Trim();
                                string uploadedcv = string.Format("<a href=\"{0}\">Uploaded CV</a>", cvupurl);
                                if (CheckForDownload(rowkey) == true)
                                {
                                    contents = contents.Replace("[UploadedCV]", uploadedcv);
                                }
                                else
                                {
                                    contents = contents.Replace("[UploadedCV]", "");
                                }
                                contents = contents.Replace("[Name]", FullName);
                                string BirthDate = basicInfodr[5] != DBNull.Value ? basicInfodr[5].ToString() : "N/A";
                                contents = contents.Replace("[BirthDate]", BirthDate);
                                string Gender = basicInfodr[6] != DBNull.Value ? basicInfodr[6].ToString() : "N/A";
                                contents = contents.Replace("[Gender]", Gender);
                                string MaritalStatus = basicInfodr[7] != DBNull.Value ? basicInfodr[7].ToString() : "N/A";
                                contents = contents.Replace("[MaritalStatus]", MaritalStatus);
                                string Nationality = basicInfodr[8] != DBNull.Value ? basicInfodr[8].ToString() : "N/A";
                                contents = contents.Replace("[Nationality]", Nationality);
                                string StateProvince = basicInfodr[9] != DBNull.Value ? basicInfodr[9].ToString() : "N/A";
                                contents = contents.Replace("[StateProvince]", StateProvince);
                                string NYSCCompletionStatus = basicInfodr[13] != DBNull.Value ? basicInfodr[13].ToString() : "N/A";
                                contents = contents.Replace("[NYSCCompletionStatus]", NYSCCompletionStatus);
                                string PostDgrs = basicInfodr[15] != DBNull.Value ? basicInfodr[15].ToString() : "N/A";
                                contents = contents.Replace("[PostDgrs]", PostDgrs);

                            }
                            basicInfodr.Close();

                            //Contact Information
                            cmd2.Transaction = transaction;
                            SqlDataReader contactinfodr = cmd2.ExecuteReader();
                            if (contactinfodr.Read())
                            {

                                // Replace the placeholders with the user-specified text
                                string CountryOfResidence = contactinfodr[2].ToString();
                                contents = contents.Replace("[Country]", CountryOfResidence);
                                string StateProvince = contactinfodr[3] != DBNull.Value ? contactinfodr[3].ToString() : "N/A";
                                contents = contents.Replace("[StateProvince]", StateProvince);
                                string City = contactinfodr[5] != DBNull.Value ? contactinfodr[5].ToString() : "N/A";
                                contents = contents.Replace("[City]", City);
                                string Address = contactinfodr[6] != DBNull.Value ? contactinfodr[6].ToString() : "N/A";
                                contents = contents.Replace("[Address]", Address);
                                string MobilePhone = contactinfodr[8] != DBNull.Value ? contactinfodr[8].ToString() : "N/A";
                                contents = contents.Replace("[MobilePhone]", MobilePhone);
                                string PhoneOther = contactinfodr[9] != DBNull.Value ? contactinfodr[9].ToString() : "N/A";
                                contents = contents.Replace("[PhoneOther]", PhoneOther);


                            }
                            contactinfodr.Close();


                            //Education History 
                            var itemEduTable = @"<table>";

                            cmd3.Transaction = transaction;
                            SqlDataReader eduinfodr = cmd3.ExecuteReader();
                            while (eduinfodr.Read())
                            {

                                string Level = eduinfodr[3] != DBNull.Value ? eduinfodr[3].ToString() : "N/A";

                                string Institution = eduinfodr[4] != DBNull.Value ? eduinfodr[4].ToString() : "N/A";

                                string StudyCourse = eduinfodr[5] != DBNull.Value ? eduinfodr[5].ToString() : "N/A";

                                string Qualification = eduinfodr[7] != DBNull.Value ? eduinfodr[7].ToString() : "N/A";

                                string Class = eduinfodr[9] != DBNull.Value ? eduinfodr[9].ToString() : "N/A";

                                string StartDate = eduinfodr[8] != DBNull.Value ? eduinfodr[8].ToString() : "N/A";

                                string EndDate = eduinfodr[9] != DBNull.Value ? eduinfodr[9].ToString() : "N/A";

                                itemEduTable += string.Format("<tr><td style=\"font-size: 10px\">Level</td><td style=\"font-size: 10px\">{0}</td></tr><tr><td style=\"font-size: 10px\">Institution</td><td style=\"font-size: 10px\">{1}</td></tr><tr><td style=\"font-size: 10px\">Study Course</td><td style=\"font-size: 10px\">{2}</td></tr><tr><td style=\"font-size: 10px\">Qualification</td><td style=\"font-size: 10px\">{3}</td></tr><tr><td style=\"font-size: 10px\">Class</td><td style=\"font-size: 10px\">{4}</td></tr><tr><td style=\"font-size: 10px\">Start Date</td><td style=\"font-size: 10px\">{5}</td></tr><tr><td style=\"font-size: 10px\">End Date</td><td style=\"font-size: 10px\">{6}</td></tr><tr><td colspan=\"2\"><hr style=\"width:50%\"/></td></tr>", Level, Institution, StudyCourse, Qualification, Class, StartDate, EndDate);
                                // Replace the placeholders with the user-specified text

                            }
                            itemEduTable += "</table><hr/>";
                            contents = contents.Replace("[EDUCATIONALHISTORY]", itemEduTable);
                            eduinfodr.Close();

                            //Profesional Info 
                            var itemprofTable = @"<table>";

                            cmd4.Transaction = transaction;
                            SqlDataReader profinfodr = cmd4.ExecuteReader();
                            while (profinfodr.Read())
                            {

                                string Organisation = profinfodr[2] != DBNull.Value ? profinfodr[2].ToString() : "N/A";

                                string Certificate = profinfodr[3] != DBNull.Value ? profinfodr[3].ToString() : "N/A";

                                string Status = profinfodr[4] != DBNull.Value ? profinfodr[4].ToString() : "N/A";

                                string DateJoined = profinfodr[5] != DBNull.Value ? profinfodr[5].ToString() : "N/A";


                                itemprofTable += string.Format("<tr><td style=\"font-size: 10px\">Organisation</td><td style=\"font-size: 10px\">{0}</td></tr><tr><td style=\"font-size: 10px\">Certificate</td><td style=\"font-size: 10px\">{1}</td></tr><tr><td style=\"font-size: 10px\">Status</td><td style=\"font-size: 10px\">{2}</td></tr><tr><td style=\"font-size: 10px\">Date Joined</td><td style=\"font-size: 10px\">{3}</td></tr><tr><td colspan=\"2\"><hr style=\"width:50%\"/></td></tr>", Organisation, Certificate, Status, DateJoined);
                                // Replace the placeholders with the user-specified text

                            }
                            itemprofTable += "</table>";
                            contents = contents.Replace("[PROFESSIONALASSOCIATION]", itemprofTable);
                            profinfodr.Close();


                            //Skill Info 
                            var itemskillTable = @"<table>";

                            cmd5.Transaction = transaction;
                            SqlDataReader skillinfodr = cmd5.ExecuteReader();
                            while (skillinfodr.Read())
                            {

                                string skill = skillinfodr[2] != DBNull.Value ? skillinfodr[2].ToString() : "N/A";

                                string skillProfiencylevel = skillinfodr[4] != DBNull.Value ? skillinfodr[4].ToString() : "N/A";

                                string YearofExperience = skillinfodr[5] != DBNull.Value ? skillinfodr[5].ToString() : "N/A";

                                itemskillTable += string.Format("<tr><td style=\"font-size: 10px\">Skill</td><td style=\"font-size: 10px\">{0}</td></tr><tr><td style=\"font-size: 10px\">Pro. Level</td><td style=\"font-size: 10px\">{1}</td></tr><tr><td style=\"font-size: 10px\">Year of Exp.</td><td style=\"font-size: 10px\">{2}</td></tr><tr><td colspan=\"2\"><hr style=\"width:50%\"/></td></tr>", skill, skillProfiencylevel, YearofExperience);
                                // Replace the placeholders with the user-specified text

                            }
                            itemskillTable += "</table>";
                            contents = contents.Replace("[SKILL]", itemskillTable);
                            skillinfodr.Close();



                            //WorkExperience Info 
                            var itemworkexpTable = @"<table>";

                            cmd6.Transaction = transaction;
                            SqlDataReader workexpinfodr = cmd6.ExecuteReader();
                            while (workexpinfodr.Read())
                            {

                                string company = workexpinfodr[2] != DBNull.Value ? workexpinfodr[2].ToString() : "N/A";

                                string position = workexpinfodr[3] != DBNull.Value ? workexpinfodr[3].ToString() : "N/A";

                                string jobdescription = workexpinfodr[4] != DBNull.Value ? workexpinfodr[4].ToString() : "N/A";

                                string startdatework = workexpinfodr[5] != DBNull.Value ? workexpinfodr[5].ToString() : "N/A";

                                string enddatework = workexpinfodr[6] != DBNull.Value ? workexpinfodr[6].ToString() : "N/A";

                                itemworkexpTable += string.Format("<tr><td style=\"font-size: 10px\">Company</td><td style=\"font-size: 10px\">{0}</td></tr><tr><td style=\"font-size: 10px\">Position</td><td style=\"font-size: 10px\">{1}</td></tr><tr><td colspan=\"2\"></td></tr><tr><td style=\"font-size: 10px\">Job Description</td><td style=\"font-size: 10px\">{2}</td></tr><tr><td style=\"font-size: 10px\">Start Date</td><td style=\"font-size: 10px\">{3}</td></tr><tr><td style=\"font-size: 10px\">End Date</td><td style=\"font-size: 10px\">{4}</td></tr><tr><td colspan=\"2\"><hr style=\"width:50%\"/></td></tr>", company, position, jobdescription, startdatework, enddatework);
                                // Replace the placeholders with the user-specified text

                            }
                            itemworkexpTable += "</table>";
                            contents = contents.Replace("[WORKEXPERIENCE]", itemworkexpTable);
                            workexpinfodr.Close();

                            builder.AppendLine(contents);
                            System.Threading.Thread.Sleep(1000);

                            parsedHtmlElements = HTMLWorker.ParseToList(new StringReader(builder.ToString()), null);
                            foreach (var htmlElement in parsedHtmlElements)
                                document.Add(htmlElement as IElement);

                            System.Threading.Thread.Sleep(1000);



                            document.Close();

                            System.Threading.Thread.Sleep(1000);

                            // Response.ContentType = "application/pdf";
                            // Response.AddHeader("Content-Disposition", string.Format("attachment;filename=Receipt-{0}.pdf", filenamefromusers.Trim()));
                            //Response.BinaryWrite(output.ToArray());
                            // System.Threading.Thread.Sleep(1000);
                           
                            //string uploadPath = TextBox1.Text;
                            
                           // uploadPath = uploadPath.Remove(2);
                           // File.AppendAllText("C:\\ExcelFile\\path.log", "\r" + uploadPath);
                            string filename = filenamefromusers.Trim();
                            string fileext = ".pdf";
                            string fnameext = filename + fileext;
                            //string newFileName = uploadPath+filename + fileext;
                           
                            //FileStream writeStream;
                          // writeStream = new FileStream(newFileName, FileMode.Create);
                          // BinaryWriter writeBinay = new BinaryWriter(writeStream);
                          // writeBinay.Write(output.ToArray());
                           MemoryStream mstream = new MemoryStream(output.ToArray());
                           //writeBinay.Close();

                            urltr =  newsubname;
                           
                            SPFile file = docLib.RootFolder.SubFolders[SPUrlUtility.CombineUrl(myWeb.ServerRelativeUrl, "/ApplicantsCV/"+newsubname)].Files.Add(fnameext, mstream, false);

                            file.Update();


                            i++;

                            






                        }


                    }
                            });

                     if (i > 0)
                     {
                         String csname1 = "PopupScript";
                       
                         Type cstype = this.GetType();
                       

                         // Get a ClientScriptManager reference from the Page class.
                         ClientScriptManager cs = Page.ClientScript;
                       

                         // Check to see if the startup script is already registered.
                         if (!cs.IsStartupScriptRegistered(cstype, csname1))
                         {
                             string urll = Request.Url.AbsoluteUri.ToString();
                             int hstint = urll.IndexOf("_");
                             string hst = urll.Substring(0, hstint - 1);
                             string newurl = hst + "/hr/ApplicantsCV/" + urltr;

                             //File.AppendAllText("C:\\ExcelFile\\error45.log", newurl);
                             String cstext1 = "if(confirm('Download Completed Successfully. Do you want to view the download??')){ window.open('" + newurl.Trim() + "'); } else {  }";
                            // String cstext1 = "alert('Download Completed Successfully');";
                             //String cstext1 = "if(confirm('Download Completed Successfully \nDo you want to view the download??')){ window.location =" + newurl.Trim() + "; }else{alert('false');}";
                            
                             cs.RegisterStartupScript(cstype, csname1, cstext1, true);
                             //String cstext2 = "if(confirm('Download Completed Successfully \nDo you want to view the download??')){ window.location =" + newurl.Trim() + "; }else{alert('false');}";
                            
                             //cs1.RegisterStartupScript(cstype, csname2, cstext2, true);
                             //cs1.RegisterStartupScript(cstype, csname2, "window.open('" + newurl + "','Applicants Cvs','height=400,width=500');", true);
                            //cs1.RegisterStartupScript(this.Page.GetType(), "", "window.open('page.aspx','Graph','height=400,width=500');", true);
                            // String doSomething = "alert('true');";

                            // Response.Write("<Script>if(confirm('Do you want to view the download??')){ window.location =" + newurl.Trim() + "; }else{alert('false');}</Script>");
                            // Response.Write("<Script>if(confirm('Do you want to view the download??')){ window.location =" + newurl.Trim() + "; }else{alert('false');}</Script>");

                    
                            
                             //Response.Write("<script type='text/javascript'>window.open('" + newurl + "');</script>");

                             //Response.Redirect(newurl, "_blank", "menubar=0,scrollbars=1,width=780,height=900,top=10");
                            //Response.Redirect(newurl);
                         }


                     }
                    //foreach (var eachContent in Contents)
                    //{
                    //var parsedHtmlElements = HTMLWorker.ParseToList(new StringReader(contents), null);
                    transaction.Commit();




                }
                catch (SqlException sqlEx)
                {
                   // File.AppendAllText("C:\\ExcelFile\\error23.log", sqlEx.ToString());
                    transaction.Rollback();
                }

                finally
                {
                    cnn.Close();
                    cnn.Dispose();
                }

                //var parsedHtmlElements = HTMLWorker.ParseToList(new StringReader(contents), null);
                //foreach (var htmlElement in parsedHtmlElements)
                //    document.Add(htmlElement as IElement);

            }
            catch (Exception ex)
            {
                
            }

        }



        protected string GetuploadedFile(string rowkey)
        {
            string url = "";
            try
            {
                SPSecurity.RunWithElevatedPrivileges(delegate()
                                 {
                                     // implementation details omitted


                                     SPWeb myWeb = SPContext.Current.Web;


                                     //SPList docList = myWeb.GetList(SPUrlUtility.CombineUrl(myWeb.ServerRelativeUrl, "/List/MTNCareerSettings"));
                                     SPListItemCollection docList = myWeb.GetList(SPUrlUtility.CombineUrl(myWeb.ServerRelativeUrl, "Lists/MTNCareerSettings")).Items;
                                     foreach (SPListItem item in docList)
                                     {
                                         string itemTitle = (string)item["Title"];
                                         if (itemTitle == "UploadedCVURL")
                                         {
                                             url=item["Values"].ToString();
                                         }

                                     }

                                 });
            }
            catch (Exception ex)
            {
                
            }

            return url;


        }

        protected bool CheckForDownload(string rowkey)
        {
            bool resp = false;
            string cnnString = WebConfigurationManager.ConnectionStrings["MTNNGCareersDBConnectionString"].ConnectionString;
            SqlConnection cnn = new SqlConnection(cnnString);
            SqlTransaction transaction;

            cnn.Open();
            transaction = cnn.BeginTransaction();
            try
            {
                SqlCommand comm = new SqlCommand("select * from [MTNNGCareers].[dbo].[CVUpload] where RegistrantRowKey =@rowkey", cnn, transaction);
                comm.Parameters.Add(new SqlParameter("rowkey", rowkey));
                SqlDataReader reader = comm.ExecuteReader();
                if (reader.HasRows)
                {

                    while (reader.Read())
                    {
                        resp = true;
                    }
                }
                reader.Close();
                transaction.Commit();

            }
            catch (SqlException sqlEx)
            {

                transaction.Rollback();
                resp = false;
            }

            finally
            {
                cnn.Close();
                cnn.Dispose();
            }

            return resp;
        }

    }
}

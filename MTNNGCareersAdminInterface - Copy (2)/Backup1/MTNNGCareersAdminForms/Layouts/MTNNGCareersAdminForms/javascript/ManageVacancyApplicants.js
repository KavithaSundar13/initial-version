﻿var lnkBtnVacancy;  //ClientID = '<% =lnkBtnVacancy.ClientID %>';
var lnkBtnCriteria;  //ClientID = '<% =lnkBtnCriteria.ClientID %>';
var lnkBtnApplicants;  //ClientID = '<% =lnkBtnApplicants.ClientID %>';

var divVacancy;  //ClientID = '<% =divVacancy.ClientID %>';
var divCriteria;  //ClientID = '<% =divCriteria.ClientID %>';
var divApplicants;  //ClientID = '<% =divApplicants.ClientID %>';

function HandleContentToPresent(event)
{
    var eventSource = GetEventSource(event);
    var eventSource_innerText = (eventSource.innerText);

    ObtainRequiredDivsReferences();

    DetermineContentToPresent(eventSource_innerText);

    PreventPostBack(event);
}

function ObtainRequiredDivsReferences()
{
    divVacancy = (document.getElementById(divVacancyClientID));
    divCriteria = (document.getElementById(divCriteriaClientID));
    divApplicants = (document.getElementById(divApplicantsClientID));
}

function DetermineContentToPresent(eventSource_textContent)
{
    if (eventSource_textContent == 'vacancy')
    {
        divCriteria.style.display = NoneDisplayStyle;
        divApplicants.style.display = NoneDisplayStyle;
        divVacancy.style.display = BlockDisplayStyle;
    }
    else if (eventSource_textContent == 'criteria')
    {
        divVacancy.style.display = NoneDisplayStyle;
        divApplicants.style.display = NoneDisplayStyle;
        divCriteria.style.display = BlockDisplayStyle
    }
    else if (eventSource_textContent == 'applicants')
    {
        divVacancy.style.display = NoneDisplayStyle;
        divCriteria.style.display = NoneDisplayStyle;
        divApplicants.style.display = BlockDisplayStyle;
    }
}

function Handle_cbEnlistHeader_onclick(event)
{
    var checkBox = GetEventSource(event);

    var isChecked = (checkBox.checked);

    // to hold the row that contains the source of the event.
    var eventSourceRow;
    // to hold the parent element(s) of the event source
    var eventSourceParentElement = (checkBox.parentElement);

    while ((eventSourceParentElement.tagName) != 'TABLE')
    {
        eventSourceParentElement = (eventSourceParentElement.parentElement);
    }

    // obtaining a reference to the table that display applicants data.
    var applicantsTable = eventSourceParentElement;

    var rowCount = ((applicantsTable.rows).length);

    var checkBoxCell;

    for (var i = 0; i < rowCount; i++)
    {
        checkBoxCell = ((((applicantsTable.rows).item(i)).cells).item(0));

        var cellChildCount = ((checkBoxCell.childNodes).length);
        for (var j = 0; j < cellChildCount; j++)
        {
            var childTagName = (((checkBoxCell.childNodes).item(j)).tagName);
            if (childTagName == 'INPUT')
            {
                var inputType = (((checkBoxCell.childNodes).item(j)).type);
                if (inputType == 'checkbox')
                {
                    if (isChecked == true)
                    {
                        (((checkBoxCell.childNodes).item(j)).checked) = true;
                    }
                    else
                    {
                        (((checkBoxCell.childNodes).item(j)).checked) = false;
                    }
                }
            }
        }
    }
}

function HandleShortListingInitiation(event)
{
    var shortlistedApplicants = '';
    var gvVacanyApplicants = (document.getElementById(gvVacanyApplicantsClientID));

    var records = (gvVacanyApplicants.rows);
    var rowCount = (records.length);
    var currentRow;
    var checkBoxCell;
    var applicantNameCell;
    var applicantNameAnchor;

    var shortlistedApplicantCount = 0;


    for (var i = 1; i < rowCount; i++)
    {
        currentRow = (records.item(i));

        checkBoxCell = ((currentRow.cells).item(0));

        var checkBox = ((checkBoxCell.childNodes).item(0));

        if (checkBox.checked)
        {

            shortlistedApplicantCount = (shortlistedApplicantCount + 1);

            applicantNameCell = ((currentRow.cells).item(2));
            applicantNameAnchor = ((applicantNameCell.childNodes).item(0));

            shortlistedApplicants += ("\n" + (shortlistedApplicantCount.toString()) + ". " + (applicantNameAnchor.innerText));
        }
    }

    if (shortlistedApplicantCount == 0)
    {
        PreventPostBack(event);

        alert('You have NOT selected any applicant to shortlist.');

        return;
    }

    var userResponse = confirm("Please, confirm that you would like to shortlist this/these applicant(s): " + shortlistedApplicants);

    if (!userResponse)
    {
        PreventPostBack(event);
    }
    else
    {
        var hfApplicantsToBeShortListedCount = (document.getElementById(hfApplicantsToBeShortListedCountClientID));
        hfApplicantsToBeShortListedCount.value = (shortlistedApplicantCount.toString());
    }
}

function HandleFilterInitiation(event)
{
    var cbJobDescription = (document.getElementById(cbJobDescriptionClientID)); 
    var cbNYSCRequirement = (document.getElementById(cbNYSCRequirementClientID));
    var cbClassOfQualification = (document.getElementById(cbClassOfQualificationClientID));
    var cbMaritalStatus = (document.getElementById(cbMaritalStatusClientID));
    var cbCountryOfOorigin = (document.getElementById(cbCountryOfOoriginClientID));
    var cbProfessionalQualifications = (document.getElementById(cbProfessionalQualificationsClientID));
    var cbSkillsRequired = (document.getElementById(cbSkillsRequiredClientID));
    var cbMinimumQualification = (document.getElementById(cbMinimumQualificationClientID));
    var cbCourseOfStudy = (document.getElementById(cbCourseOfStudyClientID));
    var cbPostDegreeYearsOfExperience = (document.getElementById(cbPostDegreeYearsOfExperienceClientID));
    var cbGender = (document.getElementById(cbGenderClientID));
    var cbAdditionalQualifications = (document.getElementById(cbAdditionalQualificationsClientID));

    var JobDescriptionInputFormTextBox = (document.getElementById(JobDescriptionInputFormTextBoxClientID));

    var ddlNYSCCompletionStatus = (document.getElementById(ddlNYSCCompletionStatusClientID));
    var ddlClassOfQualification = (document.getElementById(ddlClassOfQualificationClientID));

    var ddlCountryOfOrigin = (document.getElementById(ddlCountryOfOriginClientID));
    var iftProfessionalQualifications = (document.getElementById(iftProfessionalQualificationsClientID));
    var iftSkillsRequired = (document.getElementById(iftSkillsRequiredClientID));
    var ddlMinimumQualification = (document.getElementById(ddlMinimumQualificationClientID));
    var CourseOfStudyInputFormTextBox = (document.getElementById(CourseOfStudyInputFormTextBoxClientID));
    var iftPostDegreeYearsOfExperience = (document.getElementById(iftPostDegreeYearsOfExperienceClientID));

    var iftAdditionalQualifications = (document.getElementById(iftAdditionalQualificationsClientID));

    if ((cbMaritalStatus.checked))
    {
        var rblMaritalStatusSingle = (document.getElementById(rblMaritalStatusClientID + '_0'));
        var rblMaritalStatusMarried = (document.getElementById(rblMaritalStatusClientID + '_1'));

        if ((!(rblMaritalStatusSingle.checked)) && (!(rblMaritalStatusMarried.checked)))
        {
            PreventPostBack(event);
            alert('Please, ensure you specify the marital status. \n If you wish not to include it as a filter criterion then uncheck its checkbox.');
            return;
        }
    }

    if ((cbGender.checked))
    {
        var rblGenderMale = (document.getElementById(rblGenderClientID + '_0'));
        var rblGenderFemale = (document.getElementById(rblGenderClientID + '_1'));

        if ((!(rblGenderMale.checked)) && (!(rblGenderFemale.checked)))
        {
            PreventPostBack(event);
            alert('Please, ensure you specify the gender. \n If you wish not to include it as a filter criterion then uncheck its checkbox.');
            rblGenderMale.focus();
            return;
        }
    }

    if ((cbJobDescription.checked))
    {
        if ((JobDescriptionInputFormTextBox.value) == '')
        {
            PreventPostBack(event); 
            alert('Please, type in the expected job description. A keyword would suffice. \n If you wish not to include it as a filter criterion then uncheck its checkbox.');
            JobDescriptionInputFormTextBox.focus();
            return;
        }
    }

    if ((cbCourseOfStudy.checked))
    {
        if ((CourseOfStudyInputFormTextBox.value) == '')
        {
            PreventPostBack(event); 
            alert('Please, type in the expected course(s) of study. A keyword would suffice. \n If you wish not to include it as a filter criterion then uncheck its checkbox.');
            CourseOfStudyInputFormTextBox.focus();
            return;
        }
    }

    if ((cbProfessionalQualifications.checked))
    {
        if ((iftProfessionalQualifications.value) == '')
        {
            PreventPostBack(event);
            alert('Please, type in the professional qualification(s). A keyword would suffice. \n If you wish not to include it as a filter criterion then uncheck its checkbox.');
            iftProfessionalQualifications.focus();
            return;
        }
    }

    if ((cbAdditionalQualifications.checked))
    {
        if ((iftAdditionalQualifications.value) == '')
        {
            PreventPostBack(event);
            alert('Please, type in the additional qualification(s). A keyword would suffice. \n If you wish not to include it as a filter criterion then uncheck its checkbox.');
            iftAdditionalQualifications.focus();
            return;
        }
    }

    if ((cbSkillsRequired.checked))
    {
        if ((iftSkillsRequired.value) == '')
        {
            PreventPostBack(event);
            alert('Please, type in the required exeperience and training. A keyword would suffice. \n If you wish not to include it as a filter criterion then uncheck its checkbox.');
            iftSkillsRequired.focus();
            return;
        }
    }

    if (cbPostDegreeYearsOfExperience.checked)
    {
        if ((iftPostDegreeYearsOfExperience.value) == '')
        {
            PreventPostBack(event);
            alert('Please, type in the required post degree years of experience. \n If you wish not to include it as a filter criterion then uncheck its checkbox.');
            iftPostDegreeYearsOfExperience.focus();
            return;
        }
        else
        {
            if (isNaN((iftPostDegreeYearsOfExperience.value)))
            {
                PreventPostBack(event);
                alert('Invalid entry. Post Degree Years of Experience must be a number');
                iftPostDegreeYearsOfExperience.focus();
                return;
            }
        }
    }

    if ((!(cbJobDescription.checked)) && (!(cbNYSCRequirement.checked)) && (!(cbClassOfQualification.checked)) && (!(cbMaritalStatus.checked)) && (!(cbCountryOfOorigin.checked)) && (!(cbProfessionalQualifications.checked)) && (!(cbSkillsRequired.checked)) && (!(cbMinimumQualification.checked)) && (!(cbCourseOfStudy.checked)) && (!(cbPostDegreeYearsOfExperience.checked)) && (!(cbGender.checked)) && (!(cbAdditionalQualifications.checked)))
    {
        alert('You have not specified any filter criterion');
        PreventPostBack(event);
    }
}
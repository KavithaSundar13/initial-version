﻿using System;

using System.Configuration;
using System.Web.Configuration;

using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

using System.Data;
using System.Data.SqlClient;

namespace MTNNGCareers.Administration.Forms.ControlTemplates
{
    public partial class VacancyApplicantCVViewerCtrl : UserControl
    {
        SqlConnection connection;
        SqlCommand command;
        SqlDataAdapter dataAdapter;
        DataSet dataSet;

        protected void Page_Load(object sender, EventArgs e)
        {
            //CreateConnection();

            //DisplayRegistrant();
        }

        // Routine to create a connection to the MTN NG Careers DB.
        private void CreateConnection()
        {
            string connectionString = ((WebConfigurationManager.ConnectionStrings["MTNNGCareersDBConnectionString"]).ToString());
            connection = new SqlConnection(connectionString);
        }

        // Routine to create command to be issued against MTN NG Careers DB.
        private void CreateCommand(CommandType commandType, string commandText)
        {
            command = new SqlCommand();
            command.Connection = connection;
            command.CommandType = commandType;
            command.CommandText = commandText;
        }

        // Routine to issue command against database.
        private int IssueCommand()
        {
            dataAdapter = new SqlDataAdapter(command);
            dataSet = new DataSet();
            int rowCount = dataAdapter.Fill(dataSet);

            return rowCount;
        }

        // Routine to display Registrant data
        private void DisplayRegistrant()
        {
            CreateCommand((CommandType.StoredProcedure), "RetrieveRegistrant");

            string sRegistrantKey = (hfRegistrantKey.Value);
            command.Parameters.AddWithValue("@RegistrantRowKey", sRegistrantKey);

            command.Parameters.Add("@RowNoAffected", SqlDbType.Int).Direction = (ParameterDirection.Output);
            command.Parameters.Add("@ErrorMessage", SqlDbType.NVarChar, 256).Direction = (ParameterDirection.Output);

            int rowCount = IssueCommand();

            if (rowCount > 0)
            {
                DataTable dataTable = (dataSet.Tables[0]);
                
                lbSalutation.Text = (((dataTable.Rows[0]).ItemArray[2]).ToString());
                lbSurname.Text = (((dataTable.Rows[0]).ItemArray[3]).ToString());
                lbFirstName.Text = (((dataTable.Rows[0]).ItemArray[4]).ToString());

                object oOtherNames = ((dataTable.Rows[0]).ItemArray[5]);
                if (oOtherNames != null)
                {
                    lbOtherNames.Text = (oOtherNames.ToString());
                }
                else
                {
                    lbOtherNames.Text = "---";
                }

                lbAge.Text = (((dataTable.Rows[0]).ItemArray[6]).ToString());
                lbGender.Text = (((dataTable.Rows[0]).ItemArray[7]).ToString());
                lbMaritalStatus.Text = (((dataTable.Rows[0]).ItemArray[8]).ToString());
                lbCountryOfOrigin.Text = (((dataTable.Rows[0]).ItemArray[9]).ToString());

                object oStateOfOrigin = ((dataTable.Rows[0]).ItemArray[10]);
                if (oStateOfOrigin != null)
                {
                    lbStateOrProvinceOfOrigin.Text = (oStateOfOrigin.ToString());
                }
                else
                {
                    oStateOfOrigin = ((dataTable.Rows[0]).ItemArray[11]);
                    if (oStateOfOrigin != null)
                    {
                        lbStateOrProvinceOfOrigin.Text = (oStateOfOrigin.ToString());
                    }
                }

                object oLocalGovernmentArea = ((dataTable.Rows[0]).ItemArray[12]);
                if (oLocalGovernmentArea != null)
                {
                    lbLocalGovernmentArea.Text = (oLocalGovernmentArea.ToString());
                }
                else
                {
                    oLocalGovernmentArea = ((dataTable.Rows[0]).ItemArray[13]);
                    if (oLocalGovernmentArea != null)
                    {
                        lbLocalGovernmentArea.Text = (oLocalGovernmentArea.ToString());
                    }
                }
                
                lbNYSCCompletionStatus.Text = (((dataTable.Rows[0]).ItemArray[14]).ToString());
                lbMTNStaffQ.Text = (((dataTable.Rows[0]).ItemArray[15]).ToString());
            }
        }
    }
}

﻿using System;

using System.Web.UI;
using System.Web.UI.WebControls;

using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;

using MTNNGCareers.Administration.Common;

using CommonUtilities = Utilities.Common;

using SharePointListManager;

namespace MTNNGCareers.Administration.Forms.ControlTemplates
{
    public partial class DisplayVacancyCtrl : UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DisplayVacancy();
            }
        }

        // Routine to get the selected Vacancy's Field values and display them.
        private void DisplayVacancy()
        {
            SPWeb _PageWebSite = (((LayoutsPageBase)this.Page).Web);
            SPList _VacancyList = (_PageWebSite.Lists[(VacanciesList.ListName)]);
            int iVacancyID = (int.Parse(hfVacancyID.Value));
            SPListItem _Vacancy = (_VacancyList.GetItemById((iVacancyID)));

            lblJobTitle.Text = ((_Vacancy[(VacanciesList.JobTitleFieldDisplayName)]).ToString());

            string publisherName = ((_Vacancy[(VacanciesList.PublisherFieldDisplayName)]).ToString());
            publisherName = (publisherName.Substring(((publisherName.IndexOf("#")) + 1)));
            lblPublisher.Text = publisherName;            
                        
            ltrJobDesc.Text = ((_Vacancy[(VacanciesList.JobDescriptionFieldDisplayName)]).ToString());
            ltrJobCondition.Text = ((_Vacancy[(VacanciesList.JobConditionsFieldDisplayName)]).ToString());
            lblExperienceAndTraining.Text = ((_Vacancy[(VacanciesList.SkillsRequiredFieldDisplayName)]).ToString());

            string hiringManagerName = ((_Vacancy[(VacanciesList.RequesterFieldDisplayName)]).ToString());
            hiringManagerName = (hiringManagerName.Substring(((hiringManagerName.IndexOf("#")) + 1)));
            lblHiringManager.Text = hiringManagerName; 
            
             lblHiringManagerPosition.Text = ((_Vacancy[(VacanciesList.RequesterDesignationFieldInternalName)]).ToString());

             string businessPartnerName = ((_Vacancy[(VacanciesList.BusinessPartnerFieldDisplayName)]).ToString());
             businessPartnerName = (businessPartnerName.Substring(((businessPartnerName.IndexOf("#")) + 1)));
             lblBusinessPartner.Text = businessPartnerName;

             string hrAdvisorName = ((_Vacancy[(VacanciesList.HRAdvisorFieldDisplayName)]).ToString());
             hrAdvisorName = (hrAdvisorName.Substring(((hrAdvisorName.IndexOf("#")) + 1)));
             lblHRAdvisor.Text = hrAdvisorName; 

             string division = ((_Vacancy[(VacanciesList.DivisionFieldDisplayName)]).ToString());
             division = (division.Substring(((division.IndexOf("#")) + 1)));
             lblDivision.Text = division;

             lblStaffLevel.Text = ((_Vacancy[(VacanciesList.StaffLevelFieldDisplayName)]).ToString());
             lblIsNYSCRequired.Text = ((_Vacancy[(VacanciesList.IsNYSCRequiredFieldDisplayName)]).ToString());
            //lbAgeRange.Text = ((_Vacancy[(VacanciesList.AgeRangeDisplayName)]).ToString());

            object oMinimumQualifications = (_Vacancy[(VacanciesList.MinimumQualificationFieldDisplayName)]);
            if (oMinimumQualifications != null)
            {
                string sMinimumQualifications = (oMinimumQualifications.ToString());
                if (sMinimumQualifications != (CommonMembers._NOTSPECIFIED))
                {
                    sMinimumQualifications = (sMinimumQualifications.Substring(((sMinimumQualifications.IndexOf("#")) + 1)));
                }

                lblMinimumQualification.Text = sMinimumQualifications;
            }
            else
            {
                lblMinimumQualification.Text = (CommonMembers._NOTSPECIFIED);
            }

            object oAdditionalQuualifications = (_Vacancy[(VacanciesList.AdditionalQualificationFieldDisplayName)]);
            if (oAdditionalQuualifications != null)
            {
                lblAdditionalQualifications.Text = (oAdditionalQuualifications.ToString());
            }
            else
            {
                lblAdditionalQualifications.Text = (CommonMembers._NOTSPECIFIED);
            }

            object oProfessionalQuualifications = (_Vacancy[(VacanciesList.ProfessionalQualificationFieldDisplayName)]);
            if (oProfessionalQuualifications != null)
            {
                lblProfessionalQualifcations.Text = (oProfessionalQuualifications.ToString());
            }
            else
            {
                lblProfessionalQualifcations.Text = (CommonMembers._NOTSPECIFIED);
            }

            object oPostDegreeYearsOfExperience = (_Vacancy[(VacanciesList.PostDegreeYearsOfExperienceFieldDisplayName)]);
            if (oPostDegreeYearsOfExperience != null)
            {
                lblPostDegreeYearsOfExperience.Text = (oPostDegreeYearsOfExperience.ToString());
            }
            else
            {
                lblPostDegreeYearsOfExperience.Text = (CommonMembers._NOTSPECIFIED);
            }

            object oCourseOfStudy = (_Vacancy[(VacanciesList.CourseOfStudyFieldDisplayName)]);
            if (oCourseOfStudy != null)
            {
                lblPreferredCourseOfStudy.Text = (oCourseOfStudy.ToString());
            }
            else
            {
                lblPreferredCourseOfStudy.Text = (CommonMembers._NOTSPECIFIED);
            }

            object oClassOfQualification = (_Vacancy[(VacanciesList.ClassOfDegreeFieldDisplayName)]);
            if (oClassOfQualification != null)
            {
                string sClassOfQualification = (oClassOfQualification.ToString());
                sClassOfQualification = (sClassOfQualification.Substring(((sClassOfQualification.IndexOf("#")) + 1)));
                lblClassOfQualification.Text = sClassOfQualification;
            }
            else
            {
                lblClassOfQualification.Text = (CommonMembers._NOTSPECIFIED);
            }

            object oMaritalStatus = (_Vacancy[(VacanciesList.MaritalStatusFieldDisplayName)]);
            if (oMaritalStatus != null)
            {
                lblMaritalStatus.Text = (oMaritalStatus.ToString());
            }
            else
            {
                lblMaritalStatus.Text = (CommonMembers._NOTSPECIFIED);
            }

            object oGender = (_Vacancy[(VacanciesList.GenderFieldDisplayName)]);
            if (oGender != null)
            {
                lblGender.Text = (oGender.ToString());
            }
            else
            {
                lblGender.Text = (CommonMembers._NOTSPECIFIED);
            }

            int indexOfWhiteSpace = 0;

            string advertisementDate = ((_Vacancy[(VacanciesList.StartAdvertisementDateFieldDisplayName)]).ToString());
            indexOfWhiteSpace = advertisementDate.IndexOf(" ");
            if (indexOfWhiteSpace != -1)
            {
                lblStartAdvertisementDate.Text = (advertisementDate.Substring(0, indexOfWhiteSpace));
            }
            else
            {
                lblStartAdvertisementDate.Text = advertisementDate;
            }

            advertisementDate = ((_Vacancy[(VacanciesList.EndAdvertisementDateFieldDisplayName)]).ToString());
            indexOfWhiteSpace = advertisementDate.IndexOf(" ");
            if (indexOfWhiteSpace != -1)
            {
                lblEndAdvertisementDate.Text = (advertisementDate.Substring(0, indexOfWhiteSpace));
            }
            else
            {
                lblEndAdvertisementDate.Text = advertisementDate;
            }
        }
    }
}
    


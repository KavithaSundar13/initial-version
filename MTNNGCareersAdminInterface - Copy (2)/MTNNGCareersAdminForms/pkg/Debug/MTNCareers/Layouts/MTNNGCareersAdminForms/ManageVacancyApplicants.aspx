<%@ Assembly Name="MTNNGCareers.Administration.Forms, Version=1.0.0.0, Culture=neutral, PublicKeyToken=5e7c3f80f0f7816f" %>
<%@ Import Namespace="Microsoft.SharePoint.ApplicationPages" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ManageVacancyApplicants.aspx.cs"     Inherits="MTNNGCareers.Administration.Forms.ManageVacancyApplicants" DynamicMasterPageFile="~masterurl/default.master" %>

<%@ Register Assembly="AjaxControlToolkit, Version=3.0.30930.28736, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e"
    Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register TagPrefix="Tavia" TagName="VacancyViewer" Src="~/_controltemplates/MTNCareers/DisplayVacancyCtrl.ascx" %>
<asp:Content ID="PageHead" ContentPlaceHolderID="PlaceHolderAdditionalPageHead" runat="server">
    <script type="text/javascript" src="/HR/_layouts/MTNNGCareersAdminForms/javascript/Common.js"></script>
    <script type="text/javascript" src="/HR/_layouts/MTNNGCareersAdminForms/javascript/ManageVacancyApplicants.js"></script>
    <script type="text/javascript">
        var hfApplicantsToBeShortListedCountClientID = '<% =hfApplicantsToBeShortListedCount.ClientID %>';

        var lnkBtnVacancyClientID = '<% =lnkBtnVacancy.ClientID %>';
        var lnkBtnCriteriaClientID = '<% =lnkBtnCriteria.ClientID %>';
        var lnkBtnApplicantsClientID = '<% =lnkBtnApplicants.ClientID %>';

        var divVacancyClientID = '<% =divVacancy.ClientID %>';
        var divCriteriaClientID = '<% =divCriteria.ClientID %>';
        var divApplicantsClientID = '<% =divApplicants.ClientID %>';
        var cbJobDescriptionClientID = '<% =cbJobDescription.ClientID %>';
        var cbNYSCRequirementClientID = '<% =cbNYSCRequirement.ClientID %>';
        var cbClassOfQualificationClientID = '<% =cbClassOfQualification.ClientID %>';
        var cbMaritalStatusClientID = '<% =cbMaritalStatus.ClientID %>';
        var cbCountryOfOoriginClientID = '<% =cbCountryOfOorigin.ClientID %>';
        var cbProfessionalQualificationsClientID = '<% =cbProfessionalQualifications.ClientID %>';
        var cbSkillsRequiredClientID = '<% =cbSkillsRequired.ClientID %>';
        var cbMinimumQualificationClientID = '<% =cbMinimumQualification.ClientID %>';
        var cbCourseOfStudyClientID = '<% =cbCourseOfStudy.ClientID %>';
        var cbPostDegreeYearsOfExperienceClientID = '<% =cbPostDegreeYearsOfExperience.ClientID %>';
        var cbGenderClientID = '<% =cbGender.ClientID %>';
        var cbAdditionalQualificationsClientID = '<% =cbAdditionalQualifications.ClientID %>';

        var JobDescriptionInputFormTextBoxClientID = '<% =JobDescriptionInputFormTextBox.ClientID %>';
        
        
        var rblMaritalStatusClientID = '<% =rblMaritalStatus.ClientID %>';
        var ddlCountryOfOriginClientID = '<% =ddlCountryOfOrigin.ClientID %>';
        var iftProfessionalQualificationsClientID = '<% =iftProfessionalQualifications.ClientID %>';
        var iftSkillsRequiredClientID = '<% =iftSkillsRequired.ClientID %>';
        var ddlMinimumQualificationClientID = '<% =ddlMinimumQualification.ClientID %>';        
        var CourseOfStudyInputFormTextBoxClientID = '<% =CourseOfStudyInputFormTextBox.ClientID %>';
        var iftPostDegreeYearsOfExperienceClientID = '<% =iftPostDegreeYearsOfExperience.ClientID %>';
        var rblGenderClientID = '<% =rblGender.ClientID %>';
        var iftAdditionalQualificationsClientID = '<% =iftAdditionalQualifications.ClientID %>';

        var gvVacanyApplicantsClientID = '<% =gvVacanyApplicants.ClientID %>';
    </script>
    <script type="text/javascript">
         function showDirectory() {
           
             //var uploadPath = window.showModalDialog("browseDirectory.aspx", 'jain', "dialogHeight: 560px; dialogWidth: 360px; edge: Raised; center: Yes; help: Yes; resizable: Yes; status: No;");
             //if (uploadPath != 'undefined' && uploadPath != '' && uploadPath != null) {

                // document.getElementById('<%=TextBox1.ClientID %>').value = uploadPath;
                document.getElementById('<%=btnCallEventFromJs.ClientID%>').click();

            // } else {
                 
                // alert('You must specify a directory for the downloads!');
                   
             //}
             //document.all.TextBox1.value = window.showModalDialog("browseDirectory.aspx", 'jain', "dialogHeight: 560px; dialogWidth: 360px; edge: Raised; center: Yes; help: Yes; resizable: Yes; status: No;");
               
         }


    

      </script>
    <link type="text/css" rel="Stylesheet" href="/HR/_layouts/MTNNGCareersAdminForms/css/Master.css" />
    <link type="text/css" rel="Stylesheet" href="/HR/_layouts/MTNNGCareersAdminForms/css/Common.css" />
    <link type="text/css" rel="Stylesheet" href="/HR/_layouts/MTNNGCareersAdminForms/css/ManageVacancyApplicants.css" />
</asp:Content>
<asp:Content ID="Main" ContentPlaceHolderID="PlaceHolderMain" runat="server">
    <asp:HiddenField runat="server" ID="hfVacancyID" />
    <asp:HiddenField runat="server" ID="hfVacancyBreakDownID" />
    <asp:HiddenField runat="server" ID="hfApplicantsToBeShortListedCount" />
    <div>
        <table class="TabStyle-i" style="width: 40%">
            <tr>
                
                <td class="CareersTab">
                    <asp:LinkButton ID="lnkBtnVacancy" runat="server" CssClass="LinkButtonStyle" ToolTip="click here to view details of the vacancy  whose applicants you wish to short list"
                        OnClientClick="HandleContentToPresent(event)">vacancy</asp:LinkButton>
                </td>
                <td class="CareersTab">
                    <asp:LinkButton ID="lnkBtnCriteria" runat="server" CssClass="LinkButtonStyle" ToolTip="click here to specify the criteria you wish to use to short list applicants."
                        OnClientClick="HandleContentToPresent(event)">criteria</asp:LinkButton>
                </td>
                <td class="CareersTab">
                    <asp:LinkButton ID="lnkBtnApplicants" runat="server" CssClass="LinkButtonStyle" ToolTip="click here to view applicants"
                        OnClientClick="HandleContentToPresent(event)">applicants</asp:LinkButton>
                </td>
            </tr>
        </table>
    </div>
    <div id="divVacancy" runat="server" style="display: block;">
        <Tavia:VacancyViewer runat="server" ID="VacancyViewer1" />
    </div>
    <div>
    </div>
    <div id="divCriteria" runat="server" style="display: none;">
        <table>
            <tr>
                <td>
                    <div style="padding-left: 0.5%; padding-right: 0.5%;">
                        <table class="GTable-i">
                            <tr>
                                <td>
                                    <asp:CheckBox ID="cbNYSCRequirement" runat="server" />
                                </td>
                                <td>
                                    NYSC Completion Status
                                </td>
                                <td>
                                    <%--<asp:DropDownList ID="ddlNYSCCompletionStatus" runat="server" CssClass="DropDownListStyle"
                                        DataSourceID="sqlDSNYSCCompletionStatusCriterionPlausibleValues" DataTextField="NYSCCompletionStatus"
                                        DataValueField="NYSCCompletionStatus" OnDataBound="ddlNYSCCompletionStatus_DataBound">
                                        <asp:ListItem Value="1">required</asp:ListItem>
                                        <asp:ListItem Value="0">not required</asp:ListItem>
                                    </asp:DropDownList>

                                    <asp:RadioButtonList ID="rdoNYSCCompletionStatus" runat="server" RepeatDirection="Horizontal"
                                        CssClass="RadioButtonStyle" ToolTip="select the required nysc status">
                                        <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                        <asp:ListItem Value="No">No</asp:ListItem>
                                        <asp:ListItem Value="Exempted">Exempted</asp:ListItem>
                                    </asp:RadioButtonList>--%>

                                    <asp:CheckBoxList runat="server" ID="chkNYSCCompletionStatus" RepeatDirection="Horizontal">
                                    <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                        <asp:ListItem Value="No">No</asp:ListItem>
                                        <asp:ListItem Value="Exempted">Exempted</asp:ListItem>
                                    </asp:CheckBoxList>

                                </td>
                                <td>
                                    <asp:CheckBox ID="cbMinimumQualification" runat="server" Enabled="true" Checked="true"  />
                                </td>
                                <td>
                                    Minimum Qualification
                                </td>
                                <td>
                                    <!--<asp:DropDownList ID="RadioButtonList1" runat="server" CssClass="DropDownListStyle"
                                        ValidationGroup="ValidateVacancy" DataSourceID="sqlDSMinimumQualificationCriterionPlausibleValues"
                                        DataTextField="Name" DataValueField="QualificationID" OnDataBound="ddlMinimumQualification_DataBound">
                                        <asp:ListItem></asp:ListItem>
                                    </asp:DropDownList>
                                    <br />
                                     <asp:RadioButtonList ID="ddlMinimumQualification1"  runat="server" CssClass="DropDownListStyle"
                                        ValidationGroup="ValidateVacancy" DataSourceID="sqlDSMinimumQualificationCriterionPlausibleValues"
                                        DataTextField="Name" DataValueField="QualificationID" OnDataBound="ddlMinimumQualification_DataBound">
                                        <asp:ListItem></asp:ListItem>
                                     
                                     </asp:RadioButtonList>-->
                                     <br />
                                     <asp:RadioButtonList ID="ddlMinimumQualification"  runat="server" CssClass="DropDownListStyle"
                                        ValidationGroup="ValidateVacancy" DataSourceID="sqlDSMinimumQualificationCriterionPlausibleValues"
                                        DataTextField="Name" DataValueField="QualificationID" OnDataBound="ddlMinimumQualification_DataBound">
                                        <asp:ListItem></asp:ListItem>
                                     
                                     </asp:RadioButtonList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:CheckBox ID="cbClassOfQualification" runat="server" />
                                </td>
                                <td>
                                    Class of Qualification
                                </td>
                                <td>
                                    <%--<asp:DropDownList ID="ddlClassOfQualification" runat="server" CssClass="DropDownListStyle"
                                        ValidationGroup="ValidateVacancy" DataSourceID="sqlDSClassOfQualificationCriterionPlausibleValues"
                                        DataTextField="Name" DataValueField="ClassOfQualificationID" OnDataBound="ddlClassOfQualification_DataBound">
                                    </asp:DropDownList>--%>

                                    <asp:CheckBoxList runat="server" ID="chkClassOfQualification" RepeatDirection="Horizontal" RepeatColumns="3">
                                    <asp:ListItem Value="1">1st Class</asp:ListItem>
                                        <asp:ListItem Value="2">2:1</asp:ListItem>
                                        <asp:ListItem Value="3">2:2</asp:ListItem>
                                        <asp:ListItem Value="4">3rd Class</asp:ListItem>
                                        <asp:ListItem Value="6">Distinction</asp:ListItem>
                                        <asp:ListItem Value="8">Lower Credit</asp:ListItem>
                                        <asp:ListItem Value="5">Pass</asp:ListItem>
                                        <asp:ListItem Value="7">Upper Credit</asp:ListItem>
                                    </asp:CheckBoxList>
                                </td>
                                <td>
                                    <asp:CheckBox ID="cbCourseOfStudy" runat="server" />
                                </td>
                                <td>
                                    Course of Study
                                </td>
                                <td>
                                    <SharePoint:InputFormTextBox ID="CourseOfStudyInputFormTextBox" runat="server" MaxLength="50"></SharePoint:InputFormTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:CheckBox ID="cbMaritalStatus" runat="server" />
                                </td>
                                <td>
                                    Marital Status
                                </td>
                                <td>
                                    <asp:RadioButtonList ID="rblMaritalStatus" runat="server" RepeatDirection="Horizontal"
                                        CssClass="RadioButtonStyle" ToolTip="select the required marital status">
                                        <asp:ListItem>single</asp:ListItem>
                                        <asp:ListItem>married</asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                                <td>
                                    <asp:CheckBox ID="cbPostDegreeYearsOfExperience" runat="server" />
                                </td>
                                <td>
                                    Post Degree Years of Experience
                                </td>
                                <td>
                                    <SharePoint:InputFormTextBox ID="iftPostDegreeYearsOfExperience" runat="server" CssClass="InputFormTextYearsOfExperienceStyle"></SharePoint:InputFormTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:CheckBox ID="cbCountryOfOorigin" runat="server" Enabled="true" Checked="true"  />
                                </td>
                                <td>
                                    Nationality
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlCountryOfOrigin" runat="server" CssClass="DropDownListStyle"
                                        DataSourceID="sqlDSCountryOfOriginCriterionPlausibleValues" DataTextField="Name"
                                        DataValueField="ID" OnDataBound="ddlCountryOfOrigin_DataBound">
                                    </asp:DropDownList>
                                </td>
                                <td>
                                    <asp:CheckBox ID="cbGender" runat="server" />
                                </td>
                                <td>
                                    Gender
                                </td>
                                <td>
                                    <asp:RadioButtonList ID="rblGender" runat="server" RepeatDirection="Horizontal" CssClass="RadioButtonStyle"
                                        ToolTip="select the required gender">
                                        <asp:ListItem>male</asp:ListItem>
                                        <asp:ListItem>female</asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top;">
                                    <asp:CheckBox ID="cbJobDescription" runat="server" />
                                </td>
                                <td style="vertical-align: top;">
                                    Job Description
                                </td>
                                <td>
                                    <SharePoint:InputFormTextBox ID="JobDescriptionInputFormTextBox" runat="server" MaxLength="300" CssClass="InputTextFormLikeStyle"
                                        Columns="30" Rows="5" TextMode="MultiLine"></SharePoint:InputFormTextBox>
                                </td>
                                <td style="vertical-align: top;">
                                    <asp:CheckBox ID="cbSkillsRequired" runat="server" />
                                </td>
                                <td style="vertical-align: top;">
                                    Experience &amp; Training
                                </td>
                                <td>
                                    <SharePoint:InputFormTextBox ID="iftSkillsRequired" runat="server" MaxLength="300" CssClass="InputTextFormLikeStyle"
                                        Columns="30" Rows="5" TextMode="MultiLine"></SharePoint:InputFormTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top;">
                                    <asp:CheckBox ID="cbProfessionalQualifications" runat="server" />
                                </td>
                                <td style="vertical-align: top;">
                                    Professional Qualifications
                                </td>
                                <td style="vertical-align: top;">
                                    <SharePoint:InputFormTextBox ID="iftProfessionalQualifications" runat="server" MaxLength="300" CssClass="InputTextFormLikeStyle"
                                        Columns="30" Rows="5" TextMode="MultiLine"></SharePoint:InputFormTextBox>
                                </td>
                                <td style="vertical-align: top;">
                                    <asp:CheckBox ID="cbAdditionalQualifications" runat="server" />
                                </td>
                                <td style="vertical-align: top;">
                                    Additional Qualifications
                                </td>
                                <td>
                                    <SharePoint:InputFormTextBox ID="iftAdditionalQualifications" runat="server" MaxLength="300" CssClass="InputTextFormLikeStyle"
                                        Columns="30" Rows="5" TextMode="MultiLine"></SharePoint:InputFormTextBox>
                                </td>
                            </tr>
                             <tr>
                                <td style="vertical-align: top;">
                                    <asp:CheckBox ID="cboSkill" runat="server" />
                                </td>
                                <td style="vertical-align: top;">
                                    Skill
                                </td>
                                <td style="vertical-align: top;">
                                    <SharePoint:InputFormTextBox ID="txtSkill" runat="server" MaxLength="300" CssClass="InputTextFormLikeStyle"
                                        Columns="30" Rows="5" TextMode="MultiLine"></SharePoint:InputFormTextBox>
                                </td>
                                <td style="vertical-align: top;">
                                    <asp:CheckBox ID="cboHasWorkWithMTN" runat="server" />
                                </td>
                                <td style="vertical-align: top;">
                                    Have you ever worked for MTN as a permanent staff before?
                                </td>
                                <td style="vertical-align: top;">
                                    <asp:RadioButtonList runat="server" ID="rdoWorkWithMTN" RepeatDirection="Horizontal">
                                    <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                        <asp:ListItem Value="No">No</asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                </td>
                                <td>
                                </td>
                                <td>
                                </td>
                                <td>
                                </td>
                                <td>
                                    <asp:Button ID="btnFILTER" CssClass="MyButton" runat="server" OnClick="HandleFilterInitiation"
                                        Text="filter" OnClientClick="HandleFilterInitiation(event)" ToolTip="click here to filter applicants" />
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div>
                        <asp:SqlDataSource runat="server" ID="sqlDSNYSCCompletionStatusCriterionPlausibleValues"
                            ConnectionString="<%$ ConnectionStrings:MTNNGCareersDBConnectionString1 %>" SelectCommand="[RetrieveShortListingNYSCCompletionStatus]"
                            SelectCommandType="StoredProcedure" UpdateCommand="RetrieveShortListingCriteriaValues"
                            UpdateCommandType="StoredProcedure" ProviderName="<%$ ConnectionStrings:MTNNGCareersDBConnectionString1.ProviderName %>">
                            <SelectParameters>
                                <asp:ControlParameter ControlID="hfVacancyBreakDownID" Name="VacancyBreakDownID"
                                    PropertyName="Value" Type="Int64" />
                            </SelectParameters>
                            <UpdateParameters>
                                <asp:Parameter Name="VacancyBreakDownID" Type="Int64" />
                            </UpdateParameters>
                        </asp:SqlDataSource>
                        <asp:SqlDataSource runat="server" ID="sqlDSCourseOfStudyCriterionPlausibleValues"
                            ConnectionString="<%$ ConnectionStrings:MTNNGCareersDBConnectionString1 %>" SelectCommand="RetrieveShortListingCriterionCourseOfStudy"
                            SelectCommandType="StoredProcedure" UpdateCommand="RetrieveShortListingCriteriaValues"
                            UpdateCommandType="StoredProcedure" ProviderName="<%$ ConnectionStrings:MTNNGCareersDBConnectionString1.ProviderName %>">
                            <SelectParameters>
                                <asp:ControlParameter ControlID="hfVacancyBreakDownID" Name="VacancyBreakDownID"
                                    PropertyName="Value" Type="Int64" />
                            </SelectParameters>
                            <UpdateParameters>
                                <asp:Parameter Name="VacancyBreakDownID" Type="Int64" />
                            </UpdateParameters>
                        </asp:SqlDataSource>
                        <asp:SqlDataSource runat="server" ID="sqlDSMinimumQualificationCriterionPlausibleValues"
                            ConnectionString="<%$ ConnectionStrings:MTNNGCareersDBConnectionString1 %>" SelectCommand="RetrieveShortListingCriterionMinimumQualification"
                            SelectCommandType="StoredProcedure" UpdateCommand="RetrieveShortListingCriteriaValues"
                            UpdateCommandType="StoredProcedure" ProviderName="<%$ ConnectionStrings:MTNNGCareersDBConnectionString1.ProviderName %>">
                            <SelectParameters>
                                <asp:ControlParameter ControlID="hfVacancyBreakDownID" Name="VacancyBreakDownID"
                                    PropertyName="Value" Type="Int64" />
                            </SelectParameters>
                            <UpdateParameters>
                                <asp:Parameter Name="VacancyBreakDownID" Type="Int64" />
                            </UpdateParameters>
                        </asp:SqlDataSource>
                        <asp:SqlDataSource runat="server" ID="sqlDSClassOfQualificationCriterionPlausibleValues"
                            ConnectionString="<%$ ConnectionStrings:MTNNGCareersDBConnectionString1 %>" SelectCommand="RetrieveShortListingCriterionClassOfQualification"
                            SelectCommandType="StoredProcedure" UpdateCommand="RetrieveShortListingCriteriaValues"
                            UpdateCommandType="StoredProcedure" ProviderName="<%$ ConnectionStrings:MTNNGCareersDBConnectionString1.ProviderName %>">
                            <SelectParameters>
                                <asp:ControlParameter ControlID="hfVacancyBreakDownID" Name="VacancyBreakDownID"
                                    PropertyName="Value" Type="Int64" />
                            </SelectParameters>
                            <UpdateParameters>
                                <asp:Parameter Name="VacancyBreakDownID" Type="Int64" />
                            </UpdateParameters>
                        </asp:SqlDataSource>
                        <asp:SqlDataSource runat="server" ID="sqlDSCountryOfOriginCriterionPlausibleValues"
                            ConnectionString="<%$ ConnectionStrings:MTNNGCareersDBConnectionString1 %>" SelectCommand="RetrieveShortListingCriterionCountryOfOrigin"
                            SelectCommandType="StoredProcedure" UpdateCommand="RetrieveShortListingCriteriaValues"
                            UpdateCommandType="StoredProcedure" ProviderName="<%$ ConnectionStrings:MTNNGCareersDBConnectionString1.ProviderName %>">
                            <SelectParameters>
                                <asp:ControlParameter ControlID="hfVacancyBreakDownID" Name="VacancyBreakDownID"
                                    PropertyName="Value" Type="Int64" />
                            </SelectParameters>
                            <UpdateParameters>
                                <asp:Parameter Name="VacancyBreakDownID" Type="Int64" />
                            </UpdateParameters>
                        </asp:SqlDataSource>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                </td>
            </tr>
        </table>
    </div>
    <div>
    </div>
    <div id="divApplicants" runat="server" style="display: none;">
        <table class="TableStyle">
            <tr>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <div>
                        <table class="TableStyle">
                            <tr>
                                <td class="FieldName">
                                    Job Title
                                </td>
                                <td class="FieldValue">
                                    <asp:Label ID="lblJobTitle" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="FieldName">
                                    No. of Openings
                                </td>
                                <td class="FieldValue">
                                    <asp:Label ID="lblNoOfOpenings" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="FieldName">
                                    Employment Status
                                </td>
                                <td class="FieldValue">
                                    <asp:Label ID="lblEmploymentStatus" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="FieldName">
                                    Department
                                </td>
                                <td class="FieldValue">
                                    <asp:Label ID="lblDepartment" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="FieldName">
                                    Unit
                                </td>
                                <td class="FieldValue">
                                    <asp:Label ID="lblUnit" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="FieldName">
                                    State
                                </td>
                                <td class="FieldValue">
                                    <asp:Label ID="lblState" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="FieldName">
                                    Location
                                </td>
                                <td class="FieldValue">
                                    <asp:Label ID="lblLocation" runat="server"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="InformationIndicator">
                        <asp:Label ID="lbVacancyApplicantCount" runat="server"></asp:Label>
                        <br />
                        <asp:Label ID="lblFilteredApplicants" runat="server" ToolTip="the number of applicants who meet the requirements of the vacancy based on their CV"></asp:Label>
                        <br />
                        <asp:Label ID="lblShortListedApplicantCount" runat="server" ToolTip="the number of applicants who have been shortlisted for vacancy"
                            Font-Bold="True" Font-Names="Arial" Font-Size="X-Small" Font-Underline="True"
                            ForeColor="#FF3300"></asp:Label>
                    </div>
                   
                    <div>
                     <asp:UpdatePanel ID="UpdatePanelVacancyBreakDown" runat="server">
            <ContentTemplate>
                        <asp:GridView ID="gvVacanyApplicants" runat="server" EnableModelValidation="True"
                            AutoGenerateColumns="False" DataSourceID="vwVacancyApplicantsDataSrc" DataKeyNames="VacancyID,VacancyBreakDownID,RegistrantRowKey"
                            OnRowCommand="HandleApplicantNameLinkButtonOnClick" OnRowDataBound="gvVacanyApplicants_RowDataBound"
                            OnDataBound="gvVacanyApplicants_DataBound" CssClass="mGrid" PagerStyle-CssClass="pgr"
                            AlternatingRowStyle-CssClass="alt" PageSize="1180" AllowPaging="True" OnPageIndexChanging="gvVacanyApplicants_PageIndexChanging"
                            EmptyDataText="NO APPLICATION HAS BEEN MADE FOR THIS OPENING" OnRowCreated="gvVacanyApplicants_RowCreated">
                            <AlternatingRowStyle CssClass="alt"></AlternatingRowStyle>
                            <Columns>
                                <asp:TemplateField ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Center">
                                    <HeaderTemplate>
                                        <asp:CheckBox ID="cbEnlistHeader" onclick="Handle_cbEnlistHeader_onclick(event)"
                                            runat="server" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="cbEnlist" runat="server" />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                </asp:TemplateField>
                                <asp:BoundField DataField="VacancyID" HeaderText="VacancyID" SortExpression="VacancyID"
                                    Visible="False" />
                                <asp:BoundField DataField="VacancyBreakDownID" HeaderText="VacancyBreakDownID" SortExpression="VacancyBreakDownID"
                                    Visible="False" />
                                <asp:BoundField DataField="CompanyOrOrganizationName" HeaderText="employer" SortExpression="CompanyOrOrganizationName" />
                                <asp:ButtonField AccessibleHeaderText="Applicant Name" DataTextField="Applicant Name"
                                    HeaderText="Applicant Name" />
                                <asp:BoundField DataField="RegistrantRowKey" HeaderText="RegistrantRowKey" SortExpression="RegistrantRowKey"
                                    Visible="False" />
                                <asp:BoundField DataField="BirthDate" HeaderText="BirthDate" SortExpression="BirthDate"
                                    Visible="False" />
                                <asp:BoundField DataField="ApplicationDate" HeaderText="Date Applied" SortExpression="ApplicationDate" />
                            </Columns>
                            <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
                            <PagerStyle CssClass="pgr"></PagerStyle>
                        </asp:GridView>
                           </ContentTemplate>
                  </asp:UpdatePanel>
                        <div style="float: right;">
                            <table style="float: right;">
                                <tr>
                                    <td class="CareersTab">
                                    
                                   <div style="display: none;"><asp:Button ID="btnCallEventFromJs" runat="server" Text="Call Event From Js"   OnClick="btnCallEventFromJs_Click" />
                                   <asp:TextBox ID="TextBox1" runat="server" /></div>
                 
                    <asp:Button OnClientClick="showDirectory();" ID="Button1" runat="server" Text="Download All CV Multi Files" CssClass="MyButton" />
                                    
                       
                                     </td>
                                    <asp:Button ID="btnDownloadSingle" Visible="false" runat="server" Text="Download All CV Multi Files" ToolTip="click this button to download all applicants cv" CssClass="MyButton" OnClick="DownloadPDF_Single_Click" />
                                    <td>
                                        <asp:Button ID="btnDownload" runat="server" Text="Download All CVs Single File" ToolTip="click this button to download all applicants cv"
                                            CssClass="MyButton" OnClick="DownloadPDF_Click"/>
                                    </td>
                                    <td>
                                        <asp:Button ID="btnSHORTLIST" runat="server" Text="shortlist" ToolTip="click this button to short list selected applicants"
                                            CssClass="MyButton" OnClick="HandleShortListApplicants" OnClientClick="HandleShortListingInitiation(event)" />
                                    </td>
                                    <td>
                                        <asp:Button ID="btnSUBMIT" runat="server" Text="submit" ToolTip="click this button to submit selected (short-listed) applicants to the hiring manager who requested that the vacancy they applied for be published."
                                            CssClass="MyButton" OnClick="HandleSubmitShortListedApplicants" OnClientClick="return confirm('Are you sure you would like to submit the list of applicants you have short listed?');" Visible="false" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
            
                    <div>
                        <asp:SqlDataSource ID="vwVacancyApplicantsDataSrc" runat="server" ConnectionString="<%$ ConnectionStrings:MTNNGCareersDBConnectionString1 %>"
                            SelectCommand="RetrieveVacancyApplicants" SelectCommandType="StoredProcedure">
                            <SelectParameters>
                                <asp:ControlParameter ControlID="hfVacancyBreakDownID" Name="VacancyBreakDownID"
                                    PropertyName="Value" Type="Int64" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                </td>
            </tr>
        </table>
    </div>
    <div>
    </div>
</asp:Content>
<asp:Content ID="PageTitle" ContentPlaceHolderID="PlaceHolderPageTitle" runat="server">
    Manage Vacancy Applicants
</asp:Content>
<asp:Content ID="PageTitleInTitleArea" ContentPlaceHolderID="PlaceHolderPageTitleInTitleArea"
    runat="server">
    Manage Vacancy Applicants
</asp:Content>

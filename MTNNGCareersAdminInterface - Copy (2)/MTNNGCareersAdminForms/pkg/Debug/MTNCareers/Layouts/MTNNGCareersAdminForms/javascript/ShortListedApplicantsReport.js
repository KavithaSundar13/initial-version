﻿var divVacancy;
var divShortListedApplicants;
var divComments;

function HandleContentToPresent(event)
{
    var eventSource = GetEventSource(event);
    var eventSource_innerText = (eventSource.innerText);

    ObtainRequiredDivsReferences();

    DetermineContentToPresent(eventSource_innerText);

    PreventPostBack(event);
}

function ObtainRequiredDivsReferences()
{
    divVacancy = (document.getElementById(divVacancyClientID));
    divShortListedApplicants = (document.getElementById(divShortListedApplicantsClientID));
    divComments = (document.getElementById(divCommentsClientID));
}

function DetermineContentToPresent(eventSource_textContent)
{
    if (eventSource_textContent == 'vacancy')
    {
        if (divComments != null)
        {
            divComments.style.display = NoneDisplayStyle;
        }

        divShortListedApplicants.style.display = NoneDisplayStyle;
        divVacancy.style.display = BlockDisplayStyle;
    }
    else if (eventSource_textContent == 'short listed applicants')
    {
        divVacancy.style.display = NoneDisplayStyle;
        if (divComments != null)
        {
            divComments.style.display = NoneDisplayStyle;
        }
        divShortListedApplicants.style.display = BlockDisplayStyle;
    }
    else if (eventSource_textContent == 'comments')
    {
        divVacancy.style.display = NoneDisplayStyle;
        divShortListedApplicants.style.display = NoneDisplayStyle;
        if (divComments != null)
        {
            divComments.style.display = BlockDisplayStyle;
        }
    }
}
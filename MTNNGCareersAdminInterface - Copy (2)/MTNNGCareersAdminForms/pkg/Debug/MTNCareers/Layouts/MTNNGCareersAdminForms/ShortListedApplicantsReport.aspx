<%@ Assembly Name="MTNNGCareers.Administration.Forms, Version=1.0.0.0, Culture=neutral, PublicKeyToken=5e7c3f80f0f7816f" %>
<%@ Import Namespace="Microsoft.SharePoint.ApplicationPages" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Register TagPrefix="Tavia" TagName="VacancyViewer" Src="~/_controltemplates/MTNCareers/DisplayVacancyCtrl.ascx" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ShortListedApplicantsReport.aspx.cs"
    Inherits="MTNNGCareers.Administration.Forms.ShortListedApplicantsReport" DynamicMasterPageFile="~masterurl/default.master" %>

<asp:Content ID="PageHead" ContentPlaceHolderID="PlaceHolderAdditionalPageHead" runat="server">
    <script type="text/javascript" src="/HR/_layouts/MTNNGCareersAdminForms/javascript/Common.js"></script>
    <script type="text/javascript">
        var lnkBtnVacancy = '<% =lnkBtnVacancy.ClientID %>';
        var lnkBtnApplicantsClientID = '<% =lnkBtnApplicants.ClientID %>';
        var lnkBtnCommentsClientID = '<% =lnkBtnComments.ClientID %>';

        var divVacancyClientID = '<% =divVacancy.ClientID %>';
        var divShortListedApplicantsClientID = '<% =divShortListedApplicants.ClientID %>';
        var divCommentsClientID = '<% =divComments.ClientID %>';
    </script>
    <script type="text/javascript" src="/HR/_layouts/MTNNGCareersAdminForms/javascript/ShortListedApplicantsReport.js"></script>
    <link type="text/css" rel="Stylesheet" href="/HR/_layouts/MTNNGCareersAdminForms/css/Master.css" />
    <link type="text/css" rel="Stylesheet" href="/HR/_layouts/MTNNGCareersAdminForms/css/Common.css" />
</asp:Content>
<asp:Content ID="Main" ContentPlaceHolderID="PlaceHolderMain" runat="server">
    <div>
        <table class="TabStyle-i" style="width: 40%">
            <tr>
                <td class="CareersTab">
                    <asp:LinkButton ID="lnkBtnVacancy" runat="server" CssClass="LinkButtonStyle" ToolTip="click here to the vacancy for which applicants were short listed."
                        OnClientClick="HandleContentToPresent(event)">vacancy</asp:LinkButton>
                </td>
                <td class="CareersTab">
                    <asp:LinkButton ID="lnkBtnApplicants" runat="server" CssClass="LinkButtonStyle" ToolTip="click here to view applicants who were short listed."
                        OnClientClick="HandleContentToPresent(event)">short listed applicants</asp:LinkButton>
                </td>
                <td runat="server" id="td_CommentLinkButton" class="CareersTab">
                    <asp:LinkButton ID="lnkBtnComments" runat="server" CssClass="LinkButtonStyle" OnClientClick="HandleContentToPresent(event)"
                        ToolTip="click here to type in your comments or view the comments you made">comments</asp:LinkButton>
                </td>
            </tr>
        </table>
    </div>
    <div runat="server" id="divVacancy" style="display: block;">
        <Tavia:VacancyViewer runat="server" ID="VacancyViewer" />
    </div>
    <div runat="server" id="divShortListedApplicants" style="display: none;">
        <div style="padding-left: 5%; padding-right: 5%;">
            <table style="width: 90%;">
                <tr>
                    <td>
                        <div>
                            <table class="TableStyle">
                                <tr>
                                    <td class="FieldName">
                                        Job Title
                                    </td>
                                    <td class="FieldValue">
                                        <asp:Label ID="lblJobTitle" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="FieldName">
                                        No. of Openings
                                    </td>
                                    <td class="FieldValue">
                                        <asp:Label ID="lblNoOfOpenings" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="FieldName">
                                        Employment Status
                                    </td>
                                    <td class="FieldValue">
                                        <asp:Label ID="lblEmploymentStatus" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="FieldName">
                                        Department
                                    </td>
                                    <td class="FieldValue">
                                        <asp:Label ID="lblDepartment" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="FieldName">
                                        Unit
                                    </td>
                                    <td class="FieldValue">
                                        <asp:Label ID="lblUnit" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="FieldName">
                                        State
                                    </td>
                                    <td class="FieldValue">
                                        <asp:Label ID="lblState" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="FieldName">
                                        Location
                                    </td>
                                    <td class="FieldValue">
                                        <asp:Label ID="lblLocation" runat="server"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblSLACount" runat="server" Style="font-family: Arial; font-size: x-small;
                            font-weight: bold; color: Red; text-decoration: underline; text-transform: uppercase;"
                            Text="no. of short listed applicant(s)"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:GridView ID="gvVacancyBreakDownSLA" runat="server" CssClass="mGrid" PagerStyle-CssClass="pgr"
                            AlternatingRowStyle-CssClass="alt" AutoGenerateColumns="False" DataKeyNames="RowKey"
                            DataSourceID="_SLADataSource" EnableModelValidation="True" OnRowDataBound="gvVacancyBreakDownSLA_RowDataBound"
                            OnDataBound="gvVacancyBreakDownSLA_DataBound">
                            <AlternatingRowStyle CssClass="alt"></AlternatingRowStyle>
                            <Columns>
                                <asp:TemplateField HeaderText="s/n" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:Label ID="SerialNolbl" runat="server" Text='<%# Container.DataItemIndex + 1 %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:ButtonField DataTextField="Applicant Name" HeaderText="applicant name" />
                                <asp:TemplateField HeaderText="current employer">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCurrEmp" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="current position">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCurrPos" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="PostDegreeYearsOfExperience" HeaderText="post degree yrs of exp."
                                    SortExpression="PostDegreeYearsOfExperience" />
                                <asp:BoundField DataField="NYSCCompletionStatus" HeaderText="nysc comp. status" SortExpression="NYSCCompletionStatus" />
                            </Columns>
                            <PagerStyle CssClass="pgr"></PagerStyle>
                        </asp:GridView>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:SqlDataSource runat="server" ID="_SLADataSource" ConnectionString="<%$ ConnectionStrings:MTNNGCareersDBConnectionString %>"
                            SelectCommand="RetrieveHiringManagerReport" SelectCommandType="StoredProcedure">
                            <SelectParameters>
                                <asp:QueryStringParameter Name="VacancyBreakDownID" QueryStringField="VacancyBreakDownID"
                                    Type="Int64" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div runat="server" id="divComments" style="display: none;">
        <table class="TableStyle">
            <tr>
                <td class="FieldName" style="vertical-align: top">
                    Comments <span class="RequiredIndicator">*</span>
                </td>
                <td class="FieldValue">
                    <SharePoint:InputFormTextBox ID="iftComments" runat="server" CssClass="iftJobTitle"
                        ValidationGroup="ValidateComments" TextMode="MultiLine" Rows="15" Columns="70"></SharePoint:InputFormTextBox>
                    <br />
                    <SharePoint:InputFormRequiredFieldValidator ID="ifrfvComments" ValidationGroup="ValidateComments"
                        ControlToValidate="iftComments" runat="server" ErrorMessage="required" Text="Required"></SharePoint:InputFormRequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="FieldName">
                </td>
                <td class="FieldValue">
                    <asp:Button ID="btnApprove" runat="server" OnClientClick="return confirm('Please confirm that you approve of this/these short listed candidiate(s).');"
                        OnClick="OnSubmitFeedBackInitiation" Text="approve" CssClass="MyButton" ToolTip="click here to approve of the short listed candidates sent you." />
                    &nbsp; &nbsp; &nbsp;
                    <asp:Button ID="btnRequestChange" runat="server" OnClientClick="return confirm('Please confirm that you would like to issue a change request.');"
                        OnClick="HandleRequestChangeInitiation" ValidationGroup="ValidateComments" Text="request change"
                        CssClass="MyButton" ToolTip="click here to request a modification to the short listed candidates sent you" />
                    <%--&nbsp; &nbsp; &nbsp;
                    <asp:Button ID="btnClose" runat="server" Text="close" CssClass="MyButton" ToolTip="click here to close this form." />
                    &nbsp; &nbsp; &nbsp;--%>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
<asp:Content ID="PageTitle" ContentPlaceHolderID="PlaceHolderPageTitle" runat="server">
    Short Listed Applicants.
</asp:Content>
<asp:Content ID="PageTitleInTitleArea" ContentPlaceHolderID="PlaceHolderPageTitleInTitleArea"
    runat="server">
    MTN NG External Careers - Short Listed Applicants.
</asp:Content>

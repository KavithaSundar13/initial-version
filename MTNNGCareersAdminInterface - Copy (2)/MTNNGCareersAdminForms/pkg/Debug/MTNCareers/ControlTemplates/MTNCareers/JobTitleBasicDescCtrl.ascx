<%@ Assembly Name="MTNNGCareers.Administration.Forms, Version=1.0.0.0, Culture=neutral, PublicKeyToken=5e7c3f80f0f7816f" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="JobTitleBasicDescCtrl.ascx.cs" Inherits="MTNNGCareers.Administration.Forms.ControlTemplates.JobTitleBasicDescCtrl" %>

<link type="text/css" rel="Stylesheet" href="/HR/_layouts/MTNNGCareersAdminForms/css/Common.css" />
<link type="text/css" rel="Stylesheet" href="/HR/_layouts/MTNNGCareersAdminForms/css/JobTitleBasicDescCtrl.css" />

<div style="width: 60%;">
 <table class="Fill">
  <tr>
   <td>
       <div>
       </div>
      </td>
  </tr>
  <tr>
   <td>
       <div>
        <table class="Fill">
         <tr>
          <td class="td_Label">
              job title</td>
          <td class="td_Value">
              <asp:Label ID="lbJobTitle" runat="server" CssClass="EntityAttribute"></asp:Label>
          </td>
         </tr>
         <tr>
          <td class="td_Label">
              requester</td>
          <td class="td_Value">
              <asp:Label ID="lbRequester" runat="server" CssClass="EntityAttribute"></asp:Label>
             </td>
         </tr>
         <tr>
          <td class="td_Label">
              requester designation</td>
          <td class="td_Value">
              <asp:Label ID="lbRequesterDesignation" runat="server" 
                  CssClass="EntityAttribute"></asp:Label>
             </td>
         </tr>
         <tr>
          <td class="td_Label">
              rquester delegate</td>
          <td class="td_Value">
              <asp:Label ID="lbRequesterDelegate" runat="server" CssClass="EntityAttribute"></asp:Label>
             </td>
         </tr>
         <tr>
          <td class="td_Label">
              publisher</td>
          <td class="td_Value">
              <asp:Label ID="lbRequesterPublisher" runat="server" CssClass="EntityAttribute"></asp:Label>
             </td>
         </tr>
         <tr>
          <td class="td_Label">
              divsion</td>
          <td class="td_Value">
              <asp:Label ID="lbDivision" runat="server" CssClass="EntityAttribute"></asp:Label>
             </td>
         </tr>
         </table>
       </div>
      </td>
  </tr>
  <tr>
   <td>
       <asp:HiddenField ID="hfVacancyID" runat="server" />
      </td>
  </tr>
 </table>
</div>

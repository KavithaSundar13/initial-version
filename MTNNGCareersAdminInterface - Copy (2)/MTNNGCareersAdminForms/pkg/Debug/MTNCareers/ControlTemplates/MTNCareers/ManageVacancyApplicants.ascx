<%@ Assembly Name="MTNNGCareers.Administration.Forms, Version=1.0.0.0, Culture=neutral, PublicKeyToken=5e7c3f80f0f7816f" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ManageVacancyApplicants.ascx.cs"
    Inherits="MTNNGCareers.Administration.Forms.ControlTemplates.ManageVacancyApplicants" %>
<%@ Register Assembly="AjaxControlToolkit, Version=3.0.30930.28736, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e"
    Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<script type="text/javascript">
    var cbNYSCRequirementClientID = '<% =cbNYSCRequirement.ClientID %>';
    var cbClassOfQualificationClientID = '<% =cbClassOfQualification.ClientID %>';
    var cbMaritalStatusClientID = '<% =cbMaritalStatus.ClientID %>';
    var cbCountryOfOoriginClientID = '<% =cbCountryOfOorigin.ClientID %>';
    var cbProfessionalQualificationsClientID = '<% =cbProfessionalQualifications.ClientID %>';
    var cbSkillsRequiredClientID = '<% =cbSkillsRequired.ClientID %>';
    var cbMinimumQualificationClientID = '<% =cbMinimumQualification.ClientID %>';
    var cbCourseOfStudyClientID = '<% =cbCourseOfStudy.ClientID %>';
    var cbPostDegreeYearsOfExperienceClientID = '<% =cbPostDegreeYearsOfExperience.ClientID %>';
    var cbGenderClientID = '<% =cbGender.ClientID %>';
    var cbAdditionalQualificationsClientID = '<% =cbAdditionalQualifications.ClientID %>';

    
    var ddlClassOfQualificationClientID = '<% =ddlClassOfQualification.ClientID %>';
    var rblMaritalStatusClientID = '<% =rblMaritalStatus.ClientID %>';
    var ddlCountryOfOriginClientID = '<% =ddlCountryOfOrigin.ClientID %>';
    var iftProfessionalQualificationsClientID = '<% =iftProfessionalQualifications.ClientID %>';
    var iftSkillsRequiredClientID = '<% =iftSkillsRequired.ClientID %>';
    var ddlMinimumQualificationClientID = '<% =ddlMinimumQualification.ClientID %>';
    var ddlCourseOfStudyClientID = '<% =ddlCourseOfStudy.ClientID %>';
    var iftPostDegreeYearsOfExperienceClientID = '<% =iftPostDegreeYearsOfExperience.ClientID %>';
    var rblGenderClientID = '<% =rblGender.ClientID %>';
    var iftAdditionalQualificationsClientID = '<% =iftAdditionalQualifications.ClientID %>';
</script>

<link type="text/css" rel="Stylesheet" href="/HR/_layouts/MTNNGCareersAdminForms/css/Common.css" />
<link type="text/css" rel="Stylesheet" href="/HR/_layouts/MTNNGCareersAdminForms/css/ManageVacancyApplicants.css" />
<div style="padding-left: 1.5%; padding-right: 1.5%;">
    <table class="GTable-i">
        <tr>
            <td >
                <asp:CheckBox ID="cbNYSCRequirement" runat="server" />
            </td>
            <td >
                NYSC Completion Status</td>
            <td >
                <asp:DropDownList ID="ddlNYSCCompletionStatus" runat="server" 
                    CssClass="DropDownListStyle" 
                    DataSourceID="sqlDSNYSCCompletionStatusCriterionPlausibleValues" 
                    DataTextField="NYSCCompletionStatus" DataValueField="NYSCCompletionStatus" 
                    ondatabound="ddlNYSCCompletionStatus_DataBound">
                    <asp:ListItem Value="1">required</asp:ListItem>
                    <asp:ListItem Value="0">not required</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td >
                <asp:CheckBox ID="cbMinimumQualification" runat="server" />
            </td>
            <td >
                &nbsp;Qualification
            </td>
            <td >
                <asp:DropDownList ID="ddlMinimumQualification" runat="server" CssClass="DropDownListStyle"
                    ValidationGroup="ValidateVacancy" 
                    DataSourceID="sqlDSMinimumQualificationCriterionPlausibleValues" 
                    DataTextField="Name" DataValueField="QualificationID" 
                    ondatabound="ddlMinimumQualification_DataBound">
                    <asp:ListItem></asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td >
                <asp:CheckBox ID="cbClassOfQualification" runat="server" />
            </td>
            <td >
                Class of Qualification
            </td>
            <td >
                <asp:DropDownList ID="ddlClassOfQualification" runat="server"
                    CssClass="DropDownListStyle" 
                    DataSourceID="sqlDSClassOfQualificationCriterionPlausibleValues" 
                    DataTextField="Name" DataValueField="ClassOfQualificationID" 
                    ondatabound="ddlClassOfQualification_DataBound">
                </asp:DropDownList>
            </td>
            <td >
                <asp:CheckBox ID="cbCourseOfStudy" runat="server" />
            </td>
            <td >
                Course of Study</td>
            <td >
                <asp:DropDownList ID="ddlCourseOfStudy" runat="server" 
                    CssClass="DropDownListStyle" 
                    DataSourceID="sqlDSCourseOfStudyCriterionPlausibleValues" 
                    DataTextField="CourseOfStudy" DataValueField="CourseOfStudy" 
                    ondatabound="ddlCourseOfStudy_DataBound">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td >
                <asp:CheckBox ID="cbMaritalStatus" runat="server" />
            </td>
            <td >
                Marital Status</td>
            <td >
                <asp:RadioButtonList ID="rblMaritalStatus" runat="server" RepeatDirection="Horizontal"
                    CssClass="RadioButtonStyle" ToolTip="select the required marital status">
                    <asp:ListItem>single</asp:ListItem>
                    <asp:ListItem>married</asp:ListItem>
                </asp:RadioButtonList>
            </td>
            <td >
                <asp:CheckBox ID="cbPostDegreeYearsOfExperience" runat="server" />
            </td>
            <td >
                Post Degree Years of Experience
            </td>
            <td >
                <SharePoint:InputFormTextBox ID="iftPostDegreeYearsOfExperience" runat="server"
                    CssClass="InputFormTextYearsOfExperienceStyle"></SharePoint:InputFormTextBox>
            </td>
        </tr>
        <tr>
            <td >
                <asp:CheckBox ID="cbCountryOfOorigin" runat="server" />
            </td>
            <td >
                Nationality</td>
            <td >
                <asp:DropDownList ID="ddlCountryOfOrigin" runat="server" 
                    CssClass="DropDownListStyle" 
                    DataSourceID="sqlDSCountryOfOriginCriterionPlausibleValues" 
                    DataTextField="Name" DataValueField="ID" 
                    ondatabound="ddlCountryOfOrigin_DataBound">
                </asp:DropDownList>
            </td>
            <td >
                <asp:CheckBox ID="cbGender" runat="server" />
            </td>
            <td >
                Gender
            </td>
            <td >
                <asp:RadioButtonList ID="rblGender" runat="server" RepeatDirection="Horizontal" CssClass="RadioButtonStyle"
                    ToolTip="select the required gender">
                    <asp:ListItem>male</asp:ListItem>
                    <asp:ListItem>female</asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td style="vertical-align: top;">
                <asp:CheckBox ID="cbProfessionalQualifications" runat="server" />
            </td>
            <td style="vertical-align: top;">
                Professional Qualifications</td>
            <td  style="vertical-align: top;">
                <SharePoint:InputFormTextBox ID="iftProfessionalQualifications" runat="server"
                    CssClass="InputTextFormLikeStyle" Columns="30" Rows="5" 
                    TextMode="MultiLine"></SharePoint:InputFormTextBox>
            </td>
            <td style="vertical-align: top;">
                <asp:CheckBox ID="cbAdditionalQualifications" runat="server" />
            </td>
            <td style="vertical-align: top;" >
                Additional Qualifications
            </td>
            <td >
                <SharePoint:InputFormTextBox ID="iftAdditionalQualifications" runat="server"
                    CssClass="InputTextFormLikeStyle" Columns="30" Rows="5" 
                    TextMode="MultiLine"></SharePoint:InputFormTextBox>
            </td>
        </tr>
        <tr>
            <td style="vertical-align: top;">
                <asp:CheckBox ID="cbSkillsRequired" runat="server" />
            </td>
            <td style="vertical-align: top;" >
                Experience &amp; Training</td>
            <td >
                <SharePoint:InputFormTextBox ID="iftSkillsRequired" runat="server"
                    CssClass="InputTextFormLikeStyle" Columns="30" Rows="5" 
                    TextMode="MultiLine"></SharePoint:InputFormTextBox>
            </td>
            <td style="vertical-align: top;">
                </td>
            <td style="vertical-align: top;" >
                </td>
            <td >
                </td>
        </tr>
        <tr>
            <td >
                </td>
            <td >
                </td>
            <td >
                </td>
            <td >
                </td>
            <td >
                </td>
            <td >
                <asp:Button ID="btnFILTER" CssClass="MyButton" runat="server" onclick="btnFILTER_Click" 
                    Text="filter" onclientclick="HandleFilterInitiation(event)" />
            </td>
        </tr>
    </table>
</div>
<div>
    <asp:HiddenField runat="server" ID="hfVacancyBreakDownID" />
    <asp:SqlDataSource runat="server" ID="sqlDSNYSCCompletionStatusCriterionPlausibleValues" 
        ConnectionString="<%$ ConnectionStrings:MTNNGCareersDBConnectionString %>" 
        SelectCommand="[RetrieveShortListingNYSCCompletionStatus]" 
        SelectCommandType="StoredProcedure" 
        UpdateCommand="RetrieveShortListingCriteriaValues" 
        UpdateCommandType="StoredProcedure" 
        
        ProviderName="<%$ ConnectionStrings:MTNNGCareersDBConnectionString.ProviderName %>" >
        <SelectParameters>
            <asp:ControlParameter ControlID="hfVacancyBreakDownID" 
                Name="VacancyBreakDownID" PropertyName="Value" Type="Int64" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="VacancyBreakDownID" Type="Int64" />
        </UpdateParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource runat="server" ID="sqlDSCourseOfStudyCriterionPlausibleValues" 
        ConnectionString="<%$ ConnectionStrings:MTNNGCareersDBConnectionString %>" 
        SelectCommand="RetrieveShortListingCriterionCourseOfStudy" 
        SelectCommandType="StoredProcedure" 
        UpdateCommand="RetrieveShortListingCriteriaValues" 
        UpdateCommandType="StoredProcedure" 
        ProviderName="<%$ ConnectionStrings:MTNNGCareersDBConnectionString.ProviderName %>" >
        <SelectParameters>
            <asp:ControlParameter ControlID="hfVacancyBreakDownID" 
                Name="VacancyBreakDownID" PropertyName="Value" Type="Int64" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="VacancyBreakDownID" Type="Int64" />
        </UpdateParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource runat="server" ID="sqlDSMinimumQualificationCriterionPlausibleValues" 
        ConnectionString="<%$ ConnectionStrings:MTNNGCareersDBConnectionString %>" 
        SelectCommand="RetrieveShortListingCriterionMinimumQualification" 
        SelectCommandType="StoredProcedure" 
        UpdateCommand="RetrieveShortListingCriteriaValues" 
        UpdateCommandType="StoredProcedure" 
        ProviderName="<%$ ConnectionStrings:MTNNGCareersDBConnectionString.ProviderName %>" >
        <SelectParameters>
            <asp:ControlParameter ControlID="hfVacancyBreakDownID" 
                Name="VacancyBreakDownID" PropertyName="Value" Type="Int64" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="VacancyBreakDownID" Type="Int64" />
        </UpdateParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource runat="server" ID="sqlDSClassOfQualificationCriterionPlausibleValues" 
        ConnectionString="<%$ ConnectionStrings:MTNNGCareersDBConnectionString %>" 
        SelectCommand="RetrieveShortListingCriterionClassOfQualification" 
        SelectCommandType="StoredProcedure" 
        UpdateCommand="RetrieveShortListingCriteriaValues" 
        UpdateCommandType="StoredProcedure" 
        ProviderName="<%$ ConnectionStrings:MTNNGCareersDBConnectionString.ProviderName %>" >
        <SelectParameters>
            <asp:ControlParameter ControlID="hfVacancyBreakDownID" 
                Name="VacancyBreakDownID" PropertyName="Value" Type="Int64" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="VacancyBreakDownID" Type="Int64" />
        </UpdateParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource runat="server" ID="sqlDSCountryOfOriginCriterionPlausibleValues" 
        ConnectionString="<%$ ConnectionStrings:MTNNGCareersDBConnectionString %>" 
        SelectCommand="RetrieveShortListingCriterionCountryOfOrigin" 
        SelectCommandType="StoredProcedure" 
        UpdateCommand="RetrieveShortListingCriteriaValues" 
        UpdateCommandType="StoredProcedure" 
        
        ProviderName="<%$ ConnectionStrings:MTNNGCareersDBConnectionString.ProviderName %>" >
        <SelectParameters>
            <asp:ControlParameter ControlID="hfVacancyBreakDownID" 
                Name="VacancyBreakDownID" PropertyName="Value" Type="Int64" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="VacancyBreakDownID" Type="Int64" />
        </UpdateParameters>
    </asp:SqlDataSource>
</div>

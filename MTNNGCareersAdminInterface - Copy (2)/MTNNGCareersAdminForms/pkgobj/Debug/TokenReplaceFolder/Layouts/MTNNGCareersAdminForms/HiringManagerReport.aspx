<%@ Assembly Name="MTNNGCareers.Administration.Forms, Version=1.0.0.0, Culture=neutral, PublicKeyToken=5e7c3f80f0f7816f" %>
<%@ Import Namespace="Microsoft.SharePoint.ApplicationPages" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="HiringManagerReport.aspx.cs"
    Inherits="MTNNGCareers.Administration.Forms.HiringManagerReport"
    DynamicMasterPageFile="~masterurl/default.master" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<asp:Content ID="PageHead" ContentPlaceHolderID="PlaceHolderAdditionalPageHead" runat="server">
</asp:Content>
<asp:Content ID="Main" ContentPlaceHolderID="PlaceHolderMain" runat="server">
    <div>
        <table style="width: 80%;">
            <tr>
                <td>
                    <div style="padding-left: 30%; padding-right: 30%;">
                        <h3>
                            SHORT LISTED APPLICANTS</h3>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <rsweb:ReportViewer ProcessingMode="Local" ID="rvRequester" runat="server" Font-Names="Verdana"
                        Font-Size="8pt" Height="1000px" InteractiveDeviceInfos="(Collection)" WaitMessageFont-Names="Verdana"
                        WaitMessageFont-Size="14pt" Width="100%">
                        <LocalReport ReportEmbeddedResource="MTNNGCareers.Administration.Forms.Layouts.MTNNGCareersAdminForms.Reports.OpeningShortListedApplicants.rdlc">
                            <DataSources>
                                <rsweb:ReportDataSource DataSourceId="ObjectDataSource1" Name="OpeningShortListedApplicantsDataSet" /> 
                            </DataSources>
                        </LocalReport>
                    </rsweb:ReportViewer>
                    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" SelectMethod="GetData"
                        TypeName="MTNNGCareers.Administration.Forms.MTNNGCareersReportDataSourceTableAdapters.RetrieveVacancyRequesterShortListedApplicantsTableAdapter"
                        OnSelecting="ObjectDataSource1_Selecting" >
                        <SelectParameters>
                            <asp:Parameter DefaultValue="" Name="Requester" Type="Int32" />
                            <asp:Parameter DefaultValue="" Name="VacancyBreakDownID" Type="Int32" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
<asp:Content ID="PageTitle" ContentPlaceHolderID="PlaceHolderPageTitle" runat="server">
    Hiring Manager Report
</asp:Content>
<asp:Content ID="PageTitleInTitleArea" ContentPlaceHolderID="PlaceHolderPageTitleInTitleArea"
    runat="server">
    Hiring Manager Report
</asp:Content>

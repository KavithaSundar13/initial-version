<%@ Assembly Name="MTNNGCareers.Administration.Forms, Version=1.0.0.0, Culture=neutral, PublicKeyToken=5e7c3f80f0f7816f" %>
<%@ Import Namespace="Microsoft.SharePoint.ApplicationPages" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ManageVacancyBreakDown.aspx.cs"
    Inherits="MTNNGCareers.Administration.Forms.ManageVacancyBreakDown"
    DynamicMasterPageFile="~masterurl/default.master" %>

<asp:Content ID="PageHead" ContentPlaceHolderID="PlaceHolderAdditionalPageHead" runat="server">
    <script type="text/javascript" src="/HR/_layouts/MTNNGCareersAdminForms/javascript/Common.js"></script>
    <script type="text/javascript">
        function HandleUpdateBreakDown(event)
        {
            var iftNoOfOpenings = (document.getElementById(('<% =iftNoOfOpenings.ClientID %>')));
            if (isNaN((iftNoOfOpenings.value)))
            {
                alert('Ensure that the number of openings is a number');
                iftNoOfOpenings.focus();

                PreventPostBack(event);
            }
            
        }
    </script>
    <link type="text/css" rel="Stylesheet" href="/HR/_layouts/MTNNGCareersAdminForms/css/Common.css" />
    <link type="text/css" rel="Stylesheet" href="/HR/_layouts/MTNNGCareersAdminForms/css/NewVacancy.css" />
</asp:Content>
<asp:Content ID="Main" ContentPlaceHolderID="PlaceHolderMain" runat="server">
    <div>
    <table style="width: 80%;">
        <tr>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                <div>
                    <table style="width: 100%;">
                        <tr>
                            <td class="FieldName">
                                Job Title (Vacancy) <span class="RequiredIndicator">*</span>
                            </td>
                            <td class="FieldValue">
                                <asp:Label ID="lblJobTitle" runat="server" Text="Label" Style="font-family: Arial;
                                    font-weight: bold; font-size: small; text-transform: capitalize;" Visible="False"></asp:Label>
                                <SharePoint:InputFormTextBox ID="iftJobTitle" runat="server" CssClass="iftJobTitle"
                                    ValidationGroup="VacancyBreakDown" MaxLength="2" ToolTip="type in the name of the Job Title here"
                                    Visible="False"></SharePoint:InputFormTextBox>
                                <SharePoint:InputFormRequiredFieldValidator ID="ifrvJobTitle" runat="server" ControlToValidate="iftJobTitle"
                                    Font-Names="Arial" Font-Size="X-Small" ValidationGroup="VacancyBreakDown" ErrorMessage="REQUIRED"
                                    Visible="False"><br /><span role="alert">REQUIRED</span></SharePoint:InputFormRequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="FieldName">
                                No. of Openings <span class="RequiredIndicator">*</span>
                            </td>
                            <td class="FieldValue">
                                <SharePoint:InputFormTextBox ID="iftNoOfOpenings" runat="server" CssClass="iftPostDegreeYearsOfExperience"
                                    ValidationGroup="VacancyBreakDown" MaxLength="2"></SharePoint:InputFormTextBox>
                                <SharePoint:InputFormRequiredFieldValidator ID="ifrvPostDegreeYearsOfExperience"
                                    runat="server" ControlToValidate="iftNoOfOpenings" Font-Names="Arial" Font-Size="X-Small"
                                    ValidationGroup="VacancyBreakDown" ErrorMessage="REQUIRED"><br /><span role="alert">REQUIRED</span></SharePoint:InputFormRequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="FieldName">
                                Employment Status <span class="RequiredIndicator">*</span>
                            </td>
                            <td class="FieldValue">
                                <asp:DropDownList ID="ddlStatus" runat="server" CssClass="DropDownListStyle">
                                    <asp:ListItem>permanent</asp:ListItem>
                                    <asp:ListItem>contract</asp:ListItem>
                                    <asp:ListItem>consultant</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="FieldName">
                                Department
                            </td>
                            <td class="FieldValue">
                                <asp:DropDownList ID="ddlDepartment" runat="server" AutoPostBack="true" CssClass="DropDownListStyle"
                                    OnSelectedIndexChanged="Handle_ddlDepartment_OnSelectedIndexChanged" />
                            </td>
                        </tr>
                        <tr>
                            <td class="FieldName">
                                Unit&nbsp;
                            </td>
                            <td class="FieldValue">
                                <asp:DropDownList ID="ddlUnit" runat="server" AutoPostBack="true" CssClass="DropDownListStyle"
                                    OnSelectedIndexChanged="Handle_ddlDepartment_OnSelectedIndexChanged" />
                            </td>
                            </tr>
                            <tr>
                                <td class="FieldName">
                                    State
                                </td>
                            <td class="FieldValue">
                                <asp:DropDownList ID="ddlState" runat="server" AutoPostBack="true" OnSelectedIndexChanged="Handle_ddlState_OnSelectedIndexChanged"
                                    CssClass="DropDownListStyle" />
                            </td>
                        </tr>
                        <tr>
                            <td class="FieldName">
                                Location
                            </td>
                            <td class="FieldValue">
                                <asp:DropDownList ID="ddlLocation" runat="server" CssClass="DropDownListStyle" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                <asp:Button ID="btnCOMMAND" runat="server" OnClientClick="HandleUpdateBreakDown(event)" Text="add" CssClass="MyButton" OnClick="btnCOMMAND_Click"
                                    ValidationGroup="VacancyBreakDown" />
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
        <tr>
            <td>
            </td>
        </tr>
    </table>
</div>
</asp:Content>
<asp:Content ID="PageTitle" ContentPlaceHolderID="PlaceHolderPageTitle" runat="server">
    Vacancy Break down
</asp:Content>
<asp:Content ID="PageTitleInTitleArea" ContentPlaceHolderID="PlaceHolderPageTitleInTitleArea"
    runat="server">
    Vacancy Break down
</asp:Content>

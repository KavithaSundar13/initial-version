<%@ Assembly Name="MTNNGCareers.Administration.Forms, Version=1.0.0.0, Culture=neutral, PublicKeyToken=5e7c3f80f0f7816f" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Assembly="AjaxControlToolkit, Version=3.0.30930.28736, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e"
    Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="VacancyReport.ascx.cs"
    Inherits="MTNNGCareers.Administration.Forms.ControlTemplates.VacancyReport" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<link type="text/css" rel="Stylesheet" href="/HR/_layouts/MTNNGCareersAdminForms/css/Common.css" />
<div>
        <table class="Fill">
            <tr>
                <td>
                <asp:SqlDataSource ID="vwVacancyApplicantsDataSrc" runat="server" ConnectionString="<%$ ConnectionStrings:MTNNGCareersDBConnectionString1 %>"
                            SelectCommand="RetrieveVacancyApplicants" 
                        SelectCommandType="StoredProcedure">
                            <SelectParameters>
                                <asp:ControlParameter ControlID="hfVacancyBreakDownID" Name="VacancyBreakDownID"
                                    PropertyName="Value" Type="Int64" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                </td>
            </tr>
            <tr>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <div style="padding-left: 20%; padding-right: 20%">
                        <div>
                            <table class="Fill">
                                <tr>
                                    <td>
                                        <asp:CheckBox ID="cbAdvertisementDuration" runat="server" />
                                    </td>
                                    <td>
                                        published date
                                    </td>
                                    <td>
                                        <SharePoint:InputFormTextBox ID="iftStartAdvertisementDate" runat="server" ToolTip="specify the start date for advertising."
                                            ControlToValidate="iftStartAdvertisementDate" contentEditable="false" CssClass="ms-input"
                                            ValidationGroup="ValidateVacancy"></SharePoint:InputFormTextBox>
                                        <cc1:CalendarExtender ID="calendarExtStartAvertisementDate" TargetControlID="iftStartAdvertisementDate"
                                            PopupButtonID="imgBtnStartAdvertisementDate" runat="server" OnClientDateSelectionChanged="ValidateAdvertisementStartDate">
                                        </cc1:CalendarExtender>
                                        <span class="RequiredIndicator">*</span><asp:ImageButton ID="imgBtnStartAdvertisementDate"
                                            runat="server" ImageUrl="~/_layouts/images/CALENDAR.GIF" ImageAlign="Middle" />
                                    </td>
                                    <td>
                                        expiry date
                                    </td>
                                    <td>
                                        <SharePoint:InputFormTextBox ID="iftEndAdvertisementDate" runat="server" contentEditable="false"
                                            ToolTip="specify the end date for advertising." CssClass="ms-input"></SharePoint:InputFormTextBox>
                                        <span class="RequiredIndicator">*</span><cc1:CalendarExtender ID="calendarExtEndAvertisementDate"
                                            TargetControlID="iftEndAdvertisementDate" PopupButtonID="imgBtnEndAdvertisementDate"
                                            runat="server" OnClientDateSelectionChanged="ValidateAdvertisementEndDate">
                                        </cc1:CalendarExtender>
                                        <asp:ImageButton ID="imgBtnEndAdvertisementDate" runat="server" ImageUrl="~/_layouts/images/CALENDAR.GIF" ImageAlign="Middle" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div>
                            <table style="margin: 0px auto;">
                                <tr>
                                    <td>
                                        <asp:CheckBox ID="cbRequester" runat="server" />
                                    </td>
                                    <td>
                                        Hiring Manager
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlRequester" runat="server" CssClass="DropDownListStyle">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="cbPublisher" runat="server" />
                                    </td>
                                    <td>
                                        Publisher
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlPublisher" runat="server" CssClass="DropDownListStyle">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:CheckBox ID="cbDivision" runat="server" />
                                    </td>
                                    <td>
                                        Division
                                    </td>
                                    <td>
                                        <asp:UpdatePanel ID="upDivision" runat="server">
                                            <ContentTemplate>
                                                <asp:DropDownList ID="ddlDivision" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlDivision_SelectedIndexChanged"
                                                    CssClass="DropDownListStyle">
                                                </asp:DropDownList>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="cbDepartment" runat="server" />
                                    </td>
                                    <td>
                                        Department
                                    </td>
                                    <td>
                                        <asp:UpdatePanel ID="upDepartment" runat="server">
                                            <ContentTemplate>
                                                <asp:DropDownList ID="ddlDepartment" runat="server" CssClass="DropDownListStyle">
                                                </asp:DropDownList>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:CheckBox ID="cbStaffLevel" runat="server" />
                                    </td>
                                    <td>
                                        Staff Level
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlStaffLevel" runat="server" CssClass="DropDownListStyle">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="cbGender" runat="server" />
                                    </td>
                                    <td>
                                        Gender
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlGender" runat="server" CssClass="DropDownListStyle">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:CheckBox ID="cbStateOfOperation" runat="server" />
                                    </td>
                                    <td>
                                        State
                                    </td>
                                    <td>
                                        <asp:UpdatePanel ID="upStateOfOperation" runat="server">
                                            <ContentTemplate>
                                                <asp:DropDownList ID="ddlStateOfOperation" AutoPostBack="true" OnSelectedIndexChanged="ddlStateOfOperation_SelectedIndexChanged"
                                                    runat="server" CssClass="DropDownListStyle">
                                                </asp:DropDownList>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="cbLocationOfOperation" runat="server" />
                                    </td>
                                    <td>
                                        Location
                                    </td>
                                    <td>
                                        <asp:UpdatePanel ID="upLocationOfOperation" runat="server">
                                            <ContentTemplate>
                                                <asp:DropDownList ID="ddlLocationOfOperation" runat="server" CssClass="DropDownListStyle">
                                                </asp:DropDownList>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        <asp:Button ID="btnGenerateReport" runat="server" OnClick="btnGenerateReport_Click"
                                            Text="Generate Report" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <div>
                        <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" 
                            Font-Size="8pt" InteractiveDeviceInfos="(Collection)" 
                            WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt">
                            <LocalReport ReportEmbeddedResource="MTNNGCareers.Administration.Forms.Layouts.MTNNGCareersAdminForms.Reports.VacancyReport.rdlc">
                                <DataSources>
                                    <rsweb:ReportDataSource DataSourceId="ObjectDataSource1" 
                                        Name="VacancyReportDataSet" />
                                </DataSources>
                            </LocalReport>
                        </rsweb:ReportViewer>
                        <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" 
                            SelectMethod="GetData" 
                            TypeName="MTNNGCareersReportDataSourceTableAdapters.RetrieveVacancyReportTableAdapter">
                        </asp:ObjectDataSource>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                </td>
            </tr>
        </table>
    </div>

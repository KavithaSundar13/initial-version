<%@ Assembly Name="MTNNGCareers.Administration.Forms, Version=1.0.0.0, Culture=neutral, PublicKeyToken=5e7c3f80f0f7816f" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DisplayVacancyCtrl.ascx.cs"
    Inherits="MTNNGCareers.Administration.Forms.ControlTemplates.DisplayVacancyCtrl" %>
<link type="text/css" rel="Stylesheet" href="/HR/_layouts/MTNNGCareersAdminForms/css/Common.css" />
<link type="text/css" rel="Stylesheet" href="/HR/_layouts/MTNNGCareersAdminForms/css/Master.css" />
<div>
    <table class="TableStyle">
            <tr>
                <td class="FieldName">
                    Job Title 
                </td>
                <td class="FieldValue">
                    <asp:Label ID="lblJobTitle" runat="server" ></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="FieldName">
                    Publisher</td>
                <td class="FieldValue">
                    <asp:Label ID="lblPublisher" runat="server" ></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="FieldName">
                    Job Description 
                </td>
                <td class="FieldValue">
                    <asp:Literal ID="ltrJobDesc" runat="server"></asp:Literal>
                </td>
            </tr>
            <tr>
                <td class="FieldName">
                    Job Conditions 
                </td>
                <td class="FieldValue">
                    <asp:Literal ID="ltrJobCondition" runat="server"></asp:Literal>
                </td>
            </tr>
            <tr>
                <td class="FieldName">
                    Experience & Training 
                </td>
                <td class="FieldValue">
                    <asp:Literal ID="lblExperienceAndTraining" runat="server"></asp:Literal>
                </td>
            </tr>
            <tr>
                <td class="FieldName">
                    Hiring Manager 
                </td>
                <td class="FieldValue">
                    <asp:Label ID="lblHiringManager" runat="server" ></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="FieldName">
                    Hiring Manager Position 
                </td>
                <td class="FieldValue">
                    <asp:Label ID="lblHiringManagerPosition" runat="server" ></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="FieldName">
                    Business Partner 
                </td>
                <td class="FieldValue">
                    <asp:Label ID="lblBusinessPartner" runat="server" ></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="FieldName">
                    HR Advisor
                </td>
                <td class="FieldValue">
                    <asp:Label ID="lblHRAdvisor" runat="server" ></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="FieldName">
                    Division 
                </td>
                <td class="FieldValue">
                    <asp:Label ID="lblDivision" runat="server" ></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="FieldName">
                    Staff Level 
                </td>
                <td class="FieldValue">
                    <asp:Label ID="lblStaffLevel" runat="server" ></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="FieldName">
                    Is NYSC Required 
                </td>
                <td class="FieldValue">
                    <asp:Label ID="lblIsNYSCRequired" runat="server" ></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="FieldName">
                    Preferred Course of Study 
                </td>
                <td class="FieldValue">
                    <asp:Label ID="lblPreferredCourseOfStudy" runat="server" ></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="FieldName">
                    Minimum Qualification 
                </td>
                <td class="FieldValue">
                    <asp:Label ID="lblMinimumQualification" runat="server" ></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="FieldName">
                    Class of Qualification 
                </td>
                <td class="FieldValue">
                    <asp:Label ID="lblClassOfQualification" runat="server" ></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="FieldName">
                    Professional Qualification(s) 
                </td>
                <td class="FieldValue">
                    <asp:Label ID="lblProfessionalQualifcations" runat="server" ></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="FieldName">
                    Additional Qualification(s) 
                </td>
                <td class="FieldValue">
                    <asp:Label ID="lblAdditionalQualifications" runat="server" ></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="FieldName">
                    Post Degree Years of Experience 
                </td>
                <td class="FieldValue">
                    <asp:Label ID="lblPostDegreeYearsOfExperience" runat="server" ></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="FieldName">
                    Marital Status&nbsp;</td>
                <td class="FieldValue">
                    <asp:Label ID="lblMaritalStatus" runat="server" ></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="FieldName">
                    Gender 
                </td>
                <td class="FieldValue">
                    <asp:Label ID="lblGender" runat="server" ></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="FieldName">
                    Start Advertisement Date 
                </td>
                <td class="FieldValue">
                    <asp:Label ID="lblStartAdvertisementDate" runat="server" ></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="FieldName">
                    End Advertisement Date 
                </td>
                <td class="FieldValue">
                    <asp:Label ID="lblEndAdvertisementDate" runat="server" ></asp:Label>
                </td>
            </tr>
        </table>
</div>
<div>
    <asp:HiddenField ID="hfVacancyID" runat="server" />
</div>

<%@ Assembly Name="MTNNGCareers.Administration.Forms, Version=1.0.0.0, Culture=neutral, PublicKeyToken=5e7c3f80f0f7816f" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="VacancyApplicantCVViewerCtrl.ascx.cs"
    Inherits="MTNNGCareers.Administration.Forms.ControlTemplates.VacancyApplicantCVViewerCtrl" %>
<%@ Register Assembly="AjaxControlToolkit, Version=3.0.30930.28736, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e"
    Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<link type="text/css" rel="Stylesheet" href="/HR/_layouts/MTNNGCareersAdminForms/css/Common.css" />
<link type="text/css" rel="Stylesheet" href="/HR/_layouts/MTNNGCareersAdminForms/css/Master.css" />
<div>
    <table class="myTable">
                        <tr>
                            <td class="td_Label">
                                salutation
                            </td>
                            <td class="td_Field">
                                <asp:Label ID="lbSalutation" runat="server" CssClass="EntityAttribute"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="td_Label">
                                surname
                            </td>
                            <td class="td_Field">
                                <asp:Label ID="lbSurname" runat="server" CssClass="EntityAttribute"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="td_Label">
                                first name
                            </td>
                            <td class="td_Field">
                                <asp:Label ID="lbFirstName" runat="server" CssClass="EntityAttribute"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="td_Label">
                                other names
                            </td>
                            <td class="td_Field">
                                <asp:Label ID="lbOtherNames" runat="server" CssClass="EntityAttribute"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="td_Label">
                                gender
                            </td>
                            <td class="td_Field">
                                <asp:Label ID="lbGender" runat="server" CssClass="EntityAttribute"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="td_Label">
                                age
                            </td>
                            <td class="td_Field">
                                <asp:Label ID="lbAge" runat="server" CssClass="EntityAttribute"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="td_Label">
                                marital satatus
                            </td>
                            <td class="td_Field">
                                <asp:Label ID="lbMaritalStatus" runat="server" CssClass="EntityAttribute"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="td_Label">
                                n.y.s.c completion status
                            </td>
                            <td class="td_Field">
                                <asp:Label ID="lbNYSCCompletionStatus" runat="server" CssClass="EntityAttribute"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="td_Label">
                                &nbsp;
                            </td>
                            <td class="td_Field">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td class="td_Label">
                                is any relative an m.t.n satff
                            </td>
                            <td class="td_Field">
                                <asp:Label ID="lbMTNStaffQ" runat="server" CssClass="EntityAttribute"></asp:Label>
                            </td>
                        </tr>
                    </table>
    <div style="padding:10px 0;">
        <cc1:TabContainer ID="tcApplicant" runat="server" ActiveTabIndex="1" CssClass="MyTabStyle">
            <cc1:TabPanel runat="server" ID="tpOriginAndContact">
                <HeaderTemplate>
                    <div>
                        <span>origin/contact</span>
                    </div>
                </HeaderTemplate>
                <ContentTemplate>
                    <div>
                         <table class="myTable">
                         <tr class="myrowheader">
                                <td>
                                        origin
                                </td>
                            </tr>
                                            <tr>
                                                <td class="td_Label">
                                                    country of origin
                                                </td>
                                                <td class="td_Field">
                                                    <asp:Label ID="lbCountryOfOrigin" runat="server" CssClass="EntityAttribute"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_Label">
                                                    state/province of origin
                                                </td>
                                                <td class="td_Field">
                                                    <asp:Label ID="lbStateOrProvinceOfOrigin" runat="server" CssClass="EntityAttribute"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_Label">
                                                    local government area
                                                </td>
                                                <td class="td_Field">
                                                    <asp:Label ID="lbLocalGovernmentArea" runat="server" CssClass="EntityAttribute"></asp:Label>
                                                </td>
                                            </tr>

                                          <tr class="myrowheader">
                                <td>
                                        contacts
                                </td>
                            </tr>
                              <tr>
                                                <td class="td_Label">
                                                    country of residence
                                                </td>
                                                <td class="td_Field">
                                                    <asp:Label ID="lbCountryOfResidence" runat="server" CssClass="EntityAttribute"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_Label">
                                                    state/province of residence
                                                </td>
                                                <td class="td_Field">
                                                    <asp:Label ID="lbStateOrProvinceOfResidence" runat="server" CssClass="EntityAttribute"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_Label">
                                                    city
                                                </td>
                                                <td class="td_Field">
                                                    <asp:Label ID="lbCity" runat="server" CssClass="EntityAttribute"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_Label">
                                                    address
                                                </td>
                                                <td class="td_Field">
                                                    <asp:Label ID="lbAddress" runat="server" CssClass="EntityAttribute"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_Label">
                                                    mobile phone no.
                                                </td>
                                                <td class="td_Field">
                                                    <asp:Label ID="lbMobilePhone" runat="server" CssClass="EntityAttribute"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_Label">
                                                    contact phone no.
                                                </td>
                                                <td class="td_Field">
                                                    <asp:Label ID="lbContactPhoneNo" runat="server" CssClass="EntityAttribute"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                    </div>
                </ContentTemplate>
            </cc1:TabPanel>
            <cc1:TabPanel runat="server" ID="tpEducationalHistory">
                <HeaderTemplate>
                    <div>
                        <span>educational history</span>
                    </div>
                </HeaderTemplate>
                <ContentTemplate>
                    <div>
                        <asp:GridView ID="gvApplicantEducationalHistory" runat="server" 
                            EnableModelValidation="True" AutoGenerateColumns="False" 
                            DataSourceID="sqlDSApplicantEducationalHistory" CssClass="mGrid">
                            <AlternatingRowStyle CssClass="alt" />
                            <Columns>
                                <asp:BoundField DataField="EducationalHistoryRowKey" 
                                    HeaderText="EducationalHistoryRowKey" SortExpression="EducationalHistoryRowKey" 
                                    Visible="False" />
                                <asp:BoundField DataField="RegistrantRowKey" HeaderText="RegistrantRowKey" 
                                    SortExpression="RegistrantRowKey" Visible="False" />
                                <asp:BoundField DataField="EducationalLevelID" HeaderText="EducationalLevelID" 
                                    SortExpression="EducationalLevelID" Visible="False" />
                                <asp:BoundField DataField="EducationalLevel" HeaderText="EducationalLevel" 
                                    SortExpression="EducationalLevel" />
                                <asp:BoundField DataField="Instituition" HeaderText="Instituition" 
                                    SortExpression="Instituition" />
                                <asp:BoundField DataField="CourseOfStudy" HeaderText="CourseOfStudy" 
                                    SortExpression="CourseOfStudy" />
                                <asp:BoundField DataField="QualificationID" HeaderText="QualificationID" 
                                    SortExpression="QualificationID" Visible="False" />
                                <asp:BoundField DataField="Qualification" HeaderText="Qualification" 
                                    SortExpression="Qualification" />
                                <asp:BoundField DataField="ClassOfQualificationID" 
                                    HeaderText="ClassOfQualificationID" SortExpression="ClassOfQualificationID" 
                                    Visible="False" />
                                <asp:BoundField DataField="ClassOfQualification" 
                                    HeaderText="ClassOfQualification" SortExpression="ClassOfQualification" />
                                <asp:BoundField DataField="StartDate" HeaderText="StartDate" 
                                    SortExpression="StartDate" />
                                <asp:BoundField DataField="EndDate" HeaderText="EndDate" 
                                    SortExpression="EndDate" />
                            </Columns>
                            <PagerStyle CssClass="pgr" />
                        </asp:GridView>
                        <asp:SqlDataSource runat="server" ID="sqlDSApplicantEducationalHistory" 
                            ConnectionString="<%$ ConnectionStrings:MTNNGCareersDBConnectionString %>" 
                            SelectCommand="RetrieveRegistrantEducationalHistory" 
                            SelectCommandType="StoredProcedure">
                            <SelectParameters>
                                <asp:ControlParameter ControlID="hfRegistrantKey" Name="RegistrantRowkey" 
                                    PropertyName="Value" Type="Int64" />
                                <asp:Parameter Direction="InputOutput" Name="RowNoAffected" Type="Int32" 
                                    DefaultValue="0" />
                                <asp:Parameter Direction="InputOutput" Name="ErrorMessage" Type="String" 
                                    DefaultValue="0" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                    </div>
                </ContentTemplate>
            </cc1:TabPanel>
            <cc1:TabPanel runat="server" ID="tpProfessionalCertifications">
                <HeaderTemplate>
                    <div>
                        <span>professional certification</span>
                    </div>
                </HeaderTemplate>
                <ContentTemplate>
                    <div>
                        <asp:GridView ID="gvProfessionalCertification" runat="server" 
                            EnableModelValidation="True" AutoGenerateColumns="False" 
                            DataKeyNames="RowKey" DataSourceID="sqlDSRegistrantProfessionalCertifications" CssClass="mGrid"  PagerStyle-CssClass="pgr"   AlternatingRowStyle-CssClass="alt">
                            <Columns>
                                <asp:BoundField DataField="RowKey" HeaderText="RowKey" InsertVisible="False" 
                                    ReadOnly="True" SortExpression="RowKey" Visible="False" />
                                <asp:BoundField DataField="RegistrantRowKey" HeaderText="RegistrantRowKey" 
                                    SortExpression="RegistrantRowKey" Visible="False" />
                                <asp:BoundField DataField="ProfessionalAssociation" 
                                    HeaderText="ProfessionalAssociation" SortExpression="ProfessionalAssociation" />
                                <asp:BoundField DataField="CertificateObtained" 
                                    HeaderText="CertificateObtained" SortExpression="CertificateObtained" />
                                <asp:BoundField DataField="Role" HeaderText="Role" SortExpression="Role" />
                                <asp:BoundField DataField="DateJoined" HeaderText="DateJoined" 
                                    SortExpression="DateJoined" />
                            </Columns>
                        </asp:GridView>
                        <asp:SqlDataSource runat="server" ID="sqlDSRegistrantProfessionalCertifications" 
                            ConnectionString="<%$ ConnectionStrings:MTNNGCareersDBConnectionString %>" 
                            SelectCommand="RetrieveRegistrantProfessionalCertification" 
                            SelectCommandType="StoredProcedure">
                            <SelectParameters>
                                <asp:ControlParameter ControlID="hfRegistrantKey" Name="RegistrantRowkey" 
                                    PropertyName="Value" Type="Int64" />
                                <asp:Parameter Direction="InputOutput" Name="RowNoAffected" Type="Int32" 
                                    DefaultValue="0" />
                                <asp:Parameter Direction="InputOutput" Name="ErrorMessage" Type="String" 
                                    DefaultValue="0" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                    </div>
                </ContentTemplate>
            </cc1:TabPanel>
            <cc1:TabPanel runat="server" ID="tpSkills">
                <HeaderTemplate>
                    <div>
                        <span>Skills</span>
                    </div>
                </HeaderTemplate>
                <ContentTemplate>
                    <div>
                        <asp:GridView ID="gvRegistrantSkills" runat="server" 
                            EnableModelValidation="True" AutoGenerateColumns="False" 
                            DataKeyNames="RowKey,ID" DataSourceID="sqlDSRegistrantSkills" CssClass="mGrid"  PagerStyle-CssClass="pgr"   AlternatingRowStyle-CssClass="alt">
                            <Columns>
                                <asp:BoundField DataField="RowKey" HeaderText="RowKey" ReadOnly="True" 
                                    SortExpression="RowKey" Visible="False" />
                                <asp:BoundField DataField="RegistrantRowKey" HeaderText="RegistrantRowKey" 
                                    SortExpression="RegistrantRowKey" Visible="False" />
                                <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name" />
                                <asp:BoundField DataField="ID" HeaderText="ID" ReadOnly="True" 
                                    SortExpression="ID" Visible="False" />
                                <asp:BoundField DataField="SkillProficiencyLevel" 
                                    HeaderText="SkillProficiencyLevel" SortExpression="SkillProficiencyLevel" />
                                <asp:BoundField DataField="YearsOfExperience" HeaderText="YearsOfExperience" 
                                    SortExpression="YearsOfExperience" />
                            </Columns>
                        </asp:GridView>
                        <asp:SqlDataSource runat="server" ID="sqlDSRegistrantSkills" 
                            ConnectionString="<%$ ConnectionStrings:MTNNGCareersDBConnectionString %>" 
                            SelectCommand="RetrieveRegistrantSkills" 
                            SelectCommandType="StoredProcedure">
                            <SelectParameters>
                                <asp:ControlParameter ControlID="hfRegistrantKey" Name="RegistrantRowkey" 
                                    PropertyName="Value" Type="Int64" />
                                <asp:Parameter Direction="InputOutput" Name="RowNoAffected" Type="Int32" 
                                    DefaultValue="0" />
                                <asp:Parameter Direction="InputOutput" Name="ErrorMessage" Type="String" 
                                    DefaultValue="0" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                    </div>
                </ContentTemplate>
            </cc1:TabPanel>
            <cc1:TabPanel runat="server" ID="tpWorkExperience">
                <HeaderTemplate>
                    <div>
                        <span>work experience</span>
                    </div>
                </HeaderTemplate>
                <ContentTemplate>
                    <div>
                        <asp:GridView ID="gvRegistrantWorkExperience" runat="server" 
                            EnableModelValidation="True" AutoGenerateColumns="False" 
                            DataKeyNames="RowKey" DataSourceID="SqlDSRegistrantWorkExperience" CssClass="mGrid"  PagerStyle-CssClass="pgr"   AlternatingRowStyle-CssClass="alt">
                            <Columns>
                                <asp:BoundField DataField="RowKey" HeaderText="RowKey" InsertVisible="False" 
                                    ReadOnly="True" SortExpression="RowKey" Visible="False" />
                                <asp:BoundField DataField="RegistrantRowKey" HeaderText="RegistrantRowKey" 
                                    SortExpression="RegistrantRowKey" Visible="False" />
                                <asp:BoundField DataField="CompanyOrOrganizationName" 
                                    HeaderText="CompanyOrOrganizationName" 
                                    SortExpression="CompanyOrOrganizationName" />
                                <asp:BoundField DataField="Position" HeaderText="Position" 
                                    SortExpression="Position" />
                                <asp:BoundField DataField="JobDescription" HeaderText="JobDescription" 
                                    SortExpression="JobDescription" />
                                <asp:BoundField DataField="StartDate" HeaderText="StartDate" 
                                    SortExpression="StartDate" />
                                <asp:BoundField DataField="EndDate" HeaderText="EndDate" 
                                    SortExpression="EndDate" />
                            </Columns>
                        </asp:GridView>
                        <asp:SqlDataSource runat="server" ID="SqlDSRegistrantWorkExperience" 
                            ConnectionString="<%$ ConnectionStrings:MTNNGCareersDBConnectionString %>" 
                            SelectCommand="RetrieveRegistrantWorkExperience" 
                            SelectCommandType="StoredProcedure">
                            <SelectParameters>
                                <asp:ControlParameter ControlID="hfRegistrantKey" Name="RegistrantRowkey" 
                                    PropertyName="Value" Type="Int64" />
                                <asp:Parameter Direction="InputOutput" Name="RowNoAffected" Type="Int32" 
                                    DefaultValue="0" />
                                <asp:Parameter Direction="InputOutput" Name="ErrorMessage" Type="String" 
                                    DefaultValue="0" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                    </div>
                </ContentTemplate>
            </cc1:TabPanel>
        </cc1:TabContainer>
    </div>
</div>
<div>
    <asp:HiddenField ID="hfRegistrantKey" runat="server" />
</div>
